package com.customizingbox.cloud.manager.controller.system;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysMenuTree;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysMenu;
import com.customizingbox.cloud.common.datasource.model.admin.system.param.AdminSysRoleIdsParam;
import com.customizingbox.cloud.common.datasource.model.admin.system.param.AdminSysSaveMenuParam;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminMenuVO;
import com.customizingbox.cloud.common.datasource.util.AdminSysTreeUtil;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminSysMenuService;
import com.customizingbox.cloud.manager.service.admin.AdminSysRoleService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@AllArgsConstructor
@RequestMapping("/menu")
@Api(value = "menu", tags = "菜单管理模块")
public class AdminSysMenuController {

    private final AdminSysMenuService adminSysMenuService;
    private final AdminSysRoleService adminSysRoleService;


    @ApiOperation(value = "返回所有树形菜单集合")
    @GetMapping(value = "/all/tree")
    public ApiResponse<List<AdminSysMenuTree>> getAllTree() {
        List<AdminSysMenu> collect = adminSysMenuService.list(Wrappers.<AdminSysMenu>lambdaQuery()
                .orderByAsc(AdminSysMenu::getSort)).stream().collect(Collectors.toList());
        List<AdminSysMenuTree> appSysMenuTreeList = AdminSysTreeUtil.buildTree(collect, CommonConstants.PARENT_ID);
        return ApiResponse.ok(appSysMenuTreeList);
    }


    @ApiOperation(value = "返回当前用户的树形菜单集合")
    @GetMapping
    public ApiResponse getUserMenu() {
        // 获取符合条件的菜单
        Set<AdminMenuVO> all = new HashSet<>();
        List<String> roles = SecurityUtils.getRoles();

        roles.forEach(roleId -> all.addAll(adminSysMenuService.findMenuByRoleId(roleId)));
        List<AdminSysMenuTree> appSysMenuTreeList = all.stream()
                .filter(menuVo -> CommonConstants.MENU.equals(menuVo.getType()))
                .map(AdminSysMenuTree::new)
                .sorted(Comparator.comparingInt(AdminSysMenuTree::getSort))
                .collect(Collectors.toList());
        return ApiResponse.ok(AdminSysTreeUtil.build(appSysMenuTreeList, CommonConstants.PARENT_ID));
    }


    @ApiOperation(value = "返回树形菜单集合")
    @GetMapping(value = "/tree")
    public ApiResponse<List<AdminSysMenuTree>> getTree() {
        Set<AdminMenuVO> all = new HashSet<>();
        SecurityUtils.getRoles().forEach(roleId -> all.addAll(adminSysMenuService.findMenuByRoleId(roleId)));
        List<AdminSysMenuTree> appSysMenuTreeList = all.stream().map(AdminSysMenuTree::new).collect(Collectors.toList());
        return ApiResponse.ok(AdminSysTreeUtil.build(appSysMenuTreeList, CommonConstants.PARENT_ID));
    }


    @ApiOperation(value = "返回角色的菜单集合")
    @GetMapping("/tree/{roleId}")
    public ApiResponse getRoleTree(@PathVariable String roleId) {
        return ApiResponse.ok(adminSysMenuService.findMenuByRoleId(roleId)
                .stream()
                .map(AdminMenuVO::getId)
                .collect(Collectors.toList()));
    }

    @ApiOperation(value = "返回多角色的权限集合")
    @PostMapping("/tree/queryByRoles")
    public ApiResponse<List<String>> queryByRoles(@ApiParam(value = "角色id列表") @RequestBody AdminSysRoleIdsParam param) {
        // 获取符合条件的菜单
//        if(StringUtils.isEmpty(roleIds)) {
//            return ApiResponse.ok();
//        }
        Set<AdminMenuVO> all = new HashSet<>();
        param.getRoleIds().forEach(roleId -> all.addAll(adminSysMenuService.findMenuByRoleId(roleId)));
        List<String> menuIds = all.stream().map(AdminMenuVO::getId).collect(Collectors.toList());
        return ApiResponse.ok(menuIds);
    }


    @ApiOperation(value = "通过ID查询菜单的详细信息")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:menu:get')")
    public ApiResponse<AdminSysMenu> getById(@PathVariable String id) {
        return ApiResponse.ok(adminSysMenuService.getById(id));
    }


    @ApiOperation(value = "新增菜单")
    @SysLog("新增菜单")
    @PostMapping("/save")
    @PreAuthorize("@ato.hasAuthority('sys:menu:add')")
    public ApiResponse save(@Valid @RequestBody AdminSysSaveMenuParam sysMenu) {
        AdminSysMenu adminSysMenu = BeanUtil.copyProperties(sysMenu, AdminSysMenu.class);
        adminSysMenu.setId(null);
        adminSysMenuService.saveMenu(adminSysMenu);
        return ApiResponse.ok();
    }


    @ApiOperation(value = "删除菜单")
    @SysLog("删除菜单")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:menu:del')")
    public ApiResponse removeById(@PathVariable String id) {
        return adminSysMenuService.removeMenuById(id);
    }


    @ApiOperation(value = "更新菜单")
    @SysLog("更新菜单")
    @PostMapping("/edit")
    @PreAuthorize("@ato.hasAuthority('sys:menu:edit')")
    public ApiResponse update(@Valid @RequestBody AdminSysSaveMenuParam adminSysSaveMenuParam) {
        AdminSysMenu adminSysMenu = BeanUtil.copyProperties(adminSysSaveMenuParam, AdminSysMenu.class);
        adminSysMenu.setUpdateTime(LocalDateTime.now());
        return ApiResponse.ok(adminSysMenuService.updateMenuById(adminSysMenu));
    }


//    @ApiOperation(value = "返回租户管理员角色的菜单集合")
//    @GetMapping("/tree/tenant/{tenantId}")
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:edit')")
//    public ApiResponse getRoleTreeTenant(@PathVariable String tenantId) {
//        TenantContextHolder.setTenantId(tenantId);
//        //找出指定租户的管理员角色
//        AdminSysRole adminSysRole = adminSysRoleService.getOne(Wrappers.<AdminSysRole>lambdaQuery().eq(AdminSysRole::getRoleCode, CommonConstants.ROLE_CODE_ADMIN));
//        List<String> listMenuVO = adminSysMenuService.findMenuByRoleId(adminSysRole.getId()).stream().map(AdminMenuVO::getId).collect(Collectors.toList());
//        Map<String, Object> map = new HashMap<>();
//        map.put("sysRole", adminSysRole);
//        map.put("listMenuVO", listMenuVO);
//        //菜单集合
//        return ApiResponse.ok(map);
//    }
}
