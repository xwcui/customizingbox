package com.customizingbox.cloud.manager.config.excel;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import lombok.Data;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.List;

@Data
public class ExcelFillCellMergeStrategy implements CellWriteHandler {
	/**
	 * 合并字段的下标
	 */
	private int[] mergeColumnIndex;
	/**
	 * 合并几行
	 */
	private int mergeRowIndex;

	public ExcelFillCellMergeStrategy() {
	}

	public ExcelFillCellMergeStrategy(int mergeRowIndex, int[] mergeColumnIndex) {
		this.mergeRowIndex = mergeRowIndex;
		this.mergeColumnIndex = mergeColumnIndex;
	}

	@Override
	public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
								 Head head, Integer integer, Integer integer1, Boolean aBoolean) {

	}

	@Override
	public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
	                            Head head, Integer integer, Boolean aBoolean) {

	}

	/**
	 * Called after the cell data is converted
	 *
	 * @param writeSheetHolder
	 * @param writeTableHolder Nullable.It is null without using table writes.
	 * @param cellData         Nullable.It is null in the case of add header.
	 * @param cell
	 * @param head             Nullable.It is null in the case of fill data and without head.
	 * @param relativeRowIndex Nullable.It is null in the case of fill data.
	 * @param isHead           It will always be false when fill data.
	 */
	@Override
	public void afterCellDataConverted(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder,
									   WriteCellData<?> cellData, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {

	}

	/**
	 * Called after all operations on the cell have been completed
	 *
	 * @param writeSheetHolder
	 * @param writeTableHolder Nullable.It is null without using table writes.
	 * @param cellDataList     Nullable.It is null in the case of add header.There may be several when fill the data.
	 * @param cell
	 * @param head             Nullable.It is null in the case of fill data and without head.
	 * @param relativeRowIndex Nullable.It is null in the case of fill data.
	 * @param isHead           It will always be false when fill data.
	 */
	@Override
	public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder,
								 List<WriteCellData<?>> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
		//当前行
		int curRowIndex = cell.getRowIndex();
		//当前列
		int curColIndex = cell.getColumnIndex();

		if (curRowIndex > mergeRowIndex) {
			for (int i = 0; i < mergeColumnIndex.length; i++) {
				if (curColIndex == mergeColumnIndex[i]) {
					mergeWithPrevRow(writeSheetHolder, cell, curRowIndex, curColIndex);
					break;
				}
			}
		}
	}

	private void mergeWithPrevRow(WriteSheetHolder writeSheetHolder, Cell cell, int curRowIndex, int curColIndex) {
		//获取当前行的当前列的数据和上一行的当前列列数据，通过上一行数据是否相同进行合并
		Object curData = cell.getCellTypeEnum() == CellType.STRING ? cell.getStringCellValue() :
				cell.getNumericCellValue();
		Cell preCell = cell.getSheet().getRow(curRowIndex - 1).getCell(curColIndex);
		Object preData = preCell.getCellTypeEnum() == CellType.STRING ? preCell.getStringCellValue() :
				preCell.getNumericCellValue();

		// 比较当前行的第一列的单元格与上一行是否相同并且当前行唯一码是否以上一行相同，相同合并当前单元格与上一行
		Cell curOnlyCodeCell = cell.getSheet().getRow(curRowIndex).getCell(0);
		Cell preOnlyCodeCell = cell.getSheet().getRow(curRowIndex -1).getCell(0);
		Object curOnlyCodeData = curOnlyCodeCell.getCellTypeEnum() == CellType.STRING ? curOnlyCodeCell.getStringCellValue() :
				cell.getNumericCellValue();
		Object preOnlyCodeData =  preOnlyCodeCell.getCellTypeEnum() == CellType.STRING ? preOnlyCodeCell.getStringCellValue() :
				preOnlyCodeCell.getNumericCellValue();

		if (curData.equals(preData) && curOnlyCodeData.equals(preOnlyCodeData)) {
			Sheet sheet = writeSheetHolder.getSheet();
			List<CellRangeAddress> mergeRegions = sheet.getMergedRegions();
			boolean isMerged = false;
			for (int i = 0; i < mergeRegions.size() && !isMerged; i++) {
				CellRangeAddress cellRangeAddr = mergeRegions.get(i);
				// 若上一个单元格已经被合并，则先移出原有的合并单元，再重新添加合并单元
				if (cellRangeAddr.isInRange(curRowIndex - 1, curColIndex)) {
					sheet.removeMergedRegion(i);
					cellRangeAddr.setLastRow(curRowIndex);
					sheet.addMergedRegion(cellRangeAddr);
					isMerged = true;
				}
			}
			// 若上一个单元格未被合并，则新增合并单元
			if (!isMerged) {
				CellRangeAddress cellRangeAddress = new CellRangeAddress(curRowIndex - 1, curRowIndex, curColIndex,
						curColIndex);
				sheet.addMergedRegion(cellRangeAddress);
			}
		}
	}
}
