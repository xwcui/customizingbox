package com.customizingbox.cloud.manager.controller.transport;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminTransportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 运输方式 前端控制器
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@RestController
@RequestMapping("//adminTransport")
@Api(tags = "运输方式")
public class AdminTransportController {

    @Autowired
    private AdminTransportService service;

    @ApiOperation("/运输方式分页")
    @PostMapping("/page")
    public ApiResponse<IPage<AdminTransportPageVo>> page(Page page, @RequestBody AdminTransportSearchVo searchVo){
        if (page == null){
            page = new Page();
        }
        IPage<AdminTransportPageVo> result = service.pageAll(page,searchVo);
        return ApiResponse.ok(result);
    }

    @ApiOperation("/物流属性List")
    @PostMapping("/List")
    public ApiResponse<List<AdminTransport>> list(@RequestBody AdminTransportSearchVo searchVo){
        List<AdminTransport> result = service.getList(searchVo);
        return ApiResponse.ok(result);
    }


    @ApiOperation("/运输方式新增或修改")
    @PostMapping("/saveOrUpdate")
    public ApiResponse saveOrUpdate(@RequestBody @Validated AdminTransportDto adminTransportDto){
        BaseUser user = SecurityUtils.getUser();
        if (user == null){
            throw new ApiException("无法获取");
        }
        return  service.saveOrUpdateAdminTransportDto(adminTransportDto,user);
    }

    @ApiOperation("/运输方式开启禁用")
    @PostMapping("/openOrShut/{id}/{status}")
    public ApiResponse openOrShut(@PathVariable Long id, @PathVariable Integer status){
        BaseUser user = SecurityUtils.getUser();
        if (user == null){
            throw new ApiException("无法获取");
        }
        return  service.openOrShut(id,status,user);
    }

}
