package com.customizingbox.cloud.manager.service.admin.impl.srm;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.srm.AdminPurchaseOrderDetectionRecordMapper;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrderDetectionRecord;
import com.customizingbox.cloud.common.datasource.model.srm.vo.AdminPurchaseOrderDetectionRecordVO;
import com.customizingbox.cloud.manager.service.admin.AdminPurchaseOrderDetectionRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 质检记录表 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-05-03
 */
@Service
public class AdminPurchaseOrderDetectionRecordServiceImpl extends ServiceImpl<AdminPurchaseOrderDetectionRecordMapper, AdminPurchaseOrderDetectionRecord> implements AdminPurchaseOrderDetectionRecordService {

    @Override
    public List<AdminPurchaseOrderDetectionRecordVO> historyRecord(Long purchaseOrderId) {
        return baseMapper.historyRecord(purchaseOrderId);
    }
}
