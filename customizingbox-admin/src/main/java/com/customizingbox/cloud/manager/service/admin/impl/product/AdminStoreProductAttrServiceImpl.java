package com.customizingbox.cloud.manager.service.admin.impl.product;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductAttrMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.AdminStoreProductAttrVO;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductAttrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-31 09:34:36
 */
@Service
public class AdminStoreProductAttrServiceImpl extends ServiceImpl<AdminStoreProductAttrMapper, AdminStoreProductAttr> implements AdminStoreProductAttrService {

    @Override
    public List<String> queryByProductId(Long productId) {
        List<AdminStoreProductAttr> list = this.list(Wrappers.<AdminStoreProductAttr>lambdaQuery().eq(AdminStoreProductAttr::getProductId, productId)
                .orderByAsc(AdminStoreProductAttr::getSort)
                .select(AdminStoreProductAttr::getAttrName));
        return list.stream().map(AdminStoreProductAttr::getAttrName).collect(Collectors.toList());
    }

    @Override
    public List<AdminStoreProductAttrVO> queryAttrByProductId(Long productId, JSONArray attrValues) {
        List<AdminStoreProductAttrVO> productAttr = new ArrayList<>();

        List<String> attrNames = this.queryByProductId(productId);
        for (int i = 0; i < attrNames.size(); i++) {
            AdminStoreProductAttrVO productAttrVO = new AdminStoreProductAttrVO();
            productAttrVO.setAttrName(attrNames.get(i));
            productAttrVO.setAttrValue(String.valueOf(attrValues.get(i)));
            productAttr.add(productAttrVO);
        }
        return productAttr;
    }

    @Resource
    private AdminStoreProductAttrMapper adminStoreProductAttrMapper;

    /**
     * 根据产品id获取attr  list
     * @param productId
     * @return
     */
    @Override
    public List<AdminStoreProductAttr> selectAdminStoreProductAttrList(Long productId) {
        QueryWrapper<AdminStoreProductAttr> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("sort");
        queryWrapper.eq("product_id",productId);
        List<AdminStoreProductAttr> adminStoreProductAttrs = adminStoreProductAttrMapper.selectList(queryWrapper);
        return adminStoreProductAttrs;
    }
}
