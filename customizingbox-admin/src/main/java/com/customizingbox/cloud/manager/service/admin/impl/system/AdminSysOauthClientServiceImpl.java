package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysOauthClientMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOauthClient;
import com.customizingbox.cloud.manager.service.admin.AdminSysOauthClientService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class AdminSysOauthClientServiceImpl extends ServiceImpl<AdminSysOauthClientMapper, AdminSysOauthClient> implements AdminSysOauthClientService {

}
