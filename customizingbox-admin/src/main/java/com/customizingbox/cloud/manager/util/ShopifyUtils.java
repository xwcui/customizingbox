package com.customizingbox.cloud.manager.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShopifyUtils {

    /**
     * 设置shopify header
     */
    public static Map<String, List<String>> createShopifyHeader(String token) {
        Map<String, List<String>> header = new HashMap<>();
        header.put("Content-Type", Arrays.asList("application/json"));
        header.put("X-Shopify-Access-Token", Arrays.asList(token));
        return header;
    }
}
