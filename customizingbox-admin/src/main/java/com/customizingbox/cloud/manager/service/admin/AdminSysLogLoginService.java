package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLogLogin;

/**
 * 登录日志表
 */
public interface AdminSysLogLoginService extends IService<AdminSysLogLogin> {

}
