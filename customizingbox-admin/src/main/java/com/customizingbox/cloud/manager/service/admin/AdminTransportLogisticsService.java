package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransportLogistics;

import java.util.List;

/**
 * <p>
 * 运输方式和物流属性关联表 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminTransportLogisticsService extends IService<AdminTransportLogistics> {


    void processingAssociation(List<Long> logisticsIds, Long adminTransportId, Long id);
}
