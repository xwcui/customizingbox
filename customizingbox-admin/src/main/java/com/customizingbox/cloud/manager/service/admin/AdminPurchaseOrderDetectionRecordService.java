package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrderDetectionRecord;
import com.customizingbox.cloud.common.datasource.model.srm.vo.AdminPurchaseOrderDetectionRecordVO;

import java.util.List;

/**
 * <p>
 * 质检记录表 服务类
 * </p>
 *
 * @author Z
 * @since 2022-05-03
 */
public interface AdminPurchaseOrderDetectionRecordService extends IService<AdminPurchaseOrderDetectionRecord> {

    /**
     * 查历史质检记录
     * @param purchaseOrderId
     * @return
     */
    List<AdminPurchaseOrderDetectionRecordVO> historyRecord(Long purchaseOrderId);
}
