package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRole;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysRoleDetailVO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface AdminSysRoleService extends IService<AdminSysRole> {

    /**
     * 通过用户ID，查询角色信息
     */
    List<String> findRoleIdsByUserId(String userId);

    /**
     * 通过角色ID，删除角色
     */
    Boolean removeRoleById(String id);

    /**
     * 根据id查询角色信息, 包含权限信息
     * @param id
     * @return
     */
    AdminSysRoleDetailVO getRoleAndMenuById(String id);
}
