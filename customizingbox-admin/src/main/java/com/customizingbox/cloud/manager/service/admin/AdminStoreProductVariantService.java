package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.AdminStoreProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.ToSaiheProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.VariantPriceDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.SkuResult;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

import java.util.List;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-31 09:34:38
 */
public interface AdminStoreProductVariantService extends IService<AdminStoreProductVariant> {

    /**
     * 批量修改变体价格
     * @param variantPriceDtos
     * @param user
     * @return
     */
    ApiResponse updateProductVariantPrice(List<VariantPriceDto> variantPriceDtos, BaseUser user);

    /**
     * 删除变体
     * @param id
     * @param user
     * @return
     */
    ApiResponse delAdminProductVariant(Long id, BaseUser user);

    /**
     * 恢复变体
     * @param id
     * @param user
     * @return
     */
    ApiResponse recoveryAdminProductVariant(Long id, BaseUser user);

    /**
     * 将变体标记为已报价
     * @param variantId
     * @return
     */
    boolean updateAdminProductVariantQuoteFlag(List<Long> variantId);

    /**
     * 获取变体数据
     * @param productId
     * @return
     */
    List<AdminStoreProductVariantDto> selectAdminStoreProductVariantDtos(Long productId);

    /**
     * 根据变体ids删除变体
     * @param oldVariantIds
     * @return
     */
    int delAdminProductVariantByIds(List<Long> oldVariantIds);

    /**
     * 获取上传赛盒所需要的非定制信息
     * @param productId
     * @return
     */
    List<ToSaiheProductVariantDto> selectToSaiheProductVariantDtos(Long productId);

    /**
     * 修改上传赛盒的变体状态
     * @return
     */
    int updateSaiheState(List<SkuResult> skuUpdateResults);


    /**
     * 获取上传赛盒所需要唯一码产品的信息
     * @param onlyCodes
     * @return
     */
    List<ToSaiheProductVariantDto> selectToSaiheUniqueProductVariantDtos(List<String> onlyCodes);
}
