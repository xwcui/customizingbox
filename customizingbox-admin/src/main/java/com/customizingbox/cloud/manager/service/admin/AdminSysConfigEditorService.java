package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysConfigEditor;

/**
 * 编辑器配置
 */
public interface AdminSysConfigEditorService extends IService<AdminSysConfigEditor> {

}
