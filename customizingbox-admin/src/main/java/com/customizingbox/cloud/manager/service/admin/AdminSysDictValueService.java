package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysDictValue;

/**
 * 字典项
 */
public interface AdminSysDictValueService extends IService<AdminSysDictValue> {

    /**
     * 删除字典项
     *
     * @param id 字典项ID
     */
    ApiResponse removeDictItem(String id);

    /**
     * 更新字典项
     *
     * @param item 字典项
     */
    ApiResponse updateDictItem(AdminSysDictValue item);
}
