//package com.customizingbox.cloud.manager.controller.system;
//
//import cn.hutool.core.util.IdUtil;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.customizingbox.cloud.common.core.constant.CommonConstants;
//import com.customizingbox.cloud.common.core.util.ApiResponse;
//import com.customizingbox.cloud.common.datasource.system.entity.AdminSysTenant;
//import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
//import com.customizingbox.cloud.manager.service.admin.AdminSysTenantService;
//import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.AllArgsConstructor;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
///**
// * <p>
// * 租户管理
// * </p>
// */
//@RestController
//@AllArgsConstructor
//@RequestMapping("/tenant")
//@Api(value = "tenant", tags = "租户管理模块")
//public class AdminSysTenantController {
//
//    private final AdminSysTenantService adminSysTenantService;
//
//
//    @ApiOperation(value = "分页查询")
//    @GetMapping("/page")
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:index')")
//    public ApiResponse getUserPage(Page page, AdminSysTenant adminSysTenant) {
//        adminSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(adminSysTenantService.page(page, Wrappers.query(adminSysTenant)));
//    }
//
//
//    @ApiOperation(value = "list查询")
//    @GetMapping("/list")
//    public ApiResponse getList(AdminSysTenant adminSysTenant) {
//        adminSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(adminSysTenantService.list(Wrappers.query(adminSysTenant)));
//    }
//
//
//    @ApiOperation(value = "通过ID查询")
//    @GetMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:get')")
//    public ApiResponse getById(@PathVariable String id) {
//        TenantContextHolder.setTenantId(id);
//        return ApiResponse.ok(adminSysTenantService.getById(id));
//    }
//
//
//    @ApiOperation(value = "添加")
//    @SysLog("添加租户")
//    @PostMapping
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:add')")
//    public ApiResponse save(@Valid @RequestBody AdminSysTenant adminSysTenant) {
//        String id = String.valueOf(IdUtil.getSnowflake(1, 2).nextId());
//        TenantContextHolder.setTenantId(id);
//        adminSysTenant.setId(id);
//        adminSysTenant.setTenantId(id);
//        adminSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(adminSysTenantService.save(adminSysTenant));
//    }
//
//
//    @ApiOperation(value = "删除")
//    @SysLog("删除租户")
//    @DeleteMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:del')")
//    public ApiResponse removeById(@PathVariable String id) {
//        TenantContextHolder.setTenantId(id);
//        return ApiResponse.ok(adminSysTenantService.removeById(id));
//    }
//
//
//    @ApiOperation(value = "编辑")
//    @SysLog("编辑租户")
//    @PutMapping
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:edit')")
//    public ApiResponse update(@Valid @RequestBody AdminSysTenant adminSysTenant) {
//        TenantContextHolder.setTenantId(adminSysTenant.getId());
//        adminSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(adminSysTenantService.updateById(adminSysTenant));
//    }
//
//
//    @ApiOperation(value = "list查询服务间调用")
//    @GetMapping("/inside/list")
//    public ApiResponse getListInside() {
//        AdminSysTenant adminSysTenant = new AdminSysTenant();
//        adminSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(adminSysTenantService.list(Wrappers.query(adminSysTenant)));
//    }
//}
