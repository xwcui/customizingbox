package com.customizingbox.cloud.manager.controller.setting;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.enums.SupplierStateEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminPurchaseUserIdAndNameVo;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierRequestVo;
import com.customizingbox.cloud.common.datasource.model.admin.setting.dto.AdminSupplierDto;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminSupplier;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierResponseVo;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierResponseVoForEdit;
import com.customizingbox.cloud.manager.service.admin.AdminSupplierService;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 供应商表 前端控制器
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
@RestController
@RequestMapping("/adminSupplier")
@AllArgsConstructor
@Api(tags = "供应商表")
public class AdminSupplierController {
    // 不存供应商密码
    // 采购员保存用户id
    private final AdminSysUserService adminSysUserService;
    private AdminSupplierService adminSupplierService;


    @PostMapping("/page")
    @ApiOperation(value = "供应商表 列表分页查询")
    public ApiResponse<IPage<AdminSupplierResponseVo>> page(Page page, AdminSupplierDto adminSupplierDto) {

        return ApiResponse.ok(adminSupplierService.listByPage(page));
    }
    @PostMapping("/list")
    @ApiOperation(value = "供应商列表查询,创建产品时选择已启用供应商")
    // 禁用后 新建产品时无法选择已禁用的供应商
    public ApiResponse<List<AdminSupplier>> list() {
        return ApiResponse.ok(adminSupplierService.list(Wrappers.<AdminSupplier>lambdaQuery().eq(AdminSupplier::getStatus, SupplierStateEnum.ENABLE.getStatus())));
    }


    @PostMapping("/addSupplier")
    @ApiOperation(value = "新增 供应商表")
    public ApiResponse<Boolean> insetData(@RequestBody @Valid AdminSupplierRequestVo vo) {
        ApiResponse<Boolean> response = new ApiResponse<>();
        response.setData(adminSupplierService.addOneSupplier(vo));
        return response;
    }


    @GetMapping("/infoSupplier/{supplierId}")
    @ApiOperation(value = " 根据ID查询 供应商表 详情")
    public ApiResponse<AdminSupplierResponseVoForEdit> info(@ApiParam("供应商id") @PathVariable("supplierId") Long supplierId) {
        return ApiResponse.ok(adminSupplierService.info(supplierId));
    }


    @PutMapping("/editSupplier")
    @ApiOperation(value = "编辑 供应商表")
    public ApiResponse<Boolean> edit(@RequestBody AdminSupplierRequestVo vo) {
        ApiResponse<Boolean> response = new ApiResponse<>();
        AdminSupplier adminSupplier = vo.toAdminSupplier();
        adminSupplier.setPhone(null);
        response.setData(adminSupplierService.edit(adminSupplier));
        return response;
    }

    @PutMapping("/changeStatus/{id}/{status}")
    @ApiOperation(value = "启用禁用供货商")
    public ApiResponse<Boolean> changeStatus(@ApiParam("供应商id") @PathVariable Long id,
                                             @ApiParam("启用禁用状态，1启用 2禁用") @PathVariable Integer status) {
        // 禁用之后供应商可以继续登录
        // 若产品库有产品为该供应商，无法禁用
        return ApiResponse.ok(adminSupplierService.changeStatus(id, status));
    }

    @GetMapping("/getAdminPurchaseUser")
    @ApiOperation(value = "查询所有采购员id和用户名")
    public ApiResponse<List<AdminPurchaseUserIdAndNameVo>> getAdminPurchaseUser() {
        return ApiResponse.ok(adminSysUserService.getAdminPurchaseUser());
    }
}
