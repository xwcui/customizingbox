package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysUserRoleMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRole;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUserRole;
import com.customizingbox.cloud.manager.service.admin.AdminGlobalConfigService;
import com.customizingbox.cloud.manager.service.admin.AdminSysRoleService;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserRoleService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AdminSysUserRoleServiceImpl extends ServiceImpl<AdminSysUserRoleMapper, AdminSysUserRole> implements AdminSysUserRoleService {

    private final AdminGlobalConfigService adminGlobalConfigService;
    private final AdminSysRoleService adminSysRoleService;
    /**
     * 根据用户Id删除该用户的角色关系
     */
    @Override
    public Boolean deleteByUserId(String userId) {
        return baseMapper.deleteByUserId(userId);
    }

    @Override
    public Boolean initUserRole(Long userId, String key) {
        String roleId = adminGlobalConfigService.getValueByKey(key);
        if (StringUtils.isEmpty(roleId)) {
            return false;
        }
        AdminSysRole adminSysRole = adminSysRoleService.getById(roleId);
        if (ObjectUtils.isEmpty(adminSysRole)) {
            return false;
        }
        AdminSysUserRole adminSysUserRole = new AdminSysUserRole();
        adminSysUserRole.setRoleId(roleId);
        adminSysUserRole.setUserId(userId.toString());
        return this.save(adminSysUserRole);
    }
}
