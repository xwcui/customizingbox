package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysConfigStorageMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysConfigStorage;
import com.customizingbox.cloud.manager.service.admin.AdminSysConfigStorageService;
import org.springframework.stereotype.Service;

/**
 * 存储配置
 */
@Service
public class AdminSysConfigStorageServiceImpl extends ServiceImpl<AdminSysConfigStorageMapper, AdminSysConfigStorage> implements AdminSysConfigStorageService {

}
