package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

import java.util.List;

/**
 * <p>
 * 运输方式 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminTransportService extends IService<AdminTransport> {


    /**
     * 赛盒运输方式 分页查询
     * @param page
     * @param searchVo
     * @return
     */
    IPage<AdminTransportPageVo> pageAll(Page page, AdminTransportSearchVo searchVo);

    /**
     * 保存或修改运输方式
     * @param adminTransportDto
     * @param user
     * @return
     */
    ApiResponse saveOrUpdateAdminTransportDto(AdminTransportDto adminTransportDto, BaseUser user);

    /**
     * 运输方式启用或禁用
     * @param id
     * @param status
     * @param user
     * @return
     */
    ApiResponse openOrShut(Long id, Integer status, BaseUser user);

    /**
     * 查询所有已开启运输方式
     * @return
     * @param searchVo
     */
    List<AdminTransport> getList(AdminTransportSearchVo searchVo);
}
