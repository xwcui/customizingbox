package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.AdminOrderToSaiheSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.OrderToSaihePageVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;

/**
 * panda订单
 *
 * @author Y
 * @date 2022-04-12 16:41:50
 */
public interface AppPandaOrderService extends IService<AppPandaOrder> {


    /**
     * 获取上传赛盒订单分页
     * @param page
     * @param query
     * @return
     */
    IPage<OrderToSaihePageVO> selectSaiheToOrderPage(Page page, AdminOrderToSaiheSearchVo query);


    /**
     * 定时器上传订单到赛盒
     */
    void  OrdersToSaohe();
}
