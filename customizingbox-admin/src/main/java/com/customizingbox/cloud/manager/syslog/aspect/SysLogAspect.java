package com.customizingbox.cloud.manager.syslog.aspect;


import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import com.customizingbox.cloud.manager.syslog.event.SysLogEvent;
import com.customizingbox.cloud.manager.syslog.util.SysLogUtils;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.ApplicationEventPublisher;

/**
 * 操作日志使用spring event异步入库
 */
@Slf4j
@Aspect
@AllArgsConstructor
public class SysLogAspect {
    private final ApplicationEventPublisher publisher;

    @SneakyThrows
    @Around("@annotation(sysLog)")
    public Object around(ProceedingJoinPoint point, SysLog sysLog) {
        AdminSysLog logVo = SysLogUtils.getSysLog();
        logVo.setType(CommonConstants.LOG_TYPE_0);
        logVo.setTitle(sysLog.value());
        // 发送异步日志事件
        Long startTime = System.currentTimeMillis();
        Object obj = point.proceed();
        Long endTime = System.currentTimeMillis();
        logVo.setTime(endTime - startTime);
        publisher.publishEvent(new SysLogEvent(logVo));
        return obj;
    }

}
