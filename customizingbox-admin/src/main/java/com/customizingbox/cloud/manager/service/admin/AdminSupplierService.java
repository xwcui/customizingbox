package com.customizingbox.cloud.manager.service.admin;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierRequestVo;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminSupplier;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierResponseVo;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierResponseVoForEdit;

/**
 * <p>
 * 供应商表 服务类
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
public interface AdminSupplierService extends IService<AdminSupplier> {


    /**
    * 供应商表列表
     * @param page
     */
    IPage<AdminSupplierResponseVo> listByPage(Page page);

    /**
    * 供应商表详情
    */
    AdminSupplierResponseVoForEdit info(Long id);


    /**
    * 编辑供应商表
    */
    Boolean edit(AdminSupplier vo);

    /**
     * 修改供应商启用禁用状态
     * @param supplierId 供应商id
     * @param status 1启用 2禁用
     * @return
     */
    Boolean changeStatus(Long supplierId, Integer status);


    /**
     * 新建供应商
     * @param vo
     * @return
     */
    Boolean addOneSupplier(AdminSupplierRequestVo vo);
}
