package com.customizingbox.cloud.manager.controller.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRole;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRoleMenu;
import com.customizingbox.cloud.common.datasource.model.admin.system.param.AdminSysRoleParam;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysRoleDetailVO;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import com.customizingbox.cloud.manager.service.admin.AdminSysRoleMenuService;
import com.customizingbox.cloud.manager.service.admin.AdminSysRoleService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/role")
@Api(value = "role", tags = "角色管理模块")
public class AdminSysRoleController {

    private final AdminSysRoleService adminSysRoleService;
    private final AdminSysRoleMenuService adminSysRoleMenuService;


    @ApiOperation(value = "通过ID查询角色信息")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:role:get')")
    public ApiResponse<AdminSysRoleDetailVO> getById(@PathVariable String id) {
        AdminSysRoleDetailVO adminSysRoleDetailVO = adminSysRoleService.getRoleAndMenuById(id);
        return ApiResponse.ok(adminSysRoleDetailVO);
    }


    @ApiOperation(value = "添加角色")
    @SysLog("添加角色")
    @PostMapping
    @PreAuthorize("@ato.hasAuthority('sys:role:add')")
    public ApiResponse save(@Valid @RequestBody AdminSysRoleParam adminSysRoleParam) {
        AdminSysRole adminSysRole = BeanUtil.copyProperties(adminSysRoleParam, AdminSysRole.class);
        adminSysRoleService.save(adminSysRole);
        String menuIds = adminSysRoleParam.getMenuIds();
        if (!StringUtils.isEmpty(menuIds)) {
            // 保存菜单
            adminSysRoleMenuService.saveRoleMenus(adminSysRole.getRoleCode(), adminSysRole.getId(), menuIds);
        }
        return ApiResponse.ok();
    }


    @ApiOperation(value = "修改角色")
    @SysLog("修改角色")
    @PutMapping
    @PreAuthorize("@ato.hasAuthority('sys:role:edit')")
    public ApiResponse update(@Valid @RequestBody AdminSysRoleParam adminSysRoleParam) {
//        if (!this.judeAdmin(adminSysRoleParam.getId())) {
//            return ApiResponse.failed("管理员角色不能操作");
//        }
        AdminSysRole adminSysRole = BeanUtil.copyProperties(adminSysRoleParam, AdminSysRole.class);
        String menuIds = adminSysRoleParam.getMenuIds();
        if (!StringUtils.isEmpty(menuIds)) {
            // 保存菜单
            adminSysRoleMenuService.saveRoleMenus(adminSysRole.getRoleCode(), adminSysRole.getId(), menuIds);
        }
        return ApiResponse.ok(adminSysRoleService.updateById(adminSysRole));
    }


    @ApiOperation(value = "删除角色")
    @SysLog("删除角色")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:role:del')")
    public ApiResponse removeById(@PathVariable String id) {
        if (!this.judeAdmin(id)) {
            return ApiResponse.failed("管理员角色不能操作");
        }
        return ApiResponse.ok(adminSysRoleService.removeRoleById(id));
    }


    @ApiOperation(value = "获取角色列表")
    @GetMapping("/list")
    public ApiResponse<List<AdminSysRole>> getList() {
        return ApiResponse.ok(adminSysRoleService.list());
    }



    @ApiOperation(value = "分页查询角色信息")
    @GetMapping("/page")
    @PreAuthorize("@ato.hasAuthority('sys:role:index')")
    public ApiResponse<IPage<List<AdminSysRole>>> getRolePage(Page page) {
        return ApiResponse.ok(adminSysRoleService.page(page, Wrappers.emptyWrapper()));
    }


    @ApiOperation(value = "更新角色菜单")
    @SysLog("更新角色菜单")
    @PutMapping("/menu")
    @PreAuthorize("@ato.hasAuthority('sys:role:perm','sys:tenant:edit')")
    public ApiResponse saveRoleMenus(@RequestBody AdminSysRoleMenu adminSysRoleMenu) {
        String roleId = adminSysRoleMenu.getRoleId();
        String menuIds = adminSysRoleMenu.getMenuId();
        if (StrUtil.isBlank(roleId) || StrUtil.isBlank(menuIds)) {
            return ApiResponse.ok();
        }
//        if (!this.judeAdmin(roleId)) {
//            return ApiResponse.failed("管理员角色不能操作");
//        }
        AdminSysRole adminSysRole = adminSysRoleService.getById(roleId);
        return ApiResponse.ok(adminSysRoleMenuService.saveRoleMenus(adminSysRole.getRoleCode(), roleId, menuIds));
    }


    boolean judeAdmin(String roleId) {
        AdminSysRole adminSysRole = adminSysRoleService.getById(roleId);
        if (CommonConstants.ROLE_CODE_ADMIN.equals(adminSysRole.getRoleCode())) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }


    @ApiOperation(value = "更新租户管理员角色菜单")
    @SysLog("更新租户管理员角色菜单")
    @PutMapping("/menu/tenant")
    @PreAuthorize("@ato.hasAuthority('sys:tenant:edit')")
    public ApiResponse saveRoleMenusTenant(@RequestBody AdminSysRoleMenu adminSysRoleMenu) {
        String tenantId = adminSysRoleMenu.getTenantId();
        String roleId = adminSysRoleMenu.getRoleId();
        String menuIds = adminSysRoleMenu.getMenuId();
        if (StrUtil.isBlank(tenantId) || StrUtil.isBlank(menuIds) || StrUtil.isBlank(menuIds)) {
            return ApiResponse.ok();
        }
        TenantContextHolder.setTenantId(tenantId);
        AdminSysRole adminSysRole = adminSysRoleService.getById(roleId);
        return ApiResponse.ok(adminSysRoleMenuService.saveRoleMenus(adminSysRole.getRoleCode(), roleId, menuIds));
    }
}
