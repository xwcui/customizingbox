package com.customizingbox.cloud.manager.service.admin;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminCountryInfoDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminCountryInfo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminCountryInfoSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

/**
 * <p>
 * 国家和地区表信息 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-14
 */
public interface AdminCountryInfoService extends IService<AdminCountryInfo> {


    /**
     * 国家和地区分页
     * @param page
     * @param searchVo
     * @return
     */
    IPage<AdminCountryInfo> pageAll(Page page, AdminCountryInfoSearchVo searchVo);

    /**
     * 保存或修改国家地区
     * @param adminCountryInfoDto
     * @param user
     * @return
     */
    ApiResponse saveOrUpdateAdminCountryInfoDto(AdminCountryInfoDto adminCountryInfoDto, BaseUser user);
}
