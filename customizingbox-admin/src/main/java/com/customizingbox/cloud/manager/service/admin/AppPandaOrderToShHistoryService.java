package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderToShHistory;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Z
 * @since 2022-05-09
 */
public interface AppPandaOrderToShHistoryService extends IService<AppPandaOrderToShHistory> {


    /**
     * 上传赛盒信息
     * @param appPandaOrderToShHistory
     * @return
     */
    void saveOrUpdateHistory(AppPandaOrderToShHistory appPandaOrderToShHistory);
}
