package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysPreLogVO;

import java.util.List;

/**
 * <p>
 * 日志表 服务类
 * </p>
 */
public interface AdminSysLogService extends IService<AdminSysLog> {


    /**
     * 批量插入前端错误日志
     *
     * @param adminSysPreLogVOList 日志信息
     * @return true/false
     */
    Boolean saveBatchLogs(List<AdminSysPreLogVO> adminSysPreLogVOList);
}
