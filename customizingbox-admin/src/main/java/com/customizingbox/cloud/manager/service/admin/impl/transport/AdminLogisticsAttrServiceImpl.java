package com.customizingbox.cloud.manager.service.admin.impl.transport;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.TransportEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminLogisticsAttrMapper;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminLogisticsAttrDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsAttr;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.AdminLogisticsAttrService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 物流属性表 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Service
public class AdminLogisticsAttrServiceImpl extends ServiceImpl<AdminLogisticsAttrMapper, AdminLogisticsAttr> implements AdminLogisticsAttrService {

    @Resource
    private AdminLogisticsAttrMapper adminLogisticsAttrMapper;

    /**
     * 分页
     *
     * @param page
     * @param searchVo
     * @return
     */
    @Override
    public IPage<AdminLogisticsAttrPageVo> pageAll(Page page, AdminLogisticsAttrSearchVo searchVo) {
        return adminLogisticsAttrMapper.pageAll(page, searchVo);
    }

    /**
     * 保存或者修改
     *
     * @param adminLogisticsAttrDto
     * @param user
     * @return
     */
    @Override
    public ApiResponse saveOrUpdateAdminLogisticsAttrDto(AdminLogisticsAttrDto adminLogisticsAttrDto, BaseUser user) {
        AdminLogisticsAttr adminLogisticsAttr = new AdminLogisticsAttr();
        if (adminLogisticsAttrDto.getId() != null) {
            adminLogisticsAttr = this.getById(adminLogisticsAttrDto.getId());
            if (adminLogisticsAttr == null) {
                return ApiResponse.failed("数据库中没有该数据");
            }
        } else {
            QueryWrapper<AdminLogisticsAttr> queryWrapper = new QueryWrapper<>();
            queryWrapper.and(wrapper -> wrapper.eq("name_en",adminLogisticsAttrDto.getNameEn() )
                    .or().eq("name_cn", adminLogisticsAttrDto.getNameCn())
            );
            Integer integer = adminLogisticsAttrMapper.selectCount(queryWrapper);
            if (integer > 0 ){
                return ApiResponse.failed("物流属性中已存在该中文名或英文名");
            }
            adminLogisticsAttr.setCreateTime(LocalDateTime.now());
            adminLogisticsAttr.setCreateId(Long.parseLong(user.getId()));
        }
        adminLogisticsAttr.setStatus(adminLogisticsAttrDto.getStatus());
        adminLogisticsAttr.setDescription(adminLogisticsAttrDto.getDescription());
        adminLogisticsAttr.setNameCn(adminLogisticsAttrDto.getNameCn());
        adminLogisticsAttr.setNameEn(adminLogisticsAttrDto.getNameEn());
        adminLogisticsAttr.setUpdateId(Long.parseLong(user.getId()));
        adminLogisticsAttr.setUpdateTime(LocalDateTime.now());
        this.saveOrUpdate(adminLogisticsAttr);
        return ApiResponse.ok();
    }


    /**
     * 开启禁用
     * @param id
     * @param status
     * @return
     */
    @Override
    public ApiResponse openOrShut(Long id, Integer status) {
        AdminLogisticsAttr adminLogisticsAttr = this.getById(id);
        if (adminLogisticsAttr == null) {
            return ApiResponse.failed("数据库中没有该数据");
        }
        if (status.equals(TransportEnum.TransportStatusEnum.SHUT.getCode())){
            adminLogisticsAttr.setStatus(TransportEnum.TransportStatusEnum.SHUT.getCode());
        }else if (status.equals(TransportEnum.TransportStatusEnum.OPEN.getCode())){
            adminLogisticsAttr.setStatus(TransportEnum.TransportStatusEnum.OPEN.getCode());
        }else {
            return ApiResponse.failed("状态异常");
        }
        adminLogisticsAttr.setUpdateTime(LocalDateTime.now());
        this.updateById(adminLogisticsAttr);
        return ApiResponse.ok();
    }

    @Override
    public List<AdminLogisticsAttr> getList() {
        QueryWrapper<AdminLogisticsAttr> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", TransportEnum.TransportStatusEnum.OPEN.getCode());
        List<AdminLogisticsAttr> list = this.list(queryWrapper);
        return list;
    }
}
