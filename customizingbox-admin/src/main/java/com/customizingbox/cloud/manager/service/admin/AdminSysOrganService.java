package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysOrganTree;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrgan;

import java.util.List;

/**
 * <p>
 * 机构管理 服务类
 * </p>
 */
public interface AdminSysOrganService extends IService<AdminSysOrgan> {

    /**
     * 查询机构树菜单
     *
     * @return 树
     */
    List<AdminSysOrganTree> selectTree();

    /**
     * 添加信息机构
     *
     */
    Boolean saveOrgan(AdminSysOrgan adminSysOrgan);

    /**
     * 删除机构
     *
     * @param id 机构 ID
     * @return 成功、失败
     */
    Boolean removeOrganById(String id);

    /**
     * 更新机构
     *
     * @param adminSysOrgan 机构信息
     * @return 成功、失败
     */
    Boolean updateOrganById(AdminSysOrgan adminSysOrgan);

}
