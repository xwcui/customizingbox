package com.customizingbox.cloud.manager.controller.customer;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.customer.entity.AdminAppRelevancyRecord;
import com.customizingbox.cloud.manager.service.admin.AdminAppRelevancyRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 客户经理认领/关联 店铺用户记录表 前端控制器
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-14
 */
@RestController
@RequestMapping("//adminAppRelevancyRecord")
@Api(tags = "客户经理认领/关联 店铺用户记录表")
@AllArgsConstructor
public class AdminAppRelevancyRecordController {
    private final AdminAppRelevancyRecordService adminAppRelevancyRecordService;

    @GetMapping("/page")
    @ApiOperation(value = "客户经理认领用户记录列表查询")
    public ApiResponse<IPage<AdminAppRelevancyRecord>> page(Page page) {
        // return ApiResponse.ok(adminAppRelevancyRecordService.listReadyProduce(page));
        return null;
    }
}
