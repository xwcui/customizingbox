package com.customizingbox.cloud.manager.controller.order;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderCheckRefundParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderRefundApplyParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderRefundParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderCheckRefundDetailVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundDetailVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundVO;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderRefundService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/panda/order/refund")
@AllArgsConstructor
@Api(tags = "用户退款管理")
public class AppPandaOrderRefundController {

    private final AppPandaOrderRefundService appPandaOrderRefundService;

    @GetMapping("/apply/{orderId}")
    @ApiOperation(value = "1 - 查询申请退款页面数据")
    public ApiResponse<AppPandaOrderRefundDetailVO> apply(@ApiParam(value = "退款订单id") @PathVariable Long orderId) {
        AppPandaOrderRefundDetailVO appPandaOrderRefundDetailVO = appPandaOrderRefundService.apply(orderId);
        return ApiResponse.ok(appPandaOrderRefundDetailVO);
    }

    @PostMapping("/submitRefundApply")
    @ApiOperation(value = "2 - 提交退款申请")
    public ApiResponse<Boolean> submitRefundApply(@RequestBody AppPandaOrderRefundApplyParam appPandaOrderRefundApplyParam) {
        Boolean result = appPandaOrderRefundService.submitRefundApply(appPandaOrderRefundApplyParam);
        return ApiResponse.ok(result);
    }

    @PostMapping("/page")
    @ApiOperation(value = "3 - 退款管理页面")
    public ApiResponse<IPage<AppPandaOrderRefundVO>> refundPage(Page page, @Valid @RequestBody AppPandaOrderRefundParam appPandaOrderRefundParam) {
        return ApiResponse.ok(appPandaOrderRefundService.refundPage(page, appPandaOrderRefundParam));
    }

    @GetMapping("refundCheck/{refundId}")
    @ApiOperation(value = "4 - 退款审核(审核中)")
    public ApiResponse<AppPandaOrderCheckRefundDetailVO> refundCheck(@ApiParam(value = "退款订单id") @PathVariable Long refundId) {
        AppPandaOrderCheckRefundDetailVO applyRefundDetailVO = appPandaOrderRefundService.refundCheck(refundId);
        return ApiResponse.ok(applyRefundDetailVO);
    }

    @PostMapping("/checkRefundApply")
    @ApiOperation(value = "5 - 提交退款审核结果(拒绝 or 同意)")
    public ApiResponse<Boolean> checkRefundApply(@RequestBody @Valid AppPandaOrderCheckRefundParam param) {
        return ApiResponse.ok(appPandaOrderRefundService.checkRefundApply(param));
    }

    @GetMapping("/detail/{refundId}")
    @ApiOperation(value = "6 - 退款详情")
    public ApiResponse<AppPandaOrderCheckRefundDetailVO> detail(@ApiParam(value = "退款订单id") @PathVariable Long refundId) {
        AppPandaOrderCheckRefundDetailVO checkRefundDetailVO = appPandaOrderRefundService.refundCheck(refundId);
        return ApiResponse.ok(checkRefundDetailVO);
    }
}
