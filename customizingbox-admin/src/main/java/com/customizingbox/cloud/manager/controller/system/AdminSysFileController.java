package com.customizingbox.cloud.manager.controller.system;


import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.core.util.OSSFileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;



@Slf4j
@RestController
@RequestMapping("/file")
@Api(tags = "文件上传")
public class AdminSysFileController {

    @PostMapping(value = "/upload")
    @ApiOperation(value = "文件上传", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "图片类型：1：产品；", name = "code", required = true, paramType = "query")
    })
    public ApiResponse<String> imgUpload(@RequestParam("file") MultipartFile file, @RequestParam("code") Integer code) {
        try {
            return ApiResponse.ok(OSSFileUtil.uploadFile(file, code));
        } catch (Exception e) {
            log.error("error", e);
        }
        return ApiResponse.failed("文件上传失败");
    }

}
