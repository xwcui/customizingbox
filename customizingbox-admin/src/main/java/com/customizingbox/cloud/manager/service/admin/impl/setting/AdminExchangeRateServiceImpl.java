package com.customizingbox.cloud.manager.service.admin.impl.setting;

import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.TransactionEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.datasource.mapper.admin.setting.AdminExchangeRateMapper;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminExchangeRate;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminExchangeRateResponseVo;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminExchangeRateService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 汇率表 服务实现类
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
@Service
@AllArgsConstructor
public class AdminExchangeRateServiceImpl extends ServiceImpl<AdminExchangeRateMapper, AdminExchangeRate> implements AdminExchangeRateService {
    private final AdminExchangeRateMapper adminExchangeRateMapper;
    private final RedisTemplate<String,String> redisTemplate;

    @Override
    public Boolean addOne(AdminExchangeRate vo) {
        return super.save(vo);
    }

    @Override
    public AdminExchangeRateResponseVo getOneRate(String currencyCode) {
        return adminExchangeRateMapper.getOneRateByCode(currencyCode);
    }

    @Override
    public BigDecimal getOneRateValue(String currencyCode) {
        String rateStr = redisTemplate.opsForValue().get(currencyCode);
        if (StringUtils.isEmpty(rateStr)){
            BigDecimal rate = adminExchangeRateMapper.selectOne(Wrappers.<AdminExchangeRate>lambdaQuery().eq(AdminExchangeRate::getCurrencyCode, currencyCode)).getRate();
            if (rate == null){
                return null;
            }
            redisTemplate.opsForValue().set(currencyCode,rate.toString(),1,TimeUnit.DAYS);
            rateStr = rate.toString();
        }

        return NumberUtil.toBigDecimal(rateStr);
    }

    @Override
    public Boolean editExchangeRate(String currencyCode, String rate) {
        redisTemplate.opsForValue().set(currencyCode, rate,1, TimeUnit.DAYS);

        Long userId = SecurityUtils.getUserId();
        LocalDateTime now = LocalDateTime.now();
        return adminExchangeRateMapper.editExchangeRate(currencyCode, rate, userId, now);
    }

    @Override
    public List<AdminExchangeRateResponseVo> listAllRate() {
        return adminExchangeRateMapper.listAllRate();
    }

    @Override
    public BigDecimal rmbConvertUsd(BigDecimal money) {
        if (ObjectUtils.isEmpty(money)) {
            throw new ApiException("兑换金额不能为空");
        }
        BigDecimal rate = this.getOneRateValue(TransactionEnum.CURRENCY.USD.getCode());
        if (ObjectUtils.isEmpty(rate)) {
            throw new ApiException("货币兑换率为空");
        }
        return money.divide(rate, 2, BigDecimal.ROUND_HALF_UP);
    }
}
