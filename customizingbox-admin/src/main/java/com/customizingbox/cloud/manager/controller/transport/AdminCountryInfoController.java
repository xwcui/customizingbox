package com.customizingbox.cloud.manager.controller.transport;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminCountryInfoDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminCountryInfo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminCountryInfoSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminCountryInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * <p>
 * 国家和地区表信息 前端控制器
 * </p>
 *
 * @author Z
 * @since 2022-04-14
 */
@RestController
@RequestMapping("/adminCountryInfo")
@Api(tags = "国家和地区表信息")
public class AdminCountryInfoController {

    @Autowired
    private AdminCountryInfoService service;

    @ApiOperation("/国家和地区分页")
    @PostMapping("/page")
    public ApiResponse<IPage<AdminCountryInfo>> page(Page page, @RequestBody AdminCountryInfoSearchVo searchVo){
        if (page == null){
            page = new Page();
        }
        IPage<AdminCountryInfo> result = service.pageAll(page,searchVo);
        return ApiResponse.ok(result);
    }

    @ApiOperation("/国家和地区List")
    @PostMapping("/List")
    public ApiResponse<List<AdminCountryInfo>> list(){
        return ApiResponse.ok(service.list());
    }


    @ApiOperation("/国家和地区新增或修改")
    @PostMapping("/saveOrUpdate")
    public ApiResponse saveOrUpdate(@RequestBody @Validated AdminCountryInfoDto adminCountryInfoDto){
        BaseUser user = SecurityUtils.getUser();
        if (user == null){
            throw new ApiException("无法获取");
        }
        return  service.saveOrUpdateAdminCountryInfoDto(adminCountryInfoDto,user);
    }
}
