package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysTenantMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.*;
import com.customizingbox.cloud.manager.service.admin.*;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 租户管理
 */
@Service
@AllArgsConstructor
public class AdminSysTenantServiceImpl extends ServiceImpl<AdminSysTenantMapper, AdminSysTenant> implements AdminSysTenantService {

    private final AdminSysUserService adminSysUserService;
    private final AdminSysRoleService adminSysRoleService;
    private final AdminSysUserRoleService adminSysUserRoleService;
    private final AdminSysOrganRelationService adminSysOrganRelationService;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(AdminSysTenant entity) {
        baseMapper.insert(entity);
        //新建机构关联
        AdminSysOrgan adminSysOrgan = new AdminSysOrgan();
        BeanUtil.copyProperties(entity, adminSysOrgan);
        adminSysOrganRelationService.insertOrganRelation(adminSysOrgan);
        //新建用户
        AdminSysUser sysUser = new AdminSysUser();
        sysUser.setOrganId(adminSysOrgan.getId());
        sysUser.setUsername(entity.getUsername());
        sysUser.setPassword(ENCODER.encode(entity.getPassword()));
        adminSysUserService.save(sysUser);
        //新建角色
        AdminSysRole adminSysRole = new AdminSysRole();
        adminSysRole.setRoleName("管理员");
        adminSysRole.setRoleCode(CommonConstants.ROLE_CODE_ADMIN);
        adminSysRole.setRoleDesc(entity.getName() + "管理员");
        adminSysRole.setDsType(CommonConstants.DS_TYPE_0);
        adminSysRoleService.save(adminSysRole);
        //新建用户角色
        AdminSysUserRole adminSysUserRole = new AdminSysUserRole();
        adminSysUserRole.setRoleId(adminSysRole.getId());
        adminSysUserRole.setUserId(sysUser.getId());
        adminSysUserRoleService.save(adminSysUserRole);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        baseMapper.deleteSysTenantById(id);
        return Boolean.TRUE;
    }
}
