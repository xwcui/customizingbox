package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysConfigEditorMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysConfigEditor;
import com.customizingbox.cloud.manager.service.admin.AdminSysConfigEditorService;
import org.springframework.stereotype.Service;

/**
 * 编辑器配置
 */
@Service
public class AdminSysConfigEditorServiceImpl extends ServiceImpl<AdminSysConfigEditorMapper, AdminSysConfigEditor> implements AdminSysConfigEditorService {

}
