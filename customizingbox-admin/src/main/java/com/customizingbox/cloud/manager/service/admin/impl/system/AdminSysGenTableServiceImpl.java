package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysGenTableMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysGenTable;
import com.customizingbox.cloud.manager.service.admin.AdminSysGenTableService;
import org.springframework.stereotype.Service;

/**
 * 代码生成配置表
 */
@Service
public class AdminSysGenTableServiceImpl extends ServiceImpl<AdminSysGenTableMapper, AdminSysGenTable> implements AdminSysGenTableService {


}
