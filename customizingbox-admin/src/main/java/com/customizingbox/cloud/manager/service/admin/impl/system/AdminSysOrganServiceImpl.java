package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysOrganMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysOrganTree;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrgan;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrganRelation;
import com.customizingbox.cloud.common.datasource.util.AdminSysTreeUtil;
import com.customizingbox.cloud.manager.service.admin.AdminSysOrganRelationService;
import com.customizingbox.cloud.manager.service.admin.AdminSysOrganService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 机构管理
 */
@Service
@AllArgsConstructor
public class AdminSysOrganServiceImpl extends ServiceImpl<AdminSysOrganMapper, AdminSysOrgan> implements AdminSysOrganService {

    private final AdminSysOrganRelationService adminSysOrganRelationService;

    /**
     * 添加信息机构
     *
     * @param organ 机构
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveOrgan(AdminSysOrgan organ) {
        AdminSysOrgan adminSysOrgan = new AdminSysOrgan();
        BeanUtil.copyProperties(organ, adminSysOrgan);
        this.save(adminSysOrgan);
        adminSysOrganRelationService.insertOrganRelation(adminSysOrgan);
        return Boolean.TRUE;
    }


    /**
     * 删除机构
     *
     * @param id 机构 ID
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeOrganById(String id) {
        //级联删除机构
        List<String> idList = adminSysOrganRelationService
                .list(Wrappers.<AdminSysOrganRelation>query().lambda().eq(AdminSysOrganRelation::getAncestor, id))
                .stream()
                .map(AdminSysOrganRelation::getDescendant)
                .collect(Collectors.toList());

        if (CollUtil.isNotEmpty(idList)) {
            this.removeByIds(idList);
        }

        //删除机构级联关系
        adminSysOrganRelationService.deleteAllOrganRelation(id);
        return Boolean.TRUE;
    }

    /**
     * 更新机构
     *
     * @param adminSysOrgan 机构信息
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateOrganById(AdminSysOrgan adminSysOrgan) {
        //更新机构状态
        this.updateById(adminSysOrgan);
        //更新机构关系
        AdminSysOrganRelation relation = new AdminSysOrganRelation();
        relation.setAncestor(adminSysOrgan.getParentId());
        relation.setDescendant(adminSysOrgan.getId());
        adminSysOrganRelationService.updateOrganRelation(relation);
        return Boolean.TRUE;
    }

    /**
     * 查询全部机构树
     *
     * @return 树
     */
    @Override
    public List<AdminSysOrganTree> selectTree() {
        return getTree(this.list(Wrappers.emptyWrapper()));
    }

    /**
     * 构建机构树
     */
    private List<AdminSysOrganTree> getTree(List<AdminSysOrgan> entitys) {
        List<AdminSysOrganTree> treeList = entitys.stream()
                .filter(entity -> !entity.getId().equals(entity.getParentId()))
                .sorted(Comparator.comparingInt(AdminSysOrgan::getSort))
                .map(entity -> {
                    AdminSysOrganTree node = new AdminSysOrganTree();
                    BeanUtil.copyProperties(entity, node);
                    return node;
                }).collect(Collectors.toList());
        return AdminSysTreeUtil.build(treeList, CommonConstants.PARENT_ID);
    }
}
