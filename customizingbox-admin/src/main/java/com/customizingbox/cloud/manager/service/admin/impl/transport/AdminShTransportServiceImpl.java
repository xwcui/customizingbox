package com.customizingbox.cloud.manager.service.admin.impl.transport;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminShTransportMapper;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ApiTransport;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.ApiGetTransportResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminShTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminShTransportSearchVo;
import com.customizingbox.cloud.manager.util.SaiheApiUtils;
import com.customizingbox.cloud.manager.service.admin.AdminShTransportService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 赛合运输方式 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-13
 */
@Service
public class AdminShTransportServiceImpl extends ServiceImpl<AdminShTransportMapper, AdminShTransport> implements AdminShTransportService {

    @Autowired
    private SaiheApiUtils saiheApiUtils;

    @Resource
    private AdminShTransportMapper adminShTransportMapper;

    /**
     * 更新赛盒物流
     * 1. 根据赛盒物流修改本地赛盒物流
     * 2. 将本地赛盒物流没有的   satate标记为 0
     * @return
     */
    @Override
    @Transactional
    public ApiResponse toUpdateShTransport() {
        // 获取赛盒物流
        List<AdminShTransport> shTransports = getTransportList();
        adminShTransportMapper.updaAdminShTransport(shTransports);
        return ApiResponse.ok("success");
    }

    /**
     * 获取赛盒物流
     * @return
     */
    @Override
    public List<AdminShTransport> getTransportList() {
        List<AdminShTransport> list = new ArrayList<>();
        ApiGetTransportResponse transportList = saiheApiUtils.getTransportList();
        if (StringUtils.equalsIgnoreCase("OK", transportList.getGetTransportListResult().getStatus())) {
            List<ApiTransport> apiTransport = transportList.getGetTransportListResult().getTransportList().getApiTransport();
            for (ApiTransport transport : apiTransport) {
                AdminShTransport adminShTransport = new AdminShTransport();
                adminShTransport.setId(transport.getID());
                adminShTransport.setCarrierName(transport.getCarrierName());
                adminShTransport.setTransportName(transport.getTransportName());
                adminShTransport.setTransportNameEn(transport.getTransportNameEn());
                adminShTransport.setIsRegistered(transport.getRegistered());
                list.add(adminShTransport);
            }
        }
        return list;
    }

    @Override
    public IPage<AdminShTransport> pageAll(Page page, AdminShTransportSearchVo searchVo) {
        return adminShTransportMapper.pageAll(page,searchVo);
    }
}
