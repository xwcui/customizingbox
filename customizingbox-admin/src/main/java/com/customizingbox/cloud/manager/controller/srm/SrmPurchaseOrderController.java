package com.customizingbox.cloud.manager.controller.srm;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.util.MapUtils;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.srm.dto.FillInProgressDto;
import com.customizingbox.cloud.common.datasource.model.srm.dto.RefuseProduceDto;
import com.customizingbox.cloud.common.datasource.model.srm.query.DefectiveProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.FulfilledProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.ProducingProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.ReadyProduceQuery;
import com.customizingbox.cloud.common.datasource.model.srm.vo.*;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.DeliveryProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.ProducingProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.PurchaseProductVo;
import com.customizingbox.cloud.common.datasource.util.EasyPoiUtils;
import com.customizingbox.cloud.manager.config.excel.ExcelFillCellMergeStrategy;
import com.customizingbox.cloud.manager.service.srm.SrmPurchaseOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 采购订单表 前端控制器
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
@RestController
@AllArgsConstructor
@RequestMapping("//adminPurchaseOrder")
@Api(tags = "采购订单表")
public class SrmPurchaseOrderController {
    private final SrmPurchaseOrderService service;

    @PostMapping("/listReadyProduce")
    @ApiOperation(value = "待生产采购订单列表")
    public ApiResponse<List<PurchaseProductVo>> listReadyProduce(@RequestBody ReadyProduceQuery query) {
        ApiResponse<List<PurchaseProductVo>> response = new ApiResponse<>();
        response.setData(service.listReadyProduce(query));
        return response;
    }

    @PostMapping("/pageReadyProduce")
    @ApiOperation(value = "待生产采购订单列表分页")
    public ApiResponse<IPage<PurchaseProductVo>> pageReadyProduce(Page page) {
        ApiResponse<IPage<PurchaseProductVo>> response = new ApiResponse<>();

        IPage<PurchaseProductVo> data = service.pageReadyProduce(page);
        if (ObjectUtil.isNull(data)) {
            return ApiResponse.failed("无待生产订单");
        }
        response.setData(data);
        return response;
    }


    @PutMapping("/startProduce")
    @ApiOperation(value = "开始生产")
    public ApiResponse<Boolean> startProduce(@ApiParam("采购单id集合") @RequestBody List<Long> purchaseOrderIds) {
        // 前端对同一批次一个spu做限制
        if (service.startProduce(purchaseOrderIds)) {
            return ApiResponse.ok();
        }
        return ApiResponse.failed("开始生产失败");
    }

    @PutMapping("/refuseProduce")
    @ApiOperation("打回")
    public ApiResponse<Boolean> refuseProduce(@RequestBody RefuseProduceDto refuseProduceDto) {
        if (ObjectUtils.isEmpty(refuseProduceDto)) {
            return ApiResponse.failed("打回为空");
        }
        if (ObjectUtils.isEmpty(refuseProduceDto.getRefuseReason())) {
            return ApiResponse.failed("打回原因必选");
        }
        if (service.refuseProduce(refuseProduceDto.getPurchaseOrderId(), refuseProduceDto.getRefuseReason())) {
            return ApiResponse.ok(true);
        }
        return ApiResponse.failed("打回失败");
    }

    @PostMapping("/pageProducingProduct")
    @ApiOperation("生产中订单产品分页列表")
    public ApiResponse<IPage<ProducingProductVo>> pageProducingProduct(Page page, @RequestBody ProducingProductQuery query) {
        IPage<ProducingProductVo> data = service.pageProducingProduct(page, query);
        if (ObjectUtil.isNull(data)) {
            return ApiResponse.failed("生产中订单为空");
        }
        return ApiResponse.ok(data);
    }

    @PostMapping("/printOnlyCodeBatch")
    @ApiOperation("唯一码批量打印")
    public ApiResponse<List<PrintOnlyCodeVo>> printOnlyCodeBatch(@RequestBody ProducingProductQuery query){
        return ApiResponse.ok(service.printOnlyCodeBatch(query));
    }

    @PostMapping("/exportProductionMeansBatch")
    @ApiOperation("批量导出生产资料")
    public void exportProductionMeansBatch(@ApiParam("生产中订单产品查询条件") @RequestBody ProducingProductQuery producingProductQuery,
                                           HttpServletResponse response) throws IOException {
        List<Long> purchaseOrderIds = service.listProducingProduct(producingProductQuery);

        if (CollectionUtils.isEmpty(purchaseOrderIds)) {
            return;
        }
        List<ExportProductionMeans> exportList = service.exportProductionMeansBatch(purchaseOrderIds);
        if (CollectionUtils.isEmpty(exportList)) {
            return;
        }

        try {
            String fileName = "ProductionMeans_" + DateUtil.now().replaceAll(" ", "_").replaceAll(":", "")
                    + RandomUtil.randomNumbers(4);

            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xls");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");

            // todo 使用正确数据测试
            EasyExcel.write(response.getOutputStream(), ExportProductionMeans.class)
                    .excelType(ExcelTypeEnum.XLS)
                    .sheet("sheet1")
                    .registerWriteHandler(new ExcelFillCellMergeStrategy(1, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}))
                    .doWrite(exportList);
        } catch (Exception e) {
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            Map<String, String> map = MapUtils.newHashMap();
            map.put("status", "failure");
            map.put("message", "下载文件失败");
            response.getWriter().println(JSON.toJSONString(map));
        }
    }

    @PostMapping("/getPorducingBatch")
    @ApiOperation("查看生产中批次")
    public ApiResponse<IPage<ProducingBatchDetailVo>> getPorducingBatch(Page page) {
        return ApiResponse.ok(service.getPorducingBatch(page));
    }

    @PostMapping("/pageFulfilled")
    @ApiOperation("已发货列表分页")
    public ApiResponse<IPage<DeliveryProductVo>> pageFulfilled(Page page, @RequestBody FulfilledProductQuery query) {
        IPage<DeliveryProductVo> ipage = service.pageFulfilled(page, query);
        if (ipage == null || CollectionUtils.isEmpty(ipage.getRecords())) {
            return ApiResponse.failed("已发货订单为空");
        }
        return ApiResponse.ok(ipage);
    }

    @GetMapping("/getProductByOnlyCode/{onlyCode}")
    @ApiOperation("发货台扫描单个唯一码")
    public ApiResponse<ReadyDeliveryProductVo> getProductByOnlyCode(@PathVariable String onlyCode) {

        return ApiResponse.ok(service.getProductByOnlyCode(onlyCode));
    }

    @PostMapping("/commitDelivery")
    @ApiOperation("扫描完并填写物流之后 确认发货")
    public ApiResponse<Boolean> commitDelivery(@RequestBody CommitDeliveryRequestVo commitDeliveryVo) {

        return ApiResponse.ok(service.commitDelivery(commitDeliveryVo));
    }

    @ApiOperation("不良品列表分页")
    @PostMapping("/pageDefectiveProducts")
    public ApiResponse<IPage<DefectiveProductVo>> pageDefectiveProducts(Page page, @RequestBody DefectiveProductQuery query) {
        return ApiResponse.ok(service.pageDefectiveProducts(page, query));
    }

    @ApiOperation("质检明细")
    @GetMapping("/checkDetail/{onlyCode}")
    public ApiResponse<SrmCheckDetail> checkDetail(@PathVariable String onlyCode) {

        return ApiResponse.ok(service.checkDetail(onlyCode));
    }

    @ApiOperation("查看进度")
    @GetMapping("/getProgress")
    public ApiResponse<PurchaseOrderProgressVo> getProgress(@RequestParam Long purchaseOrderId) {
        if (ObjectUtil.isNull(purchaseOrderId)) {
            return ApiResponse.failed("采购单id为空");
        }
        return ApiResponse.ok(service.getProgress(purchaseOrderId));
    }

    @ApiOperation("重新发货填写进度，填写进度后采购状态提示已重发")
    @PostMapping("/fillInProgress")
    public ApiResponse<Boolean> fillInProgress(@Valid @RequestBody FillInProgressDto fillInProgressDto) {
        Boolean boo = service.fillInProgress(fillInProgressDto);
        if (boo) {
            return ApiResponse.ok(boo);
        }
        return ApiResponse.failed("无质检记录");
    }


}
