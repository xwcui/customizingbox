package com.customizingbox.cloud.manager.service.admin.impl.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductImgDepotMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductImgDepotService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-31 09:34:40
 */
@Service
public class AdminStoreProductImgDepotServiceImpl extends ServiceImpl<AdminStoreProductImgDepotMapper, AdminStoreProductImgDepot> implements AdminStoreProductImgDepotService {

    @Resource
    private AdminStoreProductImgDepotMapper adminStoreProductImgDepotMapper;


    @Override
    public List<AdminStoreProductImgDepot> getListByProductId(Long productId) {
        QueryWrapper<AdminStoreProductImgDepot> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("product_id",productId);
        return adminStoreProductImgDepotMapper.selectList(queryWrapper);
    }

    @Override
    public int delImageByIds(List<Long> oldImgIds) {
        return adminStoreProductImgDepotMapper.delImageByIds(oldImgIds);
    }
}
