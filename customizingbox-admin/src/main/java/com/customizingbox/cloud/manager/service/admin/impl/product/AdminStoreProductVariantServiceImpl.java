package com.customizingbox.cloud.manager.service.admin.impl.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ProductEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.AdminStoreProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.ToSaiheProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.VariantPriceDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.SkuResult;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductVariantService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-31 09:34:38
 */
@Service
public class AdminStoreProductVariantServiceImpl extends ServiceImpl<AdminStoreProductVariantMapper, AdminStoreProductVariant> implements AdminStoreProductVariantService {

    @Resource
    private AdminStoreProductVariantMapper adminStoreProductVariantMapper;

    /**
     * 修改价格
     * @param variantPriceDtos
     * @param user
     * @return
     */
    @Override
    public ApiResponse updateProductVariantPrice(List<VariantPriceDto> variantPriceDtos, BaseUser user) {
        QueryWrapper<AdminStoreProductVariant> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",variantPriceDtos.stream().map(VariantPriceDto::getId).collect(Collectors.toList()));
        List<AdminStoreProductVariant> adminStoreProductVariants = adminStoreProductVariantMapper.selectList(queryWrapper);
        for (AdminStoreProductVariant adminStoreProductVariant : adminStoreProductVariants) {
            for (VariantPriceDto variantPriceDto : variantPriceDtos) {
                if (adminStoreProductVariant.getId().equals(variantPriceDto.getId())){
                    adminStoreProductVariant.setCostPrice(variantPriceDto.getCostPrice());
                    adminStoreProductVariant.setSupplyPrice(variantPriceDto.getSupplyPrice());
                    adminStoreProductVariant.setUpdateId(Long.parseLong(user.getId()));
                    adminStoreProductVariant.setUpdateTime(LocalDateTime.now());
                }
            }
        }
        this.updateBatchById(adminStoreProductVariants);
        return ApiResponse.ok("保存成功");
    }

    /**
     * 伪删除
     * @param id
     * @param user
     * @return
     */
    @Override
    public ApiResponse delAdminProductVariant(Long id, BaseUser user) {
        AdminStoreProductVariant adminStoreProductVariant = this.getById(id);
        if (adminStoreProductVariant.getQuoteFlag()){
            return ApiResponse.failed("已经报价无法删除");
        }
        adminStoreProductVariant.setDelFlag(String.valueOf(ProductEnum.delFlag.DEL.getCode()));
        this.updateById(adminStoreProductVariant);
        return ApiResponse.ok("保存成功");
    }

    /**
     * 将伪删除产品恢复
     * @param id
     * @param user
     * @return
     */
    @Override
    public ApiResponse recoveryAdminProductVariant(Long id, BaseUser user) {
        AdminStoreProductVariant adminStoreProductVariant = this.getById(id);
        adminStoreProductVariant.setDelFlag(String.valueOf(ProductEnum.delFlag.SHOW.getCode()));
        this.updateById(adminStoreProductVariant);
        return ApiResponse.ok("保存成功");
    }

    /**
     * 修改变体为已报价
     * @param variantId
     * @return
     */
    @Override
    public boolean updateAdminProductVariantQuoteFlag(List<Long> variantId) {
        QueryWrapper<AdminStoreProductVariant> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",variantId);
        List<AdminStoreProductVariant> adminStoreProductVariants = adminStoreProductVariantMapper.selectList(queryWrapper);
        for (AdminStoreProductVariant adminStoreProductVariant : adminStoreProductVariants) {
            adminStoreProductVariant.setQuoteFlag(ProductEnum.quoteFlag.QUOTED.getCode());
        }
        return this.updateBatchById(adminStoreProductVariants);
    }

    /**
     * 获取产品变体+图片信息
     * @param productId
     * @return
     */
    @Override
    public List<AdminStoreProductVariantDto> selectAdminStoreProductVariantDtos(Long productId) {
        return adminStoreProductVariantMapper.selectAdminStoreProductVariantDtos(productId);
    }

    @Override
    public int delAdminProductVariantByIds(List<Long> oldVariantIds) {
        return adminStoreProductVariantMapper.delAdminProductVariantByIds(oldVariantIds);
    }

    @Override
    public List<ToSaiheProductVariantDto> selectToSaiheProductVariantDtos(Long productId) {
        return adminStoreProductVariantMapper.selectToSaiheProductVariantDtos(productId);
    }

    @Override
    public int updateSaiheState(List<SkuResult> skuUpdateResults) {
        return adminStoreProductVariantMapper.updateSaiheState(skuUpdateResults,new Date());
    }

    @Override
    public List<ToSaiheProductVariantDto> selectToSaiheUniqueProductVariantDtos(List<String> onlyCodes) {
        return adminStoreProductVariantMapper.selectToSaiheUniqueProductVariantDtos(onlyCodes);
    }

}
