package com.customizingbox.cloud.manager.service.app.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppOrderItemMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.manager.service.app.AppOrderItemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * panda 订单item表
 *
 * @author Y
 * @date 2022-03-30 13:48:16
 */
@Service
@Slf4j
@AllArgsConstructor
public class AppOrderItemServiceImpl extends ServiceImpl<AppOrderItemMapper, AppOrderItem> implements AppOrderItemService<AppShopifyOrderItem> {

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean updateOrderItemProductRelevancy(Long variantId, Long adminProductId, Long adminVariantId) {
        return this.update(Wrappers.<AppOrderItem>lambdaUpdate().set(AppOrderItem::getAdminProductId, adminProductId)
                .set(AppOrderItem::getAdminVariantId, adminVariantId)
                .eq(AppOrderItem::getVariantId, variantId)
        );
    }
}
