package com.customizingbox.cloud.manager.service.app.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.enums.ProductEnum;
import com.customizingbox.cloud.common.core.constant.enums.TransactionEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderMapper;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.AdminOrderToSaiheSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.OrderToSaihePageVO;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.*;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.UpLoadOrderV2Response;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderToShHistory;
import com.customizingbox.cloud.common.datasource.util.Validation;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductService;
import com.customizingbox.cloud.manager.service.admin.AppPandaOrderToShHistoryService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderItemService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderService;
import com.customizingbox.cloud.manager.util.SaiheApiUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * panda 订单表
 *
 * @author Y
 * @date 2022-04-13 17:03:42
 */
@Service
@Slf4j
public class AppPandaOrderServiceImpl extends ServiceImpl<AppPandaOrderMapper, AppPandaOrder> implements AppPandaOrderService {

    @Autowired
    private SaiheApiUtils saiheApiUtils;

    @Autowired
    private AppPandaOrderToShHistoryService appPandaOrderToShHistoryService;

    @Autowired
    private AppPandaOrderItemService appPandaOrderItemService;

    @Autowired
    private AdminStoreProductService adminStoreProductService;

    /**
     * 获取上传赛盒订单分页
     *
     * @param page
     * @param query
     * @return
     */
    @Override
    public IPage<OrderToSaihePageVO> selectSaiheToOrderPage(Page page, AdminOrderToSaiheSearchVo query) {
        return baseMapper.selectSaiheToOrderPage(page, query);
    }


    @Override
    public void OrdersToSaohe() {
        List<OrderToSaiheVo> orderToSaiheVoList = baseMapper.selectSaiheToOrderList();
        for (OrderToSaiheVo orderToSaiheVo : orderToSaiheVoList) {
            orderToSaihe(orderToSaiheVo);
        }
    }

    /**
     * 定时器上传订单到赛盒
     * 1. 判断订单的客户经理是否有 orderSourceId  没有则无法上传
     * 2. 判断订单包含的所有产品是否都上传赛盒  包含没有上传的需要先上传产品
     */
    private void orderToSaihe(OrderToSaiheVo orderToSaiheVo) {
        Long orderId = Long.parseLong(orderToSaiheVo.getClientOrderCode());
        AppPandaOrderToShHistory appPandaOrderToShHistory = new AppPandaOrderToShHistory();
        appPandaOrderToShHistory.setOrderId(orderId);
        appPandaOrderToShHistory.setCreateTime(LocalDateTime.now());
        if (orderToSaiheVo.getOrderSourceID() == null) {
            String msg = "订单的客户经理没有赛盒orderSourceID";
            appPandaOrderToShHistory.setMsg(msg);
            appPandaOrderToShHistory.setUploadShStatus(OrderEnum.SH_UPLOAD_STATUS.NOT.getCode());
            appPandaOrderToShHistoryService.saveOrUpdateHistory(appPandaOrderToShHistory);
            return;
        }
        // 获取订单中的产品
        List<OrderToSaiheOrderItemVo> itemVoList = appPandaOrderItemService.getOrderItemByOrderId(orderId);

        // 将订单中未上传赛盒的产品上传赛盒
        itemVoList = chechOrderItemBealToSaige(itemVoList, orderToSaiheVo.getClientOrderCode());

        // 判断是否还有未上传赛盒的产品
        ArrayList<String> itemErrorList = new ArrayList<>();
        List<ApiUploadOrderList> apiUploadOrderLists = new ArrayList<>();
        for (OrderToSaiheOrderItemVo orderToSaiheOrderItemVo : itemVoList) {
            if (StringUtils.isBlank(orderToSaiheOrderItemVo.getShSku())) {
                itemErrorList.add(orderToSaiheOrderItemVo.getSellerSKU() + "未上传赛盒");
                continue;
            }
            ApiUploadOrderList apiUploadOrderList = new ApiUploadOrderList();
            BeanUtils.copyProperties(orderToSaiheOrderItemVo, apiUploadOrderList);
            apiUploadOrderLists.add(apiUploadOrderList);
        }
        if (!CollectionUtils.isEmpty(itemErrorList)) {
            String msg = itemErrorList.toString();
            appPandaOrderToShHistory.setMsg(msg);
            appPandaOrderToShHistory.setUploadShStatus(OrderEnum.SH_UPLOAD_STATUS.NOT.getCode());
            Integer productUploadShStatus = itemErrorList.size() == itemVoList.size() ? OrderEnum.ORDER_PRODUCT_SH_UPLOAD_STATUS.NOT.getCode() : OrderEnum.ORDER_PRODUCT_SH_UPLOAD_STATUS.PART.getCode();
            appPandaOrderToShHistory.setProductUploadShStatus(productUploadShStatus);
            appPandaOrderToShHistoryService.saveOrUpdateHistory(appPandaOrderToShHistory);
            return;
        }
        appPandaOrderToShHistory.setProductUploadShStatus(OrderEnum.ORDER_PRODUCT_SH_UPLOAD_STATUS.SUCCESS.getCode());
        // 拼装订单上传赛盒对象
        ApiUploadOrderInfo apiUploadOrderInfo = new ApiUploadOrderInfo();
        BeanUtils.copyProperties(orderToSaiheVo, apiUploadOrderInfo);
        OrderItemList orderItemList = new OrderItemList();
        orderItemList.setApiUploadOrderLists(apiUploadOrderLists);
        apiUploadOrderInfo.setOrderItemList(orderItemList);
        String orderDescription = "vat 费用:" + orderToSaiheVo.getVatAmount() + "  运费：" + orderToSaiheVo.getFreightAmount()
                + " 产品费 ：" + orderToSaiheVo.getProductAmount();
        apiUploadOrderInfo.setOrderDescription(orderDescription);
        apiUploadOrderInfo.setCurrency(TransactionEnum.CURRENCY.USD.getCode());
        apiUploadOrderInfo.setCountry(orderToSaiheVo.getCountryCode());
        if (StringUtils.equalsIgnoreCase(apiUploadOrderInfo.getCountry(), "BR")) {
            if (StringUtils.isNotBlank(orderToSaiheVo.getCompany()) && (Validation.isValidCNPJ(orderToSaiheVo.getCompany()) || Validation.isValidCPF(orderToSaiheVo.getCompany()))) {
                // shopify
                apiUploadOrderInfo.setTaxNumber(orderToSaiheVo.getCompany());
            }
        }
        UpLoadOrderV2Response upLoadOrderV2Response = saiheApiUtils.upLoadOrderV2(apiUploadOrderInfo);
        if (StringUtils.equalsIgnoreCase("OK", upLoadOrderV2Response.getUpLoadOrderV2Result().getStatus())) {
            LambdaUpdateWrapper<AppPandaOrder> updateWrapper = new UpdateWrapper<AppPandaOrder>().lambda().eq(AppPandaOrder::getId, orderId)
                    .set(AppPandaOrder::getUploadShStatus, OrderEnum.SH_UPLOAD_STATUS.SUCCESS.getCode())
                    .set(AppPandaOrder::getUploadShTime, LocalDateTime.now());
            this.update(updateWrapper);
            appPandaOrderToShHistory.setUploadShStatus(OrderEnum.SH_UPLOAD_STATUS.SUCCESS.getCode());
            appPandaOrderToShHistory.setMsg("上传成功");
            appPandaOrderToShHistory.setShOrderCode(upLoadOrderV2Response.getUpLoadOrderV2Result().getOrderCode());
            appPandaOrderToShHistoryService.saveOrUpdateHistory(appPandaOrderToShHistory);
        } else {
            appPandaOrderToShHistory.setUploadShStatus(OrderEnum.SH_UPLOAD_STATUS.FAILED.getCode());
            appPandaOrderToShHistory.setMsg(upLoadOrderV2Response.getUpLoadOrderV2Result().getMsg());
            appPandaOrderToShHistoryService.saveOrUpdateHistory(appPandaOrderToShHistory);
        }
    }

    private List<OrderToSaiheOrderItemVo> chechOrderItemBealToSaige(List<OrderToSaiheOrderItemVo> itemVoList, String orderId) {
        ArrayList<String> onlyCodeList = new ArrayList<>();
        for (OrderToSaiheOrderItemVo orderToSaiheOrderItemVo : itemVoList) {
            if (StringUtils.isBlank(orderToSaiheOrderItemVo.getShSku())) {
                if (StringUtils.equalsIgnoreCase(orderToSaiheOrderItemVo.getCustomMode(), ProductEnum.CUSTOM_MODE.CUSTOMIZATION.getCode().toString())) {
                    onlyCodeList.add(orderToSaiheOrderItemVo.getSellerSKU());
                }
                if (StringUtils.equalsIgnoreCase(orderToSaiheOrderItemVo.getCustomMode(), ProductEnum.CUSTOM_MODE.GENERAL.getCode().toString())) {
                    adminStoreProductService.processProductsToSaihe(orderToSaiheOrderItemVo.getAdminProductId());
                }
            }
        }
        if (!CollectionUtils.isEmpty(onlyCodeList)){
            adminStoreProductService.processUniqueProductsToSaihe(onlyCodeList);
        }
        return appPandaOrderItemService.getOrderItemByOrderId(Long.parseLong(orderId));
    }
}
