package com.customizingbox.cloud.manager.tenant;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.StringValue;

import java.util.ArrayList;
import java.util.List;

/**
 * 租户维护处理器
 */
@Slf4j
public class TenantHandler implements TenantLineHandler {

    private static final String TENANT_ID_COLUMN = "tenant_id";

    /**
     * 多租户的数据表集合
     */
    private static final List<String> IGNORE_TABLES = new ArrayList<String>() {{
        add("sys_dict");
        add("sys_menu");
        add("sys_gen_table");
        add("sys_dict_value");
        add("xxl_job_log");
        add("xxl_job_info");
        add("xxl_job_lock");
        add("xxl_job_user");
        add("xxl_job_group");
        add("xxl_job_logglue");
        add("xxl_job_registry");
        add("xxl_job_log_report");
    }};

    @Override
    public Expression getTenantId() {
        String tenantId = TenantContextHolder.getTenantId();
        if (StrUtil.isBlank(tenantId)) {
            return new NullValue();
        }
        return new StringValue(tenantId);
    }

    @Override
    public String getTenantIdColumn() {
        return TENANT_ID_COLUMN;
    }

    @Override
    public boolean ignoreTable(String tableName) {
        return IGNORE_TABLES.stream().anyMatch((t) -> t.equalsIgnoreCase(tableName));
//        return "xxl_job_log".equalsIgnoreCase(tableName);
    }

}
