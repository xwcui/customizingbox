package com.customizingbox.cloud.manager.service.admin.impl.transport;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.core.util.DateUtils;
import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminLogisticsFreightMapper;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminLogisticsFreightDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminCountryInfo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsFreight;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.AdminCountryInfoService;
import com.customizingbox.cloud.manager.service.admin.AdminLogisticsFreightService;
import com.customizingbox.cloud.manager.service.admin.AdminTransportService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 物流费用 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Service
public class AdminLogisticsFreightServiceImpl extends ServiceImpl<AdminLogisticsFreightMapper, AdminLogisticsFreight> implements AdminLogisticsFreightService {

    @Resource
    private AdminLogisticsFreightMapper adminLogisticsFreightMapper;

    @Autowired
    private AdminCountryInfoService adminCountryInfoService;

    @Autowired
    private AdminTransportService adminTransportService;
    /**
     * 分页
     *
     * @param page
     * @param searchVo
     * @return
     */
    @Override
    public IPage<AdminLogisticsFreightVo> pageAll(Page page, AdminLogisticsFreightSearchVo searchVo) {
        return adminLogisticsFreightMapper.pageAll(page, searchVo);
    }

    /**
     * 保存或修改
     *
     * @param adminLogisticsFreightDto
     * @param user
     * @return
     */
    @Override
    public ApiResponse saveOrUpdateAdminLogisticsFreightDto(AdminLogisticsFreightDto adminLogisticsFreightDto, BaseUser user) {
        AdminLogisticsFreight adminLogisticsFreight = new AdminLogisticsFreight();
        if (adminLogisticsFreightDto.getId() != null) {
            adminLogisticsFreight = this.getById(adminLogisticsFreightDto.getId());
            if (adminLogisticsFreight == null) {
                return ApiResponse.failed("数据库中没有该数据");
            }
        } else {
            BeanUtils.copyProperties(adminLogisticsFreightDto, adminLogisticsFreight);
            adminLogisticsFreight.setCreateTime(LocalDateTime.now());
            adminLogisticsFreight.setCreateId(Long.parseLong(user.getId()));
        }
        adminLogisticsFreight.setUpdateId(Long.parseLong(user.getId()));
        adminLogisticsFreight.setUpdateTime(LocalDateTime.now());
        this.saveOrUpdate(adminLogisticsFreight);
        return ApiResponse.ok();
    }

    /**
     * 运输单元导入
     * 匹配运输方式
     * 国家地区
     *
     * @param result
     * @param user
     * @param enableTime
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse importAdminLogisticsFreight(List<AdminLogisticsFreightVo> result, BaseUser user, LocalDateTime enableTime) {
        List<AdminCountryInfo> countryInfos = adminCountryInfoService.list();
        AdminTransportSearchVo adminTransportSearchVo = new AdminTransportSearchVo();
        List<AdminTransport> transports = adminTransportService.getList(adminTransportSearchVo);
        ArrayList<AdminLogisticsFreight> adminLogisticsFreights = new ArrayList<>();
        // 匹配运输方式id  国家地区
        for (AdminLogisticsFreightVo adminLogisticsFreightVo : result) {
            AdminLogisticsFreight adminLogisticsFreight = new AdminLogisticsFreight();
            BeanUtils.copyProperties(adminLogisticsFreightVo, adminLogisticsFreight);
            adminLogisticsFreight.setEnableTime(enableTime);
            //   国家地区
            for (AdminCountryInfo countryInfo : countryInfos) {
                if (StringUtils.equalsIgnoreCase(countryInfo.getNameCn(), adminLogisticsFreightVo.getEndCountryName())) {
                    adminLogisticsFreight.setEndCountryId(countryInfo.getId());
                    break;
                }
            }
            if (adminLogisticsFreight.getEndCountryId() == null) {
                return ApiResponse.failed("目的地区未匹配到:" + adminLogisticsFreightVo.getEndCountryName());
            }
            //  国家地区
            for (AdminCountryInfo countryInfo : countryInfos) {
                if (StringUtils.equalsIgnoreCase(countryInfo.getNameCn(), adminLogisticsFreightVo.getBeginCountryName())) {
                    adminLogisticsFreight.setBeginCountryId(countryInfo.getId());
                    break;
                }
            }
            if (adminLogisticsFreight.getBeginCountryId() == null) {
                return ApiResponse.failed("始发地区未匹配到:" + adminLogisticsFreightVo.getBeginCountryName());
            }

            // 运输方式
            for (AdminTransport transport : transports) {
                if (StringUtils.equalsIgnoreCase(transport.getName(), adminLogisticsFreightVo.getTransportName())) {
                    adminLogisticsFreight.setTransportId(transport.getId());
                    break;
                }
            }
            if (adminLogisticsFreight.getTransportId() == null) {
                return ApiResponse.failed("运输方式没有匹配到:" + adminLogisticsFreightVo.getTransportName());
            }
            adminLogisticsFreights.add(adminLogisticsFreight);
        }

        // 根据运输方式分组
        Map<Long, List<AdminLogisticsFreight>> groupTransportIdMap = adminLogisticsFreights.stream().collect(Collectors.groupingBy(AdminLogisticsFreight::getTransportId));
        // 检验同一个运输方式下的重量区间是否重叠
        checkIntervalOverlap(groupTransportIdMap);
        List<Long> transportList = groupTransportIdMap.keySet().stream().collect(Collectors.toList());
        adminLogisticsFreightMapper.updateNotEnableTime(transportList, DateUtils.getStrByLocalDateTime(enableTime));
        this.saveBatch(adminLogisticsFreights);
        return ApiResponse.ok();
    }

    /**
     * 遍历已经根据运输方式id分组的map
     * 将map中对应的 value （list） 根据 始发地id和目的地id分组
     * 将分好组的运输单元Map 遍历   key 为 始发地id-目的地id 便利获取到每一个 key 为 始发地id-目的地id  对应的运输单元list
     *
     * @param groupTransportIdMap
     * @return
     */
    private void checkIntervalOverlap(Map<Long, List<AdminLogisticsFreight>> groupTransportIdMap) {
        for (Map.Entry<Long, List<AdminLogisticsFreight>> longListEntry : groupTransportIdMap.entrySet()) {
            Long key = longListEntry.getKey();
            List<AdminLogisticsFreight> value = longListEntry.getValue();
            // 将map中对应的 value （list） 根据 始发地id和目的地id分组
            Map<Long, List<AdminLogisticsFreight>> groupCountryIdMap = value.stream().collect(
                    Collectors.groupingBy(
                            score -> score.getBeginCountryId() + '-' + score.getEndCountryId()
                    )
            );

            // 校验同一个运输方式下运输单元重量区间
            ifverifyIntervalOverlap(groupCountryIdMap);
        }
    }

    /**
     * 对比校验有没有区间重叠
     * 使用双层for循环判断
     * 不同角标下的重量区间去校验
     * 例如   ARR【0】  区间为  1 - 3
     * @param groupCountryIdMap
     */
    private void ifverifyIntervalOverlap(Map<Long, List<AdminLogisticsFreight>> groupCountryIdMap) {
        for (Map.Entry<Long, List<AdminLogisticsFreight>> longListEntry : groupCountryIdMap.entrySet()) {
            List<AdminLogisticsFreight> value = longListEntry.getValue();
            for (int i = 0; i < value.size(); i++) {
                for (int j = 0; j < value.size(); j++) {
                    if (i == j) {
                        continue;
                    }
                    if (
                            (value.get(i).getBeginWeight() >= value.get(j).getBeginWeight() && value.get(i).getBeginWeight() < value.get(j).getEndCountryId())
                               ||
                            (value.get(i).getEndWeight() > value.get(j).getBeginWeight() && value.get(i).getEndCountryId() <= value.get(j).getEndCountryId())
                    ) {
                        throw new ApiException(value.get(i).getTransportName() + "运输单元下重量区间重叠");
                    }
                }
            }
        }
    }
}
