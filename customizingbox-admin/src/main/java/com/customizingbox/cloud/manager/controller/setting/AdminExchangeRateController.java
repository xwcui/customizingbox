package com.customizingbox.cloud.manager.controller.setting;


import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminExchangeRate;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminExchangeRateResponseVo;
import com.customizingbox.cloud.manager.service.admin.AdminExchangeRateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 汇率表 前端控制器
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
@RestController
@RequestMapping("//adminExchangeRate")
@Api(tags = "汇率")
public class AdminExchangeRateController {

    @Autowired
    private AdminExchangeRateService adminExchangeRateService;

    @GetMapping("/listAllRate")
    @ApiOperation("汇率列表查询,不分页")
    public ApiResponse<List<AdminExchangeRateResponseVo>> listAllRate() {
        return ApiResponse.ok(adminExchangeRateService.listAllRate());
    }


    // @PostMapping("addExchangeRate")
    // @ApiOperation(value = "新增汇率")
    public ApiResponse<Boolean> insetData(@RequestBody AdminExchangeRate vo) {
        ApiResponse<Boolean> response = new ApiResponse<>();
        response.setData(adminExchangeRateService.addOne(vo));
        return response;
    }


    // @GetMapping("getOneRate/{dataId}")
    // @ApiOperation(value = " 根据货币代码查询 汇率详情")
    public ApiResponse<AdminExchangeRateResponseVo> info(@PathVariable("currencyCode") String currencyCode) {
        return ApiResponse.ok(adminExchangeRateService.getOneRate(currencyCode));
    }


    @PutMapping("/editExchangeRate/{currencyCode}/{rate}")
    @ApiOperation(value = "编辑 汇率")
    public ApiResponse<Boolean> editExchangeRate(@ApiParam("货币代码") @PathVariable String currencyCode,
                                                 @ApiParam("汇率") @PathVariable String rate) {
        ApiResponse<Boolean> response = new ApiResponse<>();
        // response.setData(adminExchangeRateService.editExchangeRate(currencyCode, rate));
        response.setData(adminExchangeRateService.editExchangeRate(currencyCode, rate));
        return response;
    }
}
