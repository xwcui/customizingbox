package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminLogisticsFreightDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsFreight;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 物流费用 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminLogisticsFreightService extends IService<AdminLogisticsFreight> {


    /**
     * 分页
     * @param page
     * @param searchVo
     * @return
     */
    IPage<AdminLogisticsFreightVo> pageAll(Page page, AdminLogisticsFreightSearchVo searchVo);

    /**
     * 保存或修改
     * @param adminLogisticsAttrDto
     * @param user
     * @return
     */
    ApiResponse saveOrUpdateAdminLogisticsFreightDto(AdminLogisticsFreightDto adminLogisticsAttrDto, BaseUser user);

    /**
     *
     * @param result
     * @param user
     * @param enableTime
     * @return
     */
    ApiResponse importAdminLogisticsFreight(List<AdminLogisticsFreightVo> result, BaseUser user, LocalDateTime enableTime);
}
