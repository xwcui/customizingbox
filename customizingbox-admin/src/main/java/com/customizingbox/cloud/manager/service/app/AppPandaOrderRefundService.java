package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderRefund;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderCheckRefundParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderRefundApplyParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderRefundParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderCheckRefundDetailVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundDetailVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundVO;

/**
 * 订单退款表
 *
 * @author Y
 * @date 2022-04-23 09:55:23
 */
public interface AppPandaOrderRefundService extends IService<AppPandaOrderRefund> {

    /**
     * 查询退款页需要的数据
     */
    AppPandaOrderRefundDetailVO apply(Long orderId);

    /**
     * 提交退款申请
     */
    Boolean submitRefundApply(AppPandaOrderRefundApplyParam appPandaOrderRefundApplyParam);

    /**
     * 退款管理页面
     */
    IPage<AppPandaOrderRefundVO> refundPage(Page page, AppPandaOrderRefundParam appPandaOrderRefundParam);

    /**
     * 退款审核接口
     * @param param
     * @return
     */
    Boolean checkRefundApply(AppPandaOrderCheckRefundParam param);

    /**
     * 退款详情
     * @param refundId
     * @return
     */
    AppPandaOrderRefundDetailVO detail(Long refundId);

    /**
     * 审核退款的详情页面
     * @param refundId
     * @return
     */
    AppPandaOrderCheckRefundDetailVO refundCheck(Long refundId);
}
