package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminExchangeRate;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminExchangeRateResponseVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 汇率表 服务类
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
public interface AdminExchangeRateService extends IService<AdminExchangeRate> {
    /**
    * 新增一条汇率数据
    */
    Boolean addOne(AdminExchangeRate vo);


    /**
    * 单个汇率详情
    */
    AdminExchangeRateResponseVo getOneRate(String currencyCode);

    /**
     * 查询单个汇率值
     * @param currencyCode 货币
     * @return
     */
    BigDecimal getOneRateValue(String currencyCode);

    /**
    * 修改汇率
     * 根据币种代码
    */
    Boolean editExchangeRate(String currencyCode, String rate);

    /**
     * 查询所有汇率
     * @return
     */
    List<AdminExchangeRateResponseVo> listAllRate();

    /**
     * 人民币兑换美元
     * @param money
     * @return
     */
    BigDecimal rmbConvertUsd(BigDecimal money);


}
