package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysDictValueMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysDictValue;
import com.customizingbox.cloud.manager.service.admin.AdminSysDictValueService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 字典项
 */
@Service
@AllArgsConstructor
public class AdminSysDictValueServiceImpl extends ServiceImpl<AdminSysDictValueMapper, AdminSysDictValue> implements AdminSysDictValueService {

    /**
     * 删除字典项
     *
     * @param id 字典项ID
     * @return
     */
    @Override
    public ApiResponse removeDictItem(String id) {
        return ApiResponse.ok(this.removeById(id));
    }

    /**
     * 更新字典项
     *
     * @param item 字典项
     * @return
     */
    @Override
    public ApiResponse updateDictItem(AdminSysDictValue item) {
        return ApiResponse.ok(this.updateById(item));
    }
}
