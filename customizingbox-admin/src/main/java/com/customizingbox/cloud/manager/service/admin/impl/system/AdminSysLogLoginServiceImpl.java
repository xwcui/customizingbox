package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysLogLoginMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLogLogin;
import com.customizingbox.cloud.manager.service.admin.AdminSysLogLoginService;
import org.springframework.stereotype.Service;

/**
 * 登录日志表
 */
@Service
public class AdminSysLogLoginServiceImpl extends ServiceImpl<AdminSysLogLoginMapper, AdminSysLogLogin> implements AdminSysLogLoginService {

}
