package com.customizingbox.cloud.manager.auth.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * 扩展用户信息
 */
public class BaseUser extends User {
	/**
	 * 用户ID
	 */
	@Getter
	private String id;
	/**
	 * 机构ID
	 */
	@Getter
	private String organId;
	/**
	 * 租户ID
	 */
	@Getter
	private String tenantId;

	@Getter
	private String shCode;

	/**
	 * 查看客户数据权限. 0(false) : 可以查看全部客户,  1(true): 只能查看自己关联的客户
	 */
	@Getter
	private Boolean customerDataAuth;


	/**
	 * 查看采购员数据权限. 0(false) : 可以查看全部采购员数据,  1(true): 只能查看自己的采购员数据
	 */
	@Getter
	private Boolean purchaseDataAuth;

	/**
	 * true 管理员; false: 非管理员
	 */
	@Getter
	private Boolean isAdministrator;

	/**
	 * Construct the <code>User</code> with the details required by
	 * {@link DaoAuthenticationProvider}.
	 *
	 * @param id                    用户ID
	 * @param organId               机构ID
	 * @param tenantId              租户ID
	 * @param username              the username presented to the
	 *                              <code>DaoAuthenticationProvider</code>
	 * @param password              the password that should be presented to the
	 *                              <code>DaoAuthenticationProvider</code>
	 * @param enabled               set to <code>true</code> if the user is enabled
	 * @param accountNonExpired     set to <code>true</code> if the account has not expired
	 * @param credentialsNonExpired set to <code>true</code> if the credentials have not
	 *                              expired
	 * @param accountNonLocked      set to <code>true</code> if the account is not locked
	 * @param authorities           the authorities that should be granted to the caller if they
	 *                              presented the correct username and password and the user is enabled. Not null.
	 * @throws IllegalArgumentException if a <code>null</code> value was passed either as
	 *                                  a parameter or as an element in the <code>GrantedAuthority</code> collection
	 */
	public BaseUser(String id, String organId, String tenantId, String username, String password,
					boolean customerDataAuth, boolean purchaseDataAuth, boolean isAdministrator, String shCode,
					boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired,
					boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
		this.organId = organId;
		this.tenantId = tenantId;
		this.customerDataAuth = customerDataAuth;
		this.purchaseDataAuth = purchaseDataAuth;
		this.isAdministrator = isAdministrator;
		this.shCode = shCode;
	}
}
