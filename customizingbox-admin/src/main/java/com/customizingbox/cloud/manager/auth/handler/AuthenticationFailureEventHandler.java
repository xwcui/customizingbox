package com.customizingbox.cloud.manager.auth.handler;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.util.WebUtils;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLogLogin;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import com.customizingbox.cloud.manager.service.admin.AdminSysLogLoginService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.CompletableFuture;

/**
 * 认证失败事件处理器
 */
@Slf4j
@Component
@AllArgsConstructor
public class AuthenticationFailureEventHandler implements ApplicationListener<AbstractAuthenticationFailureEvent> {

    private final AdminSysLogLoginService adminSysLogLoginService;

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent event) {
        AuthenticationException authenticationException = event.getException();
        Authentication authentication = (Authentication) event.getSource();
        this.handle(authenticationException, authentication);
    }

    /**
     * 处理登录失败方法
     * <p>
     *
     * @param authenticationException 登录的 authentication 对象
     * @param authentication          登录的 authenticationException 对象
     */
    public void handle(AuthenticationException authenticationException, Authentication authentication) {
        log.info("<--user:{} login error:{}-->", authentication.getPrincipal(), authenticationException.getLocalizedMessage());
        //登录失败日志统一存在租户1
        TenantContextHolder.setTenantId(CommonConstants.TENANT_ID_1);
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        //异步处理登录日志
        CompletableFuture.runAsync(() -> {
            AdminSysLogLogin adminSysLogLogin = new AdminSysLogLogin();
            adminSysLogLogin.setType(CommonConstants.LOG_TYPE_9);
            adminSysLogLogin.setRemoteAddr(ServletUtil.getClientIP(request));
            adminSysLogLogin.setRequestUri(URLUtil.getPath(request.getRequestURI()));
            adminSysLogLogin.setUserAgent(request.getHeader("user-agent"));
            adminSysLogLogin.setException(StrUtil.format("user:{} login error，msg:{}", authentication.getPrincipal(), authenticationException.getLocalizedMessage()));
            adminSysLogLogin.setAddress(WebUtils.getAddresses(adminSysLogLogin.getRemoteAddr()));
            adminSysLogLoginService.save(adminSysLogLogin);
        });
    }
}
