package com.customizingbox.cloud.manager.service.admin.impl.transport;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminTransportLogisticsMapper;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransportLogistics;
import com.customizingbox.cloud.manager.service.admin.AdminTransportLogisticsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 运输方式和物流属性关联表 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Service
public class AdminTransportLogisticsServiceImpl extends ServiceImpl<AdminTransportLogisticsMapper, AdminTransportLogistics> implements AdminTransportLogisticsService {

    @Resource
    private AdminTransportLogisticsMapper adminTransportLogisticsMapper;
    /**
     * 维护关联关系
     * 1， 查询所有adminTransportId对应的关联关系
     * 2. 处理两个集合 获取需要删除的集合和需要保存的集合
      */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void processingAssociation(List<Long> logisticsIds, Long adminTransportId, Long userId) {
        QueryWrapper<AdminTransportLogistics> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("transport_id",adminTransportId);
        List<AdminTransportLogistics> adminTransportLogisticsAll = this.list(queryWrapper);
        ArrayList<Long> logisticsIdList = new ArrayList<>();
        for (AdminTransportLogistics adminTransportLogistic : adminTransportLogisticsAll) {
            logisticsIdList.add(adminTransportLogistic.getLogisticsId());
        }

        // 处理两个集合
        List<Long> delList = getDelList(logisticsIdList, logisticsIds);
        List<Long> saveList = getSaveList(logisticsIdList, logisticsIds);

        if (!CollectionUtils.isEmpty(delList)){
            QueryWrapper<AdminTransportLogistics> delQueryWapper = new QueryWrapper<>();
            delQueryWapper.eq("transport_id",adminTransportId);
            delQueryWapper.in("logistics_id",delList);
            adminTransportLogisticsMapper.delete(delQueryWapper);
        }


        if (!CollectionUtils.isEmpty(logisticsIds)){
            ArrayList<AdminTransportLogistics>  saveAdminTransportLogistics= new ArrayList<>();
            for (Long logisticsId : saveList) {
                AdminTransportLogistics transportLogistics = new AdminTransportLogistics();
                transportLogistics.setTransportId(adminTransportId);
                transportLogistics.setLogisticsId(logisticsId);
                transportLogistics.setCreateId(userId);
                transportLogistics.setCreateTime(LocalDateTime.now());
                transportLogistics.setUpdateId(userId);
                transportLogistics.setUpdateTime(LocalDateTime.now());
                saveAdminTransportLogistics.add(transportLogistics);
            }
            if (!CollectionUtils.isEmpty(saveAdminTransportLogistics)){
                this.saveBatch(saveAdminTransportLogistics);
            }
        }
    }

    /**
     * 获取需要保存的list
     * @param logisticsIdList
     * @param logisticsIds
     * @return
     */
    private List<Long> getSaveList(ArrayList<Long> logisticsIdList, List<Long> logisticsIds) {
        ArrayList<Long> list1 = new ArrayList<>();
        boolean b = list1.addAll(logisticsIdList);
        ArrayList<Long> list2 = new ArrayList<>();
        boolean b1 = list2.addAll(logisticsIds);
        if (CollectionUtils.isEmpty(list1)){
            return list2;
        }else if (CollectionUtils.isEmpty(list2)){
            return list2;
        }
        boolean b2 = list2.removeAll(list1);
        return list2;
    }

    /**
     * 获取需要删除的list
     * @param logisticsIdList  旧的
     * @param logisticsIds   新的
     * @return
     */
    private List<Long> getDelList(ArrayList<Long> logisticsIdList, List<Long> logisticsIds) {
        ArrayList<Long> list1 = new ArrayList<>();
        boolean b = list1.addAll(logisticsIdList);
        ArrayList<Long> list2 = new ArrayList<>();
        boolean b1 = list2.addAll(logisticsIds);
        if (CollectionUtils.isEmpty(list1)){
            return list1;
        }else if (CollectionUtils.isEmpty(list2)){
            return list1;
        }
        boolean b2 = list1.removeAll(list2);
        return list1;
    }



}
