package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysUserMapper;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminPurchaseUserIdAndNameVo;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysUserInfo;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrgan;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUserRole;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminMenuVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUseDetailVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUserPageVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUserVO;
import com.customizingbox.cloud.manager.service.admin.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
@Service
@AllArgsConstructor
public class AdminSysUserServiceImpl extends ServiceImpl<AdminSysUserMapper, AdminSysUser> implements AdminSysUserService {

    private final AdminSysMenuService adminSysMenuService;
    private final AdminSysRoleService adminSysRoleService;
    private final AdminSysOrganService adminSysOrganService;
    private final AdminSysUserRoleService adminSysUserRoleService;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 保存用户信息
     *
     * @param adminSysUserDto DTO 对象
     * @return ok/fail
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveUser(AdminSysUserDTO adminSysUserDto) {
        AdminSysUser sysUser = new AdminSysUser();
        BeanUtils.copyProperties(adminSysUserDto, sysUser);
        sysUser.setDelFlag(CommonConstants.STATUS_NORMAL);
        sysUser.setPassword(ENCODER.encode(adminSysUserDto.getPassword()));
        baseMapper.insert(sysUser);
        List<AdminSysUserRole> userRoleList = adminSysUserDto.getRoleIds().stream().map(roleId -> {
            AdminSysUserRole userRole = new AdminSysUserRole();
            userRole.setUserId(sysUser.getId());
            userRole.setRoleId(roleId);
            return userRole;
        }).collect(Collectors.toList());
        return adminSysUserRoleService.saveBatch(userRoleList);
    }

    /**
     * 通过查用户的全部信息
     *
     * @param sysUser 用户
     */
    @Override
    public AdminSysUserInfo findUserInfo(AdminSysUser sysUser) {
        AdminSysUserInfo adminSysUserInfo = new AdminSysUserInfo();
        adminSysUserInfo.setSysUser(sysUser);
        //设置角色列表  （ID）
        List<String> roleIds = adminSysRoleService.findRoleIdsByUserId(sysUser.getId());
        adminSysUserInfo.setRoles(ArrayUtil.toArray(roleIds, String.class));

        //设置权限列表（menu.permission）
        Set<String> permissions = new HashSet<>();
        for (String roleId : roleIds) {
            List<AdminMenuVO> menuByRoleId = adminSysMenuService.findMenuByRoleId(roleId);
            List<String> permissionList = adminSysMenuService.findMenuByRoleId(roleId).stream()
                    .filter(menuVo -> StringUtils.isNotEmpty(menuVo.getPermission()))
                    .map(AdminMenuVO::getPermission)
                    .collect(Collectors.toList());
            permissions.addAll(permissionList);
        }
        adminSysUserInfo.setPermissions(ArrayUtil.toArray(permissions, String.class));
        return adminSysUserInfo;
    }

    /**
     * 分页查询用户信息（含有角色信息）
     *
     * @param page    分页对象
     * @param adminSysUserDTO 参数列表
     * @return
     */
    @Override
    public IPage<List<AdminSysUserPageVO>> getUsersWithRolePage(Page page, AdminSysUserDTO adminSysUserDTO) {
        return baseMapper.getUserVosPage(page, adminSysUserDTO);
    }

    /**
     * 通过ID查询用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     */
    @Override
    public AdminSysUserVO selectUserVoById(String id) {
        return baseMapper.getUserVoById(id);
    }

    /**
     * 删除用户
     *
     * @param sysUser 用户
     * @return Boolean
     */
    @Override
    public Boolean deleteUserById(AdminSysUser sysUser) {
        adminSysUserRoleService.deleteByUserId(sysUser.getId());
        this.removeById(sysUser.getId());
        return Boolean.TRUE;
    }

    @Override
    public Boolean updateUserInfo(AdminSysUserDTO adminSysUserDto) {
        AdminSysUserVO adminSysUserVO = baseMapper.getUserVoById(adminSysUserDto.getId());
        AdminSysUser sysUser = new AdminSysUser();
        if (StrUtil.isNotBlank(adminSysUserDto.getPassword()) && StrUtil.isNotBlank(adminSysUserDto.getNewPassword())) {
            if (ENCODER.matches(adminSysUserDto.getPassword(), adminSysUserVO.getPassword())) {
                sysUser.setPassword(ENCODER.encode(adminSysUserDto.getNewPassword()));
            } else {
                log.warn("原密码错误，修改密码失败:{}", adminSysUserDto.getUsername());
                throw new RuntimeException("原密码错误，修改失败");
            }
        }
        sysUser.setId(adminSysUserVO.getId());
        sysUser.setAvatar(adminSysUserDto.getAvatar());
        sysUser.setEmail(adminSysUserDto.getEmail());
        baseMapper.updateById(sysUser);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUser(AdminSysUserDTO adminSysUserDto) {
        AdminSysUser sysUser = new AdminSysUser();
        BeanUtils.copyProperties(adminSysUserDto, sysUser);
        sysUser.setUpdateTime(LocalDateTime.now());
        sysUser.setPassword(null);
        this.updateById(sysUser);

        adminSysUserRoleService.remove(Wrappers.<AdminSysUserRole>update().lambda().eq(AdminSysUserRole::getUserId, adminSysUserDto.getId()));
        if (!CollectionUtils.isEmpty(adminSysUserDto.getRoleIds())) {
            adminSysUserDto.getRoleIds().forEach(roleId -> {
                AdminSysUserRole userRole = new AdminSysUserRole();
                userRole.setUserId(sysUser.getId());
                userRole.setRoleId(roleId);
                userRole.insert();
            });
        }

        return Boolean.TRUE;
    }

    /**
     * 查询上级机构的用户信息
     *
     * @param username 用户名
     * @return R
     */
    @Override
    public List<AdminSysUser> listAncestorUsers(String username) {
        AdminSysUser sysUser = this.getOne(Wrappers.<AdminSysUser>query().lambda()
                .eq(AdminSysUser::getUsername, username));

        AdminSysOrgan adminSysOrgan = adminSysOrganService.getById(sysUser.getOrganId());
        if (adminSysOrgan == null) {
            return null;
        }

        String parentId = adminSysOrgan.getParentId();
        return this.list(Wrappers.<AdminSysUser>query().lambda()
                .eq(AdminSysUser::getOrganId, parentId));
    }

    @Override
    public AdminSysUser getByNoTenant(AdminSysUser sysUser) {
        return baseMapper.getByNoTenant(sysUser);
    }

    @Override
    public List<AdminPurchaseUserIdAndNameVo> getAdminPurchaseUser() {
        QueryWrapper<AdminSysUser> wrapper = new QueryWrapper<>();
        List<AdminSysUser> users = this.list(wrapper.eq("type", 2));

        List<AdminPurchaseUserIdAndNameVo> purchaseUsers = new ArrayList<>();
        for (AdminSysUser user : users) {
            AdminPurchaseUserIdAndNameVo vo = new AdminPurchaseUserIdAndNameVo();
            vo.setAdminPurchaseUserId(Long.valueOf(user.getId()));
            vo.setAdminPurchaseUserName(user.getUsername());
            purchaseUsers.add(vo);
        }
        return purchaseUsers;
    }

    @Override
    public AdminSysUseDetailVO getUsersById(String id) {
        AdminSysUseDetailVO userDetail = baseMapper.getUsersById(id);
        List<String> roleIds = adminSysRoleService.findRoleIdsByUserId(userDetail.getId());
        userDetail.setRoleIds(roleIds);
        return userDetail;
    }

}
