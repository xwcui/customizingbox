package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrgan;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrganRelation;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface AdminSysOrganRelationService extends IService<AdminSysOrganRelation> {

    /**
     * 新建机构关系
     *
     * @param adminSysOrgan 机构
     */
    void insertOrganRelation(AdminSysOrgan adminSysOrgan);

    /**
     * 通过ID删除机构关系
     */
    void deleteAllOrganRelation(String id);

    /**
     * 更新机构关系
     */
    void updateOrganRelation(AdminSysOrganRelation relation);
}
