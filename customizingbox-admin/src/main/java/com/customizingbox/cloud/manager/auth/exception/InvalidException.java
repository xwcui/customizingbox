package com.customizingbox.cloud.manager.auth.exception;

import com.customizingbox.cloud.manager.auth.component.BaseAuth2ExceptionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 自定义 OAuth2Exception
 */
@JsonSerialize(using = BaseAuth2ExceptionSerializer.class)
public class InvalidException extends BaseAuth2Exception {

    public InvalidException(String msg, Throwable t) {
        super(msg);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "invalid_exception";
    }

    @Override
    public int getHttpErrorCode() {
        return 426;
    }

}
