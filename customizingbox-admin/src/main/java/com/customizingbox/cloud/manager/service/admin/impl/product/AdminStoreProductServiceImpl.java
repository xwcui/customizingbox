package com.customizingbox.cloud.manager.service.admin.impl.product;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ProductEnum;
import com.customizingbox.cloud.common.core.constant.enums.TransactionEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderItemMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.*;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.AdminProductPageVariantListVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductInfoVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.*;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.ApiUploadProductsResponse;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.Result;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.SkuResult;
import com.customizingbox.cloud.common.datasource.util.IdGenerate;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.*;
import com.customizingbox.cloud.manager.util.SaiheApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-31 09:34:35
 */
@Service
public class AdminStoreProductServiceImpl extends ServiceImpl<AdminStoreProductMapper, AdminStoreProduct> implements AdminStoreProductService {

    @Autowired
    private AdminStoreProductAttrService adminStoreProductAttrService;

    @Autowired
    private AdminStoreProductImgDepotService adminStoreProductImgDepotService;

    @Autowired
    private AdminStoreProductVariantService adminStoreProductVariantService;

    @Autowired
    private SpuHistoryService spuHistoryService;

    @Resource
    private AdminStoreProductMapper adminStoreProductMapper;

    @Autowired
    private SaiheApiUtils saiheApiUtils;

    @Autowired
    private AdminExchangeRateService adminExchangeRateService;

    @Resource
    private AppPandaOrderItemMapper appPandaOrderItemMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse saveProduct(ProductSaveOrUpdateDto productSaveOrUpdateDto, BaseUser user) {
        AdminStoreProductDto adminStoreProductDto = productSaveOrUpdateDto.getAdminStoreProductDto();

        // 保存 product
        AdminStoreProduct adminStoreProduct = new AdminStoreProduct();
        BeanUtils.copyProperties(adminStoreProductDto, adminStoreProduct);
        adminStoreProduct.setQuoteFlag(ProductEnum.quoteFlag.NO_QUOTATION.getCode());
        adminStoreProduct.setDelFlag(ProductEnum.delFlag.SHOW.getCode());
        adminStoreProduct.setUploadShStatus(ProductEnum.uploadShStatus.NO_UPLOAD.getCode());
        adminStoreProduct.setCreateId(Long.parseLong(user.getId()));
        adminStoreProduct.setCreateTime(LocalDateTime.now());
        adminStoreProduct.setUpdateId(Long.parseLong(user.getId()));
        adminStoreProduct.setUpdateTime(LocalDateTime.now());
        adminStoreProduct.setAuthor(user.getUsername());
        this.save(adminStoreProduct);

        // 保存 attr
        ArrayList<AdminStoreProductAttr> adminStoreProductAttrs = new ArrayList<>();
        List<AdminStoreProductAttrDto> adminStoreProductAttrDtos = productSaveOrUpdateDto.getAdminStoreProductAttrDtos();
        for (int i = 0; i < adminStoreProductAttrDtos.size(); i++) {
            AdminStoreProductAttr adminStoreProductAttr = new AdminStoreProductAttr();
            BeanUtils.copyProperties(adminStoreProductAttrDtos.get(i), adminStoreProductAttr);
            adminStoreProductAttr.setAttrValues(JSON.toJSONString(adminStoreProductAttrDtos.get(i).getAttrValues()));
            adminStoreProductAttr.setProductId(adminStoreProduct.getId());
            adminStoreProductAttr.setSort(i);
            adminStoreProductAttrs.add(adminStoreProductAttr);
        }
        adminStoreProductAttrService.saveBatch(adminStoreProductAttrs);

        // 保存图片
        ArrayList<AdminStoreProductImgDepot> adminStoreProductImgDepots = new ArrayList<>();
        List<AdminStoreProductImgDepot> imgDepots = productSaveOrUpdateDto.getAdminStoreProductImgDepots();
        HashMap<String, Long> imgMap = new HashMap<>();
        for (AdminStoreProductImgDepot imgDepot : imgDepots) {
            imgDepot.setProductId(adminStoreProduct.getId());
            imgDepot.setId(IdGenerate.nextId());
            adminStoreProductImgDepots.add(imgDepot);
            imgMap.put(imgDepot.getSrc(), imgDepot.getId());
        }
        adminStoreProductImgDepotService.saveBatch(adminStoreProductImgDepots);
        // 保存  variant
        List<AdminStoreProductVariantDto> adminStoreProductVariantDtos = productSaveOrUpdateDto.getAdminStoreProductVariantDtos();
        ArrayList<AdminStoreProductVariant> adminStoreProductVariants = new ArrayList<>();
        for (AdminStoreProductVariantDto adminStoreProductVariantDto : adminStoreProductVariantDtos) {
            AdminStoreProductVariant adminStoreProductVariant = new AdminStoreProductVariant();
            BeanUtils.copyProperties(adminStoreProductVariantDto, adminStoreProductVariant);
            adminStoreProductVariant.setImageId(imgMap.get(adminStoreProductVariantDto.getImageUrl()));
            adminStoreProductVariant.setProductId(adminStoreProduct.getId());
            adminStoreProductVariant.setUploadShStatus(ProductEnum.uploadShStatus.NO_UPLOAD.getCode());
            adminStoreProductVariant.setCreateId(Long.parseLong(user.getId()));
            adminStoreProductVariant.setCreateTime(LocalDateTime.now());
            adminStoreProductVariant.setUpdateId(Long.parseLong(user.getId()));
            adminStoreProductVariant.setUpdateTime(LocalDateTime.now());
            adminStoreProductVariants.add(adminStoreProductVariant);
        }
        adminStoreProductVariantService.saveBatch(adminStoreProductVariants);
        return ApiResponse.ok("保存成功");
    }

    /**
     * 获取=spu
     *
     * @return
     */
    @Override
    public String getSpu() {
        return spuHistoryService.getSpuIndex();
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateAdminProductQuoteFlag(Long adminProductId, List<Long> variantId) {
        AdminStoreProduct adminStoreProduct = this.getById(adminProductId);
        adminStoreProduct.setQuoteFlag(ProductEnum.quoteFlag.QUOTED.getCode());
        adminStoreProductVariantService.updateAdminProductVariantQuoteFlag(variantId);
        return this.updateById(adminStoreProduct);
    }

    /**
     * 1. 产品信息直接修改
     * 2. attr规格信息有id的修改
     * 无id的新增
     * 3. variant + image
     * variant 有id   修改
     * 图片 ： 有id  不动
     * 无id  删除新增
     * variant  无id   新增
     * 图片   直接新增
     *
     * @param productSaveOrUpdateDto
     * @param user
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse updateProduct(ProductSaveOrUpdateDto productSaveOrUpdateDto, BaseUser user) {
        AdminStoreProductDto adminStoreProductDto = productSaveOrUpdateDto.getAdminStoreProductDto();
        // 保存 product
        AdminStoreProduct adminStoreProduct = new AdminStoreProduct();
        BeanUtils.copyProperties(adminStoreProductDto, adminStoreProduct);
        adminStoreProduct.setUpdateId(Long.parseLong(user.getId()));
        adminStoreProduct.setUpdateTime(LocalDateTime.now());
        adminStoreProduct.setAuthor(user.getUsername());
        this.updateById(adminStoreProduct);
        // 保存修改 attr
        ArrayList<AdminStoreProductAttr> adminStoreProductAttrSaves = new ArrayList<>();
        ArrayList<AdminStoreProductAttr> adminStoreProductAttrUpdates = new ArrayList<>();
        List<AdminStoreProductAttrDto> adminStoreProductAttrDtos = productSaveOrUpdateDto.getAdminStoreProductAttrDtos();

        for (int i = 0; i < adminStoreProductAttrDtos.size(); i++) {
            AdminStoreProductAttr adminStoreProductAttr = new AdminStoreProductAttr();
            BeanUtils.copyProperties(adminStoreProductAttrDtos.get(i), adminStoreProductAttr);
            adminStoreProductAttr.setAttrValues(JSON.toJSONString(adminStoreProductAttrDtos.get(i).getAttrValues()));
            adminStoreProductAttr.setProductId(adminStoreProduct.getId());
            adminStoreProductAttr.setSort(i);
            if (adminStoreProductAttr.getId() == null) {
                adminStoreProductAttrSaves.add(adminStoreProductAttr);
            } else {
                adminStoreProductAttrUpdates.add(adminStoreProductAttr);
            }
        }
        if (!CollectionUtils.isEmpty(adminStoreProductAttrSaves)) {
            adminStoreProductAttrService.saveBatch(adminStoreProductAttrSaves);
        }
        if (!CollectionUtils.isEmpty(adminStoreProductAttrUpdates)) {
            adminStoreProductAttrService.updateBatchById(adminStoreProductAttrUpdates);
        }
        // 保存修改 image
        //前端传来的图片集合
        List<AdminStoreProductImgDepot> adminStoreProductImgDepots = productSaveOrUpdateDto.getAdminStoreProductImgDepots();
        //该产品之前的图片
        List<AdminStoreProductImgDepot> imgDepotDateList = adminStoreProductImgDepotService.getListByProductId(adminStoreProduct.getId());
        List<Long> newImgIds = adminStoreProductImgDepots.stream().map(AdminStoreProductImgDepot::getId).collect(Collectors.toList());
        List<Long> oldImgIds = imgDepotDateList.stream().map(AdminStoreProductImgDepot::getId).collect(Collectors.toList());
        oldImgIds.removeAll(newImgIds);
        if (!CollectionUtils.isEmpty(oldImgIds)) {
            adminStoreProductImgDepotService.delImageByIds(oldImgIds);
        }
        ArrayList<AdminStoreProductImgDepot> adminStoreProductImgDepotsaves = new ArrayList<>();
        // 该map为了方便根据图片url获取id
        HashMap<String, Long> imgMap = new HashMap<>();
        for (AdminStoreProductImgDepot adminStoreProductImgDepot : adminStoreProductImgDepots) {
            if (adminStoreProductImgDepot.getId() == null) {
                adminStoreProductImgDepot.setId(IdGenerate.nextId());
                adminStoreProductImgDepot.setProductId(adminStoreProduct.getId());
                adminStoreProductImgDepotsaves.add(adminStoreProductImgDepot);
            }
            imgMap.put(adminStoreProductImgDepot.getSrc(), adminStoreProductImgDepot.getId());
        }
        if (!CollectionUtils.isEmpty(adminStoreProductImgDepotsaves)) {
            adminStoreProductImgDepotService.saveBatch(adminStoreProductImgDepotsaves);
        }

        // 修改 新增 删除变体
        // 1。先删
        List<AdminStoreProductVariantDto> adminStoreProductVariantDtos = adminStoreProductVariantService.selectAdminStoreProductVariantDtos(adminStoreProduct.getId());
        List<Long> oldVariantIds = adminStoreProductVariantDtos.stream().map(AdminStoreProductVariantDto::getId).collect(Collectors.toList());
        List<Long> newVariantIds = productSaveOrUpdateDto.getAdminStoreProductVariantDtos().stream().map(AdminStoreProductVariantDto::getId).collect(Collectors.toList());
        boolean b = oldVariantIds.removeAll(newVariantIds);
        if (!CollectionUtils.isEmpty(oldVariantIds)){
            adminStoreProductVariantService.delAdminProductVariantByIds(oldVariantIds);
        }
        // 2. 修改新增 图片id统一从imgMap中获取
        ArrayList<AdminStoreProductVariant> adminStoreProductVariantsaves = new ArrayList<>();
        ArrayList<AdminStoreProductVariant> adminStoreProductVariantupdates = new ArrayList<>();
        for (AdminStoreProductVariantDto adminStoreProductVariantDto : productSaveOrUpdateDto.getAdminStoreProductVariantDtos()) {
            AdminStoreProductVariant adminStoreProductVariant = new AdminStoreProductVariant();
            BeanUtils.copyProperties(adminStoreProductVariantDto, adminStoreProductVariant);
            // 新增的变体
            if (adminStoreProductVariant.getId() == null) {
                adminStoreProductVariant.setImageId(imgMap.get(adminStoreProductVariantDto.getImageUrl()));
                adminStoreProductVariant.setProductId(adminStoreProduct.getId());
                adminStoreProductVariant.setUploadShStatus(ProductEnum.uploadShStatus.NO_UPLOAD.getCode());
                adminStoreProductVariant.setCreateId(Long.parseLong(user.getId()));
                adminStoreProductVariant.setCreateTime(LocalDateTime.now());
                adminStoreProductVariant.setUpdateId(Long.parseLong(user.getId()));
                adminStoreProductVariant.setUpdateTime(LocalDateTime.now());
                adminStoreProductVariantsaves.add(adminStoreProductVariant);
            } else {
                // 已经存在的变体
                if (adminStoreProductVariant.getImageId() == null) {
                    adminStoreProductVariant.setImageId(imgMap.get(adminStoreProductVariantDto.getImageUrl()));
                }
                adminStoreProductVariantupdates.add(adminStoreProductVariant);
            }
        }
        if (!CollectionUtils.isEmpty(adminStoreProductVariantupdates)) {
            adminStoreProductVariantService.updateBatchById(adminStoreProductVariantupdates);
        }
        if (!CollectionUtils.isEmpty(adminStoreProductVariantsaves)) {
            adminStoreProductVariantService.saveBatch(adminStoreProductVariantsaves);
        }

        return ApiResponse.ok("修改成功");
    }

    /**
     * 产品详情
     *
     * @param productId
     * @return
     */
    @Override
    public ApiResponse<ProductInfoVo> adminProductInfo(Long productId) {
        ProductInfoVo productInfoVo = new ProductInfoVo();
        AdminStoreProduct adminStoreProduct = this.getById(productId);
        List<AdminStoreProductAttr> attrList = adminStoreProductAttrService.selectAdminStoreProductAttrList(productId);
        productInfoVo.setAdminStoreProductAttrs(attrList);
        productInfoVo.setAdminStoreProduct(adminStoreProduct);
        productInfoVo.setAdminStoreProductVariantDtos(adminStoreProductVariantService.selectAdminStoreProductVariantDtos(productId));
        productInfoVo.setAdminStoreProductImgDepots(adminStoreProductImgDepotService.getListByProductId(productId));
        return ApiResponse.ok(productInfoVo);
    }

    /**
     * 产品列表
     *
     * @param productSearchVo
     * @param user
     * @param page
     * @return
     */
    @Override
    public IPage<ProductPageVo> adminProductPage(ProductSearchVo productSearchVo, BaseUser user, Page page) {
        return adminStoreProductMapper.adminProductPage(productSearchVo, page);
    }

    /**
     * 产品列表下变体列表
     *
     * @param productId
     * @return
     */
    @Override
    public ApiResponse<AdminProductPageVariantListVo> adminProductVariantList(Long productId) {
        AdminProductPageVariantListVo adminProductPageVariantListVo = new AdminProductPageVariantListVo();
        adminProductPageVariantListVo.setAdminStoreProductVariantDtos(adminStoreProductVariantService.selectAdminStoreProductVariantDtos(productId));
        adminProductPageVariantListVo.setAdminStoreProductAttrs(adminStoreProductAttrService.selectAdminStoreProductAttrList(productId));
        return ApiResponse.ok(adminProductPageVariantListVo);

    }

    @Override
    public ApiResponse delAdminProduct(Long id, BaseUser user) {
        AdminStoreProduct adminStoreProduct = this.getById(id);
        if (adminStoreProduct.getQuoteFlag()) {
            return ApiResponse.failed("已报价");
        }
        adminStoreProduct.setDelFlag(ProductEnum.delFlag.DEL.getCode());
        this.updateById(adminStoreProduct);
        return ApiResponse.ok();
    }

    @Override
    public ApiResponse recoveryAdminProduct(Long id, BaseUser user) {
        AdminStoreProduct adminStoreProduct = this.getById(id);
        adminStoreProduct.setDelFlag(ProductEnum.delFlag.SHOW.getCode());
        this.updateById(adminStoreProduct);
        return ApiResponse.ok();
    }

    /**
     * 唯一码产品上传赛盒
     * @param onlyCodes
     * @return
     */
    @Override
    public ApiResponse processUniqueProductsToSaihe(List<String> onlyCodes) {
        List<ToSaiheProductVariantDto> list = adminStoreProductVariantService.selectToSaiheUniqueProductVariantDtos(onlyCodes);
        if (CollectionUtils.isEmpty(list)){
            return  ApiResponse.failed(onlyCodes.toString() + "唯一码没有对应需要上传的产品");
        }
        List<String> collect = list.stream().map(ToSaiheProductVariantDto::getOnlyCode).collect(Collectors.toList());
        onlyCodes.removeAll(collect);
        if (!CollectionUtils.isEmpty(onlyCodes)){
            return  ApiResponse.failed(onlyCodes.toString() + "唯一码没有对应需要上传的产品");
        }
        List<ApiImportProductInfo> ImportProductList = new ArrayList<>();
        for (ToSaiheProductVariantDto toSaiheProductVariantDto : list) {
            List<AdminStoreProductAttr> adminStoreProductAttrs = adminStoreProductAttrService.selectAdminStoreProductAttrList(toSaiheProductVariantDto.getId());
            ApiImportProductInfo apiImportProductInfo =
                    convertApiImportProductInfo(toSaiheProductVariantDto, adminStoreProductAttrs);
            ImportProductList.add(apiImportProductInfo);
        }
        ApiUploadProductsResponse apiUploadProductsResponse = saiheApiUtils.
                processUpdateProduct(ImportProductList);

        if (apiUploadProductsResponse.getProcessUpdateProductResult().getStatus().equals("OK")) {
            Result result = apiUploadProductsResponse.getProcessUpdateProductResult().getResult();
            if (result != null && result.getApiUploadResult() != null
                    && !result.getApiUploadResult().getSuccess()) {
//                  return result.getApiUploadResult().getOperateMessage();
                //赛盒用户不匹配
                return ApiResponse.failed(JSON.toJSONString(onlyCodes) + result.getApiUploadResult().getOperateMessage());
                //return false;
            } else {
                //更新状态
                List<SkuResult> skuAddResults = apiUploadProductsResponse.getProcessUpdateProductResult().getSkuAddList().getSkuResults();
                List<SkuResult> skuUpdateResults = apiUploadProductsResponse.getProcessUpdateProductResult().getSkuUpdateList().getSkuResults();
                if (CollectionUtils.isEmpty(skuAddResults)){
                    skuAddResults = new ArrayList<>();
                }
                if (CollectionUtils.isEmpty(skuUpdateResults)){
                    skuUpdateResults = new ArrayList<>();
                }
                skuAddResults.addAll(skuUpdateResults);
                // 修改订单item 上传赛盒状态和赛盒sku
                appPandaOrderItemMapper.updateSaiheSatusAndSaiheSku(skuAddResults,LocalDateTime.now());
                return ApiResponse.ok("上传成功");
            }
        }
        return ApiResponse.failed("上传失败");
    }


    /**
     * 非定制产品上传赛盒
     * @param productId
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String processProductsToSaihe(Long productId) {
        List<ToSaiheProductVariantDto> list = adminStoreProductVariantService.selectToSaiheProductVariantDtos(productId);
        if (CollectionUtils.isEmpty(list)){
            return productId +"该产品下无需要上传赛盒的变体";
        }
        List<AdminStoreProductAttr> adminStoreProductAttrs = adminStoreProductAttrService.selectAdminStoreProductAttrList(productId);
        List<ApiImportProductInfo> ImportProductList = new ArrayList<>();
        for (ToSaiheProductVariantDto toSaiheProductVariantDto : list) {
            ApiImportProductInfo apiImportProductInfo =
                    convertApiImportProductInfo(toSaiheProductVariantDto, adminStoreProductAttrs);
            ImportProductList.add(apiImportProductInfo);
        }
        ApiUploadProductsResponse apiUploadProductsResponse = saiheApiUtils.
                processUpdateProduct(ImportProductList);

        if (apiUploadProductsResponse.getProcessUpdateProductResult().getStatus().equals("OK")) {
            Result result = apiUploadProductsResponse.getProcessUpdateProductResult().getResult();
            if (result != null && result.getApiUploadResult() != null
                    && !result.getApiUploadResult().getSuccess()) {
               return   productId + result.getApiUploadResult().getOperateMessage();
            } else {
                //更新状态
                List<SkuResult> skuAddResults = apiUploadProductsResponse.getProcessUpdateProductResult().getSkuAddList().getSkuResults();
                List<SkuResult> skuUpdateResults = apiUploadProductsResponse.getProcessUpdateProductResult().getSkuUpdateList().getSkuResults();
                if (CollectionUtils.isEmpty(skuAddResults)){
                    skuAddResults = new ArrayList<>();
                }
                if (CollectionUtils.isEmpty(skuUpdateResults)){
                    skuUpdateResults = new ArrayList<>();
                }
                skuUpdateResults.addAll(skuAddResults);
                if (!CollectionUtils.isEmpty(skuUpdateResults)){
                    adminStoreProductVariantService.updateSaiheState(skuUpdateResults);
                }
                AdminStoreProduct adminStoreProduct = this.getById(productId);
                adminStoreProduct.setUploadShStatus(ProductEnum.uploadShStatus.UPLOAD.getCode());
                return productId + "上传赛盒成功";
            }
        }
        return null;
    }

    /**
     * 组装上产赛盒实体
     *customMode 1 定制  2 非定制
     * 个别参数定制非定制上传的值不同
     * @param toSaiheProductVariantDto
     * @param adminStoreProductAttrs
     * @return
     */
    private ApiImportProductInfo convertApiImportProductInfo(ToSaiheProductVariantDto toSaiheProductVariantDto, List<AdminStoreProductAttr> adminStoreProductAttrs) {

        Integer customMode = toSaiheProductVariantDto.getCustomMode();

        ApiImportProductInfo apiImportProductInfo = new ApiImportProductInfo();

        apiImportProductInfo.setClientSKU(customMode==1 ? toSaiheProductVariantDto.getOnlyCode():toSaiheProductVariantDto.getSku());
        if (StringUtils.isBlank(apiImportProductInfo.getClientSKU())){
            apiImportProductInfo.setClientSKU(toSaiheProductVariantDto.getSku());
        }
        String attrValues = toSaiheProductVariantDto.getAttrValues();
        JSONArray array = JSONArray.parseArray(attrValues);
        for (int i = 0; i < adminStoreProductAttrs.size(); i++) {
            if (StringUtils.equalsIgnoreCase("color", adminStoreProductAttrs.get(i).getAttrName())) {
                apiImportProductInfo.setProductColor(String.valueOf(array.get(i)));
            }
            if (StringUtils.equalsIgnoreCase("size", adminStoreProductAttrs.get(i).getAttrName())) {
                apiImportProductInfo.setProductSize(String.valueOf(array.get(i)));
            }
        }
        //母体ID
        apiImportProductInfo.setProductGroupSKU(toSaiheProductVariantDto.getSpu());
        //产品来源 系统采集
        apiImportProductInfo.setComeSource(0);
        //产品英文类别名
        apiImportProductInfo.setProductClassNameEN("星群");
        //定制包装 潘达物流包材
        //商品类别是“定制包装”的产品，传到赛盒产品类别字段的"潘达物流包材"，普通商品传“潘达”，产品编辑页面，新增商品类别选项，默认普通商品
        apiImportProductInfo.setProductClassNameCN("星群");
        //产品中文名
        apiImportProductInfo.setProductName(toSaiheProductVariantDto.getTitleEn());
        //产品中文名
        apiImportProductInfo.setProductNameCN(toSaiheProductVariantDto.getTitleEn());

        apiImportProductInfo.setLength(toSaiheProductVariantDto.getLength());
        apiImportProductInfo.setWidth(toSaiheProductVariantDto.getWidth());
        apiImportProductInfo.setHeight(toSaiheProductVariantDto.getHeight());
        apiImportProductInfo.setPackLength(toSaiheProductVariantDto.getLength());
        apiImportProductInfo.setPackWidth(toSaiheProductVariantDto.getWidth());
        apiImportProductInfo.setPackHeight(toSaiheProductVariantDto.getHeight());
        apiImportProductInfo.setWithBattery(Integer.parseInt(String.valueOf(toSaiheProductVariantDto.getLogisticsAttrId())));

        /**
         * 产品报关信息
         */
        ApiImportProductDeclaration apiImportProductDeclaration = new ApiImportProductDeclaration();
        //产品报关英文名
        apiImportProductDeclaration.setDeclarationName(toSaiheProductVariantDto.getNameEn());
        //产品报关中文名
        apiImportProductDeclaration.setDeclarationNameCN(toSaiheProductVariantDto.getNameCn());
        //报关价
        apiImportProductDeclaration.setDeclarationPriceRate(customMode==1?toSaiheProductVariantDto.getQuotePrice():adminExchangeRateService.rmbConvertUsd(toSaiheProductVariantDto.getCostPrice()));
        //报关单位 USD
        apiImportProductDeclaration.setDeclarationUnit(TransactionEnum.CURRENCY.USD.getCode());
        apiImportProductInfo.setProductDeclaration(apiImportProductDeclaration);
        //供货价(人民币)
        apiImportProductInfo.setLastSupplierPrice(toSaiheProductVariantDto.getSupplyPrice());
        //单位
        apiImportProductInfo.setUnitName("Piece");
        //净重(克)
        apiImportProductInfo.setNetWeight(toSaiheProductVariantDto.getWeight());
        //毛重(克)
        apiImportProductInfo.setGrossWeight(toSaiheProductVariantDto.getWeight());
        //单个产品包裹重量(克)
        apiImportProductInfo.setPackWeight(toSaiheProductVariantDto.getWeight());
        //产品状态  正常 = 0,仅批量 = 1,停产 = 2,锁定 = 3,暂时缺货 = 4,清库 = 5
        apiImportProductInfo.setProductState("0");
        //采购人员
        apiImportProductInfo.setBuyerName(toSaiheProductVariantDto.getShCode());
        // 采购备注
        apiImportProductInfo.setReceiptRemark(customMode == 1 ? "定制产品" : "非定制产品");

        ApiImportProductAdmin apiImportProductAdmin = new ApiImportProductAdmin();
        //负责人员
        apiImportProductAdmin.setAssignDevelopAdminName(toSaiheProductVariantDto.getShCode());
        //编辑人员
        apiImportProductAdmin.setEditAdminName(toSaiheProductVariantDto.getShCode());
        //图片处理人员
        apiImportProductAdmin.setImageAdminName(toSaiheProductVariantDto.getShCode());
        //开发人员
        apiImportProductAdmin.setDevelopAdminName(toSaiheProductVariantDto.getShCode());
        //质检备注
        apiImportProductAdmin.setToProcurementCheckMemo("");
        //发货打包备注
        apiImportProductAdmin.setToDeliveryPackNoteMemo("");
        apiImportProductInfo.setProductAdmin(apiImportProductAdmin);


        //赛盒仓库 默认仓库
        apiImportProductInfo.setDefaultLocalWarehouse(230);
        //产品供应商
        ApiImportProductSupplier ProductSuppiler = new ApiImportProductSupplier();
        //供应商名称
        ProductSuppiler.setSupplierName(toSaiheProductVariantDto.getSupplierName());

        //(1. 市场采购,2. 网络采购,3. 工厂采购)
        ProductSuppiler.setSupplierType(customMode == 1 ? 3:2);
        apiImportProductInfo.setProductSuppiler(ProductSuppiler);

        //产品供应商报价
        ApiImportProductSupplierPrice ProductSupplierPrice = new ApiImportProductSupplierPrice();
        //供应商产品编码  // 货号
//        ProductSupplierPrice.setSupplierSKU("");
        //网络采购链接
//        ProductSupplierPrice.setWebProductUrl("");
        apiImportProductInfo.setProductSupplierPrice(ProductSupplierPrice);

        //产品图片(最多9张)
        List<ApiImportProductImage> imgList = new ArrayList<>();
        ApiImportProductImage apiImportProductImage = new ApiImportProductImage();
        apiImportProductImage.setIsCover(1);
        //产品主图
        apiImportProductImage.setOriginalImageUrl(toSaiheProductVariantDto.getSrc());
        apiImportProductImage.setSortBy(1);
        imgList.add(apiImportProductImage);
        ImagesList imagesList = new ImagesList();
        imagesList.setImagesList(imgList);
        apiImportProductInfo.setImagesList(imagesList);
        return apiImportProductInfo;
    }


}
