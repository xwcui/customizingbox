package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUserRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 */
public interface AdminSysUserRoleService extends IService<AdminSysUserRole> {

    /**
     * 根据用户Id删除该用户的角色关系
     */
    Boolean deleteByUserId(String userId);

    /**
     * 初始化用户权限
     * @param userId
     * @param key 全局缓存里的key值, 用来查询角色id
     * @return
     */
    Boolean initUserRole(Long userId, String key);
}
