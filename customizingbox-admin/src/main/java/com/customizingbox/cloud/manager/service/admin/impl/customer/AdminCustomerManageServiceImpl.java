package com.customizingbox.cloud.manager.service.admin.impl.customer;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.enums.ShopifyRestApiEnum;
import com.customizingbox.cloud.common.core.constant.enums.TransactionEnum;
import com.customizingbox.cloud.common.datasource.mapper.admin.customer.AdminAppRelevancyRecordMapper;
import com.customizingbox.cloud.common.datasource.mapper.admin.customer.AdminCustomerManageMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderRefundItemMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderRefundMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysUserMapper;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.*;
import com.customizingbox.cloud.common.datasource.model.admin.customer.entity.AdminAppRelevancyRecord;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerAppOrderQuery;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerProductSaleParam;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.*;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerAppOrderPayAmount;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerAppPandaOrderResponseVo;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerAppOrderVariants;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerInfoInOrderResopnseVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.*;
import com.customizingbox.cloud.manager.service.app.AppOrderItemService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderItemService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author DC_Li
 * @date 2022/4/15 10:32
 */
@Service
@AllArgsConstructor
@Slf4j
public class AdminCustomerManageServiceImpl implements AdminCustomerManageService {
    private final AdminStoreProductVariantService adminStoreProductVariantService;
    private final AdminStoreProductAttrService adminStoreProductAttrService;
    private final AdminStoreProductService adminStoreProductService;
    private final AppOrderItemService appOrderItemService;
    private final AppPandaOrderItemService appPandaOrderItemService;
    private final AppSysUserMapper appSysUserMapper;
    private final AdminAppRelevancyRecordMapper adminAppRelevancyRecordMapper;
    private final AdminCustomerManageMapper adminCustomerManageMapper;
    private final AppStoreProductVariantMapper appStoreProductVariantMapper;
    private final AppPandaOrderRefundMapper appPandaOrderRefundMapper;
    private final AppPandaOrderRefundItemMapper appPandaOrderRefundItemMapper;
    private final RestTemplate restTemplate;
    private final AdminExchangeRateService adminExchangeRateService;
    private final AppPandaOrderService appPandaOrderService;

    @Override
    public IPage<List<AdminCustomerResponseVo>> pageCustomers(Page page, AdminCustomerDto adminCustomerDto, String ip) {
        // 按当前客户经理数据隔离，只能查自己传入userId,管理员不传查所有
        BaseUser user = SecurityUtils.getUser();
        Long userId = null;
        if (user.getCustomerDataAuth() || user.getPurchaseDataAuth()) {
            userId = Long.parseLong(user.getId());
        }
        long current = page.getCurrent();
        long size = page.getSize();
        long start = (current - 1) * size;
        List<AdminCustomerResponseVo> customers = appSysUserMapper.pageCustomers(start, size, adminCustomerDto, userId);
        Long count = appSysUserMapper.pageCustomersCount(adminCustomerDto, userId);
        for (AdminCustomerResponseVo customer : customers) {
            // todo DC 根据ip获取国家

        }
        page.setRecords(customers);
        page.setTotal(count);
        return page;
    }

    @Override
    public AdminCustomerResponseVo getOneAppUser(String email, Long appUserId) {
        AdminCustomerResponseVo appUser = appSysUserMapper.getOneAppUser(email, appUserId);
        // todo DC 根据ip获取国家
        return appUser;
    }

    @Override
    public Integer claimCustomer(Long appUserId) {
        // 查找客户是否存在
        AppSysUser user = appSysUserMapper.selectOne(new QueryWrapper<AppSysUser>().eq("id", appUserId));
        if (user == null) {
            log.warn("认领用户，用户不存在，id：{}", appUserId);
            return 2;
        }
        // 客户是否绑定客户经理
        if (ObjectUtil.isNotEmpty(user.getAdminUserId())) {
            log.warn("客户已被认领,id:{}", appUserId);
            return 3;
        }
        Long adminUserId = SecurityUtils.getUserId();
        user.setAdminUserId(adminUserId);
        appSysUserMapper.updateById(user);
        AdminAppRelevancyRecord adminAppRelevancyRecord = new AdminAppRelevancyRecord();
        adminAppRelevancyRecord.setAdminUserId(adminUserId);
        adminAppRelevancyRecord.setAppUserId(Long.parseLong(user.getId()));
        adminAppRelevancyRecord.setMark("客户认领");
        adminAppRelevancyRecord.setCreateId(adminUserId);
        adminAppRelevancyRecord.setCreateTime(LocalDateTime.now());
        adminAppRelevancyRecord.setUpdateId(adminUserId);
        adminAppRelevancyRecord.setUpdateTime(LocalDateTime.now());
        adminAppRelevancyRecordMapper.insert(adminAppRelevancyRecord);
        return 1;
    }

    @Override
    public IPage<CustomerStoreResponseVo> getCustomerStores(Page<CustomerStoreResponseVo> page, CustomerStoreDto customerStoreDto) {
        long size = page.getSize();
        long current = page.getCurrent();
        long start = (current - 1) * size;

        Long userId = null;
        if (SecurityUtils.getUser().getCustomerDataAuth()) {
            userId = SecurityUtils.getUserId();
        }
        List<CustomerStoreResponseVo> stores = adminCustomerManageMapper.getCustomerStores(start, size, userId, customerStoreDto);
        for (CustomerStoreResponseVo store : stores) {
            if (store.getPlatformType() == 1) {
                try {
                    String token = store.getToken();
                    store.setToken(null);
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    headers.add("X-Shopify-Access-Token", token);
                    HttpEntity<Object> entity = new HttpEntity<>(headers);
                    String orderCountUrl = String.format(ShopifyRestApiEnum.ORDER_COUNT.getUrl(), store.getStoreName());
                    ResponseEntity<JSONObject> response = restTemplate.exchange(orderCountUrl, HttpMethod.GET, entity, JSONObject.class);
                    Integer count = response.getBody().getInteger("count");
                    store.setPlatformOrderCount(count);

                } catch (RestClientException e) {
                    log.warn("获取店铺订单总数时异常,店铺id:{}", store.getStoreId(), e);
                }
            }
        }
        Long total = adminCustomerManageMapper.getCount(start, size, userId, customerStoreDto);
        page.setTotal(total);
        page.setRecords(stores);

        return page;
    }

    @Override
    public IPage<CustomerProductResponseVo> getCustomerProducts(Page<CustomerProductResponseVo> page, CustomerStoreProductsDto customerStoreProductsDto) {
        long size = page.getSize();
        long current = page.getCurrent();
        long start = (current - 1) * size;
        BaseUser user = SecurityUtils.getUser();
        Long userId = null;
        if (user.getCustomerDataAuth() || user.getPurchaseDataAuth()) {
            userId = Long.parseLong(user.getId());
        }
        List<Long> productsIds = adminCustomerManageMapper.getCustomerProductIds(start, size, userId, customerStoreProductsDto);


        List<CustomerProductResponseVo> list = adminCustomerManageMapper.getCustomerProducts(productsIds, customerStoreProductsDto);
        for (CustomerProductResponseVo product : list) {
            String src = adminCustomerManageMapper.getProductImgSrc(product.getStoreProductId());
            product.setProductPicture(src);

            List<CustomerProductVariant> storeVariants = product.getStoreVariants();
            for (CustomerProductVariant storeVariant : storeVariants) {
                Long adminVariantId = storeVariant.getAdminVariantId();
                if (adminVariantId == null) {
                    storeVariant.setQuoteState(false);
                } else {
                    // 已报价，查询产品库变体信息
                    storeVariant.setQuoteState(true);
                    AppStoreProductVariant storeProductVariant = appStoreProductVariantMapper.selectById(storeVariant.getVariantId());
                    AdminStoreProductVariant variant = adminStoreProductVariantService.getById(adminVariantId);
                    Long productId = variant.getProductId();
                    List<AdminStoreProductAttr> attrs = adminStoreProductAttrService.list(Wrappers.<AdminStoreProductAttr>lambdaQuery()
                            .eq(AdminStoreProductAttr::getProductId, productId)
                            .orderByAsc(AdminStoreProductAttr::getSort)
                            .select(AdminStoreProductAttr::getAttrName));
                    List<String> names = attrs.stream().map(AdminStoreProductAttr::getAttrName).collect(Collectors.toList());
                    List<String> values = JSONObject.parseArray(variant.getAttrValues()).toJavaList(String.class);
                    AdminQuoteVariantResponseVo adminVariant = new AdminQuoteVariantResponseVo();
                    for (int i = 0; i < values.size(); i++) {
                        String name = names.get(i);
                        String value = values.get(i);
                        switch (i) {
                            case 0:
                                adminVariant.setOptionAName(name);
                                adminVariant.setOptionAvalue(value);
                                break;
                            case 1:
                                adminVariant.setOptionBName(name);
                                adminVariant.setOptionBvalue(value);
                                break;
                            case 2:
                                adminVariant.setOptionCName(name);
                                adminVariant.setOptionCvalue(value);
                                break;
                            default:
                        }
                    }
                    adminVariant.setSku(variant.getSku());
                    adminVariant.setSupplyPrice(variant.getSupplyPrice());
                    storeVariant.setSupplyRate(storeProductVariant.getSupplyRate());
                    storeVariant.setAdminVariant(adminVariant);
                }
            }
        }
        Long count = adminCustomerManageMapper.getCustomerProductsCount(userId, customerStoreProductsDto);

        page.setTotal(count);
        page.setRecords(list);
        return page;
    }

    @Override
    public CustomerQuoteVo getCustomerProductDetail(Long storeProductId) {
        // 获得上次报价的spu信息、产品库变体的sku和属性值
        CustomerQuoteVo customerQuoteVo = new CustomerQuoteVo();

        // 获取变体信息和上次报价的产品库变体信息
        List<CustomerProductVariantsResponseVo> customerProductDetail = adminCustomerManageMapper.getCustomerProductDetail(storeProductId);
        customerQuoteVo.setVariants(customerProductDetail);
        // 获取已报价产品库产品id
        List<Long> adminProductIds = adminCustomerManageMapper.getQuoteAdminProductIds(storeProductId);
        List<AdminQuoteProductVo> quoteAdminProductDetail = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(adminProductIds)){
            // 获取产品库产品信息
            quoteAdminProductDetail = adminCustomerManageMapper.getQuoteAdminProductDetail(adminProductIds);
            // 获取产品库产品图片
            for (AdminQuoteProductVo adminQuoteProductVo : quoteAdminProductDetail) {
                String imgSrc = adminCustomerManageMapper.getProductImg(adminQuoteProductVo.getAdminProductId());
                adminQuoteProductVo.setProductImg(imgSrc);
            }
        }
        customerQuoteVo.setAdminQuoteProductVos(quoteAdminProductDetail);

        return customerQuoteVo;
    }

    @Override
    public CustomerQuoteAdminVo getAdminProductAndVariants(String spu) {
        AdminQuoteProductVo product = adminCustomerManageMapper.getAdminProduct(spu);
        List<AdminQuoteVariantResponseVo> variants = adminCustomerManageMapper.getAdminVariants(spu);
        List<String> names = adminCustomerManageMapper.getProductAttrNames(spu);
        if (product == null) {
            return null;
        }
        // 产品的图片设置成第一个变体的图片
        product.setProductImg(variants.get(0).getVariantImg());

        for (AdminQuoteVariantResponseVo variant : variants) {
            String values = variant.getAttrValues();
            List<String> valueList = JSONObject.parseArray(values).toJavaList(String.class);
            for (int i = 0; i < valueList.size(); i++) {
                String value = valueList.get(i);
                String name = names.get(i);
                switch (i) {
                    case 0:
                        variant.setOptionAName(name);
                        variant.setOptionAvalue(value);
                        break;
                    case 1:
                        variant.setOptionBName(name);
                        variant.setOptionBvalue(value);
                        break;
                    case 2:
                        variant.setOptionCName(name);
                        variant.setOptionCvalue(value);
                        break;
                    default:
                }
            }
        }
        BigDecimal usdRate = adminExchangeRateService.getOneRateValue(TransactionEnum.CURRENCY.USD.getCode());

        CustomerQuoteAdminVo customerQuoteAdminVo = new CustomerQuoteAdminVo();
        customerQuoteAdminVo.setUsdRate(usdRate);
        customerQuoteAdminVo.setProduct(product);
        customerQuoteAdminVo.setVariants(variants);
        return customerQuoteAdminVo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveQuote(List<SaveQuoteRequestVo> requestVo) {
        Boolean aBoolean = adminCustomerManageMapper.saveQuote(requestVo);
        // 修改admin产品是否已用来报价状态
        List<Long> adminVariantIds = requestVo.stream().map(SaveQuoteRequestVo::getAdminVariantId).collect(Collectors.toList());
        for (SaveQuoteRequestVo vo : requestVo) {
            Long adminProductId = vo.getAdminProductId();
            adminStoreProductService.updateAdminProductQuoteFlag(adminProductId, adminVariantIds);

            // 重新匹配店铺订单的映射关系,潘达订单在重新识别订单商品和下单时重新映射
            // 店铺订单
            Boolean storeOrderItemResult = appOrderItemService.updateOrderItemProductRelevancy(vo.getAppVariantId(), vo.getAdminProductId(), vo.getAdminVariantId());
        }


        return aBoolean;
    }

    @Override
    public Boolean transferCustomer(Map<Long, Long> map) {
        Boolean b = true;
        for (Map.Entry<Long, Long> entry : map.entrySet()) {
            Long appUserId = entry.getKey();
            Long adminUserId = entry.getValue();
            Boolean boo = adminCustomerManageMapper.transferCustomer(appUserId, adminUserId);
            if (!boo) {
                b = false;
            }
            AdminAppRelevancyRecord record = new AdminAppRelevancyRecord();
            record.setAppUserId(appUserId);
            record.setAdminUserId(adminUserId);
            record.setCreateTime(LocalDateTime.now());
            Long userId = SecurityUtils.getUserId();
            record.setCreateId(userId);
            record.setUpdateTime(LocalDateTime.now());
            record.setUpdateId(userId);
            record.setMark("客户移交");
            adminAppRelevancyRecordMapper.insert(record);
        }
        return b;
    }

    @Override
    public IPage<CustomerProductSaleResponseVo> getCustomerProductSale(Page page, CustomerProductSaleParam param) {
        long current = page.getCurrent();
        long size = page.getSize();
        long start = (current - 1) * size;

        BaseUser user = SecurityUtils.getUser();
        Long userId = null;
        if (user.getCustomerDataAuth()) {
            userId = Long.valueOf(user.getId());
        }

        Long count = adminCustomerManageMapper.getCustomerProductSaleCount(userId, param);
        page.setTotal(count);
        List<CustomerProductSaleResponseVo> productSales = adminCustomerManageMapper.getCustomerProductSale(start, size, userId, param);
        // 查询不到客户商品，返回record为空的ipage
        if (CollectionUtils.isEmpty(productSales)) {
            return page;
        }
        List<Long> productIds = productSales.stream().map(CustomerProductSaleResponseVo::getAppProductId).collect(Collectors.toList());
        // 查询销售情况
        LocalDateTime oneDayAgo = LocalDateTime.now().plusDays(-1);
        LocalDateTime sevenDayAgo = LocalDateTime.now().plusDays(-7);
        LocalDateTime fifteenDayAgo = LocalDateTime.now().plusDays(-15);
        List<CustomerProductSalePurchaseDto> sale1 = adminCustomerManageMapper.getSaleCondition(productIds, oneDayAgo);
        List<CustomerProductSalePurchaseDto> sale2 = adminCustomerManageMapper.getSaleCondition(productIds, sevenDayAgo);
        List<CustomerProductSalePurchaseDto> sale3 = adminCustomerManageMapper.getSaleCondition(productIds, fifteenDayAgo);
        // 查询采购情况,采购指客户在我们平台下单的产品变体数量，包含定制和非定制
        List<CustomerProductSalePurchaseDto> purchase1 = adminCustomerManageMapper.getPurchaseCondition(productIds, oneDayAgo);
        List<CustomerProductSalePurchaseDto> purchase2 = adminCustomerManageMapper.getPurchaseCondition(productIds, sevenDayAgo);
        List<CustomerProductSalePurchaseDto> purchase3 = adminCustomerManageMapper.getPurchaseCondition(productIds, fifteenDayAgo);

        for (CustomerProductSaleResponseVo productSale : productSales) {
            Long appProductId = productSale.getAppProductId();
            Optional<CustomerProductSalePurchaseDto> sOptional1 = sale1.stream().filter(customerProductSalePurchaseDto -> customerProductSalePurchaseDto.getProductId().equals(appProductId)).findFirst();
            if (sOptional1.isPresent()) {
                CustomerProductSalePurchaseDto dto1 = sOptional1.get();
                productSale.setOneDaySale(dto1.getSale());
            }
            Optional<CustomerProductSalePurchaseDto> sOptional2 = sale2.stream().filter(customerProductSalePurchaseDto -> customerProductSalePurchaseDto.getProductId().equals(appProductId)).findFirst();
            if (sOptional2.isPresent()) {
                CustomerProductSalePurchaseDto dto2 = sOptional2.get();
                productSale.setSevenDaySale(dto2.getSale());
            }
            Optional<CustomerProductSalePurchaseDto> sOptional3 = sale3.stream().filter(customerProductSalePurchaseDto -> customerProductSalePurchaseDto.getProductId().equals(appProductId)).findFirst();
            if (sOptional3.isPresent()) {
                CustomerProductSalePurchaseDto dto3 = sOptional3.get();
                productSale.setSevenDaySale(dto3.getSale());
            }

            Optional<CustomerProductSalePurchaseDto> pOptional1 = purchase1.stream().filter(customerProductSalePurchaseDto -> customerProductSalePurchaseDto.getProductId().equals(appProductId)).findFirst();
            if (pOptional1.isPresent()) {
                CustomerProductSalePurchaseDto dto = pOptional1.get();
                productSale.setOneDayPurchase(dto.getPurchase());
            }
            Optional<CustomerProductSalePurchaseDto> pOptional2 = purchase2.stream().filter(customerProductSalePurchaseDto -> customerProductSalePurchaseDto.getProductId().equals(appProductId)).findFirst();
            if (pOptional2.isPresent()) {
                CustomerProductSalePurchaseDto dto = pOptional2.get();
                productSale.setSevenDayPurchase(dto.getPurchase());
            }
            Optional<CustomerProductSalePurchaseDto> pOptional3 = purchase3.stream().filter(customerProductSalePurchaseDto -> customerProductSalePurchaseDto.getProductId().equals(appProductId)).findFirst();
            if (pOptional3.isPresent()) {
                CustomerProductSalePurchaseDto dto = pOptional3.get();
                productSale.setFifteenDayPurchase(dto.getPurchase());
            }

        }

        page.setRecords(productSales);
        return page;
    }

    @Override
    public IPage<CustomerAppPandaOrderResponseVo> getCustomerStoreOrders(Page page, CustomerAppOrderQuery query) {
        long size = page.getSize();
        long current = page.getCurrent();
        long start = (current - 1) * size;
        Long userId = null;
        BaseUser user = SecurityUtils.getUser();
        if (user.getCustomerDataAuth()) {
            userId = Long.parseLong(user.getId());
        }
        BigDecimal usdRate = adminExchangeRateService.getOneRateValue(TransactionEnum.CURRENCY.USD.getCode());
        List<CustomerAppPandaOrderResponseVo> list = adminCustomerManageMapper.getCustomerStorePandaOrders(start, size, userId, query);
        for (CustomerAppPandaOrderResponseVo order : list) {
            Long pandaOrderId = order.getId();
            List<CustomerAppOrderVariants> variants = adminCustomerManageMapper.getPandaOrderItems(pandaOrderId, usdRate);
            order.setVariants(variants);
            CustomerInfoInOrderResopnseVo customerInfo = order.getCustomerInfo();
            customerInfo.setCustomerManagerNow(SecurityUtils.getUser().getUsername());
            order.setCustomerInfo(customerInfo);

            // 退款信息查询
            getRefund(pandaOrderId, order);
        }

        Long total = adminCustomerManageMapper.getCustomerStorePandaOrdersCount(userId, query);
        page.setTotal(total);
        return page.setRecords(list);
    }

    @Override
    public CustomerAppPandaOrderResponseVo reIdentifyPandaOrder(Long pandaOrderId) {
        AppPandaOrder pandaOrder = appPandaOrderService.getById(pandaOrderId);
        Boolean aBoolean = appPandaOrderItemService.reloadOrderItemProductRelevancy(pandaOrder);

        Long userId = SecurityUtils.getUserId();
        CustomerAppPandaOrderResponseVo customerPandaOrder = adminCustomerManageMapper.getCustomerStorePandaOrder(pandaOrderId);
        // 获取退款信息
        getRefund(pandaOrderId, customerPandaOrder);
        return customerPandaOrder;
    }

    /**
     * 获取退款信息
     *
     * @param pandaOrderId       潘达订单
     * @param customerPandaOrder 客户订单
     */
    private void getRefund(Long pandaOrderId, CustomerAppPandaOrderResponseVo customerPandaOrder) {
        CustomerAppOrderPayAmount payAmount = customerPandaOrder.getPayAmount();
        // 查询该潘达订单的所有退款订单的运费和vat之和
        FreightAndVatTotalDto freightAndVatTotalDto = appPandaOrderRefundMapper.getAllFreightAndVatTotal(pandaOrderId);
        // 没有退款
        if (ObjectUtil.isNull(freightAndVatTotalDto)) {
            payAmount.setRefundTransactionNumbers(null);
            payAmount.setShippingRefundMoney(BigDecimal.ZERO);
            payAmount.setVatRefundMoney(BigDecimal.ZERO);
            payAmount.setProductRefundMoney(BigDecimal.ZERO);
            customerPandaOrder.setPayAmount(payAmount);
        } else {
            List<Long> refundIds = JSONArray.parseArray(freightAndVatTotalDto.getRefundIds()).toJavaList(Long.class);
            // 订单item退款总和
            BigDecimal itemAmountTotal = appPandaOrderRefundMapper.getItemAmountTotal(refundIds);
            payAmount.setRefundTransactionNumbers(refundIds);
            payAmount.setShippingRefundMoney(freightAndVatTotalDto.getFreightAmount());
            payAmount.setVatRefundMoney(freightAndVatTotalDto.getVatAmount());
            payAmount.setProductRefundMoney(itemAmountTotal);
            customerPandaOrder.setPayAmount(payAmount);
        }
    }
}
