package com.customizingbox.cloud.manager.service.app.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderRefundItemMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderRefundItem;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundItemAmountVO;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductAttrService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderRefundItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 退款item表
 *
 * @author Y
 * @date 2022-04-23 09:55:24
 */
@Service
public class AppPandaOrderRefundItemServiceImpl extends ServiceImpl<AppPandaOrderRefundItemMapper, AppPandaOrderRefundItem> implements AppPandaOrderRefundItemService {

    @Autowired
    protected AdminStoreProductAttrService adminStoreProductAttrService;

    @Override
    public List<AppPandaOrderRefundItemAmountVO> applyRefundAmount(Long orderId, Integer status) {
         return baseMapper.applyRefundAmount(orderId, status);
    }

    @Override
    public List<AppPandaOrderRefundItem> getByRefundId(Long refundId) {
        return this.list(Wrappers.<AppPandaOrderRefundItem>lambdaQuery().eq(AppPandaOrderRefundItem::getRefundId, refundId));
    }


}
