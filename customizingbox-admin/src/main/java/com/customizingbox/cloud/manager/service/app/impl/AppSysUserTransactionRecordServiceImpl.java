package com.customizingbox.cloud.manager.service.app.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.TransactionConstants;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.enums.TransactionEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.redisson.RedissonLock;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysUserMapper;
import com.customizingbox.cloud.common.datasource.mapper.transaction.AppSysUserTransactionRecordMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.common.datasource.model.app.transaction.dto.AppSysUserTransactionMoneyDTO;
import com.customizingbox.cloud.common.datasource.model.app.transaction.entity.AppSysUserTransactionRecord;
import com.customizingbox.cloud.manager.service.app.AppSysUserTransactionRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;

/**
 * 交易记录表
 *
 * @author Y
 * @date 2022-04-21 16:09:21
 */
@Service
@Slf4j
public class AppSysUserTransactionRecordServiceImpl extends ServiceImpl<AppSysUserTransactionRecordMapper, AppSysUserTransactionRecord> implements AppSysUserTransactionRecordService {

    private static String key = "TRANSACTION_";

    @Autowired
    private RedissonLock redissonLock;
    @Autowired
    private AppSysUserMapper appSysUserMapper;


    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Long refund(BigDecimal money, Long userId, Long orderId, Long refundOrderId, String mark) {
        log.info("<===开始交易,money:{}, userId: {}, orderId: {}, refundOrderId: {} mark: {} ===>", money, userId, orderId, refundOrderId, mark);
        return transaction(money, userId, TransactionEnum.TYPE.PAY_IN.getCode(),  TransactionEnum.SOURCE_TYPE.ORDER_REFUND.getCode(),
                null, orderId, refundOrderId, null, mark);
    }

    private Long transaction(BigDecimal money, Long userId, Integer type, Integer sourceType, String transaction, Long orderId, Long refundOrderId,  String paymentId,  String mark) {
        log.info("<===开始交易,money:{}, userId: {}, type: {}, sourceType: {}, mark: {} ===>", money, userId, type, sourceType, mark);
        AppSysUserTransactionRecord transactionRecord = new AppSysUserTransactionRecord();
        if (ObjectUtils.isEmpty(money)) {
            throw new ApiException("金额不能为空");
        }
        if (ObjectUtils.isEmpty(userId)) {
            throw new ApiException("用户为空");
        }
        if (ObjectUtils.isEmpty(TransactionEnum.TYPE.getCode(type))) {
            throw new ApiException("交易类型异常");
        }
        if (ObjectUtils.isEmpty(TransactionEnum.SOURCE_TYPE.getCode(sourceType))) {
            throw new ApiException("来源类型异常");
        }
        String lockName = TransactionConstants.TRANSACTION_PREFIX + userId;
        boolean lockResult = redissonLock.tryLock(lockName, TransactionConstants.TRANSACTION_REDIS_LOCK_LEASE_TIME
                , TransactionConstants.TRANSACTION_REDIS_LOCK_WAIT_TIME);
        if (lockResult) {
            try {
                log.info("<===交易获取到锁, money:{}, userId: {}, type: {}, sourceType: {}, mark: {} ===>", money, userId, type, sourceType, mark);
                AppSysUser appSysUser = appSysUserMapper.selectById(userId);
                BigDecimal balance = appSysUser.getBalance();
                log.info("当前用户余额: {}", balance);
                if (ObjectUtils.isEmpty(balance) || balance.compareTo(BigDecimal.ZERO) < 0) {
                    throw new ApiException(ApiResStatus.BALANCE_ERROR.getStatus(), "请检查余额");
                }
                BigDecimal recordBalance = queryTransactionBalance(userId);
                log.info("当前用户余额: {}, 交易余额为: {}", balance, recordBalance);
                if (balance.compareTo(recordBalance) != 0) {
                    log.error("余额校验失败, balance: {}, record balance: {}", balance, recordBalance);
                    throw new ApiException(ApiResStatus.BALANCE_ERROR.getStatus(), "余额异常, 请联系管理员");
                }
                BigDecimal newBalance;
                if (TransactionEnum.TYPE.PAY_OUT.getCode().equals(type)) {
                    // 是支出, 要校验余额是否够
                    if (money.compareTo(balance) > 0) {
                        throw new ApiException(ApiResStatus.NOT_UFFICIENT_FUNDS);
                    }
                    newBalance = balance.subtract(money);
                    //
                } else {
                    newBalance = balance.add(money);
                }
                appSysUser.setBalance(newBalance);
                // 添加交易记录
                transactionRecord.setUserId(userId);
                transactionRecord.setMoney(money);
                transactionRecord.setType(type);
                transactionRecord.setSourceType(sourceType);
                transactionRecord.setTransaction(transaction);
                transactionRecord.setPaymentId(paymentId);
                transactionRecord.setOrderId(orderId);
                transactionRecord.setRefundOrderId(refundOrderId);
                transactionRecord.setMark(mark);
                transactionRecord.setFirstBalance(balance);
                transactionRecord.setLastBalance(newBalance);
                log.info("<===保存交易记录 ,{}===>", JSONObject.toJSONString(transactionRecord));
                boolean transactionResult = this.save(transactionRecord);
                int userResult = appSysUserMapper.updateById(appSysUser);
                if (!transactionResult || userResult != 1) {
                    throw new ApiException(ApiResStatus.BALANCE_ERROR.getStatus(), "交易失败");
                }
                log.info("<===交易成功,money:{}, userId: {}, type: {}, sourceType: {}, mark: {}===>", money, userId, type, sourceType, mark);

            } catch (ApiException apiException) {
                log.error("<===交易异常, money:{}, userId: {}, type: {}, sourceType: {}, mark: {}===>", money, userId, type, sourceType, mark, apiException);
                throw new ApiException(apiException.getErrorCode(), apiException.getMessage());
            } catch (Throwable e) {
                log.error("<===交易异常, money:{}, userId: {}, type: {}, sourceType: {}, mark: {}===>", money, userId, type, sourceType, mark, e);
                throw new ApiException("交易异常");
            } finally {
                if (redissonLock.isHeldByCurrentThread(lockName)) {
                    log.info("<===交易解锁, money:{}, userId: {}, type: {}, sourceType: {}, mark: {} ===>", money, userId, type, sourceType, mark);
                    redissonLock.unlock(lockName);
                } else {
                    log.error("<===交易中,锁被提前释放 ,money:{}, userId: {}, type: {}, sourceType: {}, mark: {}===>", money, userId, type, sourceType, mark);
                }
            }
        } else {
            log.warn("<===交易付款获取锁失败, userId: {}, type: {}, sourceType: {}, mark: {}===>", userId, type, sourceType, mark);
        }
        return transactionRecord.getId();
    }

    /**
     * 查询记录表用户余额
     * @param userId
     * @return
     */
    private BigDecimal queryTransactionBalance(Long userId) {
        AppSysUserTransactionMoneyDTO appSysUserTransactionMoneyDTO = baseMapper.queryTotalPayInPayOut(userId);
        if (ObjectUtils.isEmpty(appSysUserTransactionMoneyDTO)) {
            return BigDecimal.ZERO;
        }
        BigDecimal payinAmount = appSysUserTransactionMoneyDTO.getPayinAmount();
        BigDecimal payoutAmount = appSysUserTransactionMoneyDTO.getPayoutAmount();
        return payinAmount.subtract(payoutAmount);
    }
}
