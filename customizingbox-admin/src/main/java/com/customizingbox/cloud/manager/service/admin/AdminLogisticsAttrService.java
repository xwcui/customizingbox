package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminLogisticsAttrDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsAttr;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

import java.util.List;

/**
 * <p>
 * 物流属性表 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminLogisticsAttrService extends IService<AdminLogisticsAttr> {


    /**
     * 分页
     * @param page
     * @param searchVo
     * @return
     */
    IPage<AdminLogisticsAttrPageVo> pageAll(Page page, AdminLogisticsAttrSearchVo searchVo);

    /**
     * 保存或修改
     * @param adminLogisticsAttrDto
     * @param user
     * @return
     */
    ApiResponse saveOrUpdateAdminLogisticsAttrDto(AdminLogisticsAttrDto adminLogisticsAttrDto, BaseUser user);


    /**
     * 开启或关闭
     * @return
     * @param id
     * @param status
     */
    ApiResponse openOrShut(Long id, Integer status);

    /**
     * 所有已开启数据
     * @return
     */
    List<AdminLogisticsAttr> getList();

}
