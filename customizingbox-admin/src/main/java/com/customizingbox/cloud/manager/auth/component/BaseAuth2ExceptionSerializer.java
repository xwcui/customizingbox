package com.customizingbox.cloud.manager.auth.component;

import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.manager.auth.exception.BaseAuth2Exception;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.SneakyThrows;

/**
 * OAuth2 异常格式化
 */
public class BaseAuth2ExceptionSerializer extends StdSerializer<BaseAuth2Exception> {

    public BaseAuth2ExceptionSerializer() {
        super(BaseAuth2Exception.class);
    }

    @Override
    @SneakyThrows
    public void serialize(BaseAuth2Exception value, JsonGenerator gen, SerializerProvider provider) {
        gen.writeStartObject();
        gen.writeStringField("msg", value.getMessage());
        gen.writeStringField("data", value.getErrorCode());
        gen.writeObjectField("code", ApiResStatus.FAIL.getStatus());
        gen.writeEndObject();
    }
}
