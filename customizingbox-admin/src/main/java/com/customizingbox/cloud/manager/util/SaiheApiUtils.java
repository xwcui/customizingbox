package com.customizingbox.cloud.manager.util;


import com.baomidou.mybatisplus.extension.exceptions.ApiException;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.*;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.request.*;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.*;
import com.customizingbox.cloud.common.datasource.util.XmlAndJavaObjectConvert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cjq on 2018/12/4.
 */
@Slf4j
@Component
public class SaiheApiUtils {

    @Value("${saihe.userName}")
    String userName;

    @Value("${saihe.passWord}")
    String passWord;

    @Value("${saihe.customerId}")
    Integer customerId;

    @Value("${saihe.defaultWarehourseId}")
    Integer defaultWarehourseId;

    @Value("${saihe.url}")
    String url;


    /**
     * 获取赛盒运输方式 对应 仓库ID
     */
    public ApiGetTransportResponse getTransportList() {
        try {
            ApiGetTransportRequest apiGetTransportRequest = new ApiGetTransportRequest();
            TrRequest trRequest = new TrRequest();
            trRequest.setUserName(userName);
            trRequest.setPassword(passWord);
            trRequest.setCustomer_ID(customerId);
            trRequest.setWareHouseID(defaultWarehourseId);
            apiGetTransportRequest.setTrRequest(trRequest);
            RequestEntity requestEntity = new RequestEntity();
            RequestBody requestBody = new RequestBody();
            requestBody.setApiGetTransportRequest(apiGetTransportRequest);
            requestEntity.setBody(requestBody);
            String xmlStr = XmlAndJavaObjectConvert.convertToXml(requestEntity);
            String result = SaihePostUtils.sendPostV("http://senbo.irobotbox.com/Api/API_Irobotbox_Orders.asmx", xmlStr);
            ResponseEntity responseEntity = (ResponseEntity) XmlAndJavaObjectConvert.convertXmlStrToObject(ResponseEntity.class, result);
            return responseEntity.getBody().getApiGetTransportResponse();
        } catch (Exception e) {
            log.warn("saiheApi--> getTransportList " + e.getMessage());
            throw new ApiException(e.getMessage());
        }
    }

    /**
     * 上传产品到赛盒
     * @param list
     * @return
     */
    public ApiUploadProductsResponse processUpdateProduct(List<ApiImportProductInfo> list) {
        ApiUploadProductsResponse apiUploadProductsResponse = new ApiUploadProductsResponse("Error");
        String result = "";
        try {
            Prequest request = new Prequest();
            request.setUserName(userName);
            request.setPassword(passWord);
            request.setCustomerID(customerId);

            ImportProductList importProductList = new ImportProductList();
            importProductList.setImportProductList(list);
            request.setImportProductList(importProductList);

            ApiUploadProductsRequest apiUploadProductsRequest = new ApiUploadProductsRequest();
            apiUploadProductsRequest.setRequest(request);

            EntityToXml requestEntityToXml = new EntityToXml();
            BodyToXml RequestBodyToXml = new BodyToXml();
            RequestBodyToXml.setApiUploadProductsRequest(apiUploadProductsRequest);
            requestEntityToXml.setBody(RequestBodyToXml);
            String xmlStr2 = XmlAndJavaObjectConvert.convertToXml(requestEntityToXml);
            result = SaihePostUtils.sendPost1("http://senbo.irobotbox.com/Api/API_ProductInfoManage.asmx", "http://tempuri.org/ProcessUpdateProduct", xmlStr2);
            EntityToXml responseEntity1 = (EntityToXml) XmlAndJavaObjectConvert.convertXmlStrToObject(EntityToXml.class, result);
            return responseEntity1.getBody().getApiUploadProductsResponse();
        } catch (Exception e) {
            log.error("上传产品到赛盒异常:{}", result);
            return apiUploadProductsResponse;
        }
    }


    /**
     * 库存数量操作接口(当传入的SKU没有库存记录时，会新增一条库存记录)
     * @param list
     * @return
     */
    public UpdateProductStockNumberResponse updateProductStockNumber(List<ProductStock> list) {
        String result = "";
        try {
            for (ProductStock productStock : list) {
                productStock.setWareHouseID(defaultWarehourseId);
            }
            UpdateProductStockNumberRequest request = new UpdateProductStockNumberRequest();
            ProductStockRequestList productStockReques = new ProductStockRequestList();
            productStockReques.setUserName(userName);
            productStockReques.setPassword(passWord);
            productStockReques.setCustomerID(customerId);
            ProductStockList productStockList = new ProductStockList();
            productStockList.setProductStockList(list);
            productStockReques.setProductStockList(productStockList);
            request.setProductStockRequestList(productStockReques);
            EntityToXml requestEntityToXml = new EntityToXml();
            BodyToXml RequestBodyToXml = new BodyToXml();
            RequestBodyToXml.setUpdateProductStockNumberRequest(request);
            requestEntityToXml.setBody(RequestBodyToXml);
            String xmlStr2 = XmlAndJavaObjectConvert.convertToXml(requestEntityToXml);
            result = SaihePostUtils.sendPost1("http://senbo.irobotbox.com/Api/API_Irobotbox_Orders.asmx", "http://tempuri.org/UpdateProductStockNumber", xmlStr2);
            EntityToXml responseEntity = (EntityToXml) XmlAndJavaObjectConvert.convertXmlStrToObject(EntityToXml.class, result);
            return  responseEntity.getBody().getUpdateProductStockNumberResponse();
        } catch (Exception e) {
            log.error("上传产品到赛盒异常:{}", result);
            return null;
        }
    }

    /**
     * 上传订单到赛盒
     */
    public  UpLoadOrderV2Response upLoadOrderV2(ApiUploadOrderInfo apiUploadOrderInfo){
        UpLoadOrderV2Response upLoadOrderV2Response=new UpLoadOrderV2Response("Error");
        try {
            ApiUploadOrderRequest apiUploadOrderRequest = new ApiUploadOrderRequest();
            Request request = new Request();
            request.setUserName(userName);
            request.setPassword(passWord);
            request.setCustomerID(customerId);
            apiUploadOrderInfo.setCustomerID(customerId);
            request.setApiUploadOrderInfo(apiUploadOrderInfo);
            apiUploadOrderRequest.setRequest(request);
            RequestEntity requestEntity = new RequestEntity();
            RequestBody requestBody = new RequestBody();
            requestBody.setApiUploadOrderRequest(apiUploadOrderRequest);
            requestEntity.setBody(requestBody);
            String xmlStr = XmlAndJavaObjectConvert.convertToXml(requestEntity);
            String result = SaihePostUtils.sendPostV("http://senbo.irobotbox.com/Api/API_Irobotbox_Orders.asmx", xmlStr);
            ResponseEntity responseEntity = (ResponseEntity) XmlAndJavaObjectConvert.convertXmlStrToObject(ResponseEntity.class, result);
            upLoadOrderV2Response = responseEntity.getBody().getUpLoadOrderV2Response();
            return upLoadOrderV2Response;
        }catch (Exception e){
            log.error("上传订单到赛盒异常",e.getMessage());
            return upLoadOrderV2Response;
        }
    }

    public static String updateProductStockNumbers(List<ProductStock> list) {
        String result = "";
        try {
            for (ProductStock productStock : list) {
//                productStock.setWareHouseID(defaultWarehourseId);
                productStock.setWareHouseID(230);
            }
            UpdateProductStockNumberRequest request = new UpdateProductStockNumberRequest();
            ProductStockRequestList productStockReques = new ProductStockRequestList();
/*            productStockReques.setUserName(userName);
            productStockReques.setPassword(passWord);
            productStockReques.setCustomerID(customerId);*/
            productStockReques.setUserName("PANDADUIJIE");
            productStockReques.setPassword("LSJDKJHASDNBS");
            productStockReques.setCustomerID(1555);
            ProductStockList productStockList = new ProductStockList();
            productStockList.setProductStockList(list);
            productStockReques.setProductStockList(productStockList);
            request.setProductStockRequestList(productStockReques);
            EntityToXml requestEntityToXml = new EntityToXml();
            BodyToXml RequestBodyToXml = new BodyToXml();
            RequestBodyToXml.setUpdateProductStockNumberRequest(request);
            requestEntityToXml.setBody(RequestBodyToXml);
            String xmlStr2 = XmlAndJavaObjectConvert.convertToXml(requestEntityToXml);
            result = SaihePostUtils.sendPost1("http://senbo.irobotbox.com/Api/API_Irobotbox_Orders.asmx", "http://tempuri.org/UpdateProductStockNumber", xmlStr2);
            EntityToXml responseEntity1 = (EntityToXml) XmlAndJavaObjectConvert.convertXmlStrToObject(EntityToXml.class, result);
            return null;
        } catch (Exception e) {
            log.error("上传产品到赛盒异常:{}", result);
            return null;
        }
    }

    public static void main(String[] args) {
        List<ProductStock> list = new ArrayList<>();
        updateProductStockNumbers(list);
    }
}
