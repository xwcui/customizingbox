package com.customizingbox.cloud.manager.controller;


import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderItemMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Slf4j
public class TestController {
    private final AppPandaOrderItemMapper appPandaOrderItemMapper;
    @GetMapping("/ping")
    public ApiResponse ping() {

        AppPandaOrderItem appPandaOrderItem = appPandaOrderItemMapper.selectById("1515959926738018305");
        int i = appPandaOrderItemMapper.updateById(appPandaOrderItem);
        System.out.println(">>>" + i);

        System.out.println("客户权限: " + SecurityUtils.getUser().getCustomerDataAuth());
        System.out.println("采购权限: " + SecurityUtils.getUser().getPurchaseDataAuth());
        return ApiResponse.ok();
    }
}
