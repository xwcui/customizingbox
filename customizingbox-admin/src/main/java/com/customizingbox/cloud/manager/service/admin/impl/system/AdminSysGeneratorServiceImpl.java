package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysGeneratorMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysGenTable;
import com.customizingbox.cloud.manager.service.admin.AdminSysGeneratorService;
import com.customizingbox.cloud.manager.util.GenUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成器
 */
@Service
@AllArgsConstructor
public class AdminSysGeneratorServiceImpl implements AdminSysGeneratorService {

    private final AdminSysGeneratorMapper adminSysGeneratorMapper;

    /**
     * 分页查询表
     */
    @Override
    public IPage<List<Map<String, Object>>> getPage(Page page, String tableName, String sysDatasourceId) {
        return (IPage<List<Map<String, Object>>>) adminSysGeneratorMapper.queryList(page, tableName);
    }

    @Override
    public Map<String, String> generatorView(AdminSysGenTable adminSysGenTable) {
        //查询表信息
        Map<String, String> table = queryTable(adminSysGenTable.getTableName());
        //查询列信息
        List<Map<String, Object>> columns = queryColumns(adminSysGenTable.getTableName());
        return GenUtils.generatorCode(adminSysGenTable, table, columns, null);
    }

    /**
     * 生成代码
     *
     * @param adminSysGenTable 生成表配置
     */
    @Override
    public byte[] generatorCode(AdminSysGenTable adminSysGenTable) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);

        //查询表信息
        Map<String, String> table = this.queryTable(adminSysGenTable.getTableName());
        //查询列信息
        List<Map<String, Object>> columns = this.queryColumns(adminSysGenTable.getTableName());
        //生成代码
        GenUtils.generatorCode(adminSysGenTable, table, columns, zip);
        IoUtil.close(zip);
        return outputStream.toByteArray();
    }

    private Map<String, String> queryTable(String tableName) {
        return adminSysGeneratorMapper.queryTable(tableName);
    }

    private List<Map<String, Object>> queryColumns(String tableName) {
        return adminSysGeneratorMapper.queryColumns(tableName);
    }
}
