package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysGenTable;

/**
 * 代码生成配置表
 */
public interface AdminSysGenTableService extends IService<AdminSysGenTable> {

}
