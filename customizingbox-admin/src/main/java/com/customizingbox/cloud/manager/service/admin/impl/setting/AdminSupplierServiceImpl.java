package com.customizingbox.cloud.manager.service.admin.impl.setting;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.AdminGlobalConstants;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.enums.SupplierStateEnum;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductMapper;
import com.customizingbox.cloud.common.datasource.mapper.admin.setting.AdminSupplierMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierRequestVo;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminSupplier;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierResponseVo;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierResponseVoForEdit;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import com.customizingbox.cloud.common.datasource.util.IdGenerate;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminSupplierService;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserRoleService;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * <p>
 * 供应商表 服务实现类
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
@Service
@AllArgsConstructor
@Slf4j
public class AdminSupplierServiceImpl extends ServiceImpl<AdminSupplierMapper, AdminSupplier> implements AdminSupplierService {
    private final AdminSysUserService adminSysUserService;
    private final AdminSysUserRoleService adminSysUserRoleService;
    private final AdminSupplierMapper adminSupplierMapper;
    private final AdminStoreProductMapper adminStoreProductMapper;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Override
    public IPage<AdminSupplierResponseVo> listByPage(Page page) {
        return adminSupplierMapper.pageAll(page);
    }

    @Override
    public AdminSupplierResponseVoForEdit info(Long id) {
        AdminSupplier supplier = getOne(new QueryWrapper<AdminSupplier>().eq("id", id));
        AdminSupplierResponseVoForEdit info = new AdminSupplierResponseVoForEdit();
        BeanUtil.copyProperties(supplier,info);
        return info;
    }

    @Override
    public Boolean edit(AdminSupplier vo) {
        long userId = SecurityUtils.getUserId();;
        vo.setUpdateId(userId);
        vo.setUpdateTime(LocalDateTime.now());

        return adminSupplierMapper.updateByPrimaryKeySelective(vo);
    }

    @Override
    public Boolean changeStatus(Long supplierId, Integer status) {
        // todo 禁用后 新建产品时无法选择已禁用的供应商
        // todo 禁用之后供应商可以继续登录
        // todo DC 若产品库有产品为该供应商，无法禁用

        AdminStoreProduct result = adminStoreProductMapper.judgeSupplierHasProduct(supplierId);

        if (NumberUtil.equals(status, SupplierStateEnum.DISABLE.getStatus())){
            // 至少有一个产品属于该供应商，不能禁用
            if (null != result){
                log.warn("有产品属于该供应商，不能禁用，供应商id：{}",supplierId);
                return false;
            }
        }
        Long userId = SecurityUtils.getUserId();
        LocalDateTime now = LocalDateTime.now();
        return adminSupplierMapper.changeStatus(supplierId, status, userId, now);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addOneSupplier(AdminSupplierRequestVo vo) {
        long userNextId = IdGenerate.nextId();
        long supplierNextId = IdGenerate.nextId();

        AdminSupplier adminSupplier = vo.toAdminSupplier();
        adminSupplier.setId(supplierNextId);
        long userId = SecurityUtils.getUserId();;
        // 新建默认启用
        adminSupplier.setStatus(1);
        adminSupplier.setCreateId(userId);
        adminSupplier.setUpdateId(userId);
        adminSupplier.setCreateTime(LocalDateTime.now());
        adminSupplier.setUpdateTime(LocalDateTime.now());
        adminSupplier.setAdminUserId(userNextId);
        boolean save = save(adminSupplier);

        // 添加到amdin用户中
        AdminSysUser sysUser = new AdminSysUser();

        sysUser.setId(Long.toString(userNextId));
        sysUser.setUsername(adminSupplier.getPhone());
        sysUser.setPassword(ENCODER.encode(vo.getLoginPassword()));
        sysUser.setCreateTime(adminSupplier.getCreateTime());
        sysUser.setUpdateTime(adminSupplier.getUpdateTime());
        sysUser.setType(3);
        sysUser.setCustomerDataAuth(true);
        sysUser.setPurchaseDataAuth(true);
        sysUser.setDelFlag(CommonConstants.STATUS_NORMAL);
        sysUser.setSupplierId(supplierNextId);
        boolean result = adminSysUserService.save(sysUser);
        // 保存供应商角色
        adminSysUserRoleService.initUserRole(userNextId, AdminGlobalConstants.SRM_MENU_AUTH);
        return true;
    }


}
