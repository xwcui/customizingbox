package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysTenant;


/**
 * <p>
 * 租户管理 服务类
 * </p>
 */
public interface AdminSysTenantService extends IService<AdminSysTenant> {

}
