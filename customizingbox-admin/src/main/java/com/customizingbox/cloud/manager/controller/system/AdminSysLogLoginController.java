package com.customizingbox.cloud.manager.controller.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLogLogin;
import com.customizingbox.cloud.manager.service.admin.AdminSysLogLoginService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 登录日志
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/loglogin")
@Api(value = "loglogin", tags = "登录日志管理")
public class AdminSysLogLoginController {

    private final AdminSysLogLoginService adminSysLogLoginService;


    @ApiOperation(value = "分页列表")
    @GetMapping("/page")
    public ApiResponse getPage(Page page, AdminSysLogLogin adminSysLogLogin) {
        return ApiResponse.ok(adminSysLogLoginService.page(page, Wrappers.query(adminSysLogLogin)));
    }


    @ApiOperation(value = "登录日志查询")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:loglogin:get')")
    public ApiResponse getById(@PathVariable("id") String id) {
        return ApiResponse.ok(adminSysLogLoginService.getById(id));
    }


    @ApiOperation(value = "登录日志修改")
    @SysLog("修改登录日志")
    @PutMapping
    @PreAuthorize("@ato.hasAuthority('sys:loglogin:edit')")
    public ApiResponse updateById(@RequestBody AdminSysLogLogin adminSysLogLogin) {
        return ApiResponse.ok(adminSysLogLoginService.updateById(adminSysLogLogin));
    }


    @ApiOperation(value = "登录日志删除")
    @SysLog("删除登录日志")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:loglogin:del')")
    public ApiResponse removeById(@PathVariable String id) {
        return ApiResponse.ok(adminSysLogLoginService.removeById(id));
    }

}
