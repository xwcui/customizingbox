package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrder;
import com.customizingbox.cloud.common.datasource.model.srm.query.*;
import com.customizingbox.cloud.common.datasource.model.srm.vo.*;

import java.util.List;

/**
 * <p>
 * 采购订单表 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
public interface AdminPurchaseOrderService extends IService<AdminPurchaseOrder> {

    /**
     * 分页查询待下单列表
     * @param page
     * @param query
     * @return
     */
    IPage<AdminPurchaseOrderWaitVO> waitOrderPage(Page page, AdminPurchaseOrderWaitQuery query);

    /**
     * 向供应商下单
     * @param orderItemId
     * @return
     */
    Boolean placePurchaseOrder(String orderItemId);

    /**
     * 向供应商批量下单
     * @param orderItemIds
     * @return
     */
    Boolean placePurchaseOrders(List<Long> orderItemIds);

    /**
     * 采购进度汇总
     * @param page
     * @param query
     * @return
     */
    IPage<AdminPurchaseOrderCollectVO> purchaseCollect(Page page, AdminPurchaseOrderCollectQuery query);

    /**
     * 查询质检操作台 页面数据
     * @param onlyCode 唯一码
     */
    AdminPurchaseOrderCheckConsoleVO checkConsole(String onlyCode);
}
