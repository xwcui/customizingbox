package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOauthClient;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface AdminSysOauthClientService extends IService<AdminSysOauthClient> {

}
