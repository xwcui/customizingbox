package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.AdminCustomerDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerAppOrderQuery;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.CustomerStoreDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.CustomerStoreProductsDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerProductSaleParam;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.*;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerAppPandaOrderResponseVo;

import java.util.List;
import java.util.Map;

/**
 * 客户管理
 * @author DC_Li
 * @date 2022/4/15 10:32
 */
public interface AdminCustomerManageService {
    /**
     * 分页查询客户列表
     * @param page 分页参数
     * @param adminCustomerDto 查询条件参数
     * @param ip
     * @return
     */
    IPage<List<AdminCustomerResponseVo>> pageCustomers(Page page, AdminCustomerDto adminCustomerDto, String ip);

    /**
     * 查询单个app用户
     * @param email
     * @param appUserId
     * @return
     */
    AdminCustomerResponseVo getOneAppUser(String email, Long appUserId);

    /**
     * 认领客户
     * @param appUserId
     * @return 1 认领成功  2 客户不存在  3 客户已被认领
     */
    Integer claimCustomer(Long appUserId);

    /**
     * 分页查询用户店铺信息
     * @param page
     * @param customerStoreDto
     * @return
     */
    IPage<CustomerStoreResponseVo> getCustomerStores(Page<CustomerStoreResponseVo> page, CustomerStoreDto customerStoreDto);

    /**
     * 分页查询用户产品信息
     * @param page
     * @param dto 查询条件
     * @return
     */
    IPage<CustomerProductResponseVo> getCustomerProducts(Page<CustomerProductResponseVo> page, CustomerStoreProductsDto dto);

    /**
     * 查询单个产品的所有变体
     * @param productId 店铺产品id
     * @return
     */
    CustomerQuoteVo getCustomerProductDetail(Long productId);

    /**
     * 客户报价时查询产品库
     * @param spu 产品唯一标识
     * @return
     */
    CustomerQuoteAdminVo getAdminProductAndVariants(String spu);

    /**
     * 分页查询客户订单
     * @param page
     * @param query
     * @return
     */
    IPage<CustomerAppPandaOrderResponseVo> getCustomerStoreOrders(Page page, CustomerAppOrderQuery query);

    /**
     * 保存报价信息
     * @param requestVo
     * @return
     */
    Boolean saveQuote(List<SaveQuoteRequestVo> requestVo);

    /**
     * 移交客户经理
     * @param map key:客户id value:客户经理id
     * @return
     */
    Boolean transferCustomer(Map<Long, Long> map);

    /**
     * 查询客户商品销量采购量
     * @param page
     * @param param
     * @return
     */
    IPage<CustomerProductSaleResponseVo> getCustomerProductSale(Page page, CustomerProductSaleParam param);

    /**
     * 重新识别客户潘达订单
     * @param pandaOrderId
     * @return 当前订单数据
     */
    CustomerAppPandaOrderResponseVo reIdentifyPandaOrder(Long pandaOrderId);
}
