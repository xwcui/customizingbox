package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.ProductSaveOrUpdateDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.*;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

import java.util.List;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-31 09:34:35
 */
public interface AdminStoreProductService extends IService<AdminStoreProduct> {

    /**
     * 新增产品
     * @param productSaveOrUpdateDto
     * @param user
     * @return
     */
    ApiResponse saveProduct(ProductSaveOrUpdateDto productSaveOrUpdateDto, BaseUser user);

    /**
     * 获取spu或者sku
     * @return
     */
    String getSpu();

    /**
     * 有报价时将产品和变体标记为已报价
     */
    boolean updateAdminProductQuoteFlag(Long adminProductId, List<Long> variantId);

    /**
     * 修改产品
     * @param productSaveOrUpdateDto
     * @param user
     * @return
     */
    ApiResponse updateProduct(ProductSaveOrUpdateDto productSaveOrUpdateDto, BaseUser user);

    /**
     * 产品详情
     * @param id
     * @return
     */
    ApiResponse<ProductInfoVo> adminProductInfo(Long id);

    /**
     * 产品列表
     * @param productSearchVo
     * @param user
     * @param page
     * @return
     */
    IPage<ProductPageVo> adminProductPage(ProductSearchVo productSearchVo, BaseUser user, Page page);


    /**
     * 产品列表下变体列表
     * @param id
     * @return
     */
    ApiResponse<AdminProductPageVariantListVo> adminProductVariantList(Long id);

    /**
     * 删除产品
     * @param id
     * @param user
     * @return
     */
    ApiResponse delAdminProduct(Long id, BaseUser user);

    /**
     * 恢复产品
     * @param id
     * @param user
     * @return
     */
    ApiResponse recoveryAdminProduct(Long id, BaseUser user);

    /**
     * 产品上传赛盒
     * @param id
     * @return
     */
    String processProductsToSaihe(Long id);


    /**
     * 唯一码产品上传赛盒
     * @param onlyCodes
     * @return
     */
    ApiResponse processUniqueProductsToSaihe(List<String> onlyCodes);

}
