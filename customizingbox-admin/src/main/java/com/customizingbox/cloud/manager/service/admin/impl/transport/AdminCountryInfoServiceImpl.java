package com.customizingbox.cloud.manager.service.admin.impl.transport;
import java.time.LocalDateTime;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.TransportEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminCountryInfoMapper;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminCountryInfoDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminCountryInfo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsAttr;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminCountryInfoSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.AdminCountryInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 国家和地区表信息 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-14
 */
@Service
public class AdminCountryInfoServiceImpl extends ServiceImpl<AdminCountryInfoMapper, AdminCountryInfo> implements AdminCountryInfoService {

    @Resource
    private AdminCountryInfoMapper adminCountryInfoMapper;

    /**
     * 分页
     * @param page
     * @param searchVo
     * @return
     */
    @Override
    public IPage<AdminCountryInfo> pageAll(Page page, AdminCountryInfoSearchVo searchVo) {
        return adminCountryInfoMapper.pageAll(page,searchVo);

    }

    /**
     * /国家和地区新增或修改
     * @param adminCountryInfoDto
     * @param user
     * @return
     */
    @Override
    public ApiResponse saveOrUpdateAdminCountryInfoDto(AdminCountryInfoDto adminCountryInfoDto, BaseUser user) {
        AdminCountryInfo adminCountryInfo = new AdminCountryInfo();
        if (adminCountryInfoDto.getId() != null){
             adminCountryInfo = this.getById(adminCountryInfoDto.getId());
             if (adminCountryInfo == null){
                 return ApiResponse.failed("数据库中没有该数据");
             }
        }else {
            QueryWrapper<AdminCountryInfo> queryWrapper = new QueryWrapper<>();

            queryWrapper.and(wrapper -> wrapper.eq("name_en",adminCountryInfoDto.getNameEn() )
                    .or().eq("name_cn", adminCountryInfoDto.getNameCn())
                    .or().eq("code",adminCountryInfoDto.getCode())
            );
            Integer integer = adminCountryInfoMapper.selectCount(queryWrapper);
            if (integer > 0 ){
                return ApiResponse.failed("国家列表已存在该数据");
            }
            adminCountryInfo.setCreateTime(LocalDateTime.now());
            adminCountryInfo.setCreateId(Long.parseLong(user.getId()));
        }
        adminCountryInfo.setNameCn(adminCountryInfoDto.getNameCn());
        adminCountryInfo.setNameEn(adminCountryInfoDto.getNameEn());
        adminCountryInfo.setCode(adminCountryInfoDto.getCode());
        adminCountryInfo.setUpdateId(Long.parseLong(user.getId()));
        adminCountryInfo.setUpdateTime(LocalDateTime.now());
        this.saveOrUpdate(adminCountryInfo);
        return ApiResponse.ok();
    }

}
