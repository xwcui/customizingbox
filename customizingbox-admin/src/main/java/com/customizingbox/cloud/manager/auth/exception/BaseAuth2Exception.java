package com.customizingbox.cloud.manager.auth.exception;

import com.customizingbox.cloud.manager.auth.component.BaseAuth2ExceptionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * 自定义 OAuth2Exception
 */
@JsonSerialize(using = BaseAuth2ExceptionSerializer.class)
public class BaseAuth2Exception extends OAuth2Exception {

    @Getter
    private String errorCode;

    public BaseAuth2Exception(String msg) {
        super(msg);
    }

    public BaseAuth2Exception(String msg, String errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }
}
