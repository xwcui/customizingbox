package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysMenuMapper;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysRoleMenuMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRoleMenu;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysMenu;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminMenuVO;
import com.customizingbox.cloud.manager.service.admin.AdminSysMenuService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AdminSysMenuServiceImpl extends ServiceImpl<AdminSysMenuMapper, AdminSysMenu> implements AdminSysMenuService {

    private final AdminSysRoleMenuMapper adminSysRoleMenuMapper;

    @Override
    public List<AdminMenuVO> findMenuByRoleId(String roleId) {
        return baseMapper.listMenusByRoleId(roleId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse removeMenuById(String id) {
        // 查询父节点为当前节点的节点
        List<AdminSysMenu> menuList = this.list(Wrappers.<AdminSysMenu>query()
                .lambda().eq(AdminSysMenu::getParentId, id));
        if (CollUtil.isNotEmpty(menuList)) {
            return ApiResponse.failed("菜单含有下级不能删除");
        }

        //删除角色菜单关联
        adminSysRoleMenuMapper.delete(Wrappers.<AdminSysRoleMenu>query().lambda().eq(AdminSysRoleMenu::getMenuId, id));

        //删除当前菜单及其子菜单
        return ApiResponse.ok(this.removeById(id));
    }

    @Override
    public Boolean updateMenuById(AdminSysMenu sysMenu) {
        return this.updateById(sysMenu);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMenu(AdminSysMenu entity) {
        String roleId = entity.getRoleId();
        super.save(entity);
        if (StrUtil.isNotBlank(roleId)) {
            AdminSysRoleMenu adminSysRoleMenu = new AdminSysRoleMenu();
            adminSysRoleMenu.setRoleId(roleId);
            adminSysRoleMenu.setMenuId(entity.getId());
            adminSysRoleMenuMapper.insert(adminSysRoleMenu);
        }
    }
}
