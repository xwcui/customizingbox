package com.customizingbox.cloud.manager.controller.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRole;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUserRole;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import com.customizingbox.cloud.common.datasource.model.admin.system.param.AdminSysUsePageParam;
import com.customizingbox.cloud.common.datasource.model.admin.system.param.AdminSysUseSaveParam;
import com.customizingbox.cloud.common.datasource.model.admin.system.param.AdminSysUseUpdateParam;
import com.customizingbox.cloud.common.datasource.model.admin.system.param.AdminSysUserEnableParam;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUseDetailVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUserPageVO;
import com.customizingbox.cloud.manager.service.admin.AdminSysRoleService;
import com.customizingbox.cloud.manager.service.admin.AdminSysTenantService;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserRoleService;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/user")
@Api(value = "user", tags = "用户管理模块")
public class AdminSysUserController {

    private final AdminSysUserService adminSysUserService;
    private final AdminSysRoleService adminSysRoleService;
    private final AdminSysTenantService adminSysTenantService;
    private final AdminSysUserRoleService adminSysUserRoleService;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

//
//    @ApiOperation(value = "获取当前用户全部信息")
//    @GetMapping(value = {"/info"})
//    public ApiResponse info() {
//        String username = SecurityUtils.getUser().getUsername();
//        AdminSysUser user = adminSysUserService.getOne(Wrappers.<AdminSysUser>query()
//                .lambda().eq(AdminSysUser::getUsername, username));
//        if (user == null) {
//            return ApiResponse.failed("获取当前用户信息失败");
//        }
//        return ApiResponse.ok(adminSysUserService.findUserInfo(user));
//    }
//
//
//    @ApiOperation(value = "获取指定用户全部信息")
//    @GetMapping("/info/{username}")
//    public ApiResponse info(@PathVariable String username) {
//        AdminSysUser sysUser = new AdminSysUser();
//        sysUser.setUsername(username);
//        sysUser = adminSysUserService.getByNoTenant(sysUser);
//        if (sysUser == null) {
//            return ApiResponse.failed(String.format("用户信息为空 %s", username));
//        }
//
//        TenantContextHolder.setTenantId(sysUser.getTenantId());
//        if (CommonConstants.STATUS_NORMAL.equals(sysUser.getLockFlag())) {
//            //查询所属租户状态是否正常，否则禁止登录
//            AdminSysTenant adminSysTenant = adminSysTenantService.getById(sysUser.getTenantId());
//            if (CommonConstants.STATUS_LOCK.equals(adminSysTenant.getStatus())) {
//                sysUser.setLockFlag(CommonConstants.STATUS_LOCK);
//            }
//        }
//        AdminSysUserInfo adminSysUserInfo = adminSysUserService.findUserInfo(sysUser);
//        return ApiResponse.ok(adminSysUserInfo);
//    }
//
//
    @ApiOperation(value = "通过ID查询用户信息")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:user:get')")
    public ApiResponse<AdminSysUseDetailVO> user(@PathVariable String id) {
        return ApiResponse.ok(adminSysUserService.getUsersById(id));
    }
//
//
//    @ApiOperation(value = "根据用户名查询用户信息")
//    @GetMapping("/detail/{username}")
//    public ApiResponse userByUsername(@PathVariable String username) {
//        AdminSysUser sysUser = new AdminSysUser();
//        sysUser.setUsername(username);
//        return ApiResponse.ok(adminSysUserService.getByNoTenant(sysUser));
//    }


    @SysLog("删除用户信息")
    @GetMapping("/delete/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:user:del')")
    @ApiOperation(value = "删除用户", notes = "根据ID删除用户")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "int", paramType = "path")
    public ApiResponse userDel(@PathVariable String id) {
        //拥有管理员角色的用户不让删除
        AdminSysRole adminSysRole = adminSysRoleService.getOne(Wrappers.<AdminSysRole>update().lambda()
                .eq(AdminSysRole::getRoleCode, CommonConstants.ROLE_CODE_ADMIN));
        List<AdminSysUserRole> listAdminSysUserRole = adminSysUserRoleService.list(Wrappers.<AdminSysUserRole>update().lambda()
                .eq(AdminSysUserRole::getUserId, id).eq(AdminSysUserRole::getRoleId, adminSysRole.getId()));
        if (listAdminSysUserRole.size() > 0) {
            return ApiResponse.failed("无法删除拥有管理员角色的用户");
        }
        AdminSysUser sysUser = adminSysUserService.getById(id);
        return ApiResponse.ok(adminSysUserService.deleteUserById(sysUser));
    }

    @PostMapping("/updateStatus")
//    @PreAuthorize("@ato.hasAuthority('sys:user:forbidden')")
    @ApiOperation(value = "禁用用户", notes = "根据ID禁用用户")
    public ApiResponse userForbidden(@RequestBody AdminSysUserEnableParam adminSysUserEnableParam) {
        //拥有管理员角色的用户不让删除
        AdminSysRole adminSysRole = adminSysRoleService.getOne(Wrappers.<AdminSysRole>update().lambda()
                .eq(AdminSysRole::getRoleCode, CommonConstants.ROLE_CODE_ADMIN));
        List<AdminSysUserRole> listAdminSysUserRole = adminSysUserRoleService.list(Wrappers.<AdminSysUserRole>update().lambda()
                .eq(AdminSysUserRole::getUserId, adminSysUserEnableParam.getId()).eq(AdminSysUserRole::getRoleId, adminSysRole.getId()));
        if (listAdminSysUserRole.size() > 0) {
            return ApiResponse.failed("无法禁用拥有管理员角色的用户");
        }
        AdminSysUser sysUser = adminSysUserService.getById(adminSysUserEnableParam.getId());
        sysUser.setLockFlag(adminSysUserEnableParam.getStatus());
        return ApiResponse.ok(adminSysUserService.updateById(sysUser));
    }


    @ApiOperation(value = "添加用户")
    @SysLog("添加用户")
    @PostMapping("/save")
    @PreAuthorize("@ato.hasAuthority('sys:user:add')")
    public ApiResponse user(@RequestBody AdminSysUseSaveParam adminSysUseSaveParam) {
        try {
            AdminSysUserDTO adminSysUserDTO = BeanUtil.copyProperties(adminSysUseSaveParam, AdminSysUserDTO.class);
            return ApiResponse.ok(adminSysUserService.saveUser(adminSysUserDTO));
        } catch (DuplicateKeyException e) {
            if (e.getMessage().contains("uk_username")) {
                return ApiResponse.failed("用户名已被占用");
            } else if (e.getMessage().contains("uk_email")) {
                return ApiResponse.failed("邮箱已被占用");
            } else {
                return ApiResponse.failed(e.getMessage());
            }
        }
    }


    @ApiOperation(value = "更新用户信息")
    @SysLog("更新用户信息")
    @PostMapping("/edit")
    @PreAuthorize("@ato.hasAuthority('sys:user:edit')")
    public ApiResponse updateUser(@Valid @RequestBody AdminSysUseUpdateParam adminSysUseUpdateParam) {
        try {
            AdminSysUserDTO adminSysUserDTO = BeanUtil.copyProperties(adminSysUseUpdateParam, AdminSysUserDTO.class);
            //查询出管理员角色，判断管理员角色是否至少有1个用户
            AdminSysRole adminSysRole = adminSysRoleService.getOne(Wrappers.<AdminSysRole>update().lambda()
                    .eq(AdminSysRole::getRoleCode, CommonConstants.ROLE_CODE_ADMIN));
            if (!CollUtil.contains(adminSysUserDTO.getRoleIds(), adminSysRole.getId())) {
                List<AdminSysUserRole> listAdminSysUserRole = adminSysUserRoleService.list(Wrappers.<AdminSysUserRole>update().lambda()
                        .eq(AdminSysUserRole::getRoleId, adminSysRole.getId()));
                if (listAdminSysUserRole.size() <= 1) {
                    //只有一条记录，判断是否当前用户拥有
                    listAdminSysUserRole = adminSysUserRoleService.list(Wrappers.<AdminSysUserRole>update().lambda()
                            .eq(AdminSysUserRole::getRoleId, adminSysRole.getId()).eq(AdminSysUserRole::getUserId, adminSysUserDTO.getId()));
                    if (listAdminSysUserRole.size() > 0) {
                        return ApiResponse.failed("至少需要一个用户拥有管理员角色");
                    }
                }
            }
            return ApiResponse.ok(adminSysUserService.updateUser(adminSysUserDTO));
        } catch (DuplicateKeyException e) {
            if (e.getMessage().contains("uk_username")) {
                return ApiResponse.failed("用户名已被占用");
            } else if (e.getMessage().contains("uk_email")) {
                return ApiResponse.failed("邮箱已被占用");
            } else {
                return ApiResponse.failed(e.getMessage());
            }
        }
    }


//    @ApiOperation(value = "修改用户密码")
//    @SysLog("修改用户密码")
//    @PutMapping("/password")
//    @PreAuthorize("@ato.hasAuthority('sys:user:password')")
//    public ApiResponse editPassword(@Valid @RequestBody AdminSysUserDTO adminSysUserDto) {
//        AdminSysUser sysUser = new AdminSysUser();
//        sysUser.setId(adminSysUserDto.getId());
//        sysUser.setPassword(ENCODER.encode(adminSysUserDto.getPassword()));
//        adminSysUserService.updateById(sysUser);
//        return ApiResponse.ok();
//    }


    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    @PreAuthorize("@ato.hasAuthority('sys:user:index')")
    public ApiResponse<IPage<List<AdminSysUserPageVO>>> getUserPage(Page page, @RequestBody AdminSysUsePageParam param) {
        AdminSysUserDTO adminSysUserDTO = BeanUtil.copyProperties(param, AdminSysUserDTO.class);
        return ApiResponse.ok(adminSysUserService.getUsersWithRolePage(page, adminSysUserDTO));
    }


//    @ApiOperation(value = "数量查询")
//    @GetMapping("/count")
//    @PreAuthorize("@ato.hasAuthority('sys:user:index')")
//    public ApiResponse getCount(AdminSysUser sysUser) {
//        return ApiResponse.ok(adminSysUserService.count(Wrappers.query(sysUser)));
//    }


//    @ApiOperation(value = "修改个人信息")
//    @SysLog("修改个人信息")
//    @PutMapping("/edit")
//    public ApiResponse updateUserInfo(@Valid @RequestBody AdminSysUserDTO adminSysUserDto) {
//        return ApiResponse.ok(adminSysUserService.updateUserInfo(adminSysUserDto));
//    }
//
//
//    @ApiOperation(value = "查询")
//    @GetMapping("/ancestor/{username}")
//    public ApiResponse listAncestorUsers(@PathVariable String username) {
//        return ApiResponse.ok(adminSysUserService.listAncestorUsers(username));
//    }

}