package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysOrganRelationMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrgan;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrganRelation;
import com.customizingbox.cloud.manager.service.admin.AdminSysOrganRelationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AdminSysOrganRelationServiceImpl extends ServiceImpl<AdminSysOrganRelationMapper, AdminSysOrganRelation> implements AdminSysOrganRelationService {

    private final AdminSysOrganRelationMapper adminSysOrganRelationMapper;

    /**
     * 维护机构关系
     *
     * @param adminSysOrgan 机构
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertOrganRelation(AdminSysOrgan adminSysOrgan) {
        //增加机构关系表
        AdminSysOrganRelation condition = new AdminSysOrganRelation();
        condition.setDescendant(adminSysOrgan.getParentId());
        List<AdminSysOrganRelation> relationList = adminSysOrganRelationMapper
                .selectList(Wrappers.<AdminSysOrganRelation>query().lambda().eq(AdminSysOrganRelation::getDescendant, adminSysOrgan.getParentId()))
                .stream().peek(relation -> {
                    relation.setTenantId(null);
                    relation.setDescendant(adminSysOrgan.getId());
                }).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(relationList)) {
            this.saveBatch(relationList);
        }

        //自己也要维护到关系表中
        AdminSysOrganRelation own = new AdminSysOrganRelation();
        own.setDescendant(adminSysOrgan.getId());
        own.setAncestor(adminSysOrgan.getId());
        adminSysOrganRelationMapper.insert(own);
    }

    /**
     * 通过ID删除机构关系
     */
    @Override
    public void deleteAllOrganRelation(String id) {
        baseMapper.deleteOrganRelationsById(id);
    }

    /**
     * 更新机构关系
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateOrganRelation(AdminSysOrganRelation relation) {
        baseMapper.deleteOrganRelations(relation);
        List<AdminSysOrganRelation> relationList = baseMapper.listOrganRelations(relation).stream().map(relation2 -> {
            relation2.setTenantId(null);
            return relation2;
        }).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(relationList)) {
            this.saveBatch(relationList);
        }
    }

}
