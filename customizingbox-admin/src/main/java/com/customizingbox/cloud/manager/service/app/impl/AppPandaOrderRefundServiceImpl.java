package com.customizingbox.cloud.manager.service.app.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderItemMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderRefundMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysUserMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderRefund;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderRefundItem;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderCheckRefundParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderRefundApplyParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderRefundParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.*;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderRefundItemService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderRefundService;
import com.customizingbox.cloud.manager.service.app.AppSysUserTransactionRecordService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单退款表
 *
 * @author Y
 * @date 2022-04-23 09:55:23
 */
@Service
@AllArgsConstructor
@Slf4j
public class AppPandaOrderRefundServiceImpl extends ServiceImpl<AppPandaOrderRefundMapper, AppPandaOrderRefund> implements AppPandaOrderRefundService {

    private final AppPandaOrderRefundItemService appPandaOrderRefundItemService;
    private final AppPandaOrderMapper appPandaOrderMapper;
    private final AppPandaOrderItemMapper appPandaOrderItemMapper;
    private final AppSysUserMapper appSysUserMapper;
    private final AppSysUserTransactionRecordService appSysUserTransactionRecordService;

    @Override
    public AppPandaOrderRefundDetailVO apply(Long orderId) {
        AppPandaOrderRefundDetailVO appPandaOrderRefundDetailVO = baseMapper.apply(orderId);
        ApiException.isNullOrderException(appPandaOrderRefundDetailVO, "没有查询到订单");
        verifyCustomerDataAuth(appPandaOrderRefundDetailVO.getUserId());

        // 查询退款item里价格
        List<AppPandaOrderRefundItemDetailVO> refundItemDetailVOS = baseMapper.refundItemDetail(orderId);
        ApiException.isNullOrderException(refundItemDetailVOS, "没有查询到订单");

        // 查询订单退款/申请中的金额
        List<AppPandaOrderRefundItemAmountVO> refundAmountVOS = appPandaOrderRefundItemService.applyRefundAmount(orderId, OrderEnum.REFUND_STATUS.APPLY.getCode());
        if (!CollectionUtils.isEmpty(refundAmountVOS)) {
            Map<Long, BigDecimal> refundAmountMap = refundAmountVOS.stream().collect(Collectors.toMap(AppPandaOrderRefundItemAmountVO::getOrderItemId, AppPandaOrderRefundItemAmountVO::getAmount));
            refundItemDetailVOS.forEach(refundAmountVO -> {
                refundAmountVO.setWaitRefundAmount(refundAmountMap.get(refundAmountVO.getOrderItemId()));
            });
        }
        // 已退款
        appPandaOrderRefundDetailVO.setItemDetailVOS(refundItemDetailVOS);
        AppPandaOrder appPandaOrder = appPandaOrderMapper.selectById(orderId);
        appPandaOrderRefundDetailVO.setRefundFreightAmount(appPandaOrder.getRefundFreightAmount());
        appPandaOrderRefundDetailVO.setRefundVatAmount(appPandaOrder.getRefundVatAmount());
        // 申请中
        AppPandaOrderRefundVatFreightVO refundVatFreightVO = baseMapper.refundVatAndFreight(orderId, OrderEnum.REFUND_STATUS.APPLY.getCode() );
        if (!ObjectUtils.isEmpty(refundVatFreightVO)) {
            appPandaOrderRefundDetailVO.setWaitFreightAmount(refundVatFreightVO.getTotalFreightAmount());
            appPandaOrderRefundDetailVO.setWaitVatAmount(refundVatFreightVO.getTotalVatAmount());
        }
        return appPandaOrderRefundDetailVO;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean submitRefundApply(AppPandaOrderRefundApplyParam appPandaOrderRefundApplyParam) {
        // 校验订单状态
        Long orderId = appPandaOrderRefundApplyParam.getOrderId();
        AppPandaOrder appPandaOrder = appPandaOrderMapper.selectById(orderId);
        ApiException.isNullOrderException(appPandaOrder, "没有查询到订单");
        if (!OrderEnum.PAY_STATUS.SUCCESS_PAY.getCode().equals(appPandaOrder.getPayStatus())) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "订单状态异常");
        }
        // 退款中和已经退款的金额的金额
        Map<Long, BigDecimal> dbRefundItemAmountMap = getAmountByStatus(orderId, -1);
        verifyVatAndFreight(appPandaOrderRefundApplyParam, appPandaOrder);
        // 获取退款表
        AppPandaOrderRefund appPandaOrderRefund = getAppPandaOrderRefund(appPandaOrderRefundApplyParam, appPandaOrder.getUserId());

        List<AppPandaOrderRefundItemAmountVO> itemRefundAmounts = appPandaOrderRefundApplyParam.getRefundAmountParam();

        BigDecimal totalRefundAmount = BigDecimal.ZERO;
        // 退款订单item, 先放到即合理, 全部校验完成后再保存
        List<AppPandaOrderRefundItem> saveList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(itemRefundAmounts)) {
            for (AppPandaOrderRefundItemAmountVO itemRefundAmount : itemRefundAmounts) {
                Long orderItemId = itemRefundAmount.getOrderItemId();
                // 本次退款金额
                BigDecimal applyAmount = itemRefundAmount.getAmount();
                // 历史退款金额
                BigDecimal dbRefundAmount = dbRefundItemAmountMap.getOrDefault(orderItemId, BigDecimal.ZERO);
                BigDecimal refundAmount = applyAmount.add(dbRefundAmount);

                AppPandaOrderItem appPandaOrderItem = appPandaOrderItemMapper.selectById(orderItemId);
                ApiException.isNullOrderException(appPandaOrderItem, "没有查询到订单");
                if (!appPandaOrderItem.getOrderId().equals(orderId)) {
                    throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "申请退款数据异常");
                }

                // 订单支付金额
                BigDecimal payAmount = appPandaOrderItem.getPayAmount();

                if (refundAmount.compareTo(payAmount) > 0) {
                    throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "退款金额(含申请中的金额)大于等于支付金额, 不能申请退款");
                }
                // 插入数据
                AppPandaOrderRefundItem appPandaOrderRefundItem = new AppPandaOrderRefundItem();
                appPandaOrderRefundItem.setOrderId(orderId);
                appPandaOrderRefundItem.setRefundId(appPandaOrderRefund.getId());
                appPandaOrderRefundItem.setOrderItemId(orderItemId);
                appPandaOrderRefundItem.setAmount(applyAmount);
                appPandaOrderRefundItem.setAppUserId(appPandaOrder.getUserId());
                saveList.add(appPandaOrderRefundItem);
                totalRefundAmount = totalRefundAmount.add(applyAmount);
            }
        }
        totalRefundAmount = totalRefundAmount.add(appPandaOrderRefundApplyParam.getVatAmount()).add(appPandaOrderRefundApplyParam.getFreightAmount());
        if (totalRefundAmount.compareTo(appPandaOrderRefundApplyParam.getTotalAmount()) != 0) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "支付金额异常");
        }
        this.save(appPandaOrderRefund);
        for (AppPandaOrderRefundItem appPandaOrderRefundItem : saveList) {
            appPandaOrderRefundItem.setRefundId(appPandaOrderRefund.getId());
            appPandaOrderRefundItemService.save(appPandaOrderRefundItem);
        }
        return true;
    }

    @Override
    public IPage<AppPandaOrderRefundVO> refundPage(Page page, AppPandaOrderRefundParam appPandaOrderRefundParam) {
        BaseUser user = SecurityUtils.getUser();
        if (!user.getIsAdministrator()) {
            if (ObjectUtils.isEmpty(appPandaOrderRefundParam)) {
                appPandaOrderRefundParam = new AppPandaOrderRefundParam();
            }
            appPandaOrderRefundParam.setLoginUserId(Long.valueOf(user.getId()));
        }
        IPage<AppPandaOrderRefundVO> resultPage =  baseMapper.refundPage(page, appPandaOrderRefundParam);
        if (user.getIsAdministrator()) {
            // 管理员, 授予审核权限
            if (!CollectionUtils.isEmpty(resultPage.getRecords())) {
                resultPage.getRecords().stream().forEach(e -> e.setCheckStatus(1));
            }
        }

        return resultPage;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean checkRefundApply(AppPandaOrderCheckRefundParam param) {
        Long userId = SecurityUtils.getUserId();
        Integer checkStatus = param.getStatus();
        Long refundId = param.getId();
        log.info("用户: {}, 开始审核, 数据: {}", userId, JSONObject.toJSONString(param));
        if (!SecurityUtils.getUser().getIsAdministrator()) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "只有管理员才能审核订单");
        }

        AppPandaOrderRefund appPandaOrderRefund = this.getById(refundId);
        ApiException.isNullOrderException(appPandaOrderRefund, "没有找到需要审核的订单");

        if (!OrderEnum.REFUND_STATUS.APPLY.getCode().equals(appPandaOrderRefund.getStatus())) {
           throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "该审核已经通过或者拒绝");
        }
        appPandaOrderRefund.setCheckTime(LocalDateTime.now());
        appPandaOrderRefund.setAdminAssessorId(SecurityUtils.getUserId());
        if (OrderEnum.REFUND_STATUS.REJECT_REFUND.getCode().equals(checkStatus)) {
            // 拒绝退款, 直接修改退款订单状态
            appPandaOrderRefund.setStatus(checkStatus);
            appPandaOrderRefund.setRejectRemark(param.getMark());
            this.updateById(appPandaOrderRefund);
        } else {
            log.info("同意退款, 开始退款流程");
            // 同意退款, 这里就很麻烦,
            List<AppPandaOrderRefundItem> appPandaOrderRefundItems = appPandaOrderRefundItemService.getByRefundId(refundId);
            ApiException.isNullOrderException(appPandaOrderRefundItems, "退款订单数据异常");
            // 同意退款, 修改子订单里的退款金额
            BigDecimal itemRefundTotalAmount = updateOrderItemRefundAmount(appPandaOrderRefundItems, appPandaOrderRefund);
            // 修改父订单退款金额
            Boolean updateOrderResult = updateOrderRefundAmount(appPandaOrderRefund, itemRefundTotalAmount);
            ApiException.isNullOrderException(updateOrderResult, "修改订单数据异常");
            // 开始退款
            log.info("订单开始退款: {}" + appPandaOrderRefund.getId());
            Long transactionRecordId = appSysUserTransactionRecordService.refund(appPandaOrderRefund.getAmount(), appPandaOrderRefund.getAppUserId()
                    , appPandaOrderRefund.getOrderId(), appPandaOrderRefund.getId(), "订单退款");
            appPandaOrderRefund.setTransactionRecordId(transactionRecordId);
            appPandaOrderRefund.setStatus(OrderEnum.REFUND_STATUS.ALL_REFUND.getCode());
            this.updateById(appPandaOrderRefund);
        }
        return true;
    }

    @Override
    public AppPandaOrderRefundDetailVO detail(Long refundId) {
        AppPandaOrderRefund appPandaOrderRefund = this.getById(refundId);
        Long orderId = appPandaOrderRefund.getOrderId();
        AppPandaOrderRefundDetailVO appPandaOrderRefundDetailVO = baseMapper.apply(appPandaOrderRefund.getOrderId());
        ApiException.isNullOrderException(appPandaOrderRefundDetailVO, "没有查询到订单");
        verifyCustomerDataAuth(appPandaOrderRefundDetailVO.getUserId());

        // 查询退款item里价格
        List<AppPandaOrderRefundItemDetailVO> refundItemDetailVOS = baseMapper.refundItemDetail(orderId);
        ApiException.isNullOrderException(refundItemDetailVOS, "没有查询到订单");
        appPandaOrderRefundDetailVO.setItemDetailVOS(refundItemDetailVOS);
        return appPandaOrderRefundDetailVO;
    }

    @Override
    public AppPandaOrderCheckRefundDetailVO refundCheck(Long refundId) {
        AppPandaOrderRefund appPandaOrderRefund = this.getById(refundId);
        ApiException.isNullOrderException(appPandaOrderRefund, "没有查询到订单");
        verifyCustomerDataAuth(appPandaOrderRefund.getAppUserId());
        AppPandaOrderRefundDetailVO refundDetailVO = baseMapper.apply(appPandaOrderRefund.getOrderId());
        // 拷贝店铺等基础属性
        AppPandaOrderCheckRefundDetailVO checkRefundDetailVO = BeanUtil.copyProperties(refundDetailVO, AppPandaOrderCheckRefundDetailVO.class);

        checkRefundDetailVO.setApplyVatAmount(appPandaOrderRefund.getVatAmount());
        checkRefundDetailVO.setApplyFreightAmount(appPandaOrderRefund.getFreightAmount());
        checkRefundDetailVO.setApplyTotalAmount(appPandaOrderRefund.getAmount());
        checkRefundDetailVO.setStatus(appPandaOrderRefund.getStatus());
        checkRefundDetailVO.setTransactionRecordId(appPandaOrderRefund.getTransactionRecordId());
        checkRefundDetailVO.setApplyRemark(appPandaOrderRefund.getApplyRemark());
        checkRefundDetailVO.setRejectRemark(appPandaOrderRefund.getRejectRemark());

        // 查询历史退款备注
        // 查询退款item里价格
        List<AppPandaOrderRefundItemDetailVO> refundItemDetailVOS = baseMapper.checkRefundItemDetail(refundId);
        ApiException.isNullOrderException(refundItemDetailVOS, "没有查询到订单");
        checkRefundDetailVO.setItemDetailVOS(refundItemDetailVOS);
        return checkRefundDetailVO;
    }

    /**
     * 修改父订单退款金额
     * @param appPandaOrderRefund
     * @param itemRefundTotalAmount
     */
    private Boolean updateOrderRefundAmount(AppPandaOrderRefund appPandaOrderRefund, BigDecimal itemRefundTotalAmount) {
        // 和app panda 订单
        Long orderId = appPandaOrderRefund.getOrderId();
        AppPandaOrder appPandaOrder = appPandaOrderMapper.selectById(orderId);
        ApiException.isNullOrderException(appPandaOrder, "没有找到支付的订单");

        BigDecimal oldRefuntVatAmount = appPandaOrder.getRefundVatAmount();
        BigDecimal oldRefuntFreightAmount = appPandaOrder.getRefundFreightAmount();
        BigDecimal currentVatAmount = appPandaOrderRefund.getVatAmount();
        BigDecimal currentFreightAmount = appPandaOrderRefund.getFreightAmount();

        BigDecimal totalRefuntVatAmount = currentVatAmount.add(oldRefuntVatAmount);
        BigDecimal totalRefuntFreightAmount = currentFreightAmount.add(oldRefuntFreightAmount);

        BigDecimal payVatAmount = appPandaOrder.getVatAmount();
        BigDecimal payFreightAmount = appPandaOrder.getFreightAmount();

        // 校验vat 和运费 退款金额和支付金额
        if (totalRefuntVatAmount.compareTo(payVatAmount) > 0) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "订单VAT退款金额超过支付金额");
        }

        if (totalRefuntFreightAmount.compareTo(payFreightAmount) > 0) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "运费退款金额超过支付金额");
        }
        // 校验总金额
        BigDecimal applyTotalRefund = itemRefundTotalAmount.add(currentVatAmount).add(totalRefuntFreightAmount);
        if (applyTotalRefund.compareTo(appPandaOrderRefund.getAmount()) != 0) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "退款金额数据异常");
        }

        if (appPandaOrderRefund.getAmount().compareTo(appPandaOrder.getPayAmount()) > 0) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "退款总金额大于支付总金额");
        } else if (appPandaOrderRefund.getAmount().compareTo(appPandaOrder.getPayAmount()) == 0) {
            appPandaOrder.setRefundStatus(OrderEnum.REFUND_STATUS.ALL_REFUND.getCode());
        } else  {
            appPandaOrder.setRefundStatus(OrderEnum.REFUND_STATUS.PORTION.getCode());
        }

        appPandaOrder.setRefundTotalAmount(appPandaOrderRefund.getAmount());
        int result = appPandaOrderMapper.updateById(appPandaOrder);
        return result == 1;
    }

    /**
     * 同意退款, 修改子订单里的退款金额
     * @param appPandaOrderRefundItems
     * @param appPandaOrderRefund
     * @return 子订单总退款金额
     */
    private BigDecimal updateOrderItemRefundAmount(List<AppPandaOrderRefundItem> appPandaOrderRefundItems, AppPandaOrderRefund appPandaOrderRefund) {
        // 子订单退款总金额, 用于和总退款金额数据校验, 防止数据紊乱
        BigDecimal itemRefundTotalAmount = BigDecimal.ZERO;
        for (AppPandaOrderRefundItem appPandaOrderRefundItem :appPandaOrderRefundItems) {
            BigDecimal amount = appPandaOrderRefundItem.getAmount();
            if (ObjectUtils.isEmpty(amount) || BigDecimal.ZERO.compareTo(amount) >= 0) {
                // 金额为0不做退款操作
                continue;
            }
            Long orderItemId = appPandaOrderRefundItem.getOrderItemId();
            AppPandaOrderItem appPandaOrderItem = appPandaOrderItemMapper.selectById(orderItemId);
            ApiException.isNullOrderException(appPandaOrderRefundItems, "子订单数据异常");

            // 历史退款金额
            BigDecimal oldRefundAmount = appPandaOrderItem.getRefundAmount();

            BigDecimal payAmount = appPandaOrderItem.getPayAmount();
            BigDecimal totalRefundAmount = oldRefundAmount.add(amount);
            if (totalRefundAmount.compareTo(payAmount) > 0) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "子订单退款金额超过子订单支付金额");
            } else if (totalRefundAmount.compareTo(payAmount) == 0) {
                appPandaOrderItem.setRefundStatus(OrderEnum.REFUND_STATUS.ALL_REFUND.getCode());
            } else {
                appPandaOrderItem.setRefundStatus(OrderEnum.REFUND_STATUS.PORTION.getCode());
            }
            log.info("给用户退款, 退款订单item: {}, 金额: {}, 支付金额: {}, 历史退款金额: {}",
                    appPandaOrderRefundItem.getId(), amount, payAmount, oldRefundAmount);

            appPandaOrderItem.setRefundAmount(totalRefundAmount);
            appPandaOrderItemMapper.updateById(appPandaOrderItem);
            itemRefundTotalAmount = itemRefundTotalAmount.add(amount);
        }
        return itemRefundTotalAmount;
    }

    /**
     * 创建AppPandaOrderRefund实体
     */
    private AppPandaOrderRefund getAppPandaOrderRefund(AppPandaOrderRefundApplyParam appPandaOrderRefundApplyParam, Long userId) {
        AppPandaOrderRefund appPandaOrderRefund = new AppPandaOrderRefund();
        appPandaOrderRefund.setOrderId(appPandaOrderRefundApplyParam.getOrderId());
        appPandaOrderRefund.setAmount(appPandaOrderRefundApplyParam.getTotalAmount());
        appPandaOrderRefund.setFreightAmount(appPandaOrderRefundApplyParam.getFreightAmount());
        appPandaOrderRefund.setVatAmount(appPandaOrderRefundApplyParam.getVatAmount());
        appPandaOrderRefund.setApplyRemark(appPandaOrderRefundApplyParam.getApplyRemark());
        appPandaOrderRefund.setCreateId(SecurityUtils.getUserId());
        appPandaOrderRefund.setAppUserId(userId);
        appPandaOrderRefund.setStatus(OrderEnum.REFUND_STATUS.APPLY.getCode());
        return appPandaOrderRefund;
    }

    /**
     * 校验运费和vat
     */
    private void verifyVatAndFreight(AppPandaOrderRefundApplyParam appPandaOrderRefundApplyParam
            , AppPandaOrder appPandaOrder) {
        AppPandaOrderRefundAmountVO dbOrderRefundAmount = baseMapper.queryOrderRefundAmount(appPandaOrder.getId(), -1);
        BigDecimal applyFreightAmount = appPandaOrderRefundApplyParam.getFreightAmount();
        if (!ObjectUtils.isEmpty(applyFreightAmount)) {
            if (applyFreightAmount.add(dbOrderRefundAmount.getFreightAmount()).compareTo(appPandaOrder.getFreightAmount()) > 0) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "运费金额(含申请中的金额)大于等于支付金额, 不能申请退款");
            }
        }
        BigDecimal applyVatAmount = appPandaOrderRefundApplyParam.getVatAmount();
        if (!ObjectUtils.isEmpty(applyVatAmount)) {
            if (applyVatAmount.add(dbOrderRefundAmount.getVatAmount()).compareTo(appPandaOrder.getVatAmount()) > 0) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "VAT金额(含申请中的金额)大于等于支付金额, 不能申请退款");
            }
        }
    }

    private Map<Long, BigDecimal> getAmountByStatus(Long orderId, Integer status) {
        List<AppPandaOrderRefundItemAmountVO> successRefundAmountVOS = appPandaOrderRefundItemService.applyRefundAmount(orderId, status);
        Map<Long, BigDecimal> map;
        if (!CollectionUtils.isEmpty(successRefundAmountVOS)) {
            map = successRefundAmountVOS.stream().collect(Collectors.toMap(AppPandaOrderRefundItemAmountVO::getOrderItemId, AppPandaOrderRefundItemAmountVO::getAmount));
        } else {
            map = new HashMap<>();
        }
        return map;
    }

    /**
     * 校验当前用户是否有查看指定用户数据的权限
     * @param userId  需要查看用户的id
     */
    private void verifyCustomerDataAuth(Long userId) {
        BaseUser user = SecurityUtils.getUser();
        Boolean customerDataAuth = user.getCustomerDataAuth();

        List<AppSysUser> appSysUsers = appSysUserMapper.selectList(Wrappers.<AppSysUser>lambdaQuery().eq(AppSysUser::getAdminUserId, SecurityUtils.getUserId()).select(AppSysUser::getId));
        if (customerDataAuth) {
            // 只能看自己的
            ApiException.isNullOrderException(appSysUsers, "不能操作未关联的客户订单");
            List<String> collect = appSysUsers.stream().map(AppSysUser::getId).collect(Collectors.toList());
            if (!collect.contains(userId)) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "不能操作未关联的客户订单");
            }
        }
    }
}
