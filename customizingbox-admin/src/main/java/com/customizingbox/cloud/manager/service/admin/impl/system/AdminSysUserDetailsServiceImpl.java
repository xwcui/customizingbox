package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.constant.SecurityConstants;
import com.customizingbox.cloud.common.core.constant.enums.RoleEnum;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysUserInfo;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户详细信息
 */
@Slf4j
@Service
@AllArgsConstructor
public class AdminSysUserDetailsServiceImpl implements UserDetailsService {

    private final CacheManager cacheManager;
    private final AdminSysUserService adminSysUserService;

    /**
     * 用户密码登录
     */
    @Override
    @SneakyThrows
    public UserDetails loadUserByUsername(String username) {
        //查询缓存中是否有此用户信息，有则直接返回
        Cache cache = cacheManager.getCache(CacheConstants.ADMIN_USER_CACHE);
//        if (cache != null && cache.get(username) != null) {
//            return (BaseUser) cache.get(username).get();
//        }
        //缓存中无此用户信息
        AdminSysUserInfo adminSysUserInfo = this.getUserinfo(username);
        UserDetails userDetails = this.getUserDetails(adminSysUserInfo);
        if (userDetails.isAccountNonLocked()) {
            //合法用户，放入缓存
            if (!ObjectUtils.isEmpty(cache)) {
                cache.put(username, userDetails);
            }
        }
        return userDetails;
    }


    private AdminSysUserInfo getUserinfo(String username) {
        AdminSysUser sysUser = new AdminSysUser();
        sysUser.setUsername(username);
        sysUser = adminSysUserService.getByNoTenant(sysUser);
        if (sysUser == null) {
            return null;
        }

        TenantContextHolder.setTenantId(sysUser.getTenantId());
        return adminSysUserService.findUserInfo(sysUser);
    }

    /**
     * 构建 userDetails
     *
     * @param adminSysUserInfo 用户信息
     */
    private UserDetails getUserDetails(AdminSysUserInfo adminSysUserInfo) {
        if (adminSysUserInfo == null) {
            throw new UsernameNotFoundException("用户不存在");
        }

        Set<String> dbAuthsSet = new HashSet<>();
        Boolean isAdministrator = false;
        if (ArrayUtil.isNotEmpty(adminSysUserInfo.getRoles())) {
            // 获取资源
            dbAuthsSet.addAll(Arrays.asList(adminSysUserInfo.getPermissions()));
            // 获取角色
            for (String roleId : adminSysUserInfo.getRoles()) {
                dbAuthsSet.add(SecurityConstants.ROLE + roleId);
                if (RoleEnum.ADMINISTRATOR.getCode().equals(roleId)) {
                    isAdministrator = true;
                }
            }
        }

        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(dbAuthsSet.toArray(new String[0]));
        AdminSysUser user = adminSysUserInfo.getSysUser();
        boolean enabled = StrUtil.equals(user.getLockFlag(), CommonConstants.STATUS_NORMAL);
        Boolean customerDataAuth = user.getCustomerDataAuth();
        Boolean purchaseDataAuth = user.getPurchaseDataAuth();
        String shCode = user.getShCode();
        // 构造security用户
        return new BaseUser(user.getId(), user.getOrganId(), user.getTenantId(), user.getUsername(),
                SecurityConstants.BCRYPT + user.getPassword(), customerDataAuth, purchaseDataAuth, isAdministrator, shCode,
                enabled, true, true, CommonConstants.STATUS_NORMAL.equals(user.getLockFlag()), authorities);
    }
}
