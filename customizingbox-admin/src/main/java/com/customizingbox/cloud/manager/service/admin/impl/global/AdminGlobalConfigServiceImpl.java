package com.customizingbox.cloud.manager.service.admin.impl.global;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.global.AdminGlobalConfigMapper;
import com.customizingbox.cloud.common.datasource.model.admin.global.AdminGlobalConfig;
import com.customizingbox.cloud.manager.service.admin.AdminGlobalConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * admin全局配置 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-05-09
 */
@Service
public class AdminGlobalConfigServiceImpl extends ServiceImpl<AdminGlobalConfigMapper, AdminGlobalConfig> implements AdminGlobalConfigService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @Override
    public String getValueByKey(String key) {
        AdminGlobalConfig config = this.getOne(Wrappers.<AdminGlobalConfig>lambdaQuery().eq(AdminGlobalConfig::getConfigKey, key));
        return config.getConfigValue();
    }

    @Override
    public String getValueByCache(String key) {
        String value = stringRedisTemplate.opsForValue().get(key);
        if (ObjectUtils.isEmpty(value)) {
            value = this.getValueByKey(key);
            if (!ObjectUtils.isEmpty(value)) {
                stringRedisTemplate.opsForValue().set(key, value, 1, TimeUnit.HOURS);
            }
        }
        return value;
    }
}
