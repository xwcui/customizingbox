package com.customizingbox.cloud.manager.service.admin.impl.srm;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderItemMapper;
import com.customizingbox.cloud.common.datasource.mapper.srm.AdminPurchaseOrderMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminSupplier;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductPropertiesVO;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrder;
import com.customizingbox.cloud.common.datasource.model.srm.query.*;
import com.customizingbox.cloud.common.datasource.model.srm.vo.*;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 采购订单表 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
@Service
@AllArgsConstructor
@Slf4j
public class AdminPurchaseOrderServiceImpl extends ServiceImpl<AdminPurchaseOrderMapper, AdminPurchaseOrder> implements AdminPurchaseOrderService {

    private final AdminStoreProductImgDepotService adminStoreProductImgDepotService;
    private final AdminStoreProductAttrService adminStoreProductAttrService;
    private final AdminStoreProductService adminStoreProductService;
    private final AdminStoreProductVariantService adminStoreProductVariantService;
    private final AppPandaOrderItemMapper appPandaOrderItemMapper;
    private final AdminSupplierService adminSupplierService;
    private final AdminPurchaseOrderDetectionRecordService adminPurchaseOrderDetectionRecordService;

    @Override
    public IPage<AdminPurchaseOrderWaitVO> waitOrderPage(Page page, AdminPurchaseOrderWaitQuery query) {
        if (ObjectUtils.isEmpty(query)) {
            query = new AdminPurchaseOrderWaitQuery();
        }
        if (SecurityUtils.getUser().getPurchaseDataAuth()) {
            // 只能查看自己采购采购的数据
            query.setPurchaseName(SecurityUtils.getUser().getUsername());
        }

        query.setPayStatus(OrderEnum.PAY_STATUS.SUCCESS_PAY.getCode());

        IPage<AdminPurchaseOrderWaitVO> resultPage = baseMapper.waitOrderPage(page, query);
        if (!ObjectUtils.isEmpty(resultPage.getRecords())) {
            resultPage.getRecords().stream().forEach(waitOrder -> {
                // 这里组装规格, key 就是规格值, value 就是规格名
                JSONArray attrValues = JSONObject.parseArray(waitOrder.getAttrValues());
                waitOrder.setProductAttr(adminStoreProductAttrService.queryAttrByProductId(waitOrder.getProductId(), attrValues));
            });
        }
        return resultPage;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean placePurchaseOrder(String orderItemId) {
        AppPandaOrderItem appPandaOrderItem = appPandaOrderItemMapper.selectById(orderItemId);
        return placePurchaseOrder(appPandaOrderItem);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean placePurchaseOrders(List<Long> orderItemIds) {
        for (Long orderItemId : orderItemIds) {
            AppPandaOrderItem appPandaOrderItem = appPandaOrderItemMapper.selectById(orderItemId);
            placePurchaseOrder(appPandaOrderItem);
        }
        return true;
    }

    @Override
    public IPage<AdminPurchaseOrderCollectVO> purchaseCollect(Page page, AdminPurchaseOrderCollectQuery query) {
        if (ObjectUtils.isEmpty(query)) {
            query = new AdminPurchaseOrderCollectQuery();
        }
        if (SecurityUtils.getUser().getPurchaseDataAuth()) {
            // 只能查看自己采购采购的数据
            query.setPurchaseName(SecurityUtils.getUser().getUsername());
        }
        query.setPayStatus(OrderEnum.PAY_STATUS.SUCCESS_PAY.getCode());

        IPage<AdminPurchaseOrderCollectVO> resultPage = baseMapper.purchaseCollect(page, query);
        if (!ObjectUtils.isEmpty(resultPage.getRecords())) {
            resultPage.getRecords().stream().forEach(waitOrder -> {
                // 这里组装规格, key 就是规格值, value 就是规格名
                JSONArray attrValues = JSONObject.parseArray(waitOrder.getAttrValues());
                waitOrder.setProductAttr(adminStoreProductAttrService.queryAttrByProductId(waitOrder.getProductId(), attrValues));
            });
        }
        return resultPage;
    }

    @Override
    public AdminPurchaseOrderCheckConsoleVO checkConsole(String onlyCode) {
        AdminPurchaseOrderCheckConsoleVO purchaseOrderCheckConsoleVO = baseMapper.checkConsole(onlyCode);
        if (ObjectUtils.isEmpty(purchaseOrderCheckConsoleVO)) {
            return null;
        }
        JSONArray attrValues = JSONObject.parseArray(purchaseOrderCheckConsoleVO.getAttrValues());
        purchaseOrderCheckConsoleVO.setProductAttr(adminStoreProductAttrService.queryAttrByProductId(purchaseOrderCheckConsoleVO.getProductId(), attrValues));
        String properties = purchaseOrderCheckConsoleVO.getProperties();
        if (!StringUtils.isEmpty(properties)) {
            List<AppStoreProductPropertiesVO> textProperties = new ArrayList<>();
            List<AppStoreProductPropertiesVO> imgProperties = new ArrayList<>();

            // 区分自定义文字和图片
            if (!StringUtils.isEmpty(properties)) {
                for (Object obj : JSONObject.parseArray(properties)) {
                    JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                    String name = String.valueOf(jsonObject.get("name"));
                    String value = String.valueOf(jsonObject.get("value"));
                    AppStoreProductPropertiesVO productPropertiesVO = new AppStoreProductPropertiesVO();
                    productPropertiesVO.setName(name);
                    productPropertiesVO.setValue(value);
                    if (value.startsWith("http://") || value.startsWith("https://")) {
                        imgProperties.add(productPropertiesVO);
                    } else {
                        textProperties.add(productPropertiesVO);
                    }
                }
            }
            purchaseOrderCheckConsoleVO.setTextProperties(textProperties);
            purchaseOrderCheckConsoleVO.setImgProperties(imgProperties);
        }


        // 查历史质检记录
        List<AdminPurchaseOrderDetectionRecordVO> detectionRecordVO = adminPurchaseOrderDetectionRecordService.historyRecord(purchaseOrderCheckConsoleVO.getPurchaseOrderId());
        purchaseOrderCheckConsoleVO.setDetectionRecords(detectionRecordVO);
        return purchaseOrderCheckConsoleVO;
    }

    /**
     * 下单保存数据
     * @param appPandaOrderItem
     * @return
     */
    private Boolean placePurchaseOrder(AppPandaOrderItem appPandaOrderItem) {
        ApiException.isNullOrderException(appPandaOrderItem, "订单没有找到");

        if (!OrderEnum.PURCHASE_ORDER_STATUS.WAIT_ORDER.getCode().equals(appPandaOrderItem.getPurchaseStatus())
                || !OrderEnum.PURCHASE_ORDER_STATUS.PURCHASE_REJECT.getCode().equals(appPandaOrderItem.getPurchaseStatus())) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "订单状态异常");
        }
        AdminStoreProductVariant adminStoreProductVariant = adminStoreProductVariantService.getById(appPandaOrderItem.getVariantId());
        AdminStoreProduct adminStoreProduct = adminStoreProductService.getById(appPandaOrderItem.getProductId());
        AdminStoreProductImgDepot imgDepot = adminStoreProductImgDepotService.getById(adminStoreProductVariant.getImageId());
        AdminSupplier adminSupplier = adminSupplierService.getById(adminStoreProduct.getSupplierId());
        AdminPurchaseOrder adminPurchaseOrder = new AdminPurchaseOrder();
        adminPurchaseOrder.setVariantId(appPandaOrderItem.getVariantId());
        adminPurchaseOrder.setOnlyCode(appPandaOrderItem.getOnlyCode());
        adminPurchaseOrder.setOrderId(appPandaOrderItem.getOrderId());
        adminPurchaseOrder.setOrderItemId(appPandaOrderItem.getId());
        adminPurchaseOrder.setAttrValues(adminStoreProductVariant.getAttrValues());
        adminPurchaseOrder.setProperties(appPandaOrderItem.getProperties());
        if (!ObjectUtils.isEmpty(imgDepot)) {
            adminPurchaseOrder.setImgSrc(imgDepot.getSrc());
        }
        adminPurchaseOrder.setQuantity(appPandaOrderItem.getFulfillableQuantity());
        adminPurchaseOrder.setCostPrice(adminStoreProductVariant.getCostPrice());
        adminPurchaseOrder.setTotalCostPrice(adminStoreProductVariant.getCostPrice().multiply(BigDecimal.valueOf(appPandaOrderItem.getFulfillableQuantity())));
        adminPurchaseOrder.setStatus(OrderEnum.PURCHASE_ORDER_STATUS.PLACE_ORDER.getCode());
        adminPurchaseOrder.setAdminPurchaseUserId(adminSupplier.getAdminPurchaseUserId());
        adminPurchaseOrder.setSupplierId(adminSupplier.getId());
        adminPurchaseOrder.setOrderTime(LocalDateTime.now());
        adminPurchaseOrder.setCreateId(SecurityUtils.getUserId());
        boolean saveResult = this.save(adminPurchaseOrder);
        // 修改panda子订单下单状态
        appPandaOrderItem.setPurchaseStatus(OrderEnum.PURCHASE_ORDER_STATUS.PLACE_ORDER.getCode());
        int updatePandaOrderResult = appPandaOrderItemMapper.updateById(appPandaOrderItem);
        if (!saveResult && updatePandaOrderResult != 1) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "保存采购订单异常");
        }
        return true;
    }

}
