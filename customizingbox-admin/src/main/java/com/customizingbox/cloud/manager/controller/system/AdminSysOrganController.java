//package com.customizingbox.cloud.manager.controller.system;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.customizingbox.cloud.common.core.constant.CommonConstants;
//import com.customizingbox.cloud.common.core.util.ApiResponse;
//import com.customizingbox.cloud.common.datasource.system.entity.AdminSysOrgan;
//import com.customizingbox.cloud.manager.service.admin.AdminSysOrganService;
//import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.dao.DuplicateKeyException;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@Slf4j
//@RestController
//@AllArgsConstructor
//@RequestMapping("/organ")
//@Api(value = "organ", tags = "机构管理模块")
//public class AdminSysOrganController {
//    private final AdminSysOrganService adminSysOrganService;
//
//
//    @ApiOperation(value = "通过ID查询")
//    @GetMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:organ:get')")
//    public ApiResponse getById(@PathVariable String id) {
//        return ApiResponse.ok(adminSysOrganService.getById(id));
//    }
//
//
//    @ApiOperation(value = "list查询")
//    @GetMapping("/list")
//    public ApiResponse getList(AdminSysOrgan adminSysOrgan) {
//        return ApiResponse.ok(adminSysOrganService.list(Wrappers.query(adminSysOrgan)));
//    }
//
//
//    @ApiOperation(value = "返回树形菜单集合")
//    @GetMapping(value = "/tree")
//    public ApiResponse getTree() {
//        return ApiResponse.ok(adminSysOrganService.selectTree());
//    }
//
//
//    @ApiOperation(value = "添加")
//    @SysLog("添加机构")
//    @PostMapping
//    @PreAuthorize("@ato.hasAuthority('sys:organ:add')")
//    public ApiResponse save(@Valid @RequestBody AdminSysOrgan adminSysOrgan) {
//        try {
//            if (CommonConstants.PARENT_ID.equals(adminSysOrgan.getParentId())) {
//                throw new Exception("父级节点不能为0");
//            }
//            return ApiResponse.ok(adminSysOrganService.saveOrgan(adminSysOrgan));
//        } catch (DuplicateKeyException e) {
//            if (e.getMessage().contains("uk_tenant_id_code")) {
//                return ApiResponse.failed("机构编码已存在");
//            } else {
//                return ApiResponse.failed(e.getMessage());
//            }
//        } catch (Exception e) {
//            log.error("error", e);
//            return ApiResponse.failed(e.getMessage());
//        }
//    }
//
//
//    @ApiOperation(value = "删除")
//    @SysLog("删除机构")
//    @DeleteMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:organ:del')")
//    public ApiResponse removeById(@PathVariable String id) {
//        AdminSysOrgan adminSysOrgan2 = adminSysOrganService.getById(id);
//        if (CommonConstants.PARENT_ID.equals(adminSysOrgan2.getParentId())) {
//            return ApiResponse.failed("总机构（租户机构）不能删除");
//        }
//        return ApiResponse.ok(adminSysOrganService.removeOrganById(id));
//    }
//
//
//    @ApiOperation(value = "编辑")
//    @SysLog("编辑机构")
//    @PutMapping
//    @PreAuthorize("@ato.hasAuthority('sys:organ:edit')")
//    public ApiResponse update(@Valid @RequestBody AdminSysOrgan adminSysOrgan) {
//        try {
//            AdminSysOrgan adminSysOrgan2 = adminSysOrganService.getById(adminSysOrgan.getId());
//            if (CommonConstants.PARENT_ID.equals(adminSysOrgan2.getParentId())) {
//                adminSysOrgan.setParentId(CommonConstants.PARENT_ID);
//            }
//            return ApiResponse.ok(adminSysOrganService.updateOrganById(adminSysOrgan));
//        } catch (DuplicateKeyException e) {
//            if (e.getMessage().contains("uk_tenant_id_code")) {
//                return ApiResponse.failed("机构编码已存在");
//            } else {
//                return ApiResponse.failed(e.getMessage());
//            }
//        } catch (Exception e) {
//            log.error("error", e);
//            return ApiResponse.failed(e.getMessage());
//        }
//    }
//}
