package com.customizingbox.cloud.manager.syslog.event;


import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import com.customizingbox.cloud.manager.service.admin.AdminSysLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * 异步监听日志事件
 */
@Slf4j
@AllArgsConstructor
public class SysLogListener {

    private final AdminSysLogService adminSysLogService;

    @Async
    @Order
    @EventListener(SysLogEvent.class)
    public void saveSysLog(SysLogEvent event) {
        AdminSysLog adminSysLog = event.getAdminSysLog();
        adminSysLogService.save(adminSysLog);
    }
}
