package com.customizingbox.cloud.manager.service.admin.impl.customer;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.customer.AdminAppRelevancyRecordMapper;
import com.customizingbox.cloud.common.datasource.model.admin.customer.entity.AdminAppRelevancyRecord;
import com.customizingbox.cloud.manager.service.admin.AdminAppRelevancyRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户经理认领/关联 店铺用户记录表 服务实现类
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-14
 */
@Service
public class AdminAppRelevancyRecordServiceImpl extends ServiceImpl<AdminAppRelevancyRecordMapper, AdminAppRelevancyRecord> implements AdminAppRelevancyRecordService {

    @Override
    public IPage<AdminAppRelevancyRecord> listByPage(Page page) {
        return null;
    }
}
