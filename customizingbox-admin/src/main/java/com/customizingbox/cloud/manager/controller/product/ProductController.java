package com.customizingbox.cloud.manager.controller.product;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.ProductSaveOrUpdateDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.VariantPriceDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.AdminProductPageVariantListVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductInfoVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductService;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductVariantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/product")
@Api(tags = "product")
@AllArgsConstructor
public class ProductController {

    @Autowired
    private AdminStoreProductService adminStoreProductService;

    @Autowired
    private AdminStoreProductVariantService adminStoreProductVariantService;

    @ApiOperation(value = "新增产品 ")
    @PostMapping("saveProduct")
    public ApiResponse saveProduct(@Valid @RequestBody ProductSaveOrUpdateDto productSaveOrUpdateDto) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductService.saveProduct(productSaveOrUpdateDto, user);
    }

    @ApiOperation(value = "获取spu")
    @GetMapping("/getSpu")
    public ApiResponse getSkuOrSpu() {
        String spu = adminStoreProductService.getSpu();
        return ApiResponse.ok(spu);
    }


    @ApiOperation(value = "修改产品 quote_flag = true时不能修改规格和规格属性")
    @PostMapping("UodateProduct")
    public ApiResponse updateProduct(@Valid @RequestBody ProductSaveOrUpdateDto productSaveOrUpdateDto) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductService.updateProduct(productSaveOrUpdateDto, user);
    }

    @ApiOperation(value = "批量修改价格")
    @PostMapping("updateProductVariantPrice")
    public ApiResponse updateProductVariantPrice(@ApiParam(value = "变体报价信息数组", required = true) @RequestBody List<VariantPriceDto> variantPriceDtos) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductVariantService.updateProductVariantPrice(variantPriceDtos, user);
    }

    @ApiOperation(value = "删除产品")
    @PostMapping("delAdminProduct/{id}")
    public ApiResponse delAdminProduct(@ApiParam("产品id") @PathVariable Long id) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductService.delAdminProduct(id, user);
    }

    @ApiOperation(value = "恢复产品")
    @PostMapping("recoveryAdminProduct/{id}")
    public ApiResponse recoveryAdminProduct(@ApiParam("产品id") @PathVariable Long id) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductService.recoveryAdminProduct(id, user);
    }

    @ApiOperation(value = "删除变体")
    @PostMapping("delAdminProductVariant/{id}")
    public ApiResponse delAdminProductVariant(@ApiParam("变体id") @PathVariable Long id) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductVariantService.delAdminProductVariant(id, user);
    }

    @ApiOperation(value = "恢复变体")
    @PostMapping("recoveryAdminProductVariant/{id}")
    public ApiResponse recoveryAdminProductVariant(@ApiParam("变体id") @PathVariable Long id) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductVariantService.recoveryAdminProductVariant(id, user);
    }


    @ApiOperation(value = "产品详情")
    @PostMapping("adminProductInfo/{id}")
    public ApiResponse<ProductInfoVo> adminProductInfo(@ApiParam("产品id") @PathVariable Long id) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductService.adminProductInfo(id);
    }

    @ApiOperation(value = "产品分页")
    @PostMapping("page")
    public ApiResponse<IPage<ProductPageVo>> adminProductPage(Page page, @RequestBody ProductSearchVo productSearchVo) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        if (page == null) {
            page = new Page();
        }
        IPage<ProductPageVo> pageVo = adminStoreProductService.adminProductPage(productSearchVo, user, page);
        return ApiResponse.ok(pageVo);
    }

    /**
     * adminProductPageVariantListVo
     */
    @ApiOperation(value = "产品下变体列表")
    @PostMapping("adminProductVariantList/{id}")
    public ApiResponse<AdminProductPageVariantListVo> adminProductVariantList(@ApiParam("产品id") @PathVariable Long id) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductService.adminProductVariantList(id);
    }

    /**
     * 非定制产品上传赛盒
     */
    @ApiOperation(value = "非定制产品上传赛盒")
    @PostMapping("processProductToSaihe")
    public ApiResponse processProductToSaihe(@ApiParam("产品id集合") @RequestBody List<Long> ids) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        ArrayList<String> list = new ArrayList<>();
        for (Long id : ids) {
            try {
                String msg = adminStoreProductService.processProductsToSaihe(id);
                list.add(msg);
            } catch (Exception e) {
                log.error("非定制产品上传赛盒异常: {}", e);
            }
        }
        return ApiResponse.ok(list.toString());
    }

}
