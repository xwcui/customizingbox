package com.customizingbox.cloud.manager.service.admin;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.AdminStoreProductAttrVO;

import java.util.List;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-31 09:34:36
 */
public interface AdminStoreProductAttrService extends IService<AdminStoreProductAttr> {

    /**
     * 根据产品id获取attr  list
     * @param productId
     * @return
     */
    List<AdminStoreProductAttr> selectAdminStoreProductAttrList(Long productId);
    /**
     * 根据产品id查询属性名(有序)
     * @param productId
     * @return
     */
    List<String> queryByProductId(Long productId);

    /**
     * 根据产品id, 组装产品属性
     * @param productId
     * @param attrValues  产品属性值数组
     * @return
     */
    List<AdminStoreProductAttrVO> queryAttrByProductId(Long productId, JSONArray attrValues);
}
