package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysLogMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysPreLogVO;
import com.customizingbox.cloud.manager.service.admin.AdminSysLogService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 日志表 服务实现类
 * </p>
 */
@Service
public class AdminSysLogServiceImpl extends ServiceImpl<AdminSysLogMapper, AdminSysLog> implements AdminSysLogService {

    /**
     * 批量插入前端错误日志
     *
     * @param adminSysPreLogVOList 日志信息
     * @return true/false
     */
    @Override
    public Boolean saveBatchLogs(List<AdminSysPreLogVO> adminSysPreLogVOList) {
        List<AdminSysLog> adminSysLogs = adminSysPreLogVOList.stream().map(pre -> {
            AdminSysLog log = new AdminSysLog();
            log.setType(CommonConstants.LOG_TYPE_9);
            log.setTitle(pre.getInfo());
            log.setException(pre.getStack());
            log.setParams(pre.getMessage());
            log.setCreateTime(LocalDateTime.now());
            log.setRequestUri(pre.getUrl());
            log.setCreateBy(pre.getUser());
            return log;
        }).collect(Collectors.toList());
        return this.saveBatch(adminSysLogs);
    }
}
