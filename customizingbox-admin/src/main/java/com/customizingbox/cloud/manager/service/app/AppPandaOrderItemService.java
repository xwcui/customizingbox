package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.OrderToSaiheOrderItemVo;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

import java.util.List;

/**
 * 原平台订单item表
 *
 * @author Y
 * @date 2022-04-12 16:41:47
 */
public interface AppPandaOrderItemService extends IService<AppPandaOrderItem> {


    /**
     * 根据订单id查询item订单列表
     *
     * @param pandaOrderId
     * @return
     */
    List<AppPandaOrderItem> queryByOrderId(Long pandaOrderId);


//    /**
//     * 修改订单中产品映射关系(未支付)
//     *
//     * @param storeId 商户店铺id
//     * @param sourceProductId 商户产品id
//     * @param sourceVariantId 商户产品变体id
//     * @param adminProductId admin 产品id
//     * @param adminVariantId admin 产品变体id
//     * @return
//     */
//    Boolean updateOrderItemProductRelevancy(Long storeId, Long sourceProductId, Long sourceVariantId, Long adminProductId, Long adminVariantId);

    /**
     * 唯一码产品分页
     *
     * @param uniqueProductSearchVo
     * @param user
     * @param page
     * @return
     */
    IPage<UniqueProductPageVo> uniqueProductPage(UniqueProductSearchVo uniqueProductSearchVo, BaseUser user, Page page);

    /**
     * 唯一码产品上传库存
     * @param onlyCodes
     * @param user
     * @return
     */
    ApiResponse processUniqueProductStockNumberToSaihe(List<String> onlyCodes, BaseUser user);

    /**
     * 重新识别订单商品
     * @param appPandaOrder 订单
     * @return
     */
    Boolean reloadOrderItemProductRelevancy(AppPandaOrder appPandaOrder);



    /**
     * 获取订单上传赛盒的item
     * @param orderId
     * @return
     */
    List<OrderToSaiheOrderItemVo> getOrderItemByOrderId(Long orderId);
}
