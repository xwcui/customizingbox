package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.global.AdminGlobalConfig;

/**
 * <p>
 * admin全局配置 服务类
 * </p>
 *
 * @author Z
 * @since 2022-05-09
 */
public interface AdminGlobalConfigService extends IService<AdminGlobalConfig> {

    /**
     * 全局配置详情
     */
    String getValueByKey(String key);

    /**
     * 优先缓存中取
     */
    String getValueByCache(String key);

}
