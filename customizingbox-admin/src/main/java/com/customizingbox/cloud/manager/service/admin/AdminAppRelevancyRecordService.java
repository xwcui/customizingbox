package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.customer.entity.AdminAppRelevancyRecord;

/**
 * <p>
 * 客户经理认领/关联 店铺用户记录表 服务类
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-14
 */
public interface AdminAppRelevancyRecordService extends IService<AdminAppRelevancyRecord> {
    IPage<AdminAppRelevancyRecord> listByPage(Page page);
}
