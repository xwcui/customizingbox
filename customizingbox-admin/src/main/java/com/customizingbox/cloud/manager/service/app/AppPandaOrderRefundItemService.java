package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductSearchVo;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderRefundItem;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundItemAmountVO;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;

import java.util.List;

/**
 * 退款item表
 *
 * @author Y
 * @date 2022-04-23 09:55:24
 */
public interface AppPandaOrderRefundItemService extends IService<AppPandaOrderRefundItem> {

    /**
     * 查询待退款订单总金额
     * @param orderId 订单id
     * @param status 退款状态. 1 申请中 4 已退款(同意退款) 5 拒绝退款  -1: 申请中和已退款
     * @return
     */
    List<AppPandaOrderRefundItemAmountVO> applyRefundAmount(Long orderId, Integer status);

    /**
     * 根据退款表id 查询item表数据
     * @param refundId
     * @return
     */
    List<AppPandaOrderRefundItem> getByRefundId(Long refundId);



}
