package com.customizingbox.cloud.manager.controller.transport;



import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.core.util.DateUtils;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminLogisticsFreightDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminLogisticsFreightService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 物流费用 前端控制器
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@RestController
@RequestMapping("/adminLogisticsFreight")
@Api(tags = "物流费用（运输单元）")
public class AdminLogisticsFreightController {

    @Autowired
    private AdminLogisticsFreightService service;

    @ApiOperation("/物流费用（运输单元）分页")
    @PostMapping("/page")
    public ApiResponse<IPage<AdminLogisticsFreightVo>> page(Page page, @RequestBody AdminLogisticsFreightSearchVo searchVo){
        if (page == null){
            page = new Page();
        }
        IPage<AdminLogisticsFreightVo> result = service.pageAll(page,searchVo);
        return ApiResponse.ok(result);
    }

    @ApiOperation("/物流费用（运输单元）新增或修改")
    @PostMapping("/saveOrUpdate")
    public ApiResponse saveOrUpdate(@RequestBody @Validated AdminLogisticsFreightDto adminLogisticsAttrDto){
        BaseUser user = SecurityUtils.getUser();
        if (user == null){
            throw new ApiException("无法获取");
        }
        return  service.saveOrUpdateAdminLogisticsFreightDto(adminLogisticsAttrDto,user);
    }


    @ApiOperation("/物流单元导入")
    @PostMapping("/importExcel")
    public ApiResponse importExcel(@RequestParam("file") MultipartFile file,
                                   @RequestParam("enableTime") String enableTime)  throws Exception {
        LocalDateTime  enableTimeLocalDateTime= DateUtils.getLocalDateTimeByStr(enableTime);
        BaseUser user = SecurityUtils.getUser();
        if (user == null){
            throw new ApiException("无法获取");
        }
        ImportParams params = new ImportParams();
        params.setNeedVerfiy(true);
        List<AdminLogisticsFreightVo> result = ExcelImportUtil.importExcel(file.getInputStream(),
                AdminLogisticsFreightVo.class, params);
        if (CollectionUtils.isEmpty(result)){
            return ApiResponse.failed("excel为null");
        }
        return  service.importAdminLogisticsFreight(result,user,enableTimeLocalDateTime);
    }

}
