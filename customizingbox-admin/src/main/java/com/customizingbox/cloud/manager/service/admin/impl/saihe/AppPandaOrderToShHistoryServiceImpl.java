package com.customizingbox.cloud.manager.service.admin.impl.saihe;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderToShHistoryMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderToShHistory;
import com.customizingbox.cloud.manager.service.admin.AppPandaOrderToShHistoryService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-05-09
 */
@Service
public class AppPandaOrderToShHistoryServiceImpl extends ServiceImpl<AppPandaOrderToShHistoryMapper, AppPandaOrderToShHistory> implements AppPandaOrderToShHistoryService {

    @Override
    public void saveOrUpdateHistory(AppPandaOrderToShHistory appPandaOrderToShHistory) {
        LambdaQueryWrapper<AppPandaOrderToShHistory> queryWrapper = new QueryWrapper<AppPandaOrderToShHistory>().lambda().eq(AppPandaOrderToShHistory::getOrderId, appPandaOrderToShHistory.getOrderId());
        AppPandaOrderToShHistory history = baseMapper.selectOne(queryWrapper);
        appPandaOrderToShHistory.setCreateTime(LocalDateTime.now());
        if (history == null){
            this.save(appPandaOrderToShHistory);
        }else {
            appPandaOrderToShHistory.setId(history.getId());
            this.updateById(appPandaOrderToShHistory);
        }

    }
}
