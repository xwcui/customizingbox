package com.customizingbox.cloud.manager.controller.customer;

import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.enums.UserEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.core.util.WebUtils;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.AdminCustomerDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerAppOrderQuery;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.CustomerStoreDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.CustomerStoreProductsDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerProductSaleParam;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.GetAppUserParam;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.*;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerAppPandaOrderResponseVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminCustomerManageService;
import com.customizingbox.cloud.manager.service.admin.AdminSysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author DC_Li
 * @date 2022/4/15 10:04
 */
@RestController
@RequestMapping("/customerManage")
@Api(tags = "客户管理")
@AllArgsConstructor
public class AdminCustomerManageController {
    private final AdminCustomerManageService adminCustomerManageService;
    private final AdminSysUserService adminSysUserService;

    @PostMapping("/pageCustomers")
    @ApiOperation("分页查询客户列表，已认领客户数取分页总数")
    public ApiResponse<IPage<List<AdminCustomerResponseVo>>> pageCustomers(Page page,
                                                                           @ApiParam("查询参数") @RequestBody AdminCustomerDto adminCustomerDto,
                                                                           HttpServletRequest request) {
        String ip = WebUtils.getIp(request);
        return ApiResponse.ok(adminCustomerManageService.pageCustomers(page, adminCustomerDto, ip));
    }

    @PostMapping("/getOneAppUser")
    @ApiOperation("查询单个客户")
    public ApiResponse<AdminCustomerResponseVo> getOneAppUser(@ApiParam("查询app客户参数") @RequestBody GetAppUserParam param) {
        return ApiResponse.ok(adminCustomerManageService.getOneAppUser(param.getEmail(), param.getAppUserId()));
    }

    @GetMapping("/claimCustomer")
    @ApiOperation("认领客户")
    public ApiResponse<Integer> claimCustomer(@ApiParam("客户id") @RequestParam("appUserId") Long appUserId) {
        Integer i = adminCustomerManageService.claimCustomer(appUserId);
        if (i == 1) {
            return ApiResponse.ok(i);
        }
        if (i == 2) {
            return ApiResponse.failed("该用户不存在");
        }
        if (i == 3) {
            return ApiResponse.failed("客户已被认领");
        }
        return ApiResponse.failed("claim failed" + i);
    }

    @PostMapping
    @ApiOperation("超管移交客户经理")
    public ApiResponse<Boolean> transferCustomer(@ApiParam("客户id - 客户经理id") @RequestBody Map<Long, Long> map) {
        BaseUser user = SecurityUtils.getUser();
        if (user.getCustomerDataAuth()) {
            return ApiResponse.failed("当前用户不是管理员");
        }
        Boolean aBoolean = adminCustomerManageService.transferCustomer(map);
        if (aBoolean) {
            return ApiResponse.ok(aBoolean);
        } else {
            return ApiResponse.failed("移交失败");
        }
    }

    @GetMapping
    @ApiOperation("客户经理列表")
    public ApiResponse<List<AdminSysUser>> listAllAdminSysUser() {
        // List<AdminSysUser> adminSysUsers = adminSysUserService.list(new QueryWrapper<AdminSysUser>().eq("type", 1).select("id","username"));
        List<AdminSysUser> adminSysUsers = adminSysUserService.list(Wrappers.<AdminSysUser>lambdaQuery()
                .eq(AdminSysUser::getType, UserEnum.TYPE.CONSUMER_MANAGER.getCode())
                .select(AdminSysUser::getId));
        return ApiResponse.ok(adminSysUsers);
    }

    @PostMapping("/getCustomerStores")
    @ApiOperation("客户店铺")
    public ApiResponse<IPage<CustomerStoreResponseVo>> getCustomerStores(Page page, @RequestBody CustomerStoreDto customerStoreDto) {

        return ApiResponse.ok(adminCustomerManageService.getCustomerStores(page, customerStoreDto));
    }

    @PostMapping("/getCustomerProducts")
    @ApiOperation("客户商品报价，分页查询客户店铺产品")
    public ApiResponse<IPage<CustomerProductResponseVo>> getCustomerProducts(@ApiParam("只传size和current") Page page,
                                                                             @RequestBody CustomerStoreProductsDto customerStoreProductsDto) {
        return ApiResponse.ok(adminCustomerManageService.getCustomerProducts(page, customerStoreProductsDto));
    }

    @GetMapping("/getCustomerProductDetail")
    @ApiOperation("报价商品详情")
    public ApiResponse<CustomerQuoteVo> getCustomerProductDetail(
            @ApiParam("app产品id") @RequestParam("appProductId") Long appProductId) {
        return ApiResponse.ok(adminCustomerManageService.getCustomerProductDetail(appProductId));
    }

    @GetMapping("/getAdminProductAndVariants")
    @ApiOperation("获取产品库数据")
    public ApiResponse<CustomerQuoteAdminVo> getAdminProductAndVariants(@RequestParam("spu") String spu) {
        return ApiResponse.ok(adminCustomerManageService.getAdminProductAndVariants(spu));
    }

    // 保存报价信息
    @PostMapping("/saveQuote")
    @ApiOperation("保存报价信息")
    public ApiResponse<Boolean> saveQuote(@ApiParam(value = "变体报价信息数组", required = true) @RequestBody List<SaveQuoteRequestVo> requestVo) {
        for (SaveQuoteRequestVo saveQuoteRequestVo : requestVo) {
            BigDecimal quoteRate = saveQuoteRequestVo.getQuoteRate();
            if (NumberUtil.isLess(quoteRate, BigDecimal.ONE)) {
                return ApiResponse.failed("报价率不能小于1");
            }
        }
        return ApiResponse.ok(adminCustomerManageService.saveQuote(requestVo));
    }

    //客户商品销量采购量列表
    @PostMapping("getCustomerProductSale")
    @ApiOperation("查询客户商品销量采购量列表分页")
    public ApiResponse<IPage<CustomerProductSaleResponseVo>> getCustomerProductSale(Page page, @RequestBody CustomerProductSaleParam param) {
        return ApiResponse.ok(adminCustomerManageService.getCustomerProductSale(page, param));
    }

    @PostMapping("/getCustomerStoreOrders")
    @ApiOperation("客户店铺订单列表分页")
    public ApiResponse<IPage<CustomerAppPandaOrderResponseVo>> getCustomerStoreOrders(Page page, @RequestBody CustomerAppOrderQuery query) {
        return ApiResponse.ok(adminCustomerManageService.getCustomerStoreOrders(page, query));
    }

    @GetMapping("/reIdentifyPandaOrder/{pandaOrderId}")
    @ApiOperation("重新识别订单商品")
    // 重新匹配映射并返回当前订单数据
    public ApiResponse<CustomerAppPandaOrderResponseVo> reIdentifyPandaOrder(@PathVariable Long pandaOrderId){
        return ApiResponse.ok(adminCustomerManageService.reIdentifyPandaOrder(pandaOrderId));
    }
}
