package com.customizingbox.cloud.manager.syslog.event;

import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统日志事件
 */
@Getter
@AllArgsConstructor
public class SysLogEvent {

	private final AdminSysLog adminSysLog;
}
