package com.customizingbox.cloud.manager.syslog;

import com.customizingbox.cloud.manager.service.admin.AdminSysLogService;
import com.customizingbox.cloud.manager.syslog.aspect.SysLogAspect;
import com.customizingbox.cloud.manager.syslog.event.SysLogListener;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 日志自动配置
 */
@EnableAsync
@Configuration
@AllArgsConstructor
@ConditionalOnWebApplication
public class LogAutoConfiguration {

	private final AdminSysLogService adminSysLogService;

	@Bean
	public SysLogListener sysLogListener() {
		return new SysLogListener(adminSysLogService);
	}

	@Bean
	public SysLogAspect sysLogAspect(ApplicationEventPublisher publisher) {
		return new SysLogAspect(publisher);
	}
}
