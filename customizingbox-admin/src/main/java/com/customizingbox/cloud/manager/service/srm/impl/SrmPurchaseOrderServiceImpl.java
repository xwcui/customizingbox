package com.customizingbox.cloud.manager.service.srm.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductAttrMapper;
import com.customizingbox.cloud.common.datasource.mapper.srm.AdminPurchaseOrderDetectionRecordMapper;
import com.customizingbox.cloud.common.datasource.mapper.srm.AdminPurchaseOrderMapper;
import com.customizingbox.cloud.common.datasource.model.srm.ProductIdNames;
import com.customizingbox.cloud.common.datasource.model.srm.dto.FillInProgressDto;
import com.customizingbox.cloud.common.datasource.model.srm.dto.ProductNumDto;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrder;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrderDetectionRecord;
import com.customizingbox.cloud.common.datasource.model.srm.query.DefectiveProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.FulfilledProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.ProducingProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.ReadyProduceQuery;
import com.customizingbox.cloud.common.datasource.model.srm.vo.*;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.DeliveryProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.ProducingProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.PurchaseProductVo;
import com.customizingbox.cloud.common.datasource.util.IdGenerate;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.impl.srm.AdminPurchaseOrderServiceImpl;
import com.customizingbox.cloud.manager.service.srm.SrmPurchaseOrderDetectionRecordService;
import com.customizingbox.cloud.manager.service.srm.SrmPurchaseOrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 采购订单表 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
@Service
@AllArgsConstructor
@Slf4j
public class SrmPurchaseOrderServiceImpl extends ServiceImpl<AdminPurchaseOrderMapper, AdminPurchaseOrder> implements SrmPurchaseOrderService {
    private final AdminStoreProductAttrMapper adminStoreProductAttrMapper;
    private final SrmPurchaseOrderDetectionRecordService srmPurchaseOrderDetectionRecordService;
    private final AdminPurchaseOrderDetectionRecordMapper detectionRecordMapper;
    private final AdminPurchaseOrderServiceImpl adminPurchaseOrderService;

    /**
     * 采购订单表列表
     *
     * @param query
     */
    @Override
    public List<PurchaseProductVo> listReadyProduce(ReadyProduceQuery query) {
        Long userId = SecurityUtils.getUserId();
        List<PurchaseProductVo> list = baseMapper.listReadyProduce(userId, query);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        Set<Long> productIds = list.stream().map(PurchaseProductVo::getProductId).collect(Collectors.toSet());
        List<ProductIdNames> productIdNames = adminStoreProductAttrMapper.getName(productIds);
        for (PurchaseProductVo readyProduceVo : list) {
            for (ProductIdNames productIdName : productIdNames) {
                if (ObjectUtil.equal(readyProduceVo.getProductId(), productIdName.getProductId())) {
                    String names = productIdName.getNames();
                    names = "[" + names + "]";
                    readyProduceVo.setAttrNames(names);
                }
            }
        }

        if (query.getBatchNum() == null) {
            return list;
        }
        List<PurchaseProductVo> newList = new ArrayList<>();
        Integer totalQuantity = 0;
        for (PurchaseProductVo readyProduceVo : list) {
            Integer quantity = readyProduceVo.getQuantity();
            totalQuantity += quantity;
            // 当数量超过查询的批次数量时，向后找刚好能凑够的，保证查出来的总数量小于等于给定数量
            if (totalQuantity > query.getBatchNum()) {
                totalQuantity -= quantity;
                continue;
            }
            if (totalQuantity <= query.getBatchNum()) {
                newList.add(readyProduceVo);
            }
        }
        return newList;
    }

    @Override
    public IPage<PurchaseProductVo> pageReadyProduce(Page page) {
        IPage<PurchaseProductVo> iPage = baseMapper.pageReadyProduce(page);
        List<PurchaseProductVo> records = iPage.getRecords();
        if (CollectionUtils.isEmpty(records)) {
            return null;
        }
        Set<Long> productIds = records.stream().map(PurchaseProductVo::getProductId).collect(Collectors.toSet());
        List<ProductIdNames> productIdNames = adminStoreProductAttrMapper.getName(productIds);
        for (PurchaseProductVo readyProduceVo : records) {
            for (ProductIdNames productIdName : productIdNames) {
                if (ObjectUtil.equal(readyProduceVo.getProductId(), productIdName.getProductId())) {
                    String names = productIdName.getNames();
                    names = "[" + names + "]";
                    readyProduceVo.setAttrNames(names);
                }
            }
        }
        return iPage;
    }

    @Override
    public Boolean startProduce(List<Long> purchaseOrderIds) {
        if (CollectionUtils.isEmpty(purchaseOrderIds)) {
            return false;
        }
        Long batchId = IdGenerate.nextId();
        return baseMapper.startProduce(purchaseOrderIds, batchId, OrderEnum.PURCHASE_ORDER_STATUS.PRODUCTION.getCode(), LocalDateTime.now());
    }

    @Override
    public Boolean refuseProduce(Long purchaseOrderId, String refuseReason) {
        if (purchaseOrderId == null) {
            return false;
        }
        return baseMapper.refuseProduce(purchaseOrderId, refuseReason, OrderEnum.PURCHASE_ORDER_STATUS.PURCHASE_REJECT.getCode(), LocalDateTime.now());
    }

    @Override
    public IPage<ProducingProductVo> pageProducingProduct(Page page, ProducingProductQuery query) {
        Long userId = SecurityUtils.getUserId();
        IPage<ProducingProductVo> iPage = baseMapper.pageProducingProduct(page, userId, query);
        List<ProducingProductVo> records = iPage.getRecords();
        if (CollectionUtils.isEmpty(records)) {
            return null;
        }

        Set<Long> productIds = records.stream().map(ProducingProductVo::getProductId).collect(Collectors.toSet());
        List<ProductIdNames> idNames = new ArrayList<>();
        if (!CollectionUtils.isEmpty(productIds)) {
            idNames = adminStoreProductAttrMapper.getName(productIds);
        }
        for (ProducingProductVo record : records) {
            for (ProductIdNames idName : idNames) {
                if (ObjectUtil.equal(idName.getProductId(), record.getProductId())) {
                    String names = idName.getNames();
                    names = "[" + names + "]";
                    record.setAttrNames(names);
                }
            }

        }
        iPage.setRecords(records);

        return iPage;
    }

    @Override
    public List<PrintOnlyCodeVo> printOnlyCodeBatch(ProducingProductQuery query) {
        return baseMapper.printOnlyCodeBatch(SecurityUtils.getUserId(), query);
    }

    @Override
    public List<Long> listProducingProduct(ProducingProductQuery query) {
        Long userId = SecurityUtils.getUserId();
        List<ProducingProductVo> productVos = baseMapper.listProducingProduct(userId, query);
        Set<String> collect = productVos.stream().map(ProducingProductVo::getProductionBatch).collect(Collectors.toSet());
        // 批次数如果大于1，表示不是同一批次
        if (collect.size() > 1) {
            return null;
        }
        return productVos.stream().map(ProducingProductVo::getPurchaseOrderId).collect(Collectors.toList());
    }

    @Override
    public List<ExportProductionMeans> exportProductionMeansBatch(List<Long> purchaseOrderIds) {
        List<ProductionMeans> meansList = baseMapper.exportProductionMeansBatch(purchaseOrderIds);
        List<ExportProductionMeans> exportList = new ArrayList<>();
        for (ProductionMeans means : meansList) {
            JSONArray jsonArray = JSONArray.parseArray(means.getAttrValues());
            List<String> values = jsonArray.toJavaList(String.class);
            for (int i = 0; i < values.size(); i++) {
                switch (i) {
                    case 0:
                        means.setAttrValue1(values.get(i));
                        break;
                    case 1:
                        means.setAttrValue2(values.get(i));
                        break;
                    case 2:
                        means.setAttrValue3(values.get(i));
                        break;
                    default:
                }
            }
            try {
                means.setVariantImg(new URL(means.getVariantImgUrl()));
            } catch (MalformedURLException e) {
                log.warn("url exception", e);
            }


            JSONArray array = JSONArray.parseArray(means.getCustom());
            for (int i = 0; i < array.size(); i++) {
                ExportProductionMeans exportProductionMeans = new ExportProductionMeans();
                BeanUtil.copyProperties(means, exportProductionMeans);
                if (i > 0) {
                    exportProductionMeans.setVariantImg(null);
                }
                JSONObject jsonObject = array.getJSONObject(i);
                String name = jsonObject.getString("name");
                String value = jsonObject.getString("value");
                // if (value.startsWith("http")) {
                if (value.endsWith(".png") ||
                        value.endsWith(".jpg") ||
                        value.endsWith(".jpeg") ||
                        value.endsWith(".bmp") ||
                        value.endsWith(".gif") ||
                        value.endsWith(".webp") ||
                        value.endsWith(".psd") ||
                        value.endsWith(".svg") ||
                        value.endsWith(".tiff")) {
                    exportProductionMeans.setCustomPic(name + ":" + value);
                } else {
                    exportProductionMeans.setCustomText(name + ":" + value);
                }
                exportList.add(exportProductionMeans);
            }


            // 根据自定义信息分行
            /*JSONArray array = JSONArray.parseArray(means.getCustom());
            for (int i = 0; i < array.size(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                String name = jsonObject.getString("name");
                String value = jsonObject.getString("value");

                ProductionMeans productionMeans = new ProductionMeans();
                BeanUtil.copyProperties(means, productionMeans);
                productionMeans.setCustom(name + ":" + value);
                exportList2.add(productionMeans);
            }*/

            /*List<String> strings = JSONArray.parseArray(means.getCustom()).toJavaList(String.class);
            for (String string : strings) {
                JSONObject jsonObject = JSON.parseObject(string);
                String value = jsonObject.getString("value");
                // means.setCustom(value);
                ExportProductionMeans means2 = new ExportProductionMeans();
                BeanUtils.copyProperties(means, means2);
                means2.setCustom(value);
                exportList2.add(means2);
            }*/



            /*try {
                String imgUrl = means.getVariantImg();
                URL url = new URL(imgUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(5*1000);
                InputStream inStream = urlConnection.getInputStream();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[10240];
                int len = 0;
                while ((len = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, len);
                }
                inStream.close();
                byte[] bytes = outStream.toByteArray();
                means.setVariantImgBytes(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
        }

        return exportList;
    }

    @Override
    public IPage<ProducingBatchDetailVo> getPorducingBatch(Page page) {
        Long userId = SecurityUtils.getUserId();
        IPage<ProducingBatchDetailVo> purchaseOrderIds = baseMapper.selectBatchIds(page, userId);
        List<ProducingBatchDetailVo> records = purchaseOrderIds.getRecords();
        List<String> batchIds = records.stream().map(ProducingBatchDetailVo::getBatchId).collect(Collectors.toList());
        // 获取未发货数
        List<ProductNumDto> unFulfillCount = baseMapper.getProductNum(batchIds, OrderEnum.PURCHASE_ORDER_STATUS.PRODUCTION.getCode());
        // 批次产品总数
        List<ProductNumDto> allCount = baseMapper.getProductNum(batchIds, null);

        for (ProducingBatchDetailVo record : records) {
            // 获取每个批次详情
            ProducingBatchDetailVo vo = baseMapper.getPorducingBatchDetail(record);
            // BeanUtils.copyProperties(vo,record);
            BeanUtil.copyProperties(vo, record, CopyOptions.create().ignoreNullValue());

            // 获取未发货数
            for (ProductNumDto unfulfill : unFulfillCount) {
                String batchId = unfulfill.getBatchId();
                if (ObjectUtil.equal(record.getBatchId(), batchId)) {
                    record.setUnFulfillNum(unfulfill.getProductNum());
                }
            }
            if (ObjectUtil.isNull(record.getUnFulfillNum())) {
                record.setUnFulfillNum(0);
            }

            // 获取批次总数量
            for (ProductNumDto productNumDto : allCount) {
                String batchId = productNumDto.getBatchId();
                if (ObjectUtil.equal(record.getBatchId(), batchId)) {
                    record.setBatchProductNum(productNumDto.getProductNum());
                }
            }
            if (ObjectUtil.isNull(record.getBatchProductNum())) {
                record.setBatchProductNum(0);
            }
        }

        return purchaseOrderIds;
    }

    @Override
    public IPage<DeliveryProductVo> pageFulfilled(Page page, FulfilledProductQuery query) {
        Long userId = SecurityUtils.getUserId();
        // TODO 结算状态


        return baseMapper.pageFulfilled(page, userId, query);
    }

    @Override
    @Transactional
    public ReadyDeliveryProductVo getProductByOnlyCode(String onlyCode) {

        ReadyDeliveryProductVo readyDeliveryProductVo = baseMapper.getProductByOnlyCode(onlyCode);
        if (ObjectUtil.isNull(readyDeliveryProductVo)) {
            return null;
        }
        Long productId = readyDeliveryProductVo.getProductId();
        String names = adminStoreProductAttrMapper.getNameById(productId);
        readyDeliveryProductVo.setAttrNames(names);

        return readyDeliveryProductVo;
    }

    @Override
    public Boolean commitDelivery(CommitDeliveryRequestVo commitDeliveryVo) {
        Long deliveryBatchCode = IdGenerate.nextId();
        return baseMapper.commitDelivery(commitDeliveryVo, deliveryBatchCode,
                OrderEnum.PURCHASE_ORDER_STATUS.SHIPMENT.getCode(), LocalDateTime.now());
    }

    @Override
    public IPage<DefectiveProductVo> pageDefectiveProducts(Page page, DefectiveProductQuery query) {
        Long userId = SecurityUtils.getUserId();
        IPage<DefectiveProductVo> iPage = baseMapper.pageDefectiveProducts(page, query, userId);
        List<DefectiveProductVo> records = iPage.getRecords();

        Set<Long> productIds = records.stream().map(DefectiveProductVo::getProductId).collect(Collectors.toSet());
        List<ProductIdNames> idNames = new ArrayList<>();
        if (!CollectionUtils.isEmpty(productIds)) {
            idNames = adminStoreProductAttrMapper.getName(productIds);
        }
        for (DefectiveProductVo record : records) {
            for (ProductIdNames idName : idNames) {
                if (ObjectUtil.equal(idName.getProductId(), record.getProductId())) {
                    String names = idName.getNames();
                    names = "[" + names + "]";
                    record.setAttrNames(names);
                }
            }

            // admin每次质检不通过会同步不通过时间和质检备注

            // 获取最近的一次质检记录
            AdminPurchaseOrderDetectionRecord detectionRecord = srmPurchaseOrderDetectionRecordService.getOne(Wrappers.<AdminPurchaseOrderDetectionRecord>lambdaQuery()
            .eq(AdminPurchaseOrderDetectionRecord::getOnlyCode,record.getOnlyCode())
            // .eq(AdminPurchaseOrderDetectionRecord::getStatus,2)
            .orderByDesc(AdminPurchaseOrderDetectionRecord::getDetectionTime)
            .last("limit 1"));

            // 质检未通过,不良品中查询到的一定是质检还未通过的
            LocalDateTime progressTime = detectionRecord.getProgressTime();
            if (ObjectUtil.isNull(detectionRecord.getProgressTime()) ){
                record.setSupplierResend(0);
            }else {
                record.setSupplierResend(1);
                record.setSupplierResendMethod(detectionRecord.getProgressDeliveryMethod());
                record.setSupplierResendCode(detectionRecord.getProgressDeliveryCode());
                record.setSupplierResendTime(detectionRecord.getProgressTime());
            }
        }
        return iPage;
    }

    @Override
    public PurchaseOrderProgressVo getProgress(Long purchaseOrderId) {
        PurchaseOrderProgressVo progressVo = baseMapper.getProgress(purchaseOrderId);

        // 订单质检不通过或质检通过入库，查询质检信息
        if (OrderEnum.PURCHASE_ORDER_STATUS.QUALITY_FAIL.getCode().equals(progressVo.getPurchaseState())
                || OrderEnum.PURCHASE_ORDER_STATUS.SUCCESS.getCode().equals(progressVo.getPurchaseState())) {
            // 至少有一条质检通过或不通过记录
            List<QualityCheckRecordVo> records = detectionRecordMapper.selectCheckRecordVos(purchaseOrderId);

            QualityCheckRecordVo recordVo = records.get(records.size() - 1);
            // 质检通过
            if (recordVo.getStatus() != null && recordVo.getStatus() == 1) {
                progressVo.setQualityCheckUser(recordVo.getAdminQualityUserName());
                progressVo.setQualityCheckTime(recordVo.getDetectionTime());
                // 移除最后一条质检为通过的记录
                // records.remove(records.size() - 1);
            }
            progressVo.setQualityCheckRecords(records);
        }

        return progressVo;
    }

    @Override
    public Boolean fillInProgress(FillInProgressDto fillInProgressDto) {
        // 没有质检过可以更新进度
        // 获取最新一条质检记录
        AdminPurchaseOrderDetectionRecord detectionRecord = detectionRecordMapper.selectOne(new QueryWrapper<AdminPurchaseOrderDetectionRecord>()
                .eq("purchase_order_id", fillInProgressDto.getPurchaseOrderId())
                .orderByDesc("detection_time")
                .last("limit 1"));
        if (ObjectUtil.isNull(detectionRecord)) {
            return false;
        }

        // 更新该质检记录的供应商进度
        return detectionRecordMapper.updateProgress(detectionRecord.getId(), fillInProgressDto.getProgressDeliveryMethod(),
                fillInProgressDto.getProgressDeliveryCode(), LocalDateTime.now(), SecurityUtils.getUserId());
    }

    @Override
    public SrmCheckDetail checkDetail(String onlyCode) {
        AdminPurchaseOrderCheckConsoleVO adminPurchaseOrderCheckConsoleVO = adminPurchaseOrderService.checkConsole(onlyCode);
        SrmCheckDetail srmCheckDetail = new SrmCheckDetail();
        BeanUtil.copyProperties(adminPurchaseOrderCheckConsoleVO, srmCheckDetail);
        AdminPurchaseOrder adminPurchaseOrder = baseMapper.selectOne(Wrappers.<AdminPurchaseOrder>lambdaQuery().eq(AdminPurchaseOrder::getOnlyCode, onlyCode)
                .ne(AdminPurchaseOrder::getStatus, OrderEnum.PURCHASE_ORDER_STATUS.PURCHASE_REJECT.getCode()));
        srmCheckDetail.setAcceptQuantity(adminPurchaseOrder.getAcceptQuantity());
        srmCheckDetail.setRejectsQuantity(adminPurchaseOrder.getRejectsQuantity());
        srmCheckDetail.setLoseQuantity(adminPurchaseOrder.getLoseQuantity());
        return srmCheckDetail;
    }
}
