package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.transaction.entity.AppSysUserTransactionRecord;

import java.math.BigDecimal;

/**
 * 交易记录表
 *
 * @author Y
 * @date 2022-04-21 16:09:21
 */
public interface AppSysUserTransactionRecordService extends IService<AppSysUserTransactionRecord> {
    /**
     * 退款
     * @param money 交易金额
     * @param userId 用户id
     * @param orderId 订单id
     * @param refundOrderId 退款订单id
     * @param mark 备注
     * @return 交易记录id
     */
    Long refund(BigDecimal money, Long userId, Long orderId, Long refundOrderId, String mark);

}
