package com.customizingbox.cloud.manager.service.app.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductVariantListVO;
import com.customizingbox.cloud.manager.service.app.AppStoreProductVariantService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
@Service
@Slf4j
@AllArgsConstructor
public class AppStoreProductVariantServiceImpl extends ServiceImpl<AppStoreProductVariantMapper, AppStoreProductVariant> implements AppStoreProductVariantService {
}
