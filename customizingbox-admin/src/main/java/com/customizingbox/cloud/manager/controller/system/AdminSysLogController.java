package com.customizingbox.cloud.manager.controller.system;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysPreLogVO;
import com.customizingbox.cloud.manager.service.admin.AdminSysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 日志表 前端控制器
 * </p>
 */
@RestController
@AllArgsConstructor
@RequestMapping("/log")
@Api(value = "log", tags = "日志管理模块")
public class AdminSysLogController {

    private final AdminSysLogService adminSysLogService;


    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public ApiResponse getLogPage(Page page, AdminSysLog adminSysLog) {
        return ApiResponse.ok(adminSysLogService.page(page, Wrappers.query(adminSysLog)));
    }


    @ApiOperation(value = "删除日志")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:log:del')")
    public ApiResponse removeById(@PathVariable String id) {
        return ApiResponse.ok(adminSysLogService.removeById(id));
    }


    @ApiOperation(value = "插入日志")
    @PostMapping("/save")
    public ApiResponse save(@Valid @RequestBody AdminSysLog adminSysLog) {
        return ApiResponse.ok(adminSysLogService.save(adminSysLog));
    }


    @ApiOperation(value = "批量插入异常日志")
    @PostMapping("/logs")
    public ApiResponse saveBatchLogs(@RequestBody List<AdminSysPreLogVO> adminSysPreLogVOList) {
        return ApiResponse.ok(adminSysLogService.saveBatchLogs(adminSysPreLogVOList));
    }
}
