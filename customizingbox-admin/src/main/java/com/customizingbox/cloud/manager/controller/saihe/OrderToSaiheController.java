package com.customizingbox.cloud.manager.controller.saihe;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.AdminOrderToSaiheSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.OrderToSaihePageVO;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/saiheOrder")
@Api(tags = "订单--赛盒")
@AllArgsConstructor
public class OrderToSaiheController {


    @Autowired
    private AppPandaOrderService appPandaOrderService;


    @ApiOperation(value = "订单上传赛盒（已支付 - 未发货之间）列表")
    @PostMapping("/page")
    public ApiResponse<IPage<OrderToSaihePageVO>> saiheToOrderPage(Page page, @Valid @RequestBody AdminOrderToSaiheSearchVo query) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        if (page == null) {
            page = new Page();
        }
        if (user.getCustomerDataAuth()){
            query.setAdminUserId(user.getId());
        }
        IPage<OrderToSaihePageVO> resultPage = appPandaOrderService.selectSaiheToOrderPage(page, query);
        return ApiResponse.ok(resultPage);
    }

    @ApiOperation(value = "订单上传赛盒")
    @GetMapping("orderToSaihe")
    public ApiResponse cc() {
        appPandaOrderService.OrdersToSaohe();
        return ApiResponse.ok();
    }

}
