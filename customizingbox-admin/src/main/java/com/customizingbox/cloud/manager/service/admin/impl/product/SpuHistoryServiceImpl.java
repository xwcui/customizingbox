package com.customizingbox.cloud.manager.service.admin.impl.product;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.util.DateUtils;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.SpuHistoryMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.SpuHistory;
import com.customizingbox.cloud.manager.service.admin.SpuHistoryService;
import lombok.Synchronized;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-26
 */
@Service
public class SpuHistoryServiceImpl extends ServiceImpl<SpuHistoryMapper, SpuHistory> implements SpuHistoryService {

    @Resource
    private SpuHistoryMapper spuHistoryMapper;

    @Override
    @Synchronized
    public String getSpuIndex() {
        Integer number = null;
        String spuIndex = "XO";
        String date = DateUtils.getDateNumber();
        QueryWrapper<SpuHistory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("date", date);
        SpuHistory spuHistory = spuHistoryMapper.selectOne(queryWrapper);
        if (spuHistory == null) {
            SpuHistory saveEntity = new SpuHistory();
            saveEntity.setDate(date);
            saveEntity.setNumber(1);
            this.save(saveEntity);
            number = saveEntity.getNumber();

        } else {
            number = spuHistory.getNumber()+1;
            spuHistory.setNumber(number);
            this.updateById(spuHistory);
        }
        // 补零之后数字的总长度
        int len = 4;
        StringBuilder sb = new StringBuilder();
        sb.append(number);
        int temp = len - sb.length();
        if (temp > 0){
            //若长度不足进行补零
            while (sb.length() < len){
                // 每次都在最前面补零
                sb.insert(0, "0");
            }
        }
        return spuIndex + date + sb;
    }
}
