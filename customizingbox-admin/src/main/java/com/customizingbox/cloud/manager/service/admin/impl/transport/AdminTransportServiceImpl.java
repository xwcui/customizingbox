package com.customizingbox.cloud.manager.service.admin.impl.transport;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.TransportEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminTransportMapper;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransportLogistics;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.AdminTransportLogisticsService;
import com.customizingbox.cloud.manager.service.admin.AdminTransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 运输方式 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Service
public class AdminTransportServiceImpl extends ServiceImpl<AdminTransportMapper, AdminTransport> implements AdminTransportService {

    @Resource
    private AdminTransportMapper adminTransportMapper;

    @Autowired
    private AdminTransportLogisticsService adminTransportLogisticsService;


    /**
     * 分页
     * @param page
     * @param searchVo
     * @return
     */
    @Override
    public IPage<AdminTransportPageVo> pageAll(Page page, AdminTransportSearchVo searchVo) {
        IPage<AdminTransportPageVo> adminTransportPageVoIPage = adminTransportMapper.pageAll(page, searchVo);
                if (!CollectionUtils.isEmpty(adminTransportPageVoIPage.getRecords())){
                    adminTransportPageVoIPage.getRecords().stream().forEach(vo->{
                        QueryWrapper<AdminTransportLogistics> queryWrapper = new QueryWrapper<>();
                        queryWrapper.eq("transport_id",vo.getId());
                        List<AdminTransportLogistics> adminTransportLogisticsAll = adminTransportLogisticsService.list(queryWrapper);
                        if (!CollectionUtils.isEmpty(adminTransportLogisticsAll)){
                            List<Long> logisticsIdList = adminTransportLogisticsAll.stream().map(AdminTransportLogistics::getLogisticsId).collect(Collectors.toList());
                            List<String> result = logisticsIdList.stream().map(String::valueOf).collect(Collectors.toList());
                            vo.setLogisticsIds(result);
                        }
                    });
                }
        return adminTransportPageVoIPage;
    }

    /**
     * 保存或修改
     * @param adminTransportDto
     * @param user
     * @return
     */
    @Override
    @Transactional
    public ApiResponse saveOrUpdateAdminTransportDto(AdminTransportDto adminTransportDto, BaseUser user) {
        AdminTransport adminTransport = new AdminTransport();
        if (adminTransportDto.getId() != null) {
            adminTransport = this.getById(adminTransportDto.getId());
            if (adminTransportDto == null) {
                return ApiResponse.failed("数据库中没有该数据");
            }
        } else {
            QueryWrapper<AdminTransport> queryWrapper = new QueryWrapper<>();
            queryWrapper.and(wrapper -> wrapper.eq("name",adminTransportDto.getName())
            );
            Integer integer = adminTransportMapper.selectCount(queryWrapper);
            if (integer > 0 ){
                return ApiResponse.failed("运输方式中已存在该名称");
            }
            adminTransport.setCreateTime(LocalDateTime.now());
            adminTransport.setCreateId(Long.parseLong(user.getId()));
        }
        adminTransport.setStatus(adminTransportDto.getStatus());
        adminTransport.setVolumeFactor(adminTransportDto.getVolumeFactor());
        adminTransport.setCompleteMode(adminTransportDto.getCompleteMode());
        adminTransport.setTraceType(adminTransportDto.getTraceType());
        adminTransport.setDescription(adminTransportDto.getDescription());
        adminTransport.setName(adminTransportDto.getName());
        adminTransport.setShTransportId(adminTransportDto.getShTransportId());
        adminTransport.setDescription(adminTransportDto.getDescription());
        adminTransport.setUpdateId(Long.parseLong(user.getId()));
        adminTransport.setUpdateTime(LocalDateTime.now());
        this.saveOrUpdate(adminTransport);
        // 处理关联关系
        adminTransportLogisticsService.processingAssociation(adminTransportDto.getLogisticsIds(),adminTransport.getId(),Long.parseLong(user.getId()));
        return ApiResponse.ok();
    }

    /**
     * 开启  / 禁用
     * @param id
     * @param status
     * @param user
     * @return
     */
    @Override
    public ApiResponse openOrShut(Long id, Integer status, BaseUser user) {
        AdminTransport adminTransport = this.getById(id);
        if (adminTransport == null) {
            return ApiResponse.failed("数据库中没有该数据");
        }
        if (status.equals(TransportEnum.TransportStatusEnum.SHUT.getCode())){
            adminTransport.setStatus(TransportEnum.TransportStatusEnum.SHUT.getCode());
        }else if (status.equals(TransportEnum.TransportStatusEnum.OPEN.getCode())){
            adminTransport.setStatus(TransportEnum.TransportStatusEnum.OPEN.getCode());
        }else {
            return ApiResponse.failed("状态异常");
        }
        adminTransport.setUpdateTime(LocalDateTime.now());
        adminTransport.setUpdateId(Long.parseLong(user.getId()));
        this.updateById(adminTransport);
        return ApiResponse.ok();
    }

    @Override
    public List<AdminTransport> getList(AdminTransportSearchVo searchVo){

        List<AdminTransport> list = new ArrayList<>();
        if (searchVo != null && searchVo.getLogisticsId() != null ){
            searchVo.setStatus(TransportEnum.TransportStatusEnum.OPEN.getCode());
                list  = adminTransportMapper.getListBySearchVo(searchVo);
        }else {
            QueryWrapper<AdminTransport> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("status", TransportEnum.TransportStatusEnum.OPEN.getCode());
            list  = this.list(queryWrapper);
        }
        return list;

    }
}
