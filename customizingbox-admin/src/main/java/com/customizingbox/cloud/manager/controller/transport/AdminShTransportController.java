package com.customizingbox.cloud.manager.controller.transport;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminShTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminShTransportSearchVo;
import com.customizingbox.cloud.manager.service.admin.AdminShTransportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 赛合运输方式 前端控制器
 * </p>
 *
 * @author Z
 * @since 2022-04-13
 */
@RestController
@RequestMapping("/adminShTransport")
@Api(value = "adminShTransport", tags = "赛合运输方式")
public class AdminShTransportController {

    @Autowired
    private AdminShTransportService service;


    @ApiOperation("更新赛盒物流")
    @GetMapping("/toUpdate")
    public ApiResponse  toUpdateShTransport(){
        return  service.toUpdateShTransport();
    }


    @ApiOperation("/赛盒物流分页列表")
    @PostMapping("/page")
    public ApiResponse<IPage<AdminShTransport>> page(Page page, @RequestBody AdminShTransportSearchVo searchVo){
        if (page == null){
            page = new Page();
        }
        IPage<AdminShTransport> result = service.pageAll(page,searchVo);
        return ApiResponse.ok(result);
    }

    @ApiOperation("/赛盒物流list列表")
    @PostMapping("/list")
    public ApiResponse<List<AdminShTransport>> list(){
        List<AdminShTransport> result = service.list();
        return ApiResponse.ok(result);
    }


}
