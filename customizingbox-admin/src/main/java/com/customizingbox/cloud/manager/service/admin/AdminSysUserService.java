package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminPurchaseUserIdAndNameVo;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysUserInfo;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUseDetailVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUserPageVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUserVO;

import java.util.List;


public interface AdminSysUserService extends IService<AdminSysUser> {
    /**
     * 查询用户信息
     *
     * @param sysUser 用户
     * @return userInfo
     */
    AdminSysUserInfo findUserInfo(AdminSysUser sysUser);

    /**
     * 分页查询用户信息（含有角色信息）
     *
     * @param page    分页对象
     * @param adminSysUserDTO 参数列表
     * @return
     */
    IPage<List<AdminSysUserPageVO>> getUsersWithRolePage(Page page, AdminSysUserDTO adminSysUserDTO);

    /**
     * 删除用户
     *
     * @param sysUser 用户
     * @return boolean
     */
    Boolean deleteUserById(AdminSysUser sysUser);

    /**
     * 更新当前用户基本信息
     *
     * @param adminSysUserDto 用户信息
     * @return Boolean
     */
    Boolean updateUserInfo(AdminSysUserDTO adminSysUserDto);

    /**
     * 更新指定用户信息
     *
     * @param adminSysUserDto 用户信息
     * @return
     */
    Boolean updateUser(AdminSysUserDTO adminSysUserDto);

    /**
     * 通过ID查询用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     */
    AdminSysUserVO selectUserVoById(String id);

    /**
     * 查询上级机构的用户信息
     *
     * @param username 用户名
     * @return R
     */
    List<AdminSysUser> listAncestorUsers(String username);

    /**
     * 保存用户信息
     *
     * @param adminSysUserDto DTO 对象
     * @return ok/fail
     */
    Boolean saveUser(AdminSysUserDTO adminSysUserDto);

    /**
     * 无租户查询
     *
     * @param sysUser
     * @return SysUser
     */
    AdminSysUser getByNoTenant(AdminSysUser sysUser);

    /**
     * 获取所有采购员用户名
     * @return
     */
    List<AdminPurchaseUserIdAndNameVo> getAdminPurchaseUser();

    /**
     * 根据id查询用户信息
     */
    AdminSysUseDetailVO getUsersById(String id);
}
