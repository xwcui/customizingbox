package com.customizingbox.cloud.manager.auth.component;

import cn.hutool.http.HttpStatus;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Api;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 客户端异常处理
 * 根据 AuthenticationException不同细化异常处理
 */
@Slf4j
@Component
@AllArgsConstructor
public class BaseResourceAuthExceptionEntryPoint implements AuthenticationEntryPoint {

    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        response.setCharacterEncoding(CommonConstants.UTF8);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        ApiResponse<String> result = new ApiResponse<>();
        result.setStatus(ApiResStatus.FAIL.getStatus());
        if (exception != null) {
            result.setData(exception.getMessage());
            result.setMessage(exception.getMessage());
            if (exception instanceof InsufficientAuthenticationException
                    || exception instanceof CredentialsExpiredException) {
                result.setStatus(ApiResStatus.INVALID_TOKEN.getStatus());
                result.setMessage("证书过期请重新登录");
                result.setData("invalid_token");
            }
        }
        response.setStatus(HttpStatus.HTTP_UNAUTHORIZED);
        PrintWriter printWriter = response.getWriter();
        printWriter.append(objectMapper.writeValueAsString(result));
    }
}
