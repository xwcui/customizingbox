package com.customizingbox.cloud.manager.service.app.impl;

import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderItemMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.OrderToSaiheOrderItemVo;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ProductStock;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.ApiProductStockResult;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.ProductStockResultList;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.UpdateProductStockNumberResponse;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.UpdateProductStockNumberResult;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductAttrService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderItemService;
import com.customizingbox.cloud.manager.service.app.AppStoreProductVariantService;
import com.customizingbox.cloud.manager.util.SaiheApiUtils;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * panda订单item表
 *
 * @author Y
 * @date 2022-04-13 17:03:41
 */
@Service
@AllArgsConstructor
public class AppPandaOrderItemServiceImpl extends ServiceImpl<AppPandaOrderItemMapper, AppPandaOrderItem> implements AppPandaOrderItemService {

    private final AdminStoreProductAttrService adminStoreProductAttrService;
    private final AppStoreProductVariantService appStoreProductVariantService;
    private final SaiheApiUtils saiheApiUtils;



    @Override
    public List<AppPandaOrderItem> queryByOrderId(Long pandaOrderId) {
        return this.list(Wrappers.<AppPandaOrderItem>lambdaQuery().eq(AppPandaOrderItem::getOrderId, pandaOrderId));
    }

    @Override
    public IPage<UniqueProductPageVo> uniqueProductPage(UniqueProductSearchVo uniqueProductSearchVo, BaseUser user, Page page) {
        IPage<UniqueProductPageVo> uniqueProductPageVoIPage = baseMapper.uniqueProductPage(uniqueProductSearchVo, page);
        if (!CollectionUtils.isEmpty(uniqueProductPageVoIPage.getRecords())) {
            uniqueProductPageVoIPage.getRecords().stream().forEach(waitOrder -> {
                // 这里组装规格, key 就是规格值, value 就是规格名
                JSONArray attrValues = JSONObject.parseArray(waitOrder.getAttrValues());
                waitOrder.setProductAttr(adminStoreProductAttrService.queryAttrByProductId(waitOrder.getId(), attrValues));
            });
        }
        return uniqueProductPageVoIPage;
    }


    @Override
    public ApiResponse processUniqueProductStockNumberToSaihe(List<String> onlyCodes, BaseUser user) {
        List<ProductStock>  list = baseMapper.getProcessUniqueProductStockNumberToSaiheList(onlyCodes);
        UpdateProductStockNumberResponse updateProductStockNumberResponse = saiheApiUtils.updateProductStockNumber(list);
        UpdateProductStockNumberResult updateProductStockNumberResult = updateProductStockNumberResponse.getUpdateProductStockNumberResult();
        ArrayList<String> result = new ArrayList<>();
        if (StringUtils.equalsIgnoreCase("OK",updateProductStockNumberResult.getStatus())){
            ProductStockResultList productStockResultList = updateProductStockNumberResult.getProductStockResultList();
            ArrayList<String>  onlyCodesResult= new ArrayList<>();
            for (ApiProductStockResult apiProductStockResult : productStockResultList.getApiProductStockResult()) {
                if (apiProductStockResult.getOperateState()){
                    onlyCodesResult.add(apiProductStockResult.getClientSKU());
                    result.add(apiProductStockResult.getClientSKU() + apiProductStockResult.getOperateMsg());
                }else {
                    result.add(apiProductStockResult.getClientSKU() + apiProductStockResult.getOperateMsg());
                }
            }
            baseMapper.updateSaiheSatus(onlyCodesResult);
            return ApiResponse.ok(result.toString());
        }else {
            return ApiResponse.failed(updateProductStockNumberResult.getMsg());
        }
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean reloadOrderItemProductRelevancy(AppPandaOrder appPandaOrder) {
        Long pandaOrderId = appPandaOrder.getId();
        if(ObjectUtils.isEmpty(appPandaOrder) || !OrderEnum.PAY_STATUS.NOT_PAY.getCode().equals(appPandaOrder.getPayStatus())) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "未支付订单才能重新识别订单商品");
        }
        List<AppPandaOrderItem> appPandaOrderItems = this.queryByOrderId(pandaOrderId);
        for (AppPandaOrderItem appPandaOrderItem : appPandaOrderItems) {
            Long variantId = appPandaOrderItem.getVariantId();

            AppStoreProductVariant variant = appStoreProductVariantService.getById(variantId);
            if (!variant.getAdminVariantId().equals(appPandaOrderItem.getAdminVariantId())) {
                // 关联admin发生了变化
                appPandaOrderItem.setAdminVariantId(variant.getAdminVariantId());
                appPandaOrderItem.setAdminProductId(variant.getAdminProductId());
                this.updateById(appPandaOrderItem);
            }
        }
        return true;
    }

    /**
     * 获取订单上传赛盒的item
     * @param orderId
     * @return
     */
    @Override
    public List<OrderToSaiheOrderItemVo> getOrderItemByOrderId(Long orderId) {
        return baseMapper.getOrderItemByOrderId(orderId);
    }
}
