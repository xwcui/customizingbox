package com.customizingbox.cloud.manager.service.admin.impl.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysDictMapper;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysDictValueMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysDict;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysDictValue;
import com.customizingbox.cloud.manager.service.admin.AdminSysDictService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AdminSysDictServiceImpl extends ServiceImpl<AdminSysDictMapper, AdminSysDict> implements AdminSysDictService {

    private final AdminSysDictValueMapper adminSysDictValueMapper;

    /**
     * 根据ID 删除字典
     *
     * @param id 字典ID
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse removeDict(String id) {
        baseMapper.deleteById(id);
        adminSysDictValueMapper.delete(Wrappers.<AdminSysDictValue>lambdaQuery()
                .eq(AdminSysDictValue::getDictId, id));
        return ApiResponse.ok();
    }

    /**
     * 更新字典
     *
     * @param dict 字典
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse updateDict(AdminSysDict dict) {
        this.updateById(dict);
        AdminSysDictValue adminSysDictValue = new AdminSysDictValue();
        adminSysDictValue.setType(dict.getType());
        adminSysDictValueMapper.update(adminSysDictValue, Wrappers.<AdminSysDictValue>lambdaQuery().eq(AdminSysDictValue::getDictId, dict.getId()));
        return ApiResponse.ok();
    }
}
