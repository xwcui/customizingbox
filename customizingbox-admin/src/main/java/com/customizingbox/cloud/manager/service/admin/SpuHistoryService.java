package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.SpuHistory;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-26
 */
public interface SpuHistoryService extends IService<SpuHistory> {


    /**
     * 获取spu
     * @return
     */
    String getSpuIndex();

}
