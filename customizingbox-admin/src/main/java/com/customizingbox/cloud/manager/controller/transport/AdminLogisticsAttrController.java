package com.customizingbox.cloud.manager.controller.transport;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminLogisticsAttrDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsAttr;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminLogisticsAttrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 物流属性表 前端控制器
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@RestController
@RequestMapping("/adminLogisticsAttr")
@Api(tags = "物流属性表")
public class AdminLogisticsAttrController {

    @Autowired
    private AdminLogisticsAttrService service;

    @ApiOperation("/物流属性分页")
    @PostMapping("/page")
    public ApiResponse<IPage<AdminLogisticsAttrPageVo>> page(Page page, @RequestBody AdminLogisticsAttrSearchVo searchVo){
        if (page == null){
            page = new Page();
        }
        IPage<AdminLogisticsAttrPageVo> result = service.pageAll(page,searchVo);
        return ApiResponse.ok(result);
    }

    @ApiOperation("/物流属性List")
    @PostMapping("/List")
    public ApiResponse<List<AdminLogisticsAttr>> page(){
        List<AdminLogisticsAttr> result = service.getList();
        return ApiResponse.ok(result);
    }

    @ApiOperation("/物流属性新增或修改")
    @PostMapping("/saveOrUpdate")
    public ApiResponse saveOrUpdate(@RequestBody @Validated AdminLogisticsAttrDto adminLogisticsAttrDto){
        BaseUser user = SecurityUtils.getUser();
        if (user == null){
            throw new ApiException("无法获取");
        }
        return  service.saveOrUpdateAdminLogisticsAttrDto(adminLogisticsAttrDto,user);
    }

    @ApiOperation("/物流属性开启禁用")
    @PostMapping("/openOrShut/{id}/{status}")
    public ApiResponse openOrShut(@PathVariable Long id,@PathVariable Integer status){
        BaseUser user = SecurityUtils.getUser();
        if (user == null){
            throw new ApiException("无法获取");
        }
        return  service.openOrShut(id,status);
    }



}
