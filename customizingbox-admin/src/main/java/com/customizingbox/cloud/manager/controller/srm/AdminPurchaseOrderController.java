package com.customizingbox.cloud.manager.controller.srm;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.srm.query.AdminPurchaseOrderCollectQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.AdminPurchaseOrderWaitQuery;
import com.customizingbox.cloud.common.datasource.model.srm.vo.AdminPurchaseOrderCheckConsoleVO;
import com.customizingbox.cloud.common.datasource.model.srm.vo.AdminPurchaseOrderCollectVO;
import com.customizingbox.cloud.common.datasource.model.srm.vo.AdminPurchaseOrderWaitVO;
import com.customizingbox.cloud.common.datasource.model.srm.vo.PurchaseOrderProgressVo;
import com.customizingbox.cloud.manager.service.admin.AdminPurchaseOrderService;
import com.customizingbox.cloud.manager.service.srm.SrmPurchaseOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 采购订单表 前端控制器
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
@RestController
@RequestMapping("/purchase/order")
@Api(tags = "admin采购订单表")
@AllArgsConstructor
public class AdminPurchaseOrderController {

    private final AdminPurchaseOrderService adminPurchaseOrderService;
    private final SrmPurchaseOrderService srmPurchaseOrderService;

    @ApiOperation(value = "1 - 分页查询待下单列表")
    @PostMapping("/waitOrderPage")
    public ApiResponse<IPage<AdminPurchaseOrderWaitVO>> waitOrderPage(Page page, @Valid @RequestBody AdminPurchaseOrderWaitQuery query) {
        IPage<AdminPurchaseOrderWaitVO> resultPage = adminPurchaseOrderService.waitOrderPage(page, query);
        return ApiResponse.ok(resultPage);
    }

    @ApiOperation(value = "2 - 下单")
    @GetMapping("/placePurchaseOrder/{orderItemId}")
    public ApiResponse<Boolean> placePurchaseOrder(@PathVariable(value = "orderItemId") String orderItemId) {
        Boolean resultPage = adminPurchaseOrderService.placePurchaseOrder(orderItemId);
        return ApiResponse.ok(resultPage);
    }

    @ApiOperation(value = "2.1 - 批量下单")
    @GetMapping("/placePurchaseOrders")
    public ApiResponse<Boolean> placePurchaseOrder(@ApiParam(value = "订单列表", required = true) @RequestBody List<Long> orderItemIds) {
        Boolean resultPage = adminPurchaseOrderService.placePurchaseOrders(orderItemIds);
        return ApiResponse.ok(resultPage);
    }

    @ApiOperation(value = "3 - 采购进度汇总表")
    @PostMapping("/purchaseCollect")
    public ApiResponse<IPage<AdminPurchaseOrderCollectVO>> purchaseCollect(Page page, @Valid @RequestBody AdminPurchaseOrderCollectQuery query) {
        IPage<AdminPurchaseOrderCollectVO> resultPage = adminPurchaseOrderService.purchaseCollect(page, query);
        return ApiResponse.ok(resultPage);
    }

    @ApiOperation("4 - 查看进度")
    @GetMapping("/getProgress")
    public ApiResponse<PurchaseOrderProgressVo> getProgress(@ApiParam(value = "采购订单id") @RequestParam Long purchaseOrderId){
        return ApiResponse.ok(srmPurchaseOrderService.getProgress(purchaseOrderId));
    }

    @ApiOperation("5 - 质检操作台")
    @GetMapping("/checkConsole")
    public ApiResponse<AdminPurchaseOrderCheckConsoleVO> checkConsole(@ApiParam(value = "唯一码") @RequestParam String onlyCode){
        return ApiResponse.ok(adminPurchaseOrderService.checkConsole(onlyCode));
    }

    @ApiOperation("6 - 质检")
    @GetMapping("/check")
    public ApiResponse<Boolean> check(){
        return ApiResponse.ok();
    }
}