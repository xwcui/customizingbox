package com.customizingbox.cloud.manager.auth.component;

import com.customizingbox.cloud.common.core.constant.SecurityConstants;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 根据 checkToken 的结果转化用户信息
 */
public class BaseUserAuthenticationConverter implements UserAuthenticationConverter {

    private static final String N_A = "N/A";

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        Map<String, Object> response = new LinkedHashMap<>();
        response.put(UserAuthenticationConverter.USERNAME, authentication.getName());
        if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
            response.put(UserAuthenticationConverter.AUTHORITIES, AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
        }
        return response;
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey(UserAuthenticationConverter.USERNAME)) {
            Collection<? extends GrantedAuthority> authorities = this.getAuthorities(map);

            String username = (String) map.get(UserAuthenticationConverter.USERNAME);
            String id = (String) map.get(SecurityConstants.DETAILS_USER_ID);
            String organId = (String) map.get(SecurityConstants.DETAILS_ORGAN_ID);
            String tenantId = (String) map.get(SecurityConstants.DETAILS_TENANT_ID);
            Boolean isCustomerManager = (Boolean) map.get(SecurityConstants.IS_CUSTOMER_MANAGER);
            Boolean isPurchaseManager = (Boolean) map.get(SecurityConstants.IS_PURCHASE_MANAGER);
            Boolean isAdministrator = (Boolean) map.get(SecurityConstants.DETAILS_ROLE_ADMINISTRATOR);
            String shCode = (String) map.get(SecurityConstants.SAI_HE_CODE);
            BaseUser user = new BaseUser(id, organId, tenantId, username, N_A,
                    isCustomerManager, isPurchaseManager, isAdministrator, shCode,
                    true, true, true, true, authorities);
            return new UsernamePasswordAuthenticationToken(user, N_A, authorities);
        }
        return null;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
        Object authorities = map.get(UserAuthenticationConverter.AUTHORITIES);
        if (authorities instanceof String) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList((String) authorities);
        }
        if (authorities instanceof Collection) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils
                    .collectionToCommaDelimitedString((Collection<?>) authorities));
        }
        throw new IllegalArgumentException("Authorities must be either a String or a Collection");
    }
}
