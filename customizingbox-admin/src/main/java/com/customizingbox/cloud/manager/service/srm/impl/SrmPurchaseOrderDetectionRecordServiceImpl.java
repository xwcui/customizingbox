package com.customizingbox.cloud.manager.service.srm.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.srm.AdminPurchaseOrderDetectionRecordMapper;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrderDetectionRecord;
import com.customizingbox.cloud.manager.service.srm.SrmPurchaseOrderDetectionRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 质检记录表 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-05-03
 */
@Service
public class SrmPurchaseOrderDetectionRecordServiceImpl extends ServiceImpl<AdminPurchaseOrderDetectionRecordMapper, AdminPurchaseOrderDetectionRecord> implements SrmPurchaseOrderDetectionRecordService {

}
