package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;

import java.util.List;
import java.util.Map;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
public interface AppStoreProductVariantService extends IService<AppStoreProductVariant> {
}
