package com.customizingbox.cloud.manager.syslog.util;

import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * 系统日志工具类
 *
 */
@UtilityClass
public class SysLogUtils {
	public AdminSysLog getSysLog() {
		HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
		AdminSysLog adminSysLog = new AdminSysLog();
		adminSysLog.setCreateBy(Objects.requireNonNull(getUsername()));
		adminSysLog.setCreateId(Objects.requireNonNull(getUserId()));
		adminSysLog.setRemoteAddr(ServletUtil.getClientIP(request));
		adminSysLog.setRequestUri(URLUtil.getPath(request.getRequestURI()));
		adminSysLog.setMethod(request.getMethod());
		adminSysLog.setUserAgent(request.getHeader("user-agent"));
		adminSysLog.setParams(HttpUtil.toParams(request.getParameterMap()));
		adminSysLog.setServiceId(getClientId());
		return adminSysLog;
	}

	/**
	 * 获取客户端
	 *
	 * @return clientId
	 */
	public String getClientId() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof OAuth2Authentication) {
			OAuth2Authentication auth2Authentication = (OAuth2Authentication) authentication;
			return auth2Authentication.getOAuth2Request().getClientId();
		}
		return null;
	}

	/**
	 * 获取用户ID
	 *
	 * @return username
	 */
	public String getUserId() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return null;
		}
		return SecurityUtils.getUser(authentication).getId();
	}

	/**
	 * 获取用户名称
	 *
	 * @return username
	 */
	public String getUsername() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return null;
		}
		return authentication.getName();
	}

}
