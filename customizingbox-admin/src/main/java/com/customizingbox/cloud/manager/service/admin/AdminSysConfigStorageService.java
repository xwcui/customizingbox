package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysConfigStorage;

/**
 * 存储配置
 */
public interface AdminSysConfigStorageService extends IService<AdminSysConfigStorage> {

}
