package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysGenTable;

import java.util.List;
import java.util.Map;


public interface AdminSysGeneratorService {
	/**
	 * 生成代码预览
	 *
	 * @param adminSysGenTable
	 */
	Map<String, String> generatorView(AdminSysGenTable adminSysGenTable);

	/**
	 * 生成代码
	 *
	 * @param adminSysGenTable 生成表配置
	 */
	byte[] generatorCode(AdminSysGenTable adminSysGenTable);

	/**
	 * 分页查询表
	 *
	 * @param tableName       表名
	 * @param sysDatasourceId 数据源ID
	 */
	IPage<List<Map<String, Object>>> getPage(Page page, String tableName, String sysDatasourceId);
}
