package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysDict;

/**
 * <p>
 * 字典表 服务类
 * </p>
 */
public interface AdminSysDictService extends IService<AdminSysDict> {

    /**
     * 根据ID 删除字典
     */
    ApiResponse removeDict(String id);

    /**
     * 更新字典
     */
    ApiResponse updateDict(AdminSysDict adminSysDict);
}
