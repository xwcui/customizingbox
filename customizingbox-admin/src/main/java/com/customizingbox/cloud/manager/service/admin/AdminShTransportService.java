package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminShTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminShTransportSearchVo;

import java.util.List;

/**
 * <p>
 * 赛合运输方式 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-13
 */
public interface AdminShTransportService extends IService<AdminShTransport> {


    /**
     * 更新赛盒物流
     * @return
     */
    ApiResponse toUpdateShTransport();

    /**
     * 获取赛盒物流
     * @return
     */
    List<AdminShTransport> getTransportList();

    /**
     * 获取分页信息
     * @param page
     * @param searchVo
     * @return
     */
    IPage<AdminShTransport> pageAll(Page page, AdminShTransportSearchVo searchVo);
}
