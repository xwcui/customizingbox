package com.customizingbox.cloud.manager.service.srm;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrderDetectionRecord;

/**
 * <p>
 * 质检记录表 服务类
 * </p>
 *
 * @author Z
 * @since 2022-05-03
 */
public interface SrmPurchaseOrderDetectionRecordService extends IService<AdminPurchaseOrderDetectionRecord> {

}
