package com.customizingbox.cloud.manager.service.srm;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.srm.dto.FillInProgressDto;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrder;
import com.customizingbox.cloud.common.datasource.model.srm.query.DefectiveProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.FulfilledProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.ProducingProductQuery;
import com.customizingbox.cloud.common.datasource.model.srm.query.ReadyProduceQuery;
import com.customizingbox.cloud.common.datasource.model.srm.vo.*;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.DeliveryProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.ProducingProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.PurchaseProductVo;

import java.util.List;

/**
 * <p>
 * 采购订单表 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
public interface SrmPurchaseOrderService extends IService<AdminPurchaseOrder> {
    /**
    * 采购订单表列表
    */
    List<PurchaseProductVo> listReadyProduce(ReadyProduceQuery query);

    /**
     * 采购订单表列表分页
     * @param page
     * @return
     */
    IPage<PurchaseProductVo> pageReadyProduce(Page page);

    /**
     * 开始生产
     * @param purchaseOrderIds 采购单集合
     * @return
     */
    Boolean startProduce(List<Long> purchaseOrderIds);

    /**
     * 打回
     * @param purchaseOrderId 采购订单id
     * @param refuseReason 打回原因
     * @return
     */
    Boolean refuseProduce(Long purchaseOrderId, String refuseReason);

    /**
     * 生产中产品列表分页
     * @param page
     * @param query
     * @return
     */
    IPage<ProducingProductVo> pageProducingProduct(Page page, ProducingProductQuery query);

    /**
     * 获取打印唯一码数据
     * @param query
     * @return
     */
    List<PrintOnlyCodeVo> printOnlyCodeBatch(ProducingProductQuery query);

    /**
     * 生产中产品id
     * @param producingProductQuery
     * @return
     */
    List<Long> listProducingProduct(ProducingProductQuery producingProductQuery);

    /**
     * 导出生产资料
     * @param purchaseOrderIds
     * @return
     */
    List<ExportProductionMeans> exportProductionMeansBatch(List<Long> purchaseOrderIds);

    /**
     * 获取生产中批次总产品数
     * @return
     * @param page
     */
    IPage<ProducingBatchDetailVo> getPorducingBatch(Page page);

    /**
     * 已发货采购产品分页查询
     * @param page
     * @param query
     * @return
     */
    IPage<DeliveryProductVo> pageFulfilled(Page page, FulfilledProductQuery query);

    /**
     * 查询单个唯一码对应的产品详情
     * @param onlyCode
     * @return
     */
    ReadyDeliveryProductVo getProductByOnlyCode(String onlyCode);

    /**
     * 确认发货
     * @param commitDeliveryVo
     * @return
     */
    Boolean commitDelivery(CommitDeliveryRequestVo commitDeliveryVo);

    /**
     * 不良品列表分页查询
     * @param page
     * @param query
     * @return
     */
    IPage<DefectiveProductVo> pageDefectiveProducts(Page page, DefectiveProductQuery query);

    /**
     * 查看进度
     * @param purchaseOrderId
     * @return
     */
    PurchaseOrderProgressVo getProgress(Long purchaseOrderId);

    /**
     * 填写进度
     * @param fillInProgressDto
     * @return
     */
    Boolean fillInProgress(FillInProgressDto fillInProgressDto);

    /**
     * 不良品质检明细
     * @param onlyCode
     * @return
     */
    SrmCheckDetail checkDetail(String onlyCode);

}
