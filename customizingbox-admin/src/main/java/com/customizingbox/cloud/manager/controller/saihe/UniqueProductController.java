package com.customizingbox.cloud.manager.controller.saihe;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductSearchVo;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/uniqueProduct")
@Api(tags = "唯一码产品")
@AllArgsConstructor
public class UniqueProductController {

    private AdminStoreProductService adminStoreProductService;

    private AppPandaOrderItemService appPandaOrderItemService;


    @ApiOperation(value = "待上传赛盒产品分页")
    @PostMapping("page")
    public ApiResponse<IPage<UniqueProductPageVo>> uniqueProductPage(Page page, @RequestBody UniqueProductSearchVo uniqueProductSearchVo) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        if (page == null) {
            page = new Page();
        }
        IPage<UniqueProductPageVo> pageVo = appPandaOrderItemService.uniqueProductPage(uniqueProductSearchVo, user, page);
        return ApiResponse.ok(pageVo);
    }


    @ApiOperation(value = "待上传赛盒产品库存分页")
    @PostMapping("stockpage")
    public ApiResponse<IPage<UniqueProductPageVo>> uniqueProductToSaiheStockPage(Page page, @RequestBody UniqueProductSearchVo uniqueProductSearchVo) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        if (page == null) {
            page = new Page();
        }
        uniqueProductSearchVo.setPurchaseStatus(OrderEnum.PURCHASE_ORDER_STATUS.SUCCESS.getCode());
        IPage<UniqueProductPageVo> pageVo = appPandaOrderItemService.uniqueProductPage(uniqueProductSearchVo, user, page);
        return ApiResponse.ok(pageVo);
    }



    /**
     * 唯一码产品上传赛盒上传赛盒
     */
    @ApiOperation(value = "唯一码产品上传赛盒上传赛盒")
    @PostMapping("processUniqueProductToSaihe")
    public ApiResponse processUniqueProductToSaihe(@ApiParam("产品唯一码集合") @RequestBody List<String> onlyCodes) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return adminStoreProductService.processUniqueProductsToSaihe(onlyCodes);
    }

    /**
     * 唯一码产品上传赛盒创建库存数量
     */
    @ApiOperation(value = "唯一码产品上传赛盒创建库存数量")
    @PostMapping("processUniqueProductStockNumberToSaihe")
    public ApiResponse processUniqueProductStockNumberToSaihe(@ApiParam("产品唯一码集合") @RequestBody List<String> onlyCodes) {
        BaseUser user = SecurityUtils.getUser();
        if (user == null) {
            throw new ApiException("无法获取");
        }
        return appPandaOrderItemService.processUniqueProductStockNumberToSaihe(onlyCodes,user);
    }

}
