package com.customizingbox.cloud.manager.service.admin.impl.system;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysRoleMapper;
import com.customizingbox.cloud.common.datasource.mapper.admin.system.AdminSysRoleMenuMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRole;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRoleMenu;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminMenuVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysRoleDetailVO;
import com.customizingbox.cloud.manager.service.admin.AdminSysMenuService;
import com.customizingbox.cloud.manager.service.admin.AdminSysRoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AdminSysRoleServiceImpl extends ServiceImpl<AdminSysRoleMapper, AdminSysRole> implements AdminSysRoleService {

    private final AdminSysRoleMenuMapper adminSysRoleMenuMapper;
    private final AdminSysMenuService adminSysMenuService;
    /**
     * 通过用户ID，查询角色信息
     */
    @Override
    public List<String> findRoleIdsByUserId(String userId) {
        return baseMapper.listRoleIdsByUserId(userId);
    }

    /**
     * 通过角色ID，删除角色,并清空角色菜单缓存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeRoleById(String id) {
        adminSysRoleMenuMapper.delete(Wrappers.<AdminSysRoleMenu>update().lambda().eq(AdminSysRoleMenu::getRoleId, id));
        return this.removeById(id);
    }

    @Override
    public AdminSysRoleDetailVO getRoleAndMenuById(String id) {
        AdminSysRole adminSysRole = this.getById(id);
        AdminSysRoleDetailVO adminSysRoleDetailVO = BeanUtil.copyProperties(adminSysRole, AdminSysRoleDetailVO.class);
        List<String> collect = adminSysMenuService.findMenuByRoleId(id)
                .stream()
                .map(AdminMenuVO::getId)
                .collect(Collectors.toList());
        adminSysRoleDetailVO.setMenuIds(collect);
        return adminSysRoleDetailVO;
    }
}
