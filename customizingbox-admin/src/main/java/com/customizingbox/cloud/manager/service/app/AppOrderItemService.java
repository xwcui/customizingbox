package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;

public interface AppOrderItemService<T> extends IService<AppOrderItem> {
    /**
     * 修改订单中产品映射关系
     * @param sourceVariantId 商户产品变体id
     * @param adminProductId admin 产品id
     * @param adminVariantId admin 产品变体id
     * @return
     */
    Boolean updateOrderItemProductRelevancy(Long sourceVariantId, Long adminProductId, Long adminVariantId);
}
