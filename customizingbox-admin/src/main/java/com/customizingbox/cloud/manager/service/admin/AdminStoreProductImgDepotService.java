package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;

import java.util.List;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-31 09:34:40
 */
public interface AdminStoreProductImgDepotService extends IService<AdminStoreProductImgDepot> {


    /**
     * 根据产品id获取产品图片
     */
    List<AdminStoreProductImgDepot> getListByProductId(Long productId);

    /**
     * 根据id删除图片信息
     * @param oldImgIds
     * @return
     */
    int delImageByIds(List<Long> oldImgIds);
}
