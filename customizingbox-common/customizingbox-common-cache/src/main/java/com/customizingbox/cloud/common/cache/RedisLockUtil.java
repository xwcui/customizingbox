package com.customizingbox.cloud.common.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁实现
 */
@Slf4j
@Component
public class RedisLockUtil {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 堵塞尝试获得锁，成功返回true，如果失败立即返回false
     * lockSeconds 加锁的时间(秒)，超过这个时间后锁会自动释放
     * blockSec 等待时间(秒)，这个时间内会重试
     */
    public boolean tryLockWithBlock(String lockKey, String value, int lockSeconds, int blockSec) {
        try {
            int maxTryCount = blockSec * 1000 / 50;
            return tryLockWithBlock(lockKey, value, lockSeconds, 50, maxTryCount);
        } catch (Exception e) {
            log.error("tryLockWithBlock Error", e);
            return false;
        }
    }

    /**
     * 堵塞轮询的方式去获得锁，成功返回true，超过轮询次数或异常返回false
     *
     * @param lockSeconds       加锁的时间(秒)，超过这个时间后锁会自动释放
     * @param tryIntervalMillis 轮询的时间间隔(毫秒)
     * @param maxTryCount       最大的轮询次数
     */
    public boolean tryLockWithBlock(String lockKey, String value, final int lockSeconds, final long tryIntervalMillis, final int maxTryCount) {
        int tryCount = 0;
        while (true) {
            if (++tryCount >= maxTryCount) {
                log.info("<--tryLockWithBlock 超时 lockKey:{} -->", lockKey);
                return false;
            }
            try {
                if (doLock(lockKey, value, lockSeconds)) {
                    return true;
                }
            } catch (Exception e) {
                log.error("tryLockWithBlock tryLockWithBlock Error", e);
                return false;
            }
            try {
                Thread.sleep(tryIntervalMillis);
            } catch (Exception e) {
                log.error("tryLockWithBlock interrupted", e);
                return false;
            }
        }
    }

    /**
     * 获取锁操作
     */
    public boolean doLock(String lockKey, String lockValue, int lockSeconds) {
        return redisTemplate.opsForValue().setIfAbsent(lockKey, lockValue, lockSeconds, TimeUnit.SECONDS);
    }

    /**
     * 解锁操作
     */
    public Boolean unlock(String lockKey) {
        return redisTemplate.delete(lockKey);
    }

}