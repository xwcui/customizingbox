package com.customizingbox.cloud.common.datasource.model.admin.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 接收分页查询客户商品参数
 * @author DC_Li
 * @date 2022/4/19 17:37
 */
@Data
@ApiModel
public class CustomerStoreProductsDto {
    @ApiModelProperty("店铺id")
    private Long storeId;

    @ApiModelProperty("店铺名称")
    private String storeName;

    @ApiModelProperty("客户id")
    private Long appUserId;

    @ApiModelProperty("客户邮箱")
    private String appUserEmail;

    @ApiModelProperty("客户商品id")
    private Long productId;

    @ApiModelProperty("报价状态 true 已报价 false 未报价  null 查全部")
    private Boolean quoteState;

}
