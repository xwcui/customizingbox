package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import javax.xml.bind.annotation.XmlElement;

public class GetPackageRequest {

    /**
     * <pre>
     * 客户号
     * </pre>
     */
    private String	customerID;

    /**
     * <pre>
     *用户名
     * </pre>
     */
    private String	userName;

    /**
     * <pre>
     *密码
     * </pre>
     */
    private String	password;

    /**
     * <pre>
     *包裹单号
     * </pre>
     */
    private String	packageID;

    /**
     * <pre>
     *发货时间开始
     * </pre>
     */
    private String	shipTimeBegin;

    /**
     * <pre>
     *发货时间结束
     * </pre>
     */
    private String	shipTimeEnd;

    /**
     * <pre>
     *追踪号
     * </pre>
     */
    private String	trackNumbers;

    /**
     * <pre>
     * 渠道类型
     * </pre>
     */
    private String	orderSourceType;

    /**
     * <pre>
     *分页标识 第二次回传时填写(用于分页)
     * </pre>
     */
    private String	nextToken;

    public GetPackageRequest() {
    }

    @XmlElement(name="CustomerID")
    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    @XmlElement(name="UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlElement(name="Password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlElement(name="PackageID")
    public String getPackageID() {
        return packageID;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    @XmlElement(name="ShipTimeBegin")
    public String getShipTimeBegin() {
        return shipTimeBegin;
    }

    public void setShipTimeBegin(String shipTimeBegin) {
        this.shipTimeBegin = shipTimeBegin;
    }

    public String getShipTimeEnd() {
        return shipTimeEnd;
    }

    @XmlElement(name="ShipTimeEnd")
    public void setShipTimeEnd(String shipTimeEnd) {
        this.shipTimeEnd = shipTimeEnd;
    }

    @XmlElement(name="OrderSourceType")
    public String getOrderSourceType() {
        return orderSourceType;
    }

    public void setOrderSourceType(String orderSourceType) {
        this.orderSourceType = orderSourceType;
    }

    @XmlElement(name="NextToken")
    public String getNextToken() {
        return nextToken;
    }

    public void setNextToken(String nextToken) {
        this.nextToken = nextToken;
    }
}
