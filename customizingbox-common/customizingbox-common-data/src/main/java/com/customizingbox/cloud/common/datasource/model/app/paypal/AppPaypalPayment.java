package com.customizingbox.cloud.common.datasource.model.app.paypal;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * <p>
 * payapl交易记录
 * </p>
 *
 * @author Z
 * @since 2022-04-22
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("app_paypal_payment")
@ApiModel(value = "payapl交易记录")
public class AppPaypalPayment extends Model<AppPaypalPayment> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "收款方交易号")
    private String saleId;

    @ApiModelProperty(value = "paypal交易ID")
    private String paymentId;

    @ApiModelProperty(value = "0=余额充值")
    private Integer orderType;

    private String paymentMethod;

    @ApiModelProperty(value = "付款人邮箱")
    private String payerEmail;

    @ApiModelProperty(value = "付款人姓名")
    private String payerName;

    @ApiModelProperty(value = "付款人ID")
    private String payerId;

    @ApiModelProperty(value = "交易状态")
    private String state;

    @ApiModelProperty(value = "交易金额")
    private String amount;

    @ApiModelProperty(value = "货币")
    private String currency;

    @ApiModelProperty(value = "手续费")
    private String fixFee;

    private String createTime;

    private String updateTime;

    @ApiModelProperty(value = "收款人邮箱")
    private String payeeEmail;

    private String merchantId;

    @ApiModelProperty(value = "0 : 余额充值")
    private Integer source;

    @ApiModelProperty(value = "0 : 待审核 1 ：正常")
    private Integer type;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "返回json")
    private String resultJson;

    @ApiModelProperty(value = "用户ID")
    private Long userId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}