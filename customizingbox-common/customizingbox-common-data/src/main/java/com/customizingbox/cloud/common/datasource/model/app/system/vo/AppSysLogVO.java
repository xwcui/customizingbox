package com.customizingbox.cloud.common.datasource.model.app.system.vo;

import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLog;
import lombok.Data;

import java.io.Serializable;


@Data
public class AppSysLogVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private AppSysLog appSysLog;
	private String username;
}
