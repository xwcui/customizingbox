package com.customizingbox.cloud.common.datasource.model.srm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/5/6 15:32
 */
@Data
@ApiModel("打回采购单接收请求数据")
public class RefuseProduceDto {
    @ApiModelProperty("采购单id")
    private Long purchaseOrderId;

    @ApiModelProperty("打回原因")
    private String refuseReason;
}
