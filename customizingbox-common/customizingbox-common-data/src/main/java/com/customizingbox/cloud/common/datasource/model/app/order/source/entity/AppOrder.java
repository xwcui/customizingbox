package com.customizingbox.cloud.common.datasource.model.app.order.source.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 原平台订单表(用于给客户展示, 客户可下单至panda订单表)
 *
 * @author Y
 * @date 2022-03-30 13:48:18
 */
@Data
@TableName("app_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "原平台订单表(用于给客户展示, 客户可下单至panda订单表)")
public class AppOrder extends Model<AppOrder> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 商户订单id
     */
    @ApiModelProperty(value = "商户订单id")
    private Long sourceId;
    /**
     * 订单号, 用来给用户展示
     */
    @ApiModelProperty(value = "订单号, 用来给用户展示")
    private String orderNo;
    /**
     * 店铺id
     */
    @ApiModelProperty(value = "店铺id")
    private Long storeId;
    /**
     * 订单名称
     */
    @ApiModelProperty(value = "订单名称")
    private String orderName;
    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "店铺名称")
    private String storeName;
    /**
     * 平台类型. 1: shopify
     */
    @ApiModelProperty(value = "平台类型. 1: shopify  ")
    private Integer platformType;
    /**
     * 客户id
     */
    @ApiModelProperty(value = "客户id")
    private Long userId;

    @ApiModelProperty(value = "运费")
    private BigDecimal freightAmount;

    @ApiModelProperty(value = "运费")
    private String trackingCompany;

    @ApiModelProperty(value = "APP下单状态(1: 未下单  2: 部分下单 3: 全部下单)")
    private Integer placeStatus;

    /**
     * 和admin产品关联状态; 1: 全部关联 2: 存在未关联
     */
    @ApiModelProperty(value = "和admin产品关联状态; 1: 全部关联 2: 部分关联 3: 全部未关联")
    private Integer relevancyStatus;


    @ApiModelProperty(value = "发货状态( shipped: 已经发货, partial: 部分发货, unshipped: 没有发货,  any; unfulfilled)")
    private String financialStatus;

    @ApiModelProperty(value = "支付状态(authorized: 已授权; pending: 待支付; paid: 已支付;refunded: 退款;  voided; any)")
    private String fulfillmentStatus;
    /**
     * 原订单备注
     */
    @ApiModelProperty(value = "原订单备注")
    private String sourceMark;
    /**
     * 收货地址
     */
    @ApiModelProperty(value = "收货地址")
    private Long addressId;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
    /**
     * 店铺关联的产品经理id(就是admin用户id)
     */
    @ApiModelProperty(value = "店铺关联的产品经理id(就是admin用户id)")
    private Long adminUserId;
    /**
     * 数据版本号.  乐观锁
     */
    @ApiModelProperty(value = "数据版本号.  乐观锁")
    private Integer version;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "订单状态 (1: 正常订单, 2: 取消)")
    private Integer status;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "店铺订单创建时间")
    private LocalDateTime createdAt;

    @ApiModelProperty(value = "店铺订单修改时间")
    private LocalDateTime updatedAt;

    @ApiModelProperty(value = "店铺订单创建时间(北京时间)")
    @TableField(exist = false)
    private LocalDateTime createdAtCn;

    @ApiModelProperty(value = "店铺订单修改时间(北京时间)")
    @TableField(exist = false)
    private LocalDateTime updatedAtCn;
}
