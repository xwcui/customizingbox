package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单上传赛盒从数据库查出来的待处理数据
 */
@Data
public class OrderToSaiheVo {

    /**
     * platform_type 平台类型. 1: shopify
     */
    private Integer platformType;

    /**
     * 来源渠道ID(需在ERP系统中创建)
     */
    Integer orderSourceID;

    /**
     * 支付时间
     */
    Date payTime;

    /**
     * 客户订单号
     */
    String clientOrderCode;

    /**
     * 邮箱
     */
    String email;

    /**
     * 交易号
     */
    String transactionId;

    /**
     * 币种
     */
    String currency;

    /**
     * 订单总金额
     */
    BigDecimal totalPrice;

    /**
     * 运费收入
     */
    BigDecimal transportPay;
    /**
     * 运费收入
     */
    BigDecimal freightAmount;

    /**
     * vat 税费
     */
    BigDecimal vatAmount;

    /**
     * 产品费
     */
    BigDecimal productAmount;


    /**
     * 买家姓氏
     */
    String firstName;

    /**
     * 买家名字
     */
    String lastName;

    /**
     * 收货国家
     */
    String country;

    String countryCode;

    /**
     * company 公司 但是巴西会使用此字段存储cpf号
     */
    private String company;

    /**
     * 证件/税号
     */
    String taxNumber;

    /**
     * 寄件人税号
     */
    String senderTaxNumber;

    /**
     * 收货省份
     */
    String province;

    /**
     * 收货省份code
     */
    String provinceCode;

    /**
     * 收货城市
     */
    String city;

    /**
     * 邮编
     */
    String postCode;

    /**
     * 电话
     */
    String telephone;

    /**
     * 收货地址一
     */
    String address1;

    /**
     * 收货地址二
     */
    String address2;

    /**
     * 运输方式ID
     */
    Integer transportID;

    /**
     * 客户订单对应平台账号, 判断该平台的卖家唯一ID
     * 客户经理saihecode
     */
    String clientUserAccount;

    /**
     * 客户经理name
     */
    String adminUserName;


}
