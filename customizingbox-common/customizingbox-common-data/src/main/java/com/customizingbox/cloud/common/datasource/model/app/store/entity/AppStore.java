package com.customizingbox.cloud.common.datasource.model.app.store.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

import java.time.LocalDateTime;

/**
 * 商户表
 *
 * @author Y
 * @date 2022-03-23 09:51:00
 */
@Data
@TableName("app_store")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商户表")
public class AppStore extends Model<AppStore> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    @JsonSerialize
    private Long id;
    /**
     * 原平台商户id
     */
    @ApiModelProperty(value = "原平台商户id")
    private Long sourceId;
    /**
     * 商户名称
     */
    @ApiModelProperty(value = "商户名称")
    private String name;
    /**
     * 商户类型(1: shopify)
     */
    @ApiModelProperty(value = "商户类型(1: shopify)")
    private Integer platformType;
    /**
     * token
     */
    @ApiModelProperty(value = "token")
    private String accessToken;
    /**
     *
     */
    @ApiModelProperty(value = "")
    private Long locationId;
    /**
     * 国家
     */
    @ApiModelProperty(value = "国家")
    private String countryCode;
    /**
     * 货币
     */
    @ApiModelProperty(value = "货币")
    private String currency;
    /**
     * 商户状态. 1: 正常(默认), 2: 禁用(不在接收webhook)
     */
    @ApiModelProperty(value = "商户状态. 1: 正常(默认), 2: 禁用(不在接收webhook)")
    private Integer storeState;
    /**
     * 商户url
     */
    @ApiModelProperty(value = "商户url")
    private String domain;
    /**
     * 时区
     */
    @ApiModelProperty(value = "时区")
    private String timezone;
    /**
     *
     */
    @ApiModelProperty(value = "")
    private Integer relateType;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "删除标记（0：显示；1：隐藏）")
    @TableLogic
    private Integer delFlag;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
    /**
     * 创建者id
     */
    @ApiModelProperty(value = "创建者id")
    private Long createId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改者id
     */
    @ApiModelProperty(value = "修改者id")
    private Long updateId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 商户系统创建时间
     */
    private LocalDateTime created_at;

    /**
     * 商户系统更新时间
     */
    private LocalDateTime updated_at;

    /**
     * 商户系统创建时间（北京时间）
     */
    private LocalDateTime bj_created_at;

    /**
     * 商户系统更新时间（北京时间）
     */
    private LocalDateTime bj_updated_at;

}
