package com.customizingbox.cloud.common.datasource.model.admin.transport.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * <p>
 * 物流费用
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Data
@ApiModel(value = "物流费用")
public class AdminLogisticsFreightVo implements Serializable{


    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输方式id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long transportId;

    @ApiModelProperty(value = "运输方式名称")
    @NotBlank(message = "运输方式 不能为空")
    @Excel(name = "运输方式名称")
    private String transportName;

    @ApiModelProperty(value = "始发地id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long beginCountryId;

    @ApiModelProperty(value = "发件地区/国家")
    @NotNull(message ="发件地区/国家 不能为空" )
    @Excel(name = "发件地区/国家")
    private String beginCountryName;

    @ApiModelProperty(value = "目的地id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long endCountryId;

    @Excel(name = "收件地区/国家")
    @NotNull(message ="目的地id 不能为空" )
    private String endCountryName;

    @ApiModelProperty(value = "折扣")
    @NotNull(message ="折扣 不能为空" )
    @Excel(name = "折扣")
    private BigDecimal discount;

    @ApiModelProperty(value = "区间开始重量 单位克")
    @NotNull(message ="区间开始重量 不能为空" )
    @Excel(name = "最小重量")
    private Integer beginWeight;

    @ApiModelProperty(value = "区间结束重量 单位克")
    @NotNull(message ="区间结束重量 不能为空" )
    @Excel(name = "最大重量")
    private Integer endWeight;

    @ApiModelProperty(value = "固定费/挂号费")
    @NotNull(message ="固定费/挂号费 不能为空" )
    @Excel(name = "固定/挂号费")
    private BigDecimal fixedPrice;

    @ApiModelProperty(value = "克重费")
    @NotNull(message ="克重费 不能为空" )
    @Excel(name = "克重费")
    private BigDecimal gramPrice;

    @ApiModelProperty(value = "最早到达时间(天)")
    @NotNull(message ="最早到达时间 不能为空" )
    @Excel(name = "预计达到天数（下限）")
    private Integer earliestDay;

    @ApiModelProperty(value = "最晚到达时间(天)")
    @NotNull(message ="最晚到达时间 不能为空" )
    @Excel(name = "预计达到天数（上限）")
    private Integer longestDay;

    @ApiModelProperty(value = "备注")
    @Excel(name = "备注")
    private String mark;

    @ApiModelProperty(value = "启动时间")
    private LocalDateTime enableTime;

    @ApiModelProperty(value = "启用结束时间")
    private LocalDateTime notEnableTime;
}