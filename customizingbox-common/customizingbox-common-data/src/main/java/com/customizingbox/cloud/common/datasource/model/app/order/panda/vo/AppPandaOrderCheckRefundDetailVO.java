package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "订单退款审核页数据")
public class AppPandaOrderCheckRefundDetailVO {

    @ApiModelProperty(value = "客户名")
    private String username;

    @ApiModelProperty(value = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "当前客户经理")
    private String currentUserName;

    @ApiModelProperty(value = "下单客户经理")
    private String orderUserName;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "店铺名称")
    private String storeName;

    @ApiModelProperty(value = "店铺订单id")
    private String sourceOrderId;

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "支付状态(1: 未支付 2: 已支付,)")
    private String payStatus;

    @ApiModelProperty(value = "订单发货状态（1：未发货；2：部分发货；3：全部发货; 4: 已收货）")
    private String freightStatus;

    @ApiModelProperty(value = "支付运费")
    private BigDecimal freightAmount;

    @ApiModelProperty(value = "支付vat费用")
    private BigDecimal vatAmount;

    @ApiModelProperty(value = "本次运费申请退款费用")
    private BigDecimal applyFreightAmount;

    @ApiModelProperty(value = "本次vat申请退款费用")
    private BigDecimal applyVatAmount;

    @ApiModelProperty(value = "申请退款金额")
    private BigDecimal applyTotalAmount;

    @ApiModelProperty(value = "退款状态. 1 申请中 4 已退款(同意退款) 5 拒绝退款")
    private Integer status;

    @ApiModelProperty(value = "退款交易号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long transactionRecordId;

    @ApiModelProperty(value = "申请原因")
    private String applyRemark;

    @ApiModelProperty(value = "拒绝理由")
    private String rejectRemark;

    @ApiModelProperty(value = "item订单数据")
    private List<AppPandaOrderRefundItemDetailVO> itemDetailVOS;
}