package com.customizingbox.cloud.common.datasource.model.admin.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-31 09:34:36
 */
@Data
@TableName("admin_store_product_attr")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品属性表")
public class AdminStoreProductAttr extends Model<AdminStoreProductAttr> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性名
     */
    @ApiModelProperty(value = "属性名")
    private String attrName;
    /**
     * 属性值
     */
    @ApiModelProperty(value = "属性值")
    private String attrValues;

    @ApiModelProperty(value = "排序,与变体中的规格值顺序一致")
    private Integer sort;
}
