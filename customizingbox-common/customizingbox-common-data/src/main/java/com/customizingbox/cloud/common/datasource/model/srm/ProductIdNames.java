package com.customizingbox.cloud.common.datasource.model.srm;

import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/26 14:06
 */
@Data
public class ProductIdNames {
    private Long productId;
    private String names;
}
