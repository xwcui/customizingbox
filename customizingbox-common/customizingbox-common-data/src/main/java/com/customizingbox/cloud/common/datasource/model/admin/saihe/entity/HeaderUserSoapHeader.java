package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by cjq on 2019/1/16.
 */
public class HeaderUserSoapHeader {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";

    //用户名
    String UserName;

    //密码
    String Password;

    //客户号
    Integer Customer_ID;


    @XmlElement(name="Username")
    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    @XmlElement(name="Password")
    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    @XmlElement(name="Customer_ID")
    public Integer getCustomer_ID() {
        return Customer_ID;
    }

    public void setCustomer_ID(Integer customer_ID) {
        Customer_ID = customer_ID;
    }

}
