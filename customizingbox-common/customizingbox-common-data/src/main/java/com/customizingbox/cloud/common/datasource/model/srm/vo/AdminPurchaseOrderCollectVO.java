package com.customizingbox.cloud.common.datasource.model.srm.vo;


import com.customizingbox.cloud.common.datasource.model.admin.product.vo.AdminStoreProductAttrVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 待下单页面参数
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
@Data
@ApiModel(value = "待下单页面返回值")
public class AdminPurchaseOrderCollectVO {

    @ApiModelProperty(value = "采购订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private String purchaseOrderId;

    @ApiModelProperty(value = "订单item表id")
    private String orderItemId;

    @ApiModelProperty(value = "产品图片")
    private String productImgSrc;

    @ApiModelProperty(value = "商品英文标题")
    private String productTitle;

    @ApiModelProperty(value = "spu")
    private String spu;

    @ApiModelProperty(value = "sku. 系统生成")
    private String sku;

    @ApiModelProperty(value = "唯一码")
    private String onlyCode;

    @ApiModelProperty(value = "规格")
    private List<AdminStoreProductAttrVO> productAttr;

    @ApiModelProperty(value = "订单表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    @ApiModelProperty(value = "客户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appCustomerUserId;

    @ApiModelProperty(value = "添加时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "客户自定义商品信息")
    private String properties;

    @ApiModelProperty(value = "实际产品数据 (总产品 - 退款)")
    private Integer fulfillableQuantity;

    @ApiModelProperty(value = "采购价(RMB)")
    private BigDecimal costPrice;

    @ApiModelProperty(value = "采购员(admin用户)")
    private String adminPurchaseName;

    @ApiModelProperty(value = "供应商")
    private String adminSupplierName;

    @ApiModelProperty(value = "采购状态(1: 待下单, 2: 已下单, 3: 供应商打回), 7: 质检通过(入库)")
    private Integer purchaseStatus;

    @ApiModelProperty(value = "生产批次")
    private String productionBatch;

    @ApiModelProperty(value = "发货批次")
    private String deliveryBatch;

    @ApiModelProperty(value = "物流单号")
    private String deliveryCode;

    @ApiModelProperty(value = "供应商时效(天)")
    private String timeInterval;


    @ApiModelProperty(value = "产品变体属性值")
    @JsonIgnore
    private String attrValues;

    @ApiModelProperty(value = "产品Id")
    @JsonIgnore
    private Long productId;
}
