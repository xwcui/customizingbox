package com.customizingbox.cloud.common.datasource.model.admin.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-31 09:34:35
 */
@Data
@TableName("admin_store_product")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "产品表")
public class AdminStoreProduct extends Model<AdminStoreProduct> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * spu
     */
    @ApiModelProperty(value = "spu")
    private String spu;
    /**
     * 开发人员
     */
    @ApiModelProperty(value = "开发人员")
    private String author;
    /**
     * 产品物流属性
     */
    @ApiModelProperty(value = "产品物流属性")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logisticsAttrId;
    /**
     * 供应商id
     */
    @ApiModelProperty(value = "供应商id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierId;
    /**
     * 商品英文标题
     */
    @ApiModelProperty(value = "商品英文标题")
    private String titleEn;
    /**
     * 产品报关英文名
     */
    @ApiModelProperty(value = "产品报关英文名")
    private String nameEn;
    /**
     * 产品报关中文名
     */
    @ApiModelProperty(value = "产品报关中文名")
    private String nameCn;
    /**
     * 重量
     */
    @ApiModelProperty(value = "重量")
    private BigDecimal weight;
    /**
     * 长
     */
    @ApiModelProperty(value = "长")
    private BigDecimal length;
    /**
     * 宽
     */
    @ApiModelProperty(value = "宽")
    private BigDecimal width;
    /**
     * 高
     */
    @ApiModelProperty(value = "高")
    private BigDecimal height;
    /**
     * 定制产品; 1: 定制; 2: 非定制
     */
    @ApiModelProperty(value = "定制产品; 1: 定制; 2: 非定制")
    private Integer customMode;

    @ApiModelProperty(value = "上传赛和状态; 1: 已上传, 2: 未上传, 3: 部分上传")
    private Integer uploadShStatus;

    @ApiModelProperty(value = "上传赛和时间")
    private LocalDateTime uploadShTime;
    /**
     * 创建者id
     */
    @ApiModelProperty(value = "创建者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改者id
     */
    @ApiModelProperty(value = "修改者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标记（0：显示；1：隐藏）")
    @TableLogic
    private Integer delFlag;

    @ApiModelProperty(value = "是否报过价 ture 报过价  false 未报价")
    private Boolean quoteFlag;

}
