package com.customizingbox.cloud.common.datasource.mapper.admin.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOauthClient;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface AdminSysOauthClientMapper extends BaseMapper<AdminSysOauthClient> {

}
