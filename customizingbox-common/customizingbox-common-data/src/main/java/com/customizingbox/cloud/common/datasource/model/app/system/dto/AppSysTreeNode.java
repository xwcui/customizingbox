package com.customizingbox.cloud.common.datasource.model.app.system.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class AppSysTreeNode {

	protected String id;

	protected String parentId;

	private Integer sort;

	protected List<AppSysTreeNode> children = new ArrayList<>();

	public void addChildren(AppSysTreeNode appSysTreeNode) {
		children.add(appSysTreeNode);
	}

	public List<AppSysTreeNode> getChildren() {
		if (children.size() <= 0) {
			return null;
		}
		return children;
	}
}
