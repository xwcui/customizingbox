package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author DC_Li
 * @date 2022/4/20 16:12
 */
@Data
@ApiModel("店铺订单商品和报价信息")
public class CustomerAppOrderVariants {
    @ApiModelProperty("订单变体id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pandaOrderItemId;

    @ApiModelProperty("店铺变体图")
    private String storePicUrl;

    @ApiModelProperty("admin变体图")
    private String adminPicUrl;

    @ApiModelProperty("店铺变体属性值")
    private String storeVariationValue;

    @ApiModelProperty("admin变体属性值")
    private String adminVariationValue;

    @ApiModelProperty("数量")
    private Integer variantNum;

    @ApiModelProperty("报价价格，单位美元")
    private BigDecimal variantTotalPrice;
}
