package com.customizingbox.cloud.common.datasource.model.srm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author DC_Li
 * @date 2022/5/9 10:38
 */
@Data
@ApiModel("打印唯一码所需数据")
public class PrintOnlyCodeVo {
    @ApiModelProperty("唯一码")
    private String onlyCode;

    @ApiModelProperty("产品标题")
    private String productTitle;

    @ApiModelProperty("产品规格值")
    private String attrValues;

    @ApiModelProperty("产品自定义信息")
    private String properties;

}
