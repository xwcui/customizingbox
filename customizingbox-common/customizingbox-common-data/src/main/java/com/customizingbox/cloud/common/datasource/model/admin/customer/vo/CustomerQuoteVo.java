package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author DC_Li
 * @date 2022/5/11 10:29
 */
@ApiModel("报价信息的响应数据")
@Data
public class CustomerQuoteVo {
    @ApiModelProperty("店铺变体集合")
    private List<CustomerProductVariantsResponseVo> variants;

    @ApiModelProperty("已报价产品库产品集合")
    private List<AdminQuoteProductVo> adminQuoteProductVos;
}
