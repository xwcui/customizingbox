package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by xiwu
 * 请求运输方式参数实体类
 *  <GetTransportList xmlns="http://tempuri.org/">
 *       <trRequest>
 *       </trRequest>
 *  </GetTransportList>
 */
public class ApiGetTransportRequest {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";

    TrRequest trRequest;

    public TrRequest getTrRequest() {
        return trRequest;
    }

    public void setTrRequest(TrRequest trRequest) {
        this.trRequest = trRequest;
    }




}
