package com.customizingbox.cloud.common.datasource.model.srm.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/29 16:07
 */
@Data
@ApiModel("发货台扫码后响应的产品数据")
public class ReadyDeliveryProductVo {
    /**
     * admin产品id
     */
    @JsonIgnore
    private Long productId;

    @ApiModelProperty("产品名称")
    private String productName;

    @ApiModelProperty("SPU")
    private String spu;

    @ApiModelProperty("SKU")
    private String sku;

    @ApiModelProperty("唯一码")
    private String onlyCode;

    @ApiModelProperty("属性名")
    private String attrNames;

    @ApiModelProperty("属性值")
    private String attrValues;

    @ApiModelProperty("定制内容")
    private String properties;

    @ApiModelProperty("下单数")
    private Integer quantity;
}
