package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;

/**
 * 产品审核信息
 */
public class ApiImportProductAdmin {

    /**
     * 负责人员
     */
    String assignDevelopAdminName;

    /**
     * 编辑人员
     */
    String editAdminName;

    /**
     * 图片处理人员
     */
    String imageAdminName;

    /**
     * 开发人员
     */
    String developAdminName;

    /**
     * 质检备注
     */
    String toProcurementCheckMemo;

    /**
     * 发货打包备注
     */
    String toDeliveryPackNoteMemo;

    @XmlElement(name="AssignDevelopAdminName")
    public String getAssignDevelopAdminName() {
        return assignDevelopAdminName;
    }

    public void setAssignDevelopAdminName(String assignDevelopAdminName) {
        this.assignDevelopAdminName = assignDevelopAdminName;
    }

    @XmlElement(name="EditAdminName")
    public String getEditAdminName() {
        return editAdminName;
    }

    public void setEditAdminName(String editAdminName) {
        this.editAdminName = editAdminName;
    }

    @XmlElement(name="ImageAdminName")
    public String getImageAdminName() {
        return imageAdminName;
    }

    public void setImageAdminName(String imageAdminName) {
        this.imageAdminName = imageAdminName;
    }

    @XmlElement(name="DevelopAdminName")
    public String getDevelopAdminName() {
        return developAdminName;
    }

    public void setDevelopAdminName(String developAdminName) {
        this.developAdminName = developAdminName;
    }

    @XmlElement(name="ToProcurementCheckMemo")
    public String getToProcurementCheckMemo() {
        return toProcurementCheckMemo;
    }

    public void setToProcurementCheckMemo(String toProcurementCheckMemo) {
        this.toProcurementCheckMemo = toProcurementCheckMemo;
    }

    @XmlElement(name="ToDeliveryPackNoteMemo")
    public String getToDeliveryPackNoteMemo() {
        return toDeliveryPackNoteMemo;
    }

    public void setToDeliveryPackNoteMemo(String toDeliveryPackNoteMemo) {
        this.toDeliveryPackNoteMemo = toDeliveryPackNoteMemo;
    }
}
