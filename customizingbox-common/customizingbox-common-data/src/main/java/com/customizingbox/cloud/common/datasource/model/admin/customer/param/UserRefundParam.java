package com.customizingbox.cloud.common.datasource.model.admin.customer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/23 14:01
 */
@Data
@ApiModel
public class UserRefundParam {
    @ApiModelProperty("退款单号")
    private Long refundId;

    @ApiModelProperty("订单id")
    private Long pandaOrderId;

    @ApiModelProperty("客户id")
    private Long appUserId;

    @ApiModelProperty("客户邮箱")
    private String appUserEmail;

    @ApiModelProperty("申请人")
    private String supplyUser;

    @ApiModelProperty("退款申请状态 1 申请中 2 已退款(同意退款) 3 拒绝退款 null:全部")
    private Integer refundSupplyState;
}
