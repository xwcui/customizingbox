package com.customizingbox.cloud.common.datasource.model.admin.order.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(value = "订单上传赛盒分页对象")
public class OrderToSaihePageVO {

    @ApiModelProperty(value = "订单表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    @ApiModelProperty(value = "客户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appCustomerUserId;

    @ApiModelProperty(value = "订单客户邮箱")
    private String email;

    @ApiModelProperty(value = "添加时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "订单上传产品赛盒状态 1: 已上传, 2 未上传, 3:部分上传")
    private Integer productUploadShStatus;

    @ApiModelProperty(value = "订单上传赛盒状态 1: 已上传, 2 未上传 ,3 上传失败")
    private Integer uploadShStatus;

    @ApiModelProperty(value = "赛盒订单号")
    private String shOrderCode;

    @ApiModelProperty(value = "上传赛盒时间")
    private LocalDateTime uploadShTime;

    @ApiModelProperty(value = "上传赛盒返回信息")
    private String msg;

    @ApiModelProperty(value = "店铺名")
    private String storeName;

    @ApiModelProperty(value = "赛盒运输方式")
    private String shTransportName;
}
