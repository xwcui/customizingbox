package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class UpdateProductStockNumberRequest {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";


    /**
     * 请求库存产品操作实体列表
     */
    ProductStockRequestList productStockRequestList;

    @XmlElement(name="productStockRequestList")
    public ProductStockRequestList getProductStockRequestList() {
        return productStockRequestList;
    }

    public void setProductStockRequestList(ProductStockRequestList productStockRequestList) {
        this.productStockRequestList = productStockRequestList;
    }
}
