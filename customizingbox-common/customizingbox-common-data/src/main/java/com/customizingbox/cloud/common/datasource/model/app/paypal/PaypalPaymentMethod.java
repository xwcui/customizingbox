package com.customizingbox.cloud.common.datasource.model.app.paypal;

/**
 * @program: 
 * @description:
 * @author: 
 * @create: 
 **/
public enum PaypalPaymentMethod {

    credit_card, paypal

}

