package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AppPandaProcessingOrderItemVO extends Model<AppPandaProcessingOrderItemVO> {

    @ApiModelProperty(value = "panda订单item id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pandaOrderItemId;

    @ApiModelProperty(value = "变体图片")
    private String appImgSrc;

    @ApiModelProperty(value = "变体属性值")
    private String appAttrValues;

    @ApiModelProperty(value = "变体数量")
    private Integer fulfillableQuantity;

    @ApiModelProperty(value = "给客户的价格")
    private BigDecimal quotePrice;

    @ApiModelProperty(value = "变体图片")
    private String adminImgSrc;

    @ApiModelProperty(value = "变体属性值")
    private String adminAttrValues;

}
