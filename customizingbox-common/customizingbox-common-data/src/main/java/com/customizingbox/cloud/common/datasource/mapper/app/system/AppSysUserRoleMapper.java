package com.customizingbox.cloud.common.datasource.mapper.app.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUserRole;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 */
public interface AppSysUserRoleMapper extends BaseMapper<AppSysUserRole> {
    /**
     * 根据用户Id删除该用户的角色关系
     */
    Boolean deleteByUserId(@Param("userId") String userId);
}
