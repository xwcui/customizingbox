package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;

import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.SkuAddList;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.SkuUpdateList;

import javax.xml.bind.annotation.XmlElement;


public class ProcessUpdateProductResult {

    String Status;

    Result result=new Result();

    SkuAddList skuAddList;

    SkuUpdateList skuUpdateList;


    public ProcessUpdateProductResult() {
    }

    public ProcessUpdateProductResult(String status) {
        Status = status;
    }

    @XmlElement(name="Status")
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @XmlElement(name="Result")
    public Result getResult() {
        return result;
    }


    public void setResult(Result result) {
        this.result = result;
    }

    @XmlElement(name="SkuAddList")
    public SkuAddList getSkuAddList() {
        return skuAddList;
    }

    public void setSkuAddList(SkuAddList skuAddList) {
        this.skuAddList = skuAddList;
    }

    @XmlElement(name="SkuUpdateList")
    public SkuUpdateList getSkuUpdateList() {
        return skuUpdateList;
    }

    public void setSkuUpdateList(SkuUpdateList skuUpdateList) {
        this.skuUpdateList = skuUpdateList;
    }
}
