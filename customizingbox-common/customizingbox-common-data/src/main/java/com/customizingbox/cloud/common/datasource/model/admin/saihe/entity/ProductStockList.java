package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class ProductStockList {

    List<ProductStock>  ProductStockList;

    public ProductStockList() {
    }

    @XmlElement(name="ApiProductStock")
    public List<ProductStock> getProductStockList() {
        return ProductStockList;
    }

    public void setProductStockList(List<ProductStock> productStockList) {
        ProductStockList = productStockList;
    }
}
