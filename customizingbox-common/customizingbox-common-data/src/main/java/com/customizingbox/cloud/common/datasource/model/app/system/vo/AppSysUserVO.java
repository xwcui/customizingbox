package com.customizingbox.cloud.common.datasource.model.app.system.vo;

import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRole;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


@Data
public class AppSysUserVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 随机盐
     */
    private String salt;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 0-正常，1-删除
     */
    private String delFlag;

    /**
     * 锁定标记
     */
    private String lockFlag;

    /**
     * 简介
     */
    private String phone;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 机构ID
     */
    private String organId;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 机构名称
     */
    private String organName;

    /**
     * 角色列表
     */
    private List<AppSysRole> roleList;

    /**
     * 角色
     */
    private List<String> roleIds;
}
