package com.customizingbox.cloud.common.datasource.model.srm.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/26 13:25
 */
@Data
@ApiModel("生产中查询条件")
public class ProducingProductQuery {
    @ApiModelProperty("spu")
    private String spu;

    @ApiModelProperty("唯一码")
    private String onlyCode;

    @ApiModelProperty("生产批次")
    private String productionBatch;
}
