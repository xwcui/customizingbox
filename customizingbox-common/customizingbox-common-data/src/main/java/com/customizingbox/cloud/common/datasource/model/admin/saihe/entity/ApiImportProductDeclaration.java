package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

/**
 * 产品报关信息
 */
public class ApiImportProductDeclaration {

    /**
     * 产品报关英文名
     */
    String declarationName;

    /**
     * 产品报关中文名
     */
    String declarationNameCN;

    /**
     * 报关价
     */
    BigDecimal declarationPriceRate;

    /**
     * 报关单位
     */
    String declarationUnit;

    @XmlElement(name="DeclarationName")
    public String getDeclarationName() {
        return declarationName;
    }

    public void setDeclarationName(String declarationName) {
        this.declarationName = declarationName;
    }

    @XmlElement(name="DeclarationNameCN")
    public String getDeclarationNameCN() {
        return declarationNameCN;
    }

    public void setDeclarationNameCN(String declarationNameCN) {
        this.declarationNameCN = declarationNameCN;
    }

    @XmlElement(name="DeclarationPriceRate")
    public BigDecimal getDeclarationPriceRate() {
        return declarationPriceRate;
    }

    public void setDeclarationPriceRate(BigDecimal declarationPriceRate) {
        this.declarationPriceRate = declarationPriceRate;
    }

    @XmlElement(name="DeclarationUnit")
    public String getDeclarationUnit() {
        return declarationUnit;
    }

    public void setDeclarationUnit(String declarationUnit) {
        this.declarationUnit = declarationUnit;
    }
}
