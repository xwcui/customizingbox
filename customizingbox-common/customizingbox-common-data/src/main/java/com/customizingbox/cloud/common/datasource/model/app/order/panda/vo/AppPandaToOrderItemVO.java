package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单item列表
 */
@Data
@ApiModel(description = "订单item表")
public class AppPandaToOrderItemVO extends Model<AppPandaToOrderItemVO> {

    @ApiModelProperty(value = "panda订单itemId")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pandaOrderItemId;

    @ApiModelProperty(value = "变体图片")
    private String appImgSrc;

    @ApiModelProperty(value = "变体属性值")
    private String appAttrValues;

    @ApiModelProperty(value = "app产品变体id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long variantId;

    @ApiModelProperty(value = "变体数量")
    private Integer fulfillableQuantity;

    @ApiModelProperty(value = "关联(有值就说明已经关联了)")
    private String adminVariantId;

    @ApiModelProperty(value = "变体图片")
    private String adminImgSrc;

    @ApiModelProperty(value = "变体属性值")
    private String adminAttrValues;

//    @ApiModelProperty(value = "采购价")
//    private BigDecimal costPrice;

//    @ApiModelProperty(value = "供货价")
//    private BigDecimal supplyPrice;
//
//    @ApiModelProperty(value = "报价倍率")
//    private BigDecimal supplyRate;

//    @ApiModelProperty(value = "给客户的价格")
//    private BigDecimal cost;

    @ApiModelProperty(value = "给客户的价格")
    private BigDecimal quotePrice;
}
