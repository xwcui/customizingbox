package com.customizingbox.cloud.common.datasource.model.admin.customer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/22 9:56
 */
@Data
@ApiModel("客户店铺订单查询参数")
public class CustomerAppOrderQuery {
    @ApiModelProperty("店铺id")
    private Long storeId;

    @ApiModelProperty("店铺名称")
    private String storeName;

    @ApiModelProperty("客户id")
    private Long customerId;

    @ApiModelProperty("客户邮箱")
    private String customerEmail;

    @ApiModelProperty("订单状态 全部：null 正常：1 取消：2")
    private Integer orderState;

    @ApiModelProperty("支付状态 全部：null 未支付：1 已支付：2")
    private Integer payState;

    @ApiModelProperty("发货状态 全部：null， 未发货：1， 部分发货：2，全部发货：3")
    private Integer shipState;

    @ApiModelProperty("退款状态 全部：null，未退款：0，部分退款：3，全部退款：4")
    private Integer refundState;

    @ApiModelProperty("app订单id")
    private Long pandaOrderId;
}
