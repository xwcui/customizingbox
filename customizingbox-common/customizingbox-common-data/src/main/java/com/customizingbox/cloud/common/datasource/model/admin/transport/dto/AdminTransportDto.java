package com.customizingbox.cloud.common.datasource.model.admin.transport.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@Data
public class AdminTransportDto {

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输方式名称")
    @NotBlank(message = "运输方式名称 不能为空")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "体积因子")
    @NotNull(message = "体积因子 不能为空")
    private BigDecimal volumeFactor;

    @ApiModelProperty(value = "重量计算方式 0 体积重 1 实重")
    @NotNull(message = "重量计算方式 不能为空")
    private Integer completeMode;

    @ApiModelProperty(value = "追踪类型 0 真实追踪号 1   物流商单号")
    @NotNull(message = "追踪类型 不能为空")
    private Integer traceType;

    @ApiModelProperty(value = "关联赛和")
    @NotNull(message = "关联赛和 不能为空")
    private Integer shTransportId;

    @ApiModelProperty(value = "状态. 1: 启用; 0: 禁用")
    private Integer status;

    /**
     * 运输模板/属性 id集合
     */
    @ApiModelProperty(value = "运输模板/属性")
    @Size(min = 1 ,message = "logisticsIds不能为空")
    private List<Long> logisticsIds;
}
