package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrgan;

/**
 * <p>
 * 机构管理 Mapper 接口
 * </p>
 */
public interface AppSysOrganMapper extends BaseMapper<AppSysOrgan> {

}
