package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysDictValue;

/**
 * 字典项
 */
public interface AdminSysDictValueMapper extends BaseMapper<AdminSysDictValue> {

}
