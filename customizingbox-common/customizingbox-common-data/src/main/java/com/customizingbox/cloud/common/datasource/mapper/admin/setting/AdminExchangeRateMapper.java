package com.customizingbox.cloud.common.datasource.mapper.admin.setting;

import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminExchangeRate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminExchangeRateResponseVo;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 汇率表 Mapper 接口
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
public interface AdminExchangeRateMapper extends BaseMapper<AdminExchangeRate> {
    /**
     * 修改汇率
     *
     * @param currencyCode 货币代码
     * @param rate 汇率
     * @param userId 当前登录用户id
     * @param now 当前时间
     * @return
     */
    Boolean editExchangeRate(@Param("currencyCode") String currencyCode, @Param("rate") String rate,
                             @Param("userId") Long userId, @Param("now") LocalDateTime now);

    /**
     * 获取所有汇率
     * @return
     */
    List<AdminExchangeRateResponseVo> listAllRate();

    /**
     * 获取单个货币的汇率
     * @param currencyCode 货币代码
     * @return
     */
    AdminExchangeRateResponseVo getOneRateByCode(@Param("currencyCode") String currencyCode);
}
