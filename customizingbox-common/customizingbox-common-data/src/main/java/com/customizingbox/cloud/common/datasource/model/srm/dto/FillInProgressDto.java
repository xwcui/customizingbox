package com.customizingbox.cloud.common.datasource.model.srm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author DC_Li
 * @date 2022/5/5 17:45
 */
@Data
@ApiModel("填写重新发货请求数据")
public class FillInProgressDto {
    @ApiModelProperty("采购单id")
    private Long purchaseOrderId;

    @ApiModelProperty("供应商填写进度物流商")
    @NotBlank(message = "供应商不能为空")
    private String progressDeliveryMethod;

    @ApiModelProperty("供应商填写进度物流单号")
    @NotBlank(message = "物流单号不能为空")
    private String progressDeliveryCode;
}
