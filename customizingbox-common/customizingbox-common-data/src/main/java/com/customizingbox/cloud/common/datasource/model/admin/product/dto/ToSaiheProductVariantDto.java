package com.customizingbox.cloud.common.datasource.model.admin.product.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 产品上传赛盒所需要的参数
 */
@Data
public class ToSaiheProductVariantDto {

    /**
     * 产品id
     */
    private Long  id;

    /**
     * 变体id
     */
    private Long  variantId;

    /**
     * 变体sku  非定制产品使用此sku作为上传赛盒的 sku
     */
    private String sku;

    /**
     * 唯一码  定制产品要使用此code作为上传赛盒的sku
     */
    private String onlyCode;

    /**
     * 商品英文标题
     */
    private String titleEn;

    /**
     * 产品报关英文名
     */
    private String nameEn;

    /**
     * 产品报关中文名
     */
    private String nameCn;

    /**
     * 重量
     */
    private BigDecimal weight;

    /**
     * 长
     */
    private BigDecimal length;

    /**
     * 宽
     */
    private BigDecimal width;

    /**
     * 高
     */
    private BigDecimal height;

    /**
     * 定制产品; 1: 定制; 2: 非定制
     */
    private Integer customMode;

    /**
     * 产品spu
     */
    private String spu;

    /**
     * 产品创建人
     */
    private String username;

    /**
     * 赛盒code
     */
    private String shCode;

    /**
     * 采购价(就是我们的成本价) rmb
     */
    private BigDecimal costPrice;

    /**
     * quote_price 报价 也就是客户购买时的价格  $   报关时使用的价格
     */
    private BigDecimal quotePrice;

    /**
     * 供货价   rmb  报关价是此价格转为美元
     */
    private BigDecimal supplyPrice;

    /**
     * 变体图片
     */
    private String src;

    /**
     * 变体规格值
     */
    private String attrValues;

    /**
     * logistics_attr_id
     */
    private Long logisticsAttrId;

    /**
     * 供应商名称
     */
    private String supplierName;
}
