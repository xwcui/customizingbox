package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author DC_Li
 * @date 2022/4/20 15:42
 */
@Data
@ApiModel
public class CustomerAppPandaOrderResponseVo {
    // 潘达订单id
    @JsonIgnore
    private Long id;

    @ApiModelProperty("客户信息")
    private CustomerInfoInOrderResopnseVo customerInfo;

    @ApiModelProperty("店铺订单信息")
    private CustomerAppOrderInfo orderInfo;

    @ApiModelProperty("收件人信息")
    private CustomerAppOrderAddress address;

    @ApiModelProperty("店铺订单商品和报价信息")
    private List<CustomerAppOrderVariants> variants;

    @ApiModelProperty("app运输方式")
    private CustomerAppOrderTransport transport;

    @ApiModelProperty("app支付金额")
    private CustomerAppOrderPayAmount payAmount;

    @ApiModelProperty("app状态")
    private CustomerAppOrderState state;

    @ApiModelProperty("app时间")
    private CustomerAppOrderTime time;
}
