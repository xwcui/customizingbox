package com.customizingbox.cloud.common.datasource.model.app.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-26 18:20:37
 */
@Data
@TableName("app_store_product_img_depot")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "产品图库")
public class AppStoreProductImgDepot extends Model<AppStoreProductImgDepot> {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    private Long productId;
    /**
     * shopify 产品id
     */
    @ApiModelProperty(value = "shopify 产品id")
    private Long sourceProductId;
    @ApiModelProperty(value = "原商户数据id")
    private Long sourceId;
    @ApiModelProperty(value = "商户类型")
    private Integer platformType;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer position;
    /**
     * alt
     */
    @ApiModelProperty(value = "alt")
    private String alt;
    /**
     * 长
     */
    @ApiModelProperty(value = "长")
    private String width;
    /**
     * 高
     */
    @ApiModelProperty(value = "高")
    private String height;
    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String src;
    /**
     *
     */
    @ApiModelProperty(value = "")
    private String variantIds;
    /**
     *
     */
    @ApiModelProperty(value = "")
    private String adminGraphqlApiId;
    /**
     * shopify系统创建时间
     */
    @ApiModelProperty(value = "商户系统创建时间")
    private LocalDateTime createdAt;
    /**
     * shopify系统修改时间
     */
    @ApiModelProperty(value = "商户系统修改时间")
    private LocalDateTime updatedAt;
    /**
     * shopify系统创建时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统创建时间(北京时间)")
    private LocalDateTime bjCreatedAt;
    /**
     * shopify系统修改时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统修改时间(北京时间)")
    private LocalDateTime bjUpdatedAt;
    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标记")
    @TableLogic
    private String delFlag;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
