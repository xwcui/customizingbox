package com.customizingbox.cloud.common.datasource.model.admin.system.vo;

import lombok.Data;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.time.LocalDateTime;


@Data
public class AdminSysImageCode implements Serializable {

	private String code;

	private LocalDateTime expireTime;

	private BufferedImage image;

	public AdminSysImageCode(BufferedImage image, String sRand, int defaultImageExpire) {
		this.image = image;
		this.code = sRand;
		this.expireTime = LocalDateTime.now().plusSeconds(defaultImageExpire);
	}
}
