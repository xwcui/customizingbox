package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author DC_Li
 * @date 2022/4/20 16:12
 */
@Data
@ApiModel("app运输方式")
public class CustomerAppOrderTransport {
    @ApiModelProperty("店铺运输方式名称")
    private String storeShipMethod;
    @ApiModelProperty("店铺订单运费")
    private BigDecimal storeShipCost;
    @ApiModelProperty("app运输方式")
    private String pandaShipMethod;
    @ApiModelProperty("app发货追踪号")
    private String pandaTrackingCode;
}
