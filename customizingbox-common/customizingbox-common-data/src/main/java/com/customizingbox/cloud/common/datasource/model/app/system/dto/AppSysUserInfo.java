package com.customizingbox.cloud.common.datasource.model.app.system.dto;

import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import lombok.Data;

import java.io.Serializable;

/**
 *<p>
 * commit('SET_ROLES', data)
 * commit('SET_NAME', data)
 * commit('SET_AVATAR', data)
 * commit('SET_INTRODUCTION', data)
 * commit('SET_PERMISSIONS', data)
 */
@Data
public class AppSysUserInfo implements Serializable {

	/**
	 * 用户基本信息
	 */
	private AppSysUser sysUser;

	/**
	 * 权限标识集合
	 */
	private String[] permissions;

	/**
	 * 角色集合
	 */
	private String[] roles;
}
