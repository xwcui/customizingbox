package com.customizingbox.cloud.common.datasource.model.admin.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 短信
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "短信")
@TableName(value = "admin_sys_sms")
public class AdminSysSms extends Model<AdminSysSms> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "签名")
    private String signName;

    @ApiModelProperty(value = "号码")
    private String phoneNumbers;

    @ApiModelProperty(value = "模板编码")
    private String templateCode;

    @ApiModelProperty(value = "模板参数")
    private String templateParam;
}
