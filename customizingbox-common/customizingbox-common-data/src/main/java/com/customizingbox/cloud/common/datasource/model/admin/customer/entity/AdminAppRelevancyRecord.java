package com.customizingbox.cloud.common.datasource.model.admin.customer.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 客户经理认领/关联 店铺用户记录表
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-14
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_app_relevancy_record")
@ApiModel(value ="客户经理认领/关联 店铺用户记录表")
public class AdminAppRelevancyRecord extends Model<AdminAppRelevancyRecord> {

    private static final long serialVersionUID = 1L;

@ApiModelProperty(value = "主键id")
@TableId(type = IdType.ASSIGN_ID)
private Long id;

@ApiModelProperty(value = "app用户主键id")
private Long appUserId;

@ApiModelProperty(value = "客户经理主键id")
private Long adminUserId;

@ApiModelProperty(value = "转让说明")
private String mark;

@ApiModelProperty(value = "创建者id")
private Long createId;

@ApiModelProperty(value = "创建时间")
private LocalDateTime createTime;

@ApiModelProperty(value = "修改者id")
private Long updateId;

@ApiModelProperty(value = "修改时间")
private LocalDateTime updateTime;




        @Override
    protected Serializable pkVal() {
                return this.id;
            }


}
