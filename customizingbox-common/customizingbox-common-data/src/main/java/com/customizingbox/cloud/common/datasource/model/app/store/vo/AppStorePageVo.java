package com.customizingbox.cloud.common.datasource.model.app.store.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "商户表")
public class AppStorePageVo {

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 商户名称
     */
    @ApiModelProperty(value = "商户名称")
    private String name;

    /**
     * 商户类型(1: shopify)
     */
    @ApiModelProperty(value = "商户类型(1: shopify)")
    private Integer platformType;

    /**
     * 商户状态
     */
    @ApiModelProperty(value = "商户状态. 1: 正常(默认), 2: 禁用(不在接收webhook)")
    private Integer storeState;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

}
