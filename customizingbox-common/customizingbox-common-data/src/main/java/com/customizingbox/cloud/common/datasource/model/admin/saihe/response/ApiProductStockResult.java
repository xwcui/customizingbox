package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;

import javax.xml.bind.annotation.XmlElement;

public class ApiProductStockResult {

    /**
     * 系统SKU
     */
    private String sku;

    /**
     * 自定义SKU
     */
    private String  clientSKU;

    /**
     * 仓库id
     */
    private Integer wareHouseID;

    /**
     * 操作状态
     */
    private Boolean operateState;

    /**
     * 操作状态
     */
    private String operateMsg;

    public ApiProductStockResult() {
    }

    @XmlElement(name="SKU")
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @XmlElement(name="ClientSKU")
    public String getClientSKU() {
        return clientSKU;
    }

    public void setClientSKU(String clientSKU) {
        this.clientSKU = clientSKU;
    }

    @XmlElement(name="WareHouseID")
    public Integer getWareHouseID() {
        return wareHouseID;
    }

    public void setWareHouseID(Integer wareHouseID) {
        this.wareHouseID = wareHouseID;
    }

    @XmlElement(name="OperateState")
    public Boolean getOperateState() {
        return operateState;
    }

    public void setOperateState(Boolean operateState) {
        this.operateState = operateState;
    }

    @XmlElement(name="OperateMsg")
    public String getOperateMsg() {
        return operateMsg;
    }

    public void setOperateMsg(String operateMsg) {
        this.operateMsg = operateMsg;
    }
}
