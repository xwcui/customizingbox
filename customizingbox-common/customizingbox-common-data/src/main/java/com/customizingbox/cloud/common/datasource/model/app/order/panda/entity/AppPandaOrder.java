package com.customizingbox.cloud.common.datasource.model.app.order.panda.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotNull;

/**
 * panda订单
 *
 * @author Y
 * @date 2022-04-12 16:41:50
 */
@Data
@TableName("app_panda_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "panda订单")
public class AppPandaOrder extends Model<AppPandaOrder> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 商户订单id
     */
    @ApiModelProperty(value = "商户订单id")
    private Long sourceId;
    /**
     * 订单号, 用来给用户展示
     */
    @ApiModelProperty(value = "订单号, 用来给用户展示")
    private String orderNo;
    /**
     * 店铺id
     */
    @ApiModelProperty(value = "店铺id")
    private Long storeId;
    /**
     * 订单名称
     */
    @ApiModelProperty(value = "订单名称")
    private String orderName;
    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "店铺名称")
    private String storeName;
    /**
     * 平台类型. 1: shopify  
     */
    @ApiModelProperty(value = "平台类型. 1: shopify  ")
    private Integer platformType;
    /**
     * 客户id
     */
    @ApiModelProperty(value = "客户id")
    private Long userId;
    /**
     * 运费
     */
    @ApiModelProperty(value = "运费")
    private BigDecimal freightAmount;
    /**
     * 商品总价
     */
    @ApiModelProperty(value = "商品总价")
    private BigDecimal productAmount;
    /**
     * 抵扣金额
     */
    @ApiModelProperty(value = "抵扣金额")
    private BigDecimal deductionAmount;
    /**
     * 实际支付金额
     */
    @ApiModelProperty(value = "实际支付金额")
    private BigDecimal payAmount;

    @ApiModelProperty(value = "退款总金额")
    private BigDecimal refundTotalAmount;

    @ApiModelProperty(value = "vat退款金额")
    private BigDecimal refundVatAmount;

    @ApiModelProperty(value = "运费退款金额")
    private BigDecimal refundFreightAmount;
    /**
     * 支付方式; 1: 余额支付
     */
    @ApiModelProperty(value = "支付方式; 1: 余额支付")
    private Integer payType;

    @ApiModelProperty(value = "交易记录id, 交易号")
    private Long transactionRecordId;
    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;
    /**
     * 支付id
     */
    @ApiModelProperty(value = "支付id")
    private String paymentId;

    @ApiModelProperty(value = "订单状态 (1: 正常订单, 2: 取消)")
    private Integer status;

    /**
     * 支付状态(1: 未支付 2: 已支付,)
     */
    @ApiModelProperty(value = "支付状态(1: 未支付 2: 已支付,)")
    private Integer payStatus;
    /**
     * 订单状态（1：未发货；2：部分发货；3：全部发货;；）
     */
    @ApiModelProperty(value = "订单发货状态（1：未发货；2：部分发货；3：全部发货; 4: 已收货）")
    private Integer freightStatus;
    /**
     * 退款状态;  0: 未退款 1: 部分退款; 2: 全部退款
     */
    @ApiModelProperty(value = "退款状态;  1: 未退款 2: 部分退款; 3: 全部退款")
    private Integer refundStatus;
    /**
     * 原订单支付状态: 1: 未支付, 3: 已支付, 4: 已取消
     */
    @ApiModelProperty(value = "原订单支付状态")
    private String sourcePaidStatus;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String mark;
    /**
     * 原订单备注
     */
    @ApiModelProperty(value = "原订单备注")
    private String sourceMark;
    /**
     * 收货地址
     */
    @ApiModelProperty(value = "收货地址")
    private Long addressId;

    @ApiModelProperty(value = "运输方式id")
    private Long transportId;

    /**
     * 税率
     */
    @ApiModelProperty(value = "税率")
    private BigDecimal vatAmount;
    /**
     * 快递单号
     */
    @ApiModelProperty(value = "快递单号")
    private String deliveryNo;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
    /**
     * 上传赛盒状态; 1: 已上传, 2: 未上传; 3: 部分上传
     */
    @ApiModelProperty(value = "上传赛盒状态; 1: 已上传, 2: 未上传; 3: 部分上传")
    private Integer uploadShStatus;
    /**
     * 上传赛盒时间
     */
    @ApiModelProperty(value = "上传赛盒时间")
    private LocalDateTime uploadShTime;
    /**
     * 店铺关联的产品经理id(就是admin用户id)
     */
    @ApiModelProperty(value = "店铺关联的产品经理id(就是admin用户id)")
    private Long adminUserId;

    @ApiModelProperty(value = "下单时该用户的产品经理id")
    private Long adminPlaceOrderUserId;
    /**
     * 数据版本号.  乐观锁
     */
    @ApiModelProperty(value = "数据版本号.  乐观锁")
    private Integer version;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "店铺运费")
    private BigDecimal sourceFreightAmount;

    @ApiModelProperty(value = "店铺运输公司; 运输方式")
    private String sourceTrackingCompany;

}
