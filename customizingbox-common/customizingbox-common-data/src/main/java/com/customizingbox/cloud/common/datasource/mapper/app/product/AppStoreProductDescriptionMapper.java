package com.customizingbox.cloud.common.datasource.mapper.app.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductDescription;

/**
 * shopify 产品详情表
 *
 * @author Y
 * @date 2022-03-26 10:32:17
 */
public interface AppStoreProductDescriptionMapper extends BaseMapper<AppStoreProductDescription> {

}
