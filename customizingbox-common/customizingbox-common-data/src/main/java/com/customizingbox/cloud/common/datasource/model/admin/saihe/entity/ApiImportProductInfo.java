package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

/**
 *
 */
public class ApiImportProductInfo {

    /**
     * 产品SKU
     */
    String sku;

    /**
     * 自定义SKU
     */
    String clientSKU;

    /**
     * 产品颜色英文
     */
    String productColor;

    /**
     * 产品尺码
     */
    String productSize;

    /**
     * 产品来源（0系统采集 1 开发采集 2 新样品）
     */
    Integer comeSource;

    /**
     * 产品类别英文名
     */
    String productClassNameEN;

    /**
     * 产品类别中文名
     */
    String productClassNameCN;

    /**
     * 产品中文名
     */
    String ProductName;

    /**
     * 产品中文名
     */
    String productNameCN;

    /**
     * 产品简要描述
     */
    String mateDescription;

    /**
     * 产品详细描述
     */
    String productDescription;

    /**
     * 产品尺寸长（CM）
     */
    BigDecimal length;

    /**
     * 产品尺寸宽（CM）
     */
    BigDecimal width;

    /**
     * Decimal	产品尺寸高（CM）
     */
    BigDecimal height;

    /**
     * 产品包装尺寸长（CM）
     */
    BigDecimal packLength;

    /**
     * 产品包装尺寸宽（CM）
     */
    BigDecimal packWidth;

    /**
     * 产品包装尺寸高（CM）
     */
    BigDecimal packHeight;

    /**
     * 产品包装清单
     */
    String packingList;

    /**
     * 售价（美元）
     */
    BigDecimal salePrice;

    /**
     * 供货价(人民币)
     */
    BigDecimal lastSupplierPrice;

    /**
     * 净重(克)
     */
    BigDecimal netWeight;

    /**
     * 毛重(克)
     */
    BigDecimal grossWeight;

    /**
     * 单个产品包裹重量(克)
     */
    BigDecimal packWeight;

    /**
     * 产品特征亮点
     */
    String featureList;

    /**
     * 采购人员
     */
    String buyerName;

    /**
     * 开发类型 (自主开发 = 1,供应商后台 =2,指定开发 = 3)
     */
    Integer developType;

    /**
     * 图片来源 (采集图 = 1,供应商提供图 = 2,自己拍照图 = 3)
     */
    Integer pictureSource;

    /**
     * 外箱长
     */
    BigDecimal cartonLength;

    /**
     * 外箱宽
     */
    BigDecimal cartonWidth;

    /**
     * 外箱高
     */
    BigDecimal cartonHeight;

    /**
     * 每箱PCS数量
     */
    BigDecimal cartonPcsNum;

    /**
     * 整箱产品毛重
     */
    BigDecimal cartonGrossWeight;

    /**
     * 整箱产品净重
     */
    BigDecimal cartonNetWeight;

    /**
     * 产品状态
     */
    String productState;

    /**
     * 母体ID
     */
    String productGroupSKU;

    /**
     * 单位
     */
    String unitName;

    /**
     * 产品物流属性
     */
    Integer withBattery;

    /**
     * 默认本地发货仓库
     */
    Integer defaultLocalWarehouse;

    /**
     * 采购收货备注
     */
    String receiptRemark;


    /**
     * 产品图片(最多9张)
     */
    ImagesList imagesList;

    /**
     * 产品供应商
     */
    ApiImportProductSupplier productSuppiler;

    /**
     * 产品供应商报价
     */
    ApiImportProductSupplierPrice productSupplierPrice;

    /**
     * 产品报关信息
     */
    ApiImportProductDeclaration productDeclaration;

    /**
     * 产品审核信息
     */
    ApiImportProductAdmin productAdmin;


    @XmlElement(name="SKU")
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @XmlElement(name="ClientSKU")
    public String getClientSKU() {
        return clientSKU;
    }

    public void setClientSKU(String clientSKU) {
        this.clientSKU = clientSKU;
    }

    @XmlElement(name="ProductColor")
    public String getProductColor() {
        return productColor;
    }

    public void setProductColor(String productColor) {
        this.productColor = productColor;
    }

    @XmlElement(name="ProductSize")
    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    @XmlElement(name="ComeSource")
    public Integer getComeSource() {
        return comeSource;
    }

    public void setComeSource(Integer comeSource) {
        this.comeSource = comeSource;
    }

    @XmlElement(name="ProductClassNameEN")
    public String getProductClassNameEN() {
        return productClassNameEN;
    }

    public void setProductClassNameEN(String productClassNameEN) {
        this.productClassNameEN = productClassNameEN;
    }

    @XmlElement(name="ProductClassNameCN")
    public String getProductClassNameCN() {
        return productClassNameCN;
    }

    public void setProductClassNameCN(String productClassNameCN) {
        this.productClassNameCN = productClassNameCN;
    }

    @XmlElement(name="ProductName")
    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    @XmlElement(name="ProductNameCN")
    public String getProductNameCN() {
        return productNameCN;
    }

    public void setProductNameCN(String productNameCN) {
        this.productNameCN = productNameCN;
    }

    @XmlElement(name="MateDescription")
    public String getMateDescription() {
        return mateDescription;
    }

    public void setMateDescription(String mateDescription) {
        this.mateDescription = mateDescription;
    }

    @XmlElement(name="ProductDescription")
    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    @XmlElement(name="Length")
    public BigDecimal getLength() {
        return length;
    }

    public void setLength(BigDecimal length) {
        this.length = length;
    }

    @XmlElement(name="Width")
    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    @XmlElement(name="Height")
    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    @XmlElement(name="Pack_Length")
    public BigDecimal getPackLength() {
        return packLength;
    }

    public void setPackLength(BigDecimal packLength) {
        this.packLength = packLength;
    }

    @XmlElement(name="Pack_Width")
    public BigDecimal getPackWidth() {
        return packWidth;
    }

    public void setPackWidth(BigDecimal packWidth) {
        this.packWidth = packWidth;
    }

    @XmlElement(name="Pack_Height")
    public BigDecimal getPackHeight() {
        return packHeight;
    }

    public void setPackHeight(BigDecimal packHeight) {
        this.packHeight = packHeight;
    }

    @XmlElement(name="PackingList")
    public String getPackingList() {
        return packingList;
    }

    public void setPackingList(String packingList) {
        this.packingList = packingList;
    }

    @XmlElement(name="SalePrice")
    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public BigDecimal getLastSupplierPrice() {
        return lastSupplierPrice;
    }

    @XmlElement(name="LastSupplierPrice")
    public void setLastSupplierPrice(BigDecimal lastSupplierPrice) {
        this.lastSupplierPrice = lastSupplierPrice;
    }

    @XmlElement(name="NetWeight")
    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    @XmlElement(name="GrossWeight")
    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    @XmlElement(name="PackWeight")
    public BigDecimal getPackWeight() {
        return packWeight;
    }

    public void setPackWeight(BigDecimal packWeight) {
        this.packWeight = packWeight;
    }

    @XmlElement(name="FeatureList")
    public String getFeatureList() {
        return featureList;
    }

    public void setFeatureList(String featureList) {
        this.featureList = featureList;
    }

    public String getBuyerName() {
        return buyerName;
    }

    @XmlElement(name="BuyerName")
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @XmlElement(name="DevelopType")
    public Integer getDevelopType() {
        return developType;
    }

    public void setDevelopType(Integer developType) {
        this.developType = developType;
    }

    @XmlElement(name="PictureSource")
    public Integer getPictureSource() {
        return pictureSource;
    }

    public void setPictureSource(Integer pictureSource) {
        this.pictureSource = pictureSource;
    }

    @XmlElement(name="Carton_Length")
    public BigDecimal getCartonLength() {
        return cartonLength;
    }

    public void setCartonLength(BigDecimal cartonLength) {
        this.cartonLength = cartonLength;
    }

    @XmlElement(name="Carton_Width")
    public BigDecimal getCartonWidth() {
        return cartonWidth;
    }

    public void setCartonWidth(BigDecimal cartonWidth) {
        this.cartonWidth = cartonWidth;
    }

    @XmlElement(name="Carton_Height")
    public BigDecimal getCartonHeight() {
        return cartonHeight;
    }

    public void setCartonHeight(BigDecimal cartonHeight) {
        this.cartonHeight = cartonHeight;
    }

    @XmlElement(name="Carton_PcsNum")
    public BigDecimal getCartonPcsNum() {
        return cartonPcsNum;
    }

    public void setCartonPcsNum(BigDecimal cartonPcsNum) {
        this.cartonPcsNum = cartonPcsNum;
    }

    @XmlElement(name="Carton_GrossWeight")
    public BigDecimal getCartonGrossWeight() {
        return cartonGrossWeight;
    }

    public void setCartonGrossWeight(BigDecimal cartonGrossWeight) {
        this.cartonGrossWeight = cartonGrossWeight;
    }

    @XmlElement(name="Carton_NetWeight")
    public BigDecimal getCartonNetWeight() {
        return cartonNetWeight;
    }

    public void setCartonNetWeight(BigDecimal cartonNetWeight) {
        this.cartonNetWeight = cartonNetWeight;
    }

    @XmlElement(name="ProductState")
    public String getProductState() {
        return productState;
    }

    public void setProductState(String productState) {
        this.productState = productState;
    }

    @XmlElement(name="ProductGroupSKU")
    public String getProductGroupSKU() {
        return productGroupSKU;
    }

    public void setProductGroupSKU(String productGroupSKU) {
        this.productGroupSKU = productGroupSKU;
    }

    @XmlElement(name="UnitName")
    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @XmlElement(name="WithBattery")
    public Integer getWithBattery() {
        return withBattery;
    }

    public void setWithBattery(Integer withBattery) {
        this.withBattery = withBattery;
    }

    @XmlElement(name="DefaultLocalWarehouse")
    public Integer getDefaultLocalWarehouse() {
        return defaultLocalWarehouse;
    }

    public void setDefaultLocalWarehouse(Integer defaultLocalWarehouse) {
        this.defaultLocalWarehouse = defaultLocalWarehouse;
    }

    @XmlElement(name="ReceiptRemark")
    public String getReceiptRemark() {
        return receiptRemark;
    }

    public void setReceiptRemark(String receiptRemark) {
        this.receiptRemark = receiptRemark;
    }

    @XmlElement(name="ImagesList")
    public ImagesList getImagesList() {
        return imagesList;
    }

    public void setImagesList(ImagesList imagesList) {
        this.imagesList = imagesList;
    }

    @XmlElement(name="ProductSuppiler")
    public ApiImportProductSupplier getProductSuppiler() {
        return productSuppiler;
    }

    public void setProductSuppiler(ApiImportProductSupplier productSuppiler) {
        this.productSuppiler = productSuppiler;
    }

    @XmlElement(name="ProductSupplierPrice")
    public ApiImportProductSupplierPrice getProductSupplierPrice() {
        return productSupplierPrice;
    }

    public void setProductSupplierPrice(ApiImportProductSupplierPrice productSupplierPrice) {
        this.productSupplierPrice = productSupplierPrice;
    }

    @XmlElement(name="ProductDeclaration")
    public ApiImportProductDeclaration getProductDeclaration() {
        return productDeclaration;
    }

    public void setProductDeclaration(ApiImportProductDeclaration productDeclaration) {
        this.productDeclaration = productDeclaration;
    }

    @XmlElement(name="ProductAdmin")
    public ApiImportProductAdmin getProductAdmin() {
        return productAdmin;
    }

    public void setProductAdmin(ApiImportProductAdmin productAdmin) {
        this.productAdmin = productAdmin;
    }
}
