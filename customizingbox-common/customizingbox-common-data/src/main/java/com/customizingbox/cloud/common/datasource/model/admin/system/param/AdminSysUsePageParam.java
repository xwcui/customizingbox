package com.customizingbox.cloud.common.datasource.model.admin.system.param;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;


@Data
public class AdminSysUsePageParam {

	@ApiModelProperty(value = "PK")
	@NotNull
	private String id;

	@ApiModelProperty(value = "用户名")
	@NotNull
	private String username;
}
