package com.customizingbox.cloud.common.datasource.model.app.product.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "产品自定义数据")
public class AppStoreProductPropertiesVO {

    @ApiModelProperty(value = "name")
    private String name;

    @ApiModelProperty(value = "value")
    private String value;
}
