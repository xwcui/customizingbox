package com.customizingbox.cloud.common.datasource.mapper.admin.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.SpuHistory;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-26
 */
public interface SpuHistoryMapper extends BaseMapper<SpuHistory> {

}
