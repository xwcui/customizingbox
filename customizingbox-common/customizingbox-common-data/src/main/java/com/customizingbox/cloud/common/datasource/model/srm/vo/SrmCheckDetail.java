package com.customizingbox.cloud.common.datasource.model.srm.vo;

import com.customizingbox.cloud.common.datasource.model.admin.product.vo.AdminStoreProductAttrVO;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductPropertiesVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author DC_Li
 * @date 2022/5/9 11:22
 */
@Data
@ApiModel("不良品质检明细")
public class SrmCheckDetail {
    @ApiModelProperty(value = "采购订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaseOrderId;

    @ApiModelProperty(value = "订单item表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderItemId;

    @ApiModelProperty(value = "产品图片")
    private String productImgSrc;

    @ApiModelProperty(value = "商品标题")
    private String productTitle;

    @ApiModelProperty(value = "spu")
    private String spu;

    @ApiModelProperty(value = "sku. 系统生成")
    private String sku;

    @ApiModelProperty(value = "唯一码")
    private String onlyCode;

    @ApiModelProperty(value = "规格")
    private List<AdminStoreProductAttrVO> productAttr;

    @ApiModelProperty(value = "采购数量")
    private Integer quantity;

    @ApiModelProperty(value = "良品")
    private Integer acceptQuantity;

    @ApiModelProperty(value = "不良品")
    private Integer rejectsQuantity;

    @ApiModelProperty(value = "遗失")
    private Integer loseQuantity;

    @ApiModelProperty(value = "客户自定义商品信息")
    @JsonIgnore
    private String properties;

    @ApiModelProperty(value = "文本自定义商品信息")
    private List<AppStoreProductPropertiesVO> textProperties;

    @ApiModelProperty(value = "图片自定义商品信息")
    private List<AppStoreProductPropertiesVO> imgProperties;

    @ApiModelProperty(value = "历史质检记录")
    List<AdminPurchaseOrderDetectionRecordVO> detectionRecords;

    @ApiModelProperty(value = "产品变体属性值")
    @JsonIgnore
    private String attrValues;

    @ApiModelProperty(value = "产品Id")
    @JsonIgnore
    private Long productId;

}
