package com.customizingbox.cloud.common.datasource.model.admin.system.vo;

import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;
import lombok.Data;

import java.io.Serializable;


@Data
public class AdminSysLogVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private AdminSysLog adminSysLog;
	private String username;
}
