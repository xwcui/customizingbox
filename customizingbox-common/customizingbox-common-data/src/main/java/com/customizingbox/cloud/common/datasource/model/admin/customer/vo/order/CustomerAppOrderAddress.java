package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/20 16:11
 */
@Data
@ApiModel("收件人信息")
public class CustomerAppOrderAddress {
    @ApiModelProperty("收件人")
    private String receiverName;
    @ApiModelProperty("收件人地址")
    private String receiverAddress;
    @ApiModelProperty("订单备注")
    private String orderNote;
}
