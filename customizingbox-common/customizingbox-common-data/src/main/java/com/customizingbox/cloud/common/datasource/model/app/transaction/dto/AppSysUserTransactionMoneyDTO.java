package com.customizingbox.cloud.common.datasource.model.app.transaction.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 交易记录表
 *
 * @author Y
 * @date 2022-04-21 16:09:21
 */
@Data
@ApiModel(description = "用户总支出和总收入")
public class AppSysUserTransactionMoneyDTO extends Model<AppSysUserTransactionMoneyDTO> {

    @ApiModelProperty(value = "总收入")
    private BigDecimal payinAmount;

    @ApiModelProperty(value = "总收入")
    private BigDecimal payoutAmount;

}
