package com.customizingbox.cloud.common.datasource.model.app.order.panda.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 原平台订单item表
 *
 * @author Y
 * @date 2022-04-12 16:41:47
 */
@Data
@TableName("app_panda_order_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "原平台订单item表")
public class AppPandaOrderItem extends Model<AppPandaOrderItem> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 商户订单item表id
     */
    @ApiModelProperty(value = "商户订单item表id")
    private Long sourceId;
    /**
     * 订单表id
     */
    @ApiModelProperty(value = "订单表id")
    private Long orderId;
    /**
     * 商户订单表id
     */
    @ApiModelProperty(value = "商户订单表id")
    private Long sourceOrderId;
    /**
     * 平台类型. 1: shopify  
     */
    @ApiModelProperty(value = "平台类型. 1: shopify  ")
    private Integer platformType;
    /**
     * 产品表id
     */
    @ApiModelProperty(value = "产品表id")
    private Long productId;
    /**
     * 产品变体id
     */
    @ApiModelProperty(value = "产品变体id")
    private Long variantId;
    
    @ApiModelProperty(value = "唯一码")
    private String onlyCode;

    @ApiModelProperty(value = "定制产品; 1: 定制; 2: 非定制")
    private Integer customMode;
    /**
     * 原商户产品id
     */
    @ApiModelProperty(value = "原商户产品id")
    private Long sourceProductId;
    /**
     * 原商户变体id
     */
    @ApiModelProperty(value = "原商户变体id")
    private Long sourceVariantId;

    @ApiModelProperty(value = "admin产品id")
    private Long adminProductId;

    @ApiModelProperty(value = "admin变体id")
    private Long adminVariantId;
    /**
     * 总产品产品数量
     */
    @ApiModelProperty(value = "总产品产品数量")
    private Integer quantity;
    /**
     * 客户自定义商品信息
     */
    @ApiModelProperty(value = "客户自定义商品信息")
    private String properties;
    /**
     * 实际产品数据 (总产品 - 退款)
     */
    @ApiModelProperty(value = "实际产品数据 (总产品 - 退款)")
    private Integer fulfillableQuantity;
    /**
     * 退款产品数量
     */
    @ApiModelProperty(value = "退款产品数量")
    private Integer refundQuantity;
    /**
     * sku
     */
    @ApiModelProperty(value = "sku")
    private String sku;
    /**
     * 产品快照id
     */
    @ApiModelProperty(value = "产品快照id")
    private Long productSnapshootId;
    /**
     * 成本价(RMB)
     */
    @ApiModelProperty(value = "成本价(RMB)")
    private BigDecimal costPrice;
    /**
     * 供货价(RMB)
     */
    @ApiModelProperty(value = "供货价(RMB)")
    private BigDecimal supplyPrice;
    /**
     * 报价(美元)
     */
    @ApiModelProperty(value = "报价(美元)")
    private BigDecimal quotePrice;
    /**
     * 报价倍率
     */
    @ApiModelProperty(value = "报价倍率")
    private BigDecimal quoteRate;

    @ApiModelProperty(value = "支付金额")
    private BigDecimal payAmount;

    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundAmount;
    /**
     * 美元汇率
     */
    @ApiModelProperty(value = "美元汇率")
    private BigDecimal usExchangeRate;
    /**
     * 产品名称
     */
    @ApiModelProperty(value = "产品名称")
    private String productName;
    /**
     * 税率
     */
    @ApiModelProperty(value = "税率")
    private BigDecimal vatAmount;
    /**
     * 税费
     */
    @ApiModelProperty(value = "税费")
    private BigDecimal totalTax;
    /**
     * 供应商
     */
    @ApiModelProperty(value = "供应商")
    private Long SupplierId;

    /**
     * 0 未退款 1 申请中 2 退款中  3 部分退款 4 全部退款
     */
    @ApiModelProperty(value = "0 未退款 1 申请中 2 退款中  3 部分退款 4 全部退款")
    private Integer refundStatus;
    /**
     * 采购状态(1: 待下单, 2: 已下单, 3: 供应商打回),7: 质检通过(入库)
     */
    @ApiModelProperty(value = "采购状态 1: 待下单, 2: 已下单, 3: 供应商打回, 4:生产中，5:已发货，6:质检不通过，7:已入库，8:采购订单已取消")
    private Integer purchaseStatus;
    /**
     * 数据版本号.  乐观锁
     */
    @ApiModelProperty(value = "数据版本号.  乐观锁")
    private Integer version;
    /**
     * 上传赛和状态; 1: 已上传, 2: 未上传; 
     */
    @ApiModelProperty(value = "上传赛和状态; 1: 已上传, 2: 未上传; ")
    private Integer uploadShStatus;
    /**
     * 上传赛和时间
     */
    @ApiModelProperty(value = "上传赛和时间")
    private LocalDateTime uploadShTime;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

}
