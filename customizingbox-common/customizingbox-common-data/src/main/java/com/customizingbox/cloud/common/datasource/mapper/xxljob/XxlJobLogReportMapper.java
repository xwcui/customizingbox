package com.customizingbox.cloud.common.datasource.mapper.xxljob;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.xxljob.XxlJobLogReport;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface XxlJobLogReportMapper extends BaseMapper<XxlJobLogReport> {

	int save(XxlJobLogReport xxlJobLogReport);

	int update(XxlJobLogReport xxlJobLogReport);

	List<XxlJobLogReport> queryLogReport(@Param("triggerDayFrom") Date triggerDayFrom,
										 @Param("triggerDayTo") Date triggerDayTo);

	XxlJobLogReport queryLogReportTotal();

}
