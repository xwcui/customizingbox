package com.customizingbox.cloud.common.datasource.model.admin.setting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 接收查询条件
 * @author DC_Li
 * @date 2022/4/15 9:51
 */
@Data
@ApiModel("供应商查询条件")
public class AdminSupplierDto {
    /*@ApiModelProperty("供应商姓名")
    private String name;*/
}
