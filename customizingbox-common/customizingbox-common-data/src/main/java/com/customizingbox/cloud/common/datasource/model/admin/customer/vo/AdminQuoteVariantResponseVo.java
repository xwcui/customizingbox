package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 客户报价时获取的产品库变体数据
 * @author DC_Li
 * @date 2022/4/19 14:25
 */
@Data
@ApiModel
public class AdminQuoteVariantResponseVo {
    @ApiModelProperty("admin变体id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminVariantId;

    @ApiModelProperty("sku")
    private String sku;

    @ApiModelProperty("子体图")
    private String variantImg;

    /**
     * 所有规格值
     */
    @JsonIgnore
    private String attrValues;

    @ApiModelProperty("一级属性名")
    private String optionAName;

    @ApiModelProperty("一级属性值")
    private String optionAvalue;

    @ApiModelProperty("二级属性名")
    private String optionBName;

    @ApiModelProperty("二级属性值")
    private String optionBvalue;

    @ApiModelProperty("三级属性名")
    private String optionCName;

    @ApiModelProperty("三级属性值")
    private String optionCvalue;

    @ApiModelProperty("供货价(CNY)")
    private BigDecimal supplyPrice;
}
