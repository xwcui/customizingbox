package com.customizingbox.cloud.common.datasource.model.admin.setting.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author DC_Li
 * @date 2022/4/14 15:42
 */
@Data
@ApiModel("汇率查询响应数据")
public class AdminExchangeRateResponseVo extends Model<AdminExchangeRateResponseVo> {
    @ApiModelProperty("币种中文名")
    private String currencyName;

    @ApiModelProperty("币种代码")
    private String currencyCode;

    @ApiModelProperty("兑人民币汇率")
    private BigDecimal rate;
}
