package com.customizingbox.cloud.common.datasource.model.admin.transport.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * <p>
 * 赛合运输方式
 * </p>
 *
 * @author Z
 * @since 2022-04-14
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_sh_transport")
@ApiModel(value = "赛合运输方式")
public class AdminShTransport extends Model<AdminShTransport> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer id;

    @ApiModelProperty(value = "运输商")
    private String carrierName;

    @ApiModelProperty(value = "运输方式中文名")
    private String transportName;

    @ApiModelProperty(value = "运输方式应英文名")
    private String transportNameEn;

    @ApiModelProperty(value = "是否挂号")
    private Boolean isRegistered;

    @ApiModelProperty(value = "trackingMoreCode")
    private String trackingMoreCode;

    @ApiModelProperty(value = "17track运输商编码")
    private String fcCode;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}