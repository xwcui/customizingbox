package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLog;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 */
public interface AdminSysLogMapper extends BaseMapper<AdminSysLog> {

}
