package com.customizingbox.cloud.common.datasource.model.app.order.source.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "app订单修改状态参数")
public class AppOrderStatusParam {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "订单状态(1: 正常, 2: 取消)")
    private Integer status;

}
