package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysGenTable;


/**
 * 代码生成配置表
 */
public interface AppSysGenTableMapper extends BaseMapper<AppSysGenTable> {

}
