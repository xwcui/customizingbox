package com.customizingbox.cloud.common.datasource.mapper.xxljob;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.xxljob.XxlJobLog;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface XxlJobLogMapper extends BaseMapper<XxlJobLog> {

	List<XxlJobLog> pageList(@Param("offset") int offset,
							 @Param("pagesize") int pagesize,
							 @Param("jobGroup") int jobGroup,
							 @Param("jobId") int jobId,
							 @Param("triggerTimeStart") Date triggerTimeStart,
							 @Param("triggerTimeEnd") Date triggerTimeEnd,
							 @Param("logStatus") int logStatus);

	int pageListCount(@Param("offset") int offset,
					  @Param("pagesize") int pagesize,
					  @Param("jobGroup") int jobGroup,
					  @Param("jobId") int jobId,
					  @Param("triggerTimeStart") Date triggerTimeStart,
					  @Param("triggerTimeEnd") Date triggerTimeEnd,
					  @Param("logStatus") int logStatus);
	
	XxlJobLog load(@Param("id") long id);

	long save(XxlJobLog xxlJobLog);

	int updateTriggerInfo(XxlJobLog xxlJobLog);

	int updateHandleInfo(XxlJobLog xxlJobLog);
	
	int delete(@Param("jobId") int jobId);

	Map<String, Object> findLogReport(@Param("from") Date from,
									  @Param("to") Date to);

	List<Long> findClearLogIds(@Param("jobGroup") int jobGroup,
							   @Param("jobId") int jobId,
							   @Param("clearBeforeTime") Date clearBeforeTime,
							   @Param("clearBeforeNum") int clearBeforeNum,
							   @Param("pagesize") int pagesize);

	int clearLog(@Param("logIds") List<Long> logIds);

	@InterceptorIgnore(tenantLine="true")
	List<Long> findFailJobLogIds(@Param("pagesize") int pagesize);

	int updateAlarmStatus(@Param("logId") long logId,
						  @Param("oldAlarmStatus") int oldAlarmStatus,
						  @Param("newAlarmStatus") int newAlarmStatus);

	List<Long> findLostJobIds(@Param("losedTime") Date losedTime);

}
