package com.customizingbox.cloud.common.datasource.model.admin.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-31 09:34:40
 */
@Data
@TableName("admin_store_product_img_depot")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "产品图库")
public class AdminStoreProductImgDepot extends Model<AdminStoreProductImgDepot> {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;
    /**
     * 图片oss地址
     */
    @ApiModelProperty(value = "图片oss地址")
    private String src;

}
