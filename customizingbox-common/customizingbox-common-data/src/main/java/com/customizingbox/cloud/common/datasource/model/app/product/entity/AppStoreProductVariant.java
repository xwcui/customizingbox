package com.customizingbox.cloud.common.datasource.model.app.product.entity;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
@Data
@TableName("app_store_product_variant")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "产品属性值表")
public class AppStoreProductVariant extends Model<AppStoreProductVariant> {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    private Long productId;
    /**
     * 商户产品id
     */
    @ApiModelProperty(value = "商户产品id")
    private Long sourceProductId;
    @ApiModelProperty(value = "原商户数据id")
    private Long sourceId;

    @ApiModelProperty(value = "关联admin产品变体id")
    private Long adminVariantId;

    @ApiModelProperty(value = "关联admin产品变体id")
    private Long adminProductId;

    @ApiModelProperty(value = "商户类型")
    private Integer platformType;

    @ApiModelProperty(value = "报价倍率")
    private BigDecimal supplyRate;

    /**
     * 属性title
     */
    @ApiModelProperty(value = "属性title")
    private String title;
    /**
     * sku
     */
    @ApiModelProperty(value = "sku")
    private String sku;
    /**
     * 条码
     */
    @ApiModelProperty(value = "条码")
    private String barcode;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer position;
    /**
     * 销售价格
     */
    @ApiModelProperty(value = "销售价格")
    private BigDecimal price;
    /**
     * 原价
     */
    @ApiModelProperty(value = "原价")
    private BigDecimal compareAtPrice;

    /**
     * 属性, json格式
     */
    @ApiModelProperty(value = "属性, json格式")
    @Getter(AccessLevel.NONE)
    private String attrValues;
    /**
     * 图片id
     */
    @ApiModelProperty(value = "图片id")
    private Long imageId;
    /**
     * 克为单位重量
     */
    @ApiModelProperty(value = "克为单位重量")
    private Long grams;
    /**
     * 重量
     */
    @ApiModelProperty(value = "重量")
    private Double weight;
    /**
     * 重量单位
     */
    @ApiModelProperty(value = "重量单位")
    private String weightUnit;
    /**
     * 长
     */
    @ApiModelProperty(value = "长")
    private Double length;
    /**
     * 宽
     */
    @ApiModelProperty(value = "宽")
    private Double width;
    /**
     * 高
     */
    @ApiModelProperty(value = "高")
    private Double height;
    /**
     * 库存
     */
    @ApiModelProperty(value = "库存")
    private Integer inventoryQuantity;
    /**
     * 原库存
     */
    @ApiModelProperty(value = "原库存")
    private Integer oldInventoryQuantity;
    /**
     * 商户系统创建时间
     */
    @ApiModelProperty(value = "商户系统创建时间")
    private LocalDateTime createdAt;
    /**
     * 商户系统修改时间
     */
    @ApiModelProperty(value = "商户系统修改时间")
    private LocalDateTime updatedAt;
    /**
     * 商户系统创建时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统创建时间(北京时间)")
    private LocalDateTime bjCreatedAt;
    /**
     * 商户系统修改时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统修改时间(北京时间)")
    private LocalDateTime bjUpdatedAt;
    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标记")
    @TableLogic
    private String delFlag;

    @ApiModelProperty(value = "option1")
    @TableField(exist = false)
    private String option1;

    @ApiModelProperty(value = "option2")
    @TableField(exist = false)
    private String option2;

    @ApiModelProperty(value = "option3")
    @TableField(exist = false)
    private String option3;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;


    public String getAttrValues() {
        List<String> list = new ArrayList();
        if (!StringUtils.isEmpty(option1)) {
            list.add(option1);
        }
        if (!StringUtils.isEmpty(option2)) {
            list.add(option2);
        }
        if (!StringUtils.isEmpty(option3)) {
            list.add(option3);
        }
        return JSONObject.toJSONString(list);
    }
}
