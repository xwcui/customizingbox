package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRole;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface AdminSysRoleMapper extends BaseMapper<AdminSysRole> {

    /**
     * 通过用户ID，查询角色信息
     */
    List<String> listRoleIdsByUserId(String userId);
}
