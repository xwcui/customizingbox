package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;


import com.customizingbox.cloud.common.datasource.model.admin.saihe.request.ApiUploadOrderRequest;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.request.ApiUploadProductsRequest;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.request.UpdateProductStockNumberRequest;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.ApiUploadProductsResponse;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.UpLoadOrderV2Response;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.UpdateProductStockNumberResponse;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by cjq on 2018/12/4.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "soap:Body")
@Data
public class BodyToXml {

    /**
     * 产品新增编辑请求对象
     */
    @XmlElement(name="ProcessUpdateProduct")
    ApiUploadProductsRequest apiUploadProductsRequest;

    /**
     * 产品新增编辑返回对象
     */
    @XmlElement(name="ProcessUpdateProductResponse")
    ApiUploadProductsResponse apiUploadProductsResponse;

    /**
     * 赛盒创建库存请求对象
     * 请求库存产品操作实体类
     */
    @XmlElement(name="UpdateProductStockNumber")
    UpdateProductStockNumberRequest updateProductStockNumberRequest;

    /**
     * 赛盒创建库存返回对象
     * 请求库存产品操作结果实体类
     */
    @XmlElement(name="UpdateProductStockNumberResponse")
    UpdateProductStockNumberResponse updateProductStockNumberResponse;

    /**
     * 订单上传赛盒
     */
    @XmlElement(name="UpLoadOrderV2")
    ApiUploadOrderRequest apiUploadOrderRequest;


    /**
     * 订单上传赛盒返回对象
     */
    @XmlElement(name="UpLoadOrderV2Response")
    UpLoadOrderV2Response upLoadOrderV2Response;


}
