package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by cjq on 2018/12/4.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "soap:Body")
public class ResponseBody {


    @XmlElement(name="GetTransportListResponse")
    ApiGetTransportResponse apiGetTransportResponse;

    @XmlElement(name="UpLoadOrderV2Response")
    UpLoadOrderV2Response upLoadOrderV2Response;

    public ApiGetTransportResponse getApiGetTransportResponse() {
        return apiGetTransportResponse;
    }

    public void setApiGetTransportResponse(ApiGetTransportResponse apiGetTransportResponse) {
        this.apiGetTransportResponse = apiGetTransportResponse;
    }

    public UpLoadOrderV2Response getUpLoadOrderV2Response() {
        return upLoadOrderV2Response;
    }

    public void setUpLoadOrderV2Response(UpLoadOrderV2Response upLoadOrderV2Response) {
        this.upLoadOrderV2Response = upLoadOrderV2Response;
    }
}
