package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/4/20 16:11
 */
@Data
@ApiModel("店铺订单信息")
public class CustomerAppOrderInfo {
    @ApiModelProperty("订单支付时间")
    private LocalDateTime orderPayTime;

    @ApiModelProperty("店铺名")
    private String storeName;

    @ApiModelProperty("订单号，源店铺订单id")
    private String storeOrderId;

    @ApiModelProperty("app订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pandaOrderId;

    @ApiModelProperty("订单支付状态 1：未支付 2：已支付")
    private Integer orderPayState;

    @ApiModelProperty("订单发货状态 1：未发货 2：部分发货 3：全部发货")
    private Integer orderShipState;
}
