package com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author DC_Li
 * @date 2022/5/3 9:45
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProducingProductVo extends PurchaseProductVo {
    @ApiModelProperty("生产批次")
    private String productionBatch;
}
