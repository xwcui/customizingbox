package com.customizingbox.cloud.common.datasource.model.admin.system.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 * 菜单权限表
 * </p>
 */
@Data
@ApiModel(description = "菜单")
public class AdminSysSaveMenuParam {
    /**
     * 菜单ID
     */
    @ApiModelProperty(value = "ID")
    private String id;


    @ApiModelProperty(value = "菜单名称")
    @NotNull(message = "菜单名称不能为空")
    private String name;

    @ApiModelProperty(value = "菜单权限标识")
    @NotNull(message = "菜单权限标识不能为空")
    private String permission;

    @ApiModelProperty(value = "权限编码")
    @NotNull(message = "权限编码不能为空")
    private String permissionCode;

    @ApiModelProperty(value = "父菜单ID")
    @NotNull(message = "菜单父ID不能为空")
    private String parentId;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "页面路径")
    private String component;

    @ApiModelProperty(value = "排序值")
    @NotNull(message = "排序值不能为空")
    private Integer sort;

    @NotNull(message = "菜单类型 （0菜单 1按钮）")
    @ApiModelProperty(value = "菜单类型 （0菜单 1按钮）")
    private String type;
//
//    @ApiModelProperty(value = "	角色Id")
//    @TableField(exist = false)
//    private String roleId;
}
