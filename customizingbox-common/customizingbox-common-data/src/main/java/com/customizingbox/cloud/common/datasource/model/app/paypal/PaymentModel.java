package com.customizingbox.cloud.common.datasource.model.app.paypal;

import lombok.Data;

@Data
public class PaymentModel {

    private String description;

    private String amount;

    private String currency;
}
