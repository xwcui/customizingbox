package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrganRelation;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface AdminSysOrganRelationMapper extends BaseMapper<AdminSysOrganRelation> {

    /**
     * 删除机构关系表数据
     *
     * @param id 机构ID
     */
    int deleteOrganRelationsById(String id);

    /**
     * 更改部分关系表数据
     */
    int deleteOrganRelations(AdminSysOrganRelation adminSysOrganRelation);

    /**
     * 更改部分关系表数据
     */
    List<AdminSysOrganRelation> listOrganRelations(AdminSysOrganRelation adminSysOrganRelation);

}
