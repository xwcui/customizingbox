package com.customizingbox.cloud.common.datasource.model.admin.system.param;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@Data
public class AdminSysUseSaveParam {

	@ApiModelProperty(value = "PK")
	private String id;

	@ApiModelProperty(value = "用户名")
	private String username;

	@ApiModelProperty(value = "角色ID")
	private List<String> roleIds;

	@ApiModelProperty(value = "密码")
	private String password;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "用户类型; 1: 客户经理, 2: 采购员,  3: (srm)工厂用户")
	private Integer type;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "英文名")
	private String englishName;

	@ApiModelProperty(value = "用户编号")
	private String userNumber;

	@ApiModelProperty(value = "职位")
	private String job;

	@ApiModelProperty(value = "赛盒code")
	private String shCode;

	@ApiModelProperty(value = "查看客户权限. 0 : 可以查看全部客户,  1: 只能查看自己关联的客户")
	private Boolean customerDataAuth;

	@ApiModelProperty(value = "查看采购员权限. 0 : 可以查看全部采购员,  1: 只能查看自己的数据")
	private Boolean purchaseDataAuth;
}
