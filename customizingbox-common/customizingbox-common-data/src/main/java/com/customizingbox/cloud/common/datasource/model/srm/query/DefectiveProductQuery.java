package com.customizingbox.cloud.common.datasource.model.srm.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/5/3 13:17
 */
@ApiModel("不良品列表查询条件")
@Data
public class DefectiveProductQuery {
    @ApiModelProperty("唯一码")
    private String onlyCode;

    @ApiModelProperty("生产批次")
    private String productionBatch;

    @ApiModelProperty("发货批次")
    private String deliveryBatch;

    @ApiModelProperty("物流单号")
    private String deliveryCode;
}
