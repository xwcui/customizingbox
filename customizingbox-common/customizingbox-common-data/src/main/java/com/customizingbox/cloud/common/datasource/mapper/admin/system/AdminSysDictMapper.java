package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 */
public interface AdminSysDictMapper extends BaseMapper<AdminSysDict> {

}
