package com.customizingbox.cloud.common.datasource.model.srm.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/29 13:42
 */
@Data
@ApiModel("已发货列表查询参数")
public class FulfilledProductQuery {
    @ApiModelProperty("SPU")
    private String spu;

    @ApiModelProperty("唯一码")
    private String onlyCode;

    @ApiModelProperty("生产批次")
    private String productionBatch;

    @ApiModelProperty("发货批次")
    private String deliveryBatch;

    @ApiModelProperty("物流单号")
    private String deliveryCode;

    @ApiModelProperty("结算状态 0：未结算，1：已结算")
    private Integer settlementState;

}
