package com.customizingbox.cloud.common.datasource.model.admin.system.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 公共参数
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminSysParamVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公共参数键
     */
    private String publicKey;

    /**
     * 公共参数值
     */
    private String publicValue;

    /**
     * 公共参数名
     */
    private String publicName;
}