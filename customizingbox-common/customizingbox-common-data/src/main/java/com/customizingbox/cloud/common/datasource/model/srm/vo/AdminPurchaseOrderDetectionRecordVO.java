package com.customizingbox.cloud.common.datasource.model.srm.vo;


import com.customizingbox.cloud.common.datasource.model.admin.product.vo.AdminStoreProductAttrVO;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductPropertiesVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(value = "质检记录返回值")
public class AdminPurchaseOrderDetectionRecordVO {

    @ApiModelProperty(value = "质检状态: 1: 通过, 2 不通过")
    private Integer status;

    @ApiModelProperty(value = "不良品数量")
    private Integer rejectsQuantity;

    @ApiModelProperty(value = "良品数量")
    private Integer acceptQuantity;

    @ApiModelProperty(value = "遗失数量")
    private Integer loseQuantity;

    @ApiModelProperty(value = "备注")
    private String detectionMark;

    @ApiModelProperty(value = "质检用户")
    private String adminDetectionUser;

    @ApiModelProperty(value = "质检时间")
    private LocalDateTime detectionTime;
}
