package com.customizingbox.cloud.common.datasource.model.xxljob;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class XxlJobInfo {

	@TableId(type = IdType.AUTO)
	private Integer id;
	
	private Integer jobGroup;
	private String jobDesc;
	
	private Date addTime;
	private Date updateTime;
	
	private String author;
	private String alarmEmail;

	private String scheduleType;
	private String scheduleConf;
	private String misfireStrategy;

	private String executorRouteStrategy;
	private String executorHandler;
	private String executorParam;
	private String executorBlockStrategy;
	private int executorTimeout;
	private int executorFailRetryCount;
	
	private String glueType;
	private String glueSource;
	private String glueRemark;
	private Date glueUpdatetime;

	private String childJobId;

	private int triggerStatus;
	private long triggerLastTime;
	private long triggerNextTime;

}
