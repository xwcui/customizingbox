package com.customizingbox.cloud.common.datasource.mapper.admin.setting;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminSupplier;
import com.customizingbox.cloud.common.datasource.model.admin.setting.vo.AdminSupplierResponseVo;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;

/**
 * <p>
 * 供应商表 Mapper 接口
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
public interface AdminSupplierMapper extends BaseMapper<AdminSupplier> {

    IPage<AdminSupplierResponseVo> pageAll(Page page);

    Boolean updateByPrimaryKeySelective(AdminSupplier vo);

    /**
     * 修改供应商启用禁用状态
     * @param supplierId 供应商id
     * @param status 1启用 2禁用
     * @param userId
     * @param now
     * @return
     */
    Boolean changeStatus(@Param("supplierId") Long supplierId, @Param("status") Integer status,
                         @Param("userId") Long userId, @Param("now") LocalDateTime now);
}
