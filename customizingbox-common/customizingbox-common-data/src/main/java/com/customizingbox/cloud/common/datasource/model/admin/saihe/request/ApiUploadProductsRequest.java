package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;


import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.Prequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * 赛盒产品新增编辑
 */
public class ApiUploadProductsRequest {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";

    Prequest request;

    @XmlElement(name="request")
    public Prequest getRequest() {
        return request;
    }

    public void setRequest(Prequest request) {
        this.request = request;
    }

}
