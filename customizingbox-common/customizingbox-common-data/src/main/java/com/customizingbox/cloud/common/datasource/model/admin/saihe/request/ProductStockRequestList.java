package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ProductStockList;

import javax.xml.bind.annotation.XmlElement;

/**
 * 请求库存产品操作实体列表
 */
public class ProductStockRequestList {

    String userName;


    String password;


    Integer customerID;

    /**
     * 产品库存信息集合
     */
    ProductStockList productStockList;

    public ProductStockRequestList() {
    }

    @XmlElement(name = "UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlElement(name = "Password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlElement(name = "CustomerID")
    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    @XmlElement(name = "ProductStockList")
    public ProductStockList getProductStockList() {
        return productStockList;
    }

    public void setProductStockList(ProductStockList productStockList) {
        this.productStockList = productStockList;
    }
}
