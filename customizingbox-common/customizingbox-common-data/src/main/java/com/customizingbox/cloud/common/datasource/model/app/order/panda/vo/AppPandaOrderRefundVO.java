package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "退款管理页面参数")
public class AppPandaOrderRefundVO {

    @ApiModelProperty(value = "退款id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    @ApiModelProperty(value = "客户名")
    private String customerName;

    @ApiModelProperty(value = "客户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long customerId;

    @ApiModelProperty(value = "客户经理")
    private String customerManager;

    @ApiModelProperty(value = "申请人")
    private String applyUser;

    @ApiModelProperty(value = "状态:  1 申请中 4 已退款(同意退款) 5 拒绝退款")
    private Integer status;

    @ApiModelProperty(value = "审核权限:  1: 有; 2: 没有(默认)")
    private Integer checkStatus = 2;

    @ApiModelProperty(value = "退款交易号")
    private String transactionRecordId;

    @ApiModelProperty(value = "退款金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "审核时间")
    private LocalDateTime checkTime;
}