package com.customizingbox.cloud.common.datasource.model.app.order.panda.param;

import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundItemAmountVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(description = "申请退款参数")
public class AppPandaOrderRefundApplyParam {

    @ApiModelProperty(value = "订单id")
    @NotNull
    private Long orderId;

    @ApiModelProperty(value = "运费")
    private BigDecimal freightAmount;

    @ApiModelProperty(value = "申请原因")
    private String applyRemark;

    @ApiModelProperty(value = "vat费用")
    private BigDecimal vatAmount;

    @ApiModelProperty(value = "退款总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "item订单数据")
    private List<AppPandaOrderRefundItemAmountVO> refundAmountParam;
}