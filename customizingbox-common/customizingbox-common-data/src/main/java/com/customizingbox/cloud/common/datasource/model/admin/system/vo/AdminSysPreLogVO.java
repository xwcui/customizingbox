package com.customizingbox.cloud.common.datasource.model.admin.system.vo;

import lombok.Data;

/**
 * 前端日志vo
 */
@Data
public class AdminSysPreLogVO {

	private String url;

	private String time;

	private String user;

	private String type;

	private String message;

	private String stack;

	private String info;
}
