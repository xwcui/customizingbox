package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLog;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 */
public interface AppSysLogMapper extends BaseMapper<AppSysLog> {

}
