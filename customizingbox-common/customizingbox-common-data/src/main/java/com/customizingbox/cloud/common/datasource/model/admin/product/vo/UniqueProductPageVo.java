package com.customizingbox.cloud.common.datasource.model.admin.product.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "产品列表vo")
public class UniqueProductPageVo {

    @ApiModelProperty(value = "产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * only_code
     */
    @ApiModelProperty(value = "onlyCode")
    private String onlyCode;

    /**
     * title_en 英文产品名
     */
    @ApiModelProperty(value = "英文产品名")
    private String titleEn;
    /**
     * 产品报关英文名
     */
    @ApiModelProperty(value = "产品报关英文名")
    private String nameEn;
    /**
     * 产品报关中文名
     */
    @ApiModelProperty(value = "产品报关中文名")
    private String nameCn;
    /**
     * spu
     */
    @ApiModelProperty(value = "spu")
    private String spu;
    /**
     * sku
     */
    @ApiModelProperty(value = "sku")
    private String sku;

    /**
     * sh_sku
     */
    @ApiModelProperty(value = "sh_sku")
    private String shSku;

    @ApiModelProperty("变体属性 此处不展示此字段")
    private String attrValues;

    @ApiModelProperty(value = "规格 此处展示")
    private List<AdminStoreProductAttrVO> productAttr;

    @ApiModelProperty(value = "潘达订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pandaOrderId;

    @ApiModelProperty(value = "userId")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;


    @ApiModelProperty(value = "订单创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "pay_time")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "数量")
    private Integer fulfillableQuantity;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String imageUrl;


    @ApiModelProperty(value = "上传赛和库存状态; 1: 已上传, 2: 未上传")
    private Integer uploadShStatus;

    @ApiModelProperty(value = "上传赛和时间")
    private LocalDateTime uploadShTime;
}
