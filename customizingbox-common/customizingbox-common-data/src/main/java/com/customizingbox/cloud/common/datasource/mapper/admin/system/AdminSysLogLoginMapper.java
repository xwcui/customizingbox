package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysLogLogin;

/**
 * 登录日志表
 */
public interface AdminSysLogLoginMapper extends BaseMapper<AdminSysLogLogin> {

}
