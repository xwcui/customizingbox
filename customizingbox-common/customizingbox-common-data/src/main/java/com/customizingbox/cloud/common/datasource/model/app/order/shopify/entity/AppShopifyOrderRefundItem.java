package com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * shopify 订单item退款表
 *
 * @author Y
 * @date 2022-03-30 14:19:51
 */
@Data
@TableName("app_shopify_order_refund_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "shopify 订单item退款表")
public class AppShopifyOrderRefundItem extends Model<AppShopifyOrderRefundItem> {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "")
    private Long id;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long lineItemId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long locationId;
    /**
     * 退款数量
     */
    @ApiModelProperty(value = "退款数量")
    private Integer quantity;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String restockType;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal subtotal;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String subtotalSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalTax;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalTaxSet;
    /**
     * 商品信息
     */
    @ApiModelProperty(value = "商品信息")
    private String lineItem;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

}
