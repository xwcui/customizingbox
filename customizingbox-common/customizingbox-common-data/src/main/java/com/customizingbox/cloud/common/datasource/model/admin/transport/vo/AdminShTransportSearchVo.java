package com.customizingbox.cloud.common.datasource.model.admin.transport.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * xiwu
 * 赛盒物流方式查询vo
 */
@Data
@ApiModel(value = "赛盒物流方式查询vo")
public class AdminShTransportSearchVo {


    @ApiModelProperty(value = "主键id")
    private Integer id;

    @ApiModelProperty(value = "运输商")
    private String carrierName;

    @ApiModelProperty(value = "运输方式中文名")
    private String transportName;

    @ApiModelProperty(value = "运输方式应英文名")
    private String transportNameEn;

    @ApiModelProperty(value = "是否挂号")
    private Boolean isRegistered;

    @ApiModelProperty(value = "trackingMoreCode")
    private String trackingMoreCode;

    @ApiModelProperty(value = "17track运输商编码")
    private String fcCode;
}
