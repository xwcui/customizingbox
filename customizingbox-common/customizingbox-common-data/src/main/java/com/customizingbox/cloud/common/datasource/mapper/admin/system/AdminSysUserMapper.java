package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUseDetailVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUserPageVO;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminSysUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 */
public interface AdminSysUserMapper extends BaseMapper<AdminSysUser> {

    /**
     * 无租户查询
     *
     * @return SysUser
     */
    @InterceptorIgnore(tenantLine="true")
    AdminSysUser getByNoTenant(AdminSysUser sysUser);

    /**
     * 分页查询用户信息（含角色）
     *
     * @param page    分页
     * @param adminSysUserDTO 查询参数
     * @return list
     */
    IPage<List<AdminSysUserPageVO>> getUserVosPage(Page<AdminSysUserDTO> page, @Param("query") AdminSysUserDTO adminSysUserDTO);

    /**
     * 通过ID查询用户信息
     *
     * @param id 用户ID
     * @return userVo
     */
    AdminSysUserVO getUserVoById(String id);

    /**
     * 根据id查询用户信息
     */
    AdminSysUseDetailVO getUsersById(@Param("id") String id);
}
