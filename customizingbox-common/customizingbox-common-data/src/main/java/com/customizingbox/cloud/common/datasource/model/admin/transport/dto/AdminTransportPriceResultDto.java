package com.customizingbox.cloud.common.datasource.model.admin.transport.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel(value = "订单运费")
@Data
public class AdminTransportPriceResultDto {

    @ApiModelProperty(value = "运输方式id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long transportId;

    @ApiModelProperty(value = "运输名称")
    private String transportName;

    @ApiModelProperty(value = "运费")
    private BigDecimal transportPrice;

    @ApiModelProperty(value = "最早到达时间(天)")
    private Integer earliestDay;

    @ApiModelProperty(value = "最晚到达时间(天)")
    private Integer longestDay;
}
