package com.customizingbox.cloud.common.datasource.model.app.order.source.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单item列表
 */
@Data
@ApiModel(description = "订单item表")
public class AppQuotationOrderItemVO extends Model<AppQuotationOrderItemVO> {

    @ApiModelProperty(value = "变体图片")
    private String appImgSrc;

    @ApiModelProperty(value = "变体属性值")
    private String appAttrValues;

    @ApiModelProperty(value = "变体数量")
    private String appFulfillableQuantity;

    @ApiModelProperty(value = "关联(有值就说明已经关联了)")
    private String adminVariantId;

    @ApiModelProperty(value = "变体图片")
    private String adminImgSrc;

    @ApiModelProperty(value = "变体属性值")
    private String adminAttrValues;

    @ApiModelProperty(value = "给客户的价格")
    private BigDecimal quotePrice;

}
