package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @author DC_Li
 * @date 2022/4/22 16:25
 */
@Data
@ApiModel("保存报价信息参数")
public class SaveQuoteRequestVo {
    @ApiModelProperty(value = "app产品id",required = true)
    private Long appProductId;

    @ApiModelProperty(value = "app变体id",required = true)
    private Long appVariantId;

    @ApiModelProperty(value = "admin产品id",required = true)
    private Long adminProductId;

    @ApiModelProperty(value = "admin变体id",required = true)
    private Long adminVariantId;

    @ApiModelProperty(value = "admin变体sku",required = true)
    private String adminVariantSku;

    @ApiModelProperty(value = "报价倍率",required = true)
    private BigDecimal quoteRate;
}
