package com.customizingbox.cloud.common.datasource.mapper.admin.transport;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsFreight;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsFreightVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 物流费用 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminLogisticsFreightMapper extends BaseMapper<AdminLogisticsFreight> {

    IPage<AdminLogisticsFreightVo> pageAll(Page page, @Param("searchVo") AdminLogisticsFreightSearchVo searchVo);

    int updateNotEnableTime(@Param("transportIds") List<Long> transportList, @Param("enableTime") String enableTime);

    List<AdminTransportPriceResultDto> getTransportPrice(@Param("dto") AdminTransportPriceDTO adminTransportPriceDTO);
}
