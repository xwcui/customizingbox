package com.customizingbox.cloud.common.datasource.model.admin.setting.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/5/4 16:16
 */
@Data
@ApiModel("采购员的id和用户名")
public class AdminPurchaseUserIdAndNameVo {
    @ApiModelProperty("采购员id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminPurchaseUserId;

    @ApiModelProperty("采购员用户名")
    private String adminPurchaseUserName;
}
