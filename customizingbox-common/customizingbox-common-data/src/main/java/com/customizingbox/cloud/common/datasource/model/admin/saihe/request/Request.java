package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ApiUploadOrderInfo;

import javax.xml.bind.annotation.XmlElement;


public class Request {

    /**
     * 用户名
     */
    String userName;

    /**
     * 密码
     */
    String password;

    /**
     * 客户号
     */
    Integer customerID;

    /**
     * 上传订单信息实体
     */
    ApiUploadOrderInfo apiUploadOrderInfo;

    @XmlElement(name = "ApiUploadOrderInfo")
    public ApiUploadOrderInfo getApiUploadOrderInfo() {
        return apiUploadOrderInfo;
    }

    public void setApiUploadOrderInfo(ApiUploadOrderInfo apiUploadOrderInfo) {
        this.apiUploadOrderInfo = apiUploadOrderInfo;
    }

    @XmlElement(name = "UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlElement(name = "Password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlElement(name = "CustomerID")
    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

}
