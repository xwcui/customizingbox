package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

/**
 * 产品供应商报价
 */
public class ApiImportProductSupplierPrice {

    /**
     * 供应商产品编码
     */
    String supplierSKU;

    /**
     * 处理费
     */
    BigDecimal processPrice;

    /**
     * 其他费
     */
    BigDecimal otherPrice;

    /**
     * 采购天数
     */
    Integer procurementDay;

    /**
     * 网络采购链接
     */
    String webProductUrl;

    @XmlElement(name="SupplierSKU")
    public String getSupplierSKU() {
        return supplierSKU;
    }

    public void setSupplierSKU(String supplierSKU) {
        this.supplierSKU = supplierSKU;
    }

    @XmlElement(name="ProcessPrice")
    public BigDecimal getProcessPrice() {
        return processPrice;
    }

    public void setProcessPrice(BigDecimal processPrice) {
        this.processPrice = processPrice;
    }

    @XmlElement(name="OtherPrice")
    public BigDecimal getOtherPrice() {
        return otherPrice;
    }

    public void setOtherPrice(BigDecimal otherPrice) {
        this.otherPrice = otherPrice;
    }

    @XmlElement(name="ProcurementDay")
    public Integer getProcurementDay() {
        return procurementDay;
    }

    public void setProcurementDay(Integer procurementDay) {
        this.procurementDay = procurementDay;
    }

    @XmlElement(name="WebProductUrl")
    public String getWebProductUrl() {
        return webProductUrl;
    }

    public void setWebProductUrl(String webProductUrl) {
        this.webProductUrl = webProductUrl;
    }
}
