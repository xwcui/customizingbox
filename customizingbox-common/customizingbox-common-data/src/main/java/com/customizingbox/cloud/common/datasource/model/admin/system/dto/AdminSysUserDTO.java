package com.customizingbox.cloud.common.datasource.model.admin.system.dto;


import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
public class AdminSysUserDTO extends AdminSysUser {

	/**
	 * 角色ID
	 */
	private List<String> roleIds;

	/**
	 * organId
	 */
	private String organId;

	/**
	 * 新密码
	 */
	private String newPassword;

	/**
	 * 验证码
	 */
	private String code;

	/**
	 * 操作类型
	 */
	private String doType;
}
