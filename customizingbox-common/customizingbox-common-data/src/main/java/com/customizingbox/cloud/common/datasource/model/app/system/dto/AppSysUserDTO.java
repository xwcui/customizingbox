package com.customizingbox.cloud.common.datasource.model.app.system.dto;


import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
public class AppSysUserDTO extends AppSysUser {

	/**
	 * 角色ID
	 */
	private List<String> roleIds;

	/**
	 * organId
	 */
	private String organId;

	/**
	 * 新密码
	 */
	private String newPassword;

	/**
	 * 验证码
	 */
	private String code;

	/**
	 * 操作类型
	 */
	private String doType;
}
