package com.customizingbox.cloud.common.datasource.model.admin.system.param;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@Data
public class AdminSysUseUpdateParam {

	@ApiModelProperty(value = "用户id")
	private Long id;

	@ApiModelProperty(value = "角色ID")
	private List<String> roleIds;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "英文名")
	private String englishName;

	@ApiModelProperty(value = "用户编号")
	private String userNumber;

	@ApiModelProperty(value = "职位")
	private String job;

	@ApiModelProperty(value = "赛盒code")
	private String shCode;

	@ApiModelProperty(value = "查看客户权限. 0 : 可以查看全部客户,  1: 只能查看自己关联的客户")
	private Boolean customerDataAuth;

	@ApiModelProperty(value = "查看客户权限. 0 : 可以查看全部客户,  1: 只能查看自己关联的客户")
	private Boolean purchaseDataAuth;
}
