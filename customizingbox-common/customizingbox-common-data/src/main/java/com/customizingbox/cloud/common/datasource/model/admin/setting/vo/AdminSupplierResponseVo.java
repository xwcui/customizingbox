package com.customizingbox.cloud.common.datasource.model.admin.setting.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/4/14 16:09
 */
@Data
@ApiModel("供应商查询响应数据")
public class AdminSupplierResponseVo extends Model<AdminSupplierResponseVo> {
    @ApiModelProperty("供应商id")
    private String supplierId;

    @ApiModelProperty("供应商名称")
    private String name;

    @ApiModelProperty("状态 1：正常 2：禁用")
    private String status;

    @ApiModelProperty("采购员")
    private AdminPurchaseUserIdAndNameVo adminPurchaseUserIdAndNameVo;

    @ApiModelProperty("更新日期")
    private LocalDateTime updateTime;
}
