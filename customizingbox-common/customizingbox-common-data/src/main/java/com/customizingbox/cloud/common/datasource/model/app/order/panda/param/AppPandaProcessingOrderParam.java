package com.customizingbox.cloud.common.datasource.model.app.order.panda.param;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Data
@ApiModel(description = "processing 页面 和 shipped 页面需要的参数")
public class AppPandaProcessingOrderParam extends AppOrderBaseParam {

    @ApiModelProperty(value = "1: 待发货, 2: 已发货")
    @NotNull
    private Integer status;
}
