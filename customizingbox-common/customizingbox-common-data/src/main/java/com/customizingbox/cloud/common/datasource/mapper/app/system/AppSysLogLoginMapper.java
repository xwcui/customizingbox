package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLogLogin;

/**
 * 登录日志表
 */
public interface AppSysLogLoginMapper extends BaseMapper<AppSysLogLogin> {

}
