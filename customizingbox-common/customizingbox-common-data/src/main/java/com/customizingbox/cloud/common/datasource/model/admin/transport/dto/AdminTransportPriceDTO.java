package com.customizingbox.cloud.common.datasource.model.admin.transport.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@ApiModel(value = "订单运费")
@Data
public class AdminTransportPriceDTO {

    @ApiModelProperty(value = "运输方式id")
    private Long transportId;

    @ApiModelProperty(value = "物流属性id列表")
    private List<Long> logisticsId;

    @ApiModelProperty(value = "总体积/单位cm")
    private BigDecimal volume;

    @ApiModelProperty(value = "总重")
    private BigDecimal weight;

    @ApiModelProperty(value = "始发地id")
    private Long beginCountryId;

    @ApiModelProperty(value = "目的地id")
    private Long endCountryId;

    @ApiModelProperty(value = "当前时间")
    private LocalDateTime nowDate;

    @ApiModelProperty(value = "物流属性id列表(用于数据库返回, 逗号分割)")
    private String logisticsIds;



}
