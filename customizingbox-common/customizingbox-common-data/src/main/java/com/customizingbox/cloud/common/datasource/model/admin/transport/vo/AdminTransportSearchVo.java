package com.customizingbox.cloud.common.datasource.model.admin.transport.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 运输方式分页查询Vo
 */
@Data
public class AdminTransportSearchVo {

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输模板id")
    private Long logisticsId;

    @ApiModelProperty(value = "运输方式名称")
    private String name;

    @ApiModelProperty(value = "重量计算方式")
    private Integer completeMode;

    @ApiModelProperty(value = "追踪类型 0 真实追踪号 1   物流商单号")
    private Integer traceType;

    @ApiModelProperty(value = "状态. 1: 启用; 0: 禁用")
    private Integer status;


}
