package com.customizingbox.cloud.common.datasource.model.admin.product.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "产品列表vo")
public class ProductPageVo {

    @ApiModelProperty(value = "产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * spu
     */
    @ApiModelProperty(value = "spu")
    private String spu;

    /**
     * 商品英文标题
     */
    @ApiModelProperty(value = "商品英文标题")
    private String titleEn;
    /**
     * 产品报关英文名
     */
    @ApiModelProperty(value = "产品报关英文名")
    private String nameEn;
    /**
     * 产品报关中文名
     */
    @ApiModelProperty(value = "产品报关中文名")
    private String nameCn;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String imageUrl;

    /**
     * 开发人员
     */
    @ApiModelProperty(value = "开发人员")
    private String author;

    /**
     * 供应商名称
     */
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(value = "删除标记（0：显示；1：隐藏）")
    private Integer delFlag;

    @ApiModelProperty(value = "是否报过价 ture 报过价  false 未报价")
    private Boolean quoteFlag;

    /**
     * 重量
     */
    @ApiModelProperty(value = "重量")
    private BigDecimal weight;
    /**
     * 长
     */
    @ApiModelProperty(value = "长")
    private BigDecimal length;
    /**
     * 宽
     */
    @ApiModelProperty(value = "宽")
    private BigDecimal width;
    /**
     * 高
     */
    @ApiModelProperty(value = "高")
    private BigDecimal height;
    /**
     * 定制产品; 1: 定制; 2: 非定制
     */
    @ApiModelProperty(value = "定制产品; 1: 定制; 2: 非定制")
    private Integer customMode;

    @ApiModelProperty(value = "上传赛和状态; 1: 已上传, 2: 未上传, 3: 部分上传")
    private Integer uploadShStatus;

    @ApiModelProperty(value = "上传赛和时间")
    private LocalDateTime uploadShTime;
}
