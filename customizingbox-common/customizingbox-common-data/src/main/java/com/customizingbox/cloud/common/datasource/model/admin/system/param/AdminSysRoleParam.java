package com.customizingbox.cloud.common.datasource.model.admin.system.param;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 角色新增参数
 * </p>
 */
@Data
@ApiModel(description = "角色新增参数")
public class AdminSysRoleParam {

	@ApiModelProperty(value = "PK")
	private String id;

	@ApiModelProperty(value = "角色名称")
	@NotNull(message = "角色名称 不能为空")
	private String roleName;

	@ApiModelProperty(value = "角色描述")
	@NotNull(message = "角色描述 不能为空")
	private String roleDesc;

	@ApiModelProperty(value = "菜单id集合(逗号分割)")
	private String menuIds;

}
