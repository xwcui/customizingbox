package com.customizingbox.cloud.common.datasource.model.app.order.source.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Null;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "app订单查询基础参数")
public class AppOrderBaseParam {

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "店铺id")
    private Long storeId;

    @ApiModelProperty(value = "开始时间")
    private String startTime;

    @ApiModelProperty(value = "结束时间")
    private String endTime;

}
