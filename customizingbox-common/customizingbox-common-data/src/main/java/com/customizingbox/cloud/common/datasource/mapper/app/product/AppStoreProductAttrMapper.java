package com.customizingbox.cloud.common.datasource.mapper.app.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductAttr;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-26 10:32:21
 */
public interface AppStoreProductAttrMapper extends BaseMapper<AppStoreProductAttr> {

}
