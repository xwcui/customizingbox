package com.customizingbox.cloud.common.datasource.mapper.app.order.panda;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.AdminOrderToSaiheSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.order.vo.OrderToSaihePageVO;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.OrderToSaiheVo;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaProcessingOrderParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaPaidOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaProcessingOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaToOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * panda订单
 *
 * @author Y
 * @date 2022-04-12 16:41:50
 */
public interface AppPandaOrderMapper extends BaseMapper<AppPandaOrder> {

    /**
     * 查询待付款列表页面(未付款, 未取消)
     * @param page
     * @return
     */
    IPage<AppPandaToOrderVO> queryToOrderPage(Page page,@Param(value = "usRate") BigDecimal usRate
            ,  @Param(value = "status") Integer status
            ,  @Param(value = "param") AppOrderBaseParam baseParam);

    /**
     * 查询待支付的订单列表
     * @param pandaOrderId
     * @param usRate
     * @return
     */
    List<AppPandaPaidOrderVO> queryPaidOrder(@Param(value = "pandaOrderId") Long pandaOrderId, @Param(value = "usRate") BigDecimal usRate);

    /**
     * 分页查询已支付订单列表
     * @param page
     * @param param
     * @return
     */
    IPage<AppPandaProcessingOrderVO> paidOrderList(Page page, @Param(value = "param") AppPandaProcessingOrderParam param);

    /**
     * 获取上传赛盒订单分页
     * @param page
     * @param query
     * @return
     */
    IPage<OrderToSaihePageVO> selectSaiheToOrderPage(@Param("page") Page page, @Param("searchVo") AdminOrderToSaiheSearchVo query);

    /**
     * 获取需要上传赛盒的订单数据
     * @return
     */
    List<OrderToSaiheVo> selectSaiheToOrderList();

}
