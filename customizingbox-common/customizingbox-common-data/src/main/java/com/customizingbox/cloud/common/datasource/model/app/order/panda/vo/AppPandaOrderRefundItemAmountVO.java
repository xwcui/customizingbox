package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "查询退款金额")
public class AppPandaOrderRefundItemAmountVO {

    @ApiModelProperty(value = "订单item")
    private Long orderItemId;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount = BigDecimal.ZERO;
}