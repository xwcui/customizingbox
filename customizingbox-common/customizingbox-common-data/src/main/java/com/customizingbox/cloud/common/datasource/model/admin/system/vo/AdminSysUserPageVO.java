package com.customizingbox.cloud.common.datasource.model.admin.system.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
public class AdminSysUserPageVO implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "角色名")
    private String roleName;

    @ApiModelProperty(value = "用户类型; 1: 客户经理, 2: 采购员,  3: (srm)工厂用户")
    private Integer type;

    @ApiModelProperty(value = "职位")
    private String job;

    @ApiModelProperty(value = "'0-正常，9-停用(锁定)")
    private String lockFlag;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
