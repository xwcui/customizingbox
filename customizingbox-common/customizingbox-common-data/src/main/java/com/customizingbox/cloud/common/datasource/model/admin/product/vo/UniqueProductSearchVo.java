package com.customizingbox.cloud.common.datasource.model.admin.product.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "唯一码产品查询")
public class UniqueProductSearchVo {

    /**
     * 潘达订单id
     */
    @ApiModelProperty(value = "潘达订单id")
    private Long pandaOrderId;

    /**
     * app用户id
     */
    @ApiModelProperty(value = "app用户id")
    private Long userId;

    /**
     * 唯一码
     */
    @ApiModelProperty(value = "OnlyId")
    private String onlyCode;

    /**
     * 赛盒库存上传状态
     */
    @ApiModelProperty(value = "赛盒库存上传状态  1: 已上传, 2: 未上传")
    private Integer uploadShStatus;


    /**
     * 产品上传赛盒状态
     */
    @ApiModelProperty(value = "赛盒上传状态  1: 已上传, 2: 未上传")
    private Integer bealShSku;

    /**
     * 赛盒sku
     */
    @ApiModelProperty(value = "赛盒sku")
    private String shSku;

    /**
     * spu
     */
    @ApiModelProperty(value = "spu")
    private String spu;


    /**
     * 采购状态
     */
    @ApiModelProperty(value = "采购状态 1: 待下单, 2: 已下单, 3: 供应商打回, 4:生产中，5:已发货，6:质检不通过，7:已入库，8:采购订单已取消")
    private Integer purchaseStatus;

}
