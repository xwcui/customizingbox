package com.customizingbox.cloud.common.datasource.mapper.app.order.shopify;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrder;

/**
 * shopify 订单表
 *
 * @author Y
 * @date 2022-03-30 14:19:55
 */
public interface AppShopifyOrderMapper extends BaseMapper<AppShopifyOrder> {

}
