package com.customizingbox.cloud.common.datasource.model.admin.setting.vo;

import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminSupplier;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * @author DC_Li
 * @date 2022/4/20 11:37
 */
@Data
@ApiModel(value = "供应商VO")
public class AdminSupplierRequestVo {
    @ApiModelProperty(value = "供应商id",required = true)
    @NotNull(message = "供应商id不能为空")
    private Long id;

    @ApiModelProperty(value = "供应商名称",required = true)
    @NotBlank(message = "供应商名称不能为空")
    private String name;

    @ApiModelProperty(value = "联系人",required = true)
    @NotBlank(message = "联系人不能为空")
    private String contacts;

    @ApiModelProperty(value = "登录手机号码",required = true)
    @NotBlank(message = "登录手机号码不能为空")
    private String phone;

    @ApiModelProperty(value = "登录密码",required = true)
    @NotBlank(message = "登录密码不能为空")
    private String loginPassword;

    @ApiModelProperty(value = "收款银行账户",required = true)
    @NotBlank(message = "收款银行账户不能为空")
    private String bankAccount;

    @ApiModelProperty(value = "收款银行信息",required = true)
    @NotBlank(message = "收款银行信息不能为空")
    private String bankInfo;

    @ApiModelProperty(value = "收款人",required = true)
    @NotBlank(message = "收款人不能为空")
    private String payee;

    @ApiModelProperty(value = "采购员,admin用户id",required = true)
    @NotNull(message = "采购员不能为空")
    private Long adminPurchaseUserId;

    public AdminSupplier toAdminSupplier(){
        AdminSupplier adminSupplier = new AdminSupplier();
        adminSupplier.setId(this.id);
        adminSupplier.setName(this.name);
        adminSupplier.setContacts(this.contacts);
        adminSupplier.setPhone(this.phone);
        adminSupplier.setBankAccount(this.bankAccount);
        adminSupplier.setBankInfo(this.bankInfo);
        adminSupplier.setPayee(this.payee);
        adminSupplier.setAdminPurchaseUserId(this.adminPurchaseUserId);
        return adminSupplier;
    }
}
