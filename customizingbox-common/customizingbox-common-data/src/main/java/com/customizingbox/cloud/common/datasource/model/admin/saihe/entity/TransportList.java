package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ApiTransport;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cjq on 2019/1/11.
 */
public class TransportList {


    /**
     * 运输方式
     */
    List<ApiTransport> ApiTransport = new ArrayList<>();

    @XmlElement(name="ApiTransport")
    public List<ApiTransport> getApiTransport() {
        return ApiTransport;
    }

    public void setApiTransport(List<ApiTransport> apiTransport) {
        ApiTransport = apiTransport;
    }
}
