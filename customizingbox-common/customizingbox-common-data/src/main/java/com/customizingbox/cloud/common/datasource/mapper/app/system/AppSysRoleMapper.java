package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRole;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface AppSysRoleMapper extends BaseMapper<AppSysRole> {

    /**
     * 通过用户ID，查询角色信息
     */
    List<String> listRoleIdsByUserId(String userId);
}
