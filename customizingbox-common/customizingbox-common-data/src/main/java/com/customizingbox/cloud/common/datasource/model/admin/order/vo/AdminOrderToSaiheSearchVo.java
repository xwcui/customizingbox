package com.customizingbox.cloud.common.datasource.model.admin.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Data
 * @ApiModel(value = "admin 订单上传赛盒页面查询vo")
 */
@Data
public class AdminOrderToSaiheSearchVo {

    @ApiModelProperty(value = "spu")
    private String spu;

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "赛盒订单号")
    private String shOrderCode;

    @ApiModelProperty(value = "客户id")
    private String appUserId;

    @ApiModelProperty(value = "客户邮箱")
    private String appUserEmail;

    @ApiModelProperty(value = "唯一码")
    private String onlyCode;

    @ApiModelProperty(value = "订单产品上传赛盒状态, 1: 已上传, 2 未上传, 3:部分上传")
    private Integer productUploudShStatus;

    @ApiModelProperty(value = "订单上传赛盒状态, 1: 已上传, 2 未上传")
    private Integer uploadShStatus;

    @ApiModelProperty(value = "管理员id 前端不用管 后端根据是否是管理员给set值")
    private String adminUserId;
}
