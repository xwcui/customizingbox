package com.customizingbox.cloud.common.datasource.model.app.product.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@TableName("app_store_product")
@ApiModel(description = "产品表")
public class AppStoreProductListVO {

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "商品图片")
    private String imgSrc;

    @ApiModelProperty(value = "最小售价")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "最大售价")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "变体数量")
    private Integer variantCnt;

    @ApiModelProperty(value = "已关联变体数量")
    private Integer variantRelevancyCnt = 0;

    /**
     * 商户系统创建时间
     */
    @ApiModelProperty(value = "商户系统创建时间")
    private LocalDateTime createdAt;


}
