package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysConfigEditor;

/**
 * 编辑器配置
 */
public interface AdminSysConfigEditorMapper extends BaseMapper<AdminSysConfigEditor> {

}
