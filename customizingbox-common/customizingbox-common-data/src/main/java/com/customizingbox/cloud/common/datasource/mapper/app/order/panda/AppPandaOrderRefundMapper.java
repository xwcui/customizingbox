package com.customizingbox.cloud.common.datasource.mapper.app.order.panda;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.FreightAndVatTotalDto;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderRefund;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderRefundParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.*;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单退款表
 *
 * @author Y
 * @date 2022-04-23 09:55:23
 */
public interface AppPandaOrderRefundMapper extends BaseMapper<AppPandaOrderRefund> {

    /**
     * 查询退款页需要的数据
     */
    AppPandaOrderRefundDetailVO apply(@Param(value = "orderId") Long orderId);

    /**
     * 查询退款item里需要的数据
     */
    List<AppPandaOrderRefundItemDetailVO> refundItemDetail(Long orderId);

    /**
     * 查询待退款订单总金额
     * @param orderId 订单id
     * @param status 退款状态. 1 申请中 4 已退款(同意退款) 5 拒绝退款  -1: 申请中和已退款
     * @return
     */
    AppPandaOrderRefundAmountVO queryOrderRefundAmount(Long orderId, int status);

    /**
     * 退款管理页面
     */
    IPage<AppPandaOrderRefundVO> refundPage(Page page, @Param(value = "param") AppPandaOrderRefundParam appPandaOrderRefundParam);

    /**
     * 退款审核item详情
     * @param orderId
     * @return
     */
    List<AppPandaOrderRefundItemDetailVO> checkRefundItemDetail(Long orderId);

    /**
     * 根据订单id和订单状态查询退款订单中vat和运费的价格
     * @param orderId
     * @param refundStatus
     * @return
     */
    AppPandaOrderRefundVatFreightVO refundVatAndFreight(Long orderId, Integer refundStatus);

    /**
     * 查询潘达订单的所有已申请运费退款和vat退款
     * @param pandaOrderId
     * @return
     */
    FreightAndVatTotalDto getAllFreightAndVatTotal(@Param("pandaOrderId") Long pandaOrderId);

    /**
     * 查询潘达订单的订单变体退款总和
     * @param refundIds 退款表id集合
     * @return
     */
    BigDecimal getItemAmountTotal(@Param("refundIds") List<Long> refundIds);
}
