package com.customizingbox.cloud.common.datasource.mapper.admin.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.ProductSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ProductStock;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-31 09:34:35
 */
public interface AdminStoreProductMapper extends BaseMapper<AdminStoreProduct> {

    /**
     * 分页
     * @param productSearchVo
     * @param page
     * @return
     */
    IPage<ProductPageVo> adminProductPage(@Param("searchVo") ProductSearchVo productSearchVo, @Param("page") Page page);

    /**
     * 获取上传赛盒 库存数量 所需要数据
     * @param onlyCodes
     * @return
     */
    List<ProductStock> getProcessUniqueProductStockNumberToSaiheList(@Param("onlyCodes") List<String> onlyCodes);

    /**
     * 判断产品库中是否有产品属于该供应商
     * @param supplierId
     * @return
     */
    AdminStoreProduct judgeSupplierHasProduct(@Param("supplierId") Long supplierId);
}
