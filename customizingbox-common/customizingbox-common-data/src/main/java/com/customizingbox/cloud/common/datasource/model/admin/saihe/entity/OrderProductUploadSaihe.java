package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import lombok.Data;

/**
 * 订单上传前先将订单中未上传的产品上传到赛盒
 */
@Data
public class OrderProductUploadSaihe {

}
