package com.customizingbox.cloud.common.datasource.model.admin.transport.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AdminLogisticsAttrDto {

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "中文名称")
    @NotBlank(message = "中文名称不能为空")
    private String nameCn;

    @ApiModelProperty(value = "英文名称")
    @NotBlank(message = "英文名称不能为空")
    private String nameEn;

    @ApiModelProperty(value = "描述")
    @NotBlank(message = "描述不能为空")
    private String description;

    @ApiModelProperty(value = "状态. 1: 启用; 2: 禁用")
    @NotNull(message = "状态不能为空")
    private Integer status;
}
