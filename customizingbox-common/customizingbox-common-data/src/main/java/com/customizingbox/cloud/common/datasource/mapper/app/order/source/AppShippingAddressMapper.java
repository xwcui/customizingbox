package com.customizingbox.cloud.common.datasource.mapper.app.order.source;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShippingAddress;

/**
 * 用户地址表
 *
 * @author Y
 * @date 2022-03-30 13:48:15
 */
public interface AppShippingAddressMapper extends BaseMapper<AppShippingAddress> {

}
