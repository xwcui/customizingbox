package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysOrgan;

/**
 * <p>
 * 机构管理 Mapper 接口
 * </p>
 */
public interface AdminSysOrganMapper extends BaseMapper<AdminSysOrgan> {

}
