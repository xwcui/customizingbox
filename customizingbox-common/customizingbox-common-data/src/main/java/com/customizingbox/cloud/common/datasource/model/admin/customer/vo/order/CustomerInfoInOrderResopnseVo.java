package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 客户订单中的客户信息
 * @author DC_Li
 * @date 2022/4/20 15:52
 */
@Data
@ApiModel("客户订单中的客户信息")
public class CustomerInfoInOrderResopnseVo {
    @ApiModelProperty("客户名")
    private String  customerName;
    @ApiModelProperty("客户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long  appUserId;
    @ApiModelProperty("当前客户经理")
    private String  customerManagerNow;
    @ApiModelProperty("下单客户经理")
    private String  customerManagerBefore;
}
