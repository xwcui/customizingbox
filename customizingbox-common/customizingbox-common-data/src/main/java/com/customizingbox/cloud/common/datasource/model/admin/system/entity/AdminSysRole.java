package com.customizingbox.cloud.common.datasource.model.admin.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色表
 * </p>
 */
@Data
@ApiModel(description = "角色")
@EqualsAndHashCode(callSuper = true)
@TableName(value = "admin_sys_role")
public class AdminSysRole extends Model<AdminSysRole> {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@ApiModelProperty(value = "租户ID")
	private String tenantId;

	@ApiModelProperty(value = "角色名称")
	@NotNull(message = "角色名称 不能为空")
	private String roleName;

	@ApiModelProperty(value = "角色标识")
	@NotNull(message = "角色标识 不能为空")
	private String roleCode;

	@ApiModelProperty(value = "角色描述")
	@NotNull(message = "角色描述 不能为空")
	private String roleDesc;

	@ApiModelProperty(value = "数据权限")
	private Integer dsType = 2;

	@ApiModelProperty(value = "dsScope")
	private String dsScope;

	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	@ApiModelProperty(value = "更新时间")
	private LocalDateTime updateTime;

//	@TableLogic
	@ApiModelProperty(value = "删除标识")
	private String delFlag;

	@Override
	public Serializable pkVal() {
		return this.id;
	}

}
