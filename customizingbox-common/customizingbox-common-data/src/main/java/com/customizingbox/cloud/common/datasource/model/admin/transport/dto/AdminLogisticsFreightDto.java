package com.customizingbox.cloud.common.datasource.model.admin.transport.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * <p>
 * 物流费用
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Data
@ApiModel(value = "物流费用")
public class AdminLogisticsFreightDto implements Serializable {


    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输方式id")
    @NotBlank(message = "运输方式 不能为空")
    private Long transportId;

    @ApiModelProperty(value = "运输方式名称")
    @NotBlank(message = "运输方式 不能为空")
    private String transportName;

    @ApiModelProperty(value = "始发地id")
    @NotNull(message = "始发地id 不能为空")
    private Long beginCountryId;

    @ApiModelProperty(value = "目的地id")
    @NotNull(message = "目的地id 不能为空")
    private Long endCountryId;

    @ApiModelProperty(value = "折扣")
    @NotNull(message = "折扣 不能为空")
    private BigDecimal discount;

    @ApiModelProperty(value = "区间开始重量 单位克")
    @NotNull(message = "区间开始重量 不能为空")
    private Integer beginWeight;

    @ApiModelProperty(value = "区间结束重量 单位克")
    @NotNull(message = "区间结束重量 不能为空")
    private Integer endWeight;

    @ApiModelProperty(value = "固定费/挂号费")
    @NotNull(message = "固定费/挂号费 不能为空")
    private BigDecimal fixedPrice;

    @ApiModelProperty(value = "克重费")
    @NotNull(message = "克重费 不能为空")
    private BigDecimal gramPrice;

    @ApiModelProperty(value = "最早到达时间(天)")
    @NotNull(message = "最早到达时间 不能为空")
    private Integer earliestDay;

    @ApiModelProperty(value = "最晚到达时间(天)")
    @NotNull(message = "最晚到达时间 不能为空")
    private Integer longestDay;

    @ApiModelProperty(value = "备注")
    private String mark;

    @ApiModelProperty(value = "启动时间")
    private LocalDateTime enableTime;

    @ApiModelProperty(value = "启用结束时间")
    private LocalDateTime notEnableTime;
}