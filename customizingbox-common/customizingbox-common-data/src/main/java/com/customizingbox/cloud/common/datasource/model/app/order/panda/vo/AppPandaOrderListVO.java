package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderItemVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 客户向panda下单后返回刚下单的
 *
 */
@Data
public class AppPandaOrderListVO extends Model<AppPandaOrderListVO> {

     @ApiModelProperty(value = "订单id")
     private Long id;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "商铺名称")
    private String storeName;

    @ApiModelProperty(value = "订单号, 用来给用户展示")
    private String orderNo;

    @ApiModelProperty(value = "支付状态(1: 未支付 2: 已支付)")
    private Integer payStatus;

    @ApiModelProperty(value = "订单发货状态（1：未发货；2：部分发货；3：全部发货; 4: 已收货）")
    private Integer freightStatus;

    @ApiModelProperty(value = "收货地址")
    private String address;

    @ApiModelProperty(value = "收件人")
    private String username;

    @ApiModelProperty(value = "备注")
    private String mark;

    @ApiModelProperty(value = "店铺运输方式")
    private String transport;

    @ApiModelProperty(value = "店铺运费")
    private String freightPrice;

    @ApiModelProperty(value = "变体列表")
    private List<AppQuotationOrderItemVO> orderItems = new ArrayList<>();


    @ApiModelProperty(value = "变体图片")
    @JsonIgnore
    private String appImgSrc;

    @ApiModelProperty(value = "变体属性值")
    @JsonIgnore
    private String appAttrValues;

    @ApiModelProperty(value = "变体数量")
    @JsonIgnore
    private String appFulfillableQuantity;

    @ApiModelProperty(value = "关联(有值就说明已经关联了)")
    @JsonIgnore
    private String adminVariantId;

    @ApiModelProperty(value = "变体图片")
    @JsonIgnore
    private String adminImgSrc;

    @ApiModelProperty(value = "变体属性值")
    @JsonIgnore
    private String adminAttrValues;

    @ApiModelProperty(value = "给客户的价格")
    @JsonIgnore
    private BigDecimal cost;

}
