package com.customizingbox.cloud.common.datasource.model.srm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 提交发货的数据
 * @author DC_Li
 * @date 2022/5/3 11:20
 */
@Data
@ApiModel("提交发货")
public class CommitDeliveryRequestVo {
    @ApiModelProperty("唯一码集合")
    private List<String> onlyCodes;

    @ApiModelProperty("物流商名称")
    private String logisticName;

    @ApiModelProperty("物流单号")
    private String trackingCode;
}
