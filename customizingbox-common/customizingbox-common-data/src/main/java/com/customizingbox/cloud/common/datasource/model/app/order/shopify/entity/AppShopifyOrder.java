package com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShippingAddress;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShopifyCustomer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * shopify 订单表
 *
 * @author Y
 * @date 2022-03-30 14:19:55
 */
@Data
@TableName("app_shopify_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "shopify 订单表")
public class AppShopifyOrder extends Model<AppShopifyOrder> {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "")
    private Long id;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String adminGraphqlApiId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long appId;
    /**
     *
     */
    @ApiModelProperty(value = "")
    private String browserIp;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer buyerAcceptsMarketing;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String cancelReason;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String cancelledAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String cartToken;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long checkoutId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String checkoutToken;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String clientDetails;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String closedAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer confirmed;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String contactEmail;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime createdAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currency;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal currentSubtotalPrice;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currentSubtotalPriceSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal currentTotalDiscounts;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currentTotalDiscountsSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currentTotalDutiesSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal currentTotalPrice;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currentTotalPriceSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currentTotalTax;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currentTotalTaxSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String customerLocale;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String deviceId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String discountCodes;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String email;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer estimatedTaxes;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String financialStatus;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String fulfillmentStatus;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String gateway;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String landingSite;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String landingSiteRef;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String locationId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String name;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String note;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String noteAttributes;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer number;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long orderNumber;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String orderStatusUrl;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String originalTotalDutiesSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String paymentGatewayNames;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String phone;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String presentmentCurrency;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime processedAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String processingMethod;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String reference;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String referringSite;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String sourceIdentifier;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String sourceName;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String sourceUrl;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal subtotalPrice;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String subtotalPriceSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String tags;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String taxLines;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer taxesIncluded;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer test;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String token;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalDiscounts;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalDiscountsSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalLineItemsPrice;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalLineItemsPriceSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalOutstanding;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalPrice;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalPriceSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalPriceUsd;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalShippingPriceSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalTax;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalTaxSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalTipReceived;
    /**
     * 总重量
     */
    @ApiModelProperty(value = "总重量")
    private Long totalWeight;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime updatedAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long userId;
    /**
     * 物流信息
     */
    @ApiModelProperty(value = "物流信息")
    private String billingAddress;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String discountApplications;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String fulfillments;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String paymentTerms;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String shippingLines;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(exist=false)
    @ApiModelProperty(value = "订单item表")
    private List<AppShopifyOrderItem> lineItems;

    @TableField(exist=false)
    @ApiModelProperty(value = "退款属性")
    private List<AppShopifyOrderRefund> refunds;

    @TableField(exist=false)
    @ApiModelProperty(value = "收货地址")
    private AppShippingAddress shippingAddress;

    @TableField(exist=false)
    @ApiModelProperty(value = "收货地址Id")
    private Long shippingAddressId;

    @TableField(exist=false)
    @ApiModelProperty(value = "收货地址")
    private AppShopifyCustomer customer;

    @TableField(exist=false)
    @ApiModelProperty(value = "店铺名称")
    private String shopifyShopName;


}
