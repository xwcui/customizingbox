package com.customizingbox.cloud.common.datasource.model.srm.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.converters.string.StringImageConverter;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URL;

/**
 * 导出生产资料
 *
 * @author DC_Li
 * @date 2022/4/26 17:26
 */
@Data
@ColumnWidth(value = 20)
@ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER,verticalAlignment = VerticalAlignmentEnum.CENTER)
public class ProductionMeans implements Serializable {
    @ExcelProperty(value ="唯一码")
    @ColumnWidth(20)
    private String onlyCode;
    // @ContentLoopMerge
    @ExcelProperty(value = "产品变体图地址")
    private String variantImgUrl;

    @ExcelProperty(value = "产品变体图")
    private URL variantImg;

    @ExcelProperty(value = "产品名称")
    private String productName;

    @ExcelProperty(value = "SPU")
    @ColumnWidth(10)
    private String spu;

    @ExcelProperty(value ="SKU")
    @ColumnWidth(10)
    private String sku;

    @ExcelIgnore
    private String attrValues;

    @ExcelProperty(value ="属性值1")
    @ColumnWidth(12)
    private String attrValue1;

    @ExcelProperty(value ="属性值2")
    @ColumnWidth(12)
    private String attrValue2;

    @ExcelProperty(value ="属性值3")
    @ColumnWidth(12)
    private String attrValue3;

    @ExcelProperty(value ="自定义信息")
    private String custom;

    @ExcelProperty(value ="数量")
    @ColumnWidth(10)
    private Integer quantity;

    @ExcelProperty(value ="下单采购价")
    @NumberFormat("0.00")
    private BigDecimal purchasePrice;


    /*@Excel(name = "产品变体图地址", orderNum = "0", width = 20,height = 20)
    private String variantImgUrl;

    @Excel(name = "产品变体图", orderNum = "0", width = 20,height = 20,type = 2)
    private String variantImg;

    @Excel(name = "产品名称", orderNum = "1", width = 20, mergeVertical = true)
    private String productName;

    @Excel(name = "SPU", orderNum = "2", needMerge = true)
    private String spu;

    @Excel(name = "SKU", orderNum = "3", needMerge = true)
    private String sku;

    @Excel(name = "唯一码", orderNum = "4", width = 20, needMerge = true)
    private String onlyCode;

    private String attrValues;

    @Excel(name = "属性值1", orderNum = "5", width = 20, needMerge = true)
    private String attrValue1;

    @Excel(name = "属性值2", orderNum = "6", width = 20, needMerge = true)
    private String attrValue2;

    @Excel(name = "属性值3", orderNum = "7", width = 20, needMerge = true)
    private String attrValue3;

    @Excel(name = "自定义信息", orderNum = "8", width = 20,type = 1, needMerge = true)
    private String custom;

    @Excel(name = "附件图片", orderNum = "9",type = 1, needMerge = true)
    private String appendix;

    @Excel(name = "数量", orderNum = "10", needMerge = true)
    private Integer quantity;

    @Excel(name = "下单采购价", orderNum = "11", width = 20, needMerge = true)
    private BigDecimal purchasePrice;*/
}
