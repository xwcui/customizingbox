package com.customizingbox.cloud.common.datasource.model.app.transaction.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 交易记录表
 *
 * @author Y
 * @date 2022-04-21 16:09:21
 */
@Data
@TableName("app_sys_user_transaction_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "交易记录表")
public class AppSysUserTransactionRecord extends Model<AppSysUserTransactionRecord> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Long userId;
    /**
     * 交易类型(1: 充值; 2: 支出)
     */
    @ApiModelProperty(value = "交易类型(1: 充值; 2: 支出)")
    private Integer type;
    /**
     * 来源类型, 1: 订单支付, 2: 订单退款,  3: 充值
     */
    @ApiModelProperty(value = "来源类型, 1: 充值, 2: 订单支付, 3: 订单退款")
    private Integer sourceType;

    @ApiModelProperty(value = "交易金额")
    private BigDecimal money;
    /**
     * 交易后余额
     */
    @ApiModelProperty(value = "交易前余额")
    private BigDecimal firstBalance;

    @ApiModelProperty(value = "交易后余额")
    private BigDecimal lastBalance;

    /**
     * 流水号(用于充值时第三方支付的交易单号)
     */
    @ApiModelProperty(value = "流水号(用于充值时第三方支付的交易单号)")
    private String transaction;
    /**
     * 如果是支出, 则保存订单id
     */
    @ApiModelProperty(value = "如果是支出, 则保存订单id")
    private Long orderId;

    @ApiModelProperty(value = "如果是支出, 则保存订单id")
    private Long refundOrderId;
    /**
     * 支付id
     */
    @ApiModelProperty(value = "支付id")
    private String paymentId;
    /**
     * 店铺id (订单支付时, 读取订单店铺id)
     */
    @ApiModelProperty(value = "店铺id (订单支付时, 读取订单店铺id)")
    private Long storeId;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String mark;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

}
