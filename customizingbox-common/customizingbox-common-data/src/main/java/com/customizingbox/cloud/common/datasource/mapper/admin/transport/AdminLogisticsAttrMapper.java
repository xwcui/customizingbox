package com.customizingbox.cloud.common.datasource.mapper.admin.transport;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminLogisticsAttr;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminLogisticsAttrSearchVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 物流属性表 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminLogisticsAttrMapper extends BaseMapper<AdminLogisticsAttr> {

    IPage<AdminLogisticsAttrPageVo> pageAll(Page page, @Param("searchVo") AdminLogisticsAttrSearchVo searchVo);
}
