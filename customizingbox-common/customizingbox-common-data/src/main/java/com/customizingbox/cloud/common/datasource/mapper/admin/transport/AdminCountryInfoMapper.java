package com.customizingbox.cloud.common.datasource.mapper.admin.transport;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminCountryInfo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminCountryInfoSearchVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 国家和地区表信息 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-14
 */
public interface AdminCountryInfoMapper extends BaseMapper<AdminCountryInfo> {

    IPage<AdminCountryInfo> pageAll(Page page, @Param("searchVo") AdminCountryInfoSearchVo searchVo);

    /**
     * 根据收货地址id,查询目的地id
     * @return
     */
    Long queryEndCountryByAddrId(@Param("addressId") Long addressId);
}
