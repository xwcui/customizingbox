package com.customizingbox.cloud.common.datasource.model.app.order.source.dto;

import lombok.Data;

/**
 *
 */
@Data
public class OrderShowStatusDTO {

    /**
     * 订单产品映射状态
     */
    private Integer relevancyStatus = 3;

    /**
     * 订单运输匹配状态
     */
    private Integer transportStatus;
}
