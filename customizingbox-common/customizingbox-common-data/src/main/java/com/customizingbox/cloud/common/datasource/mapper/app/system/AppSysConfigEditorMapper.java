package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysConfigEditor;

/**
 * 编辑器配置
 */
public interface AppSysConfigEditorMapper extends BaseMapper<AppSysConfigEditor> {

}
