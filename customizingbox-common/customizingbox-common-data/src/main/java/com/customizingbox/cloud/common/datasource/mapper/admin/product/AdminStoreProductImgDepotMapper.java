package com.customizingbox.cloud.common.datasource.mapper.admin.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-31 09:34:40
 */
public interface AdminStoreProductImgDepotMapper extends BaseMapper<AdminStoreProductImgDepot> {


    int delImageByIds(@Param("ids")List<Long> oldImgIds);
}
