package com.customizingbox.cloud.common.datasource.model.srm.vo;

import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.PurchaseProductVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/5/3 13:20
 */
@ApiModel("不良品列表响应数据")
@Data
@EqualsAndHashCode(callSuper = true)
public class DefectiveProductVo extends PurchaseProductVo {
    @ApiModelProperty("生产批次")
    private String productionBatch;

    @ApiModelProperty("发货批次")
    private String deliveryBatch;

    @ApiModelProperty("发货时间")
    private String deliveryTime;

    @ApiModelProperty("物流商")
    private String deliveryMethod;

    @ApiModelProperty("物流单号")
    private String deliveryCode;

    @ApiModelProperty("不良品数")
    private Integer rejectsQuantity;

    @ApiModelProperty("遗失数")
    private Integer loseQuantity;

    @ApiModelProperty("质检不通过备注")
    private String qualityMsg;

    @ApiModelProperty("质检不通过时间")
    private LocalDateTime rejectQualityTime;

    @ApiModelProperty("供应商是否重发 0：未重发，1：已重发")
    private Integer supplierResend;

    @ApiModelProperty("重发物流方式")
    private String supplierResendMethod;

    @ApiModelProperty("重发物流单号")
    private String supplierResendCode;

    @ApiModelProperty("重发时间")
    private LocalDateTime supplierResendTime;

}


