package com.customizingbox.cloud.common.datasource.mapper.admin.customer;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.customer.entity.AdminAppRelevancyRecord;

/**
 * <p>
 * 客户经理认领/关联 店铺用户记录表 Mapper 接口
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-14
 */
public interface AdminAppRelevancyRecordMapper extends BaseMapper<AdminAppRelevancyRecord> {

}
