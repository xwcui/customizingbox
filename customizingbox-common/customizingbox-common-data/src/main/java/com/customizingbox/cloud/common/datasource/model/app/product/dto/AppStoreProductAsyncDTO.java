package com.customizingbox.cloud.common.datasource.model.app.product.dto;

import com.baomidou.mybatisplus.annotation.*;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductAttr;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductImgDepot;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-26 10:32:23
 */
@Data
@ApiModel(description = "产品表")
public class AppStoreProductAsyncDTO {

    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 原平台产品id
     */
    @ApiModelProperty(value = "原平台产品id")
    private Long sourceProductId;
    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private Long storeId;
    /**
     * 产品描述id
     */
    @ApiModelProperty(value = "产品描述id")
    private Long descriptionId;
    /**
     * 平台类型. 1: shopify
     */
    @ApiModelProperty(value = "平台类型. 1: shopify  ")
    private String platformType;
    /**
     * 标题
     */
    @ApiModelProperty(value = "标题")
    private String title;
    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "产品来源店铺名称(可能是第三方)")
    private String vendor;

    @ApiModelProperty(value = "店铺名称")
    private String shopifyShopName;
    /**
     * 产品类型
     */
    @ApiModelProperty(value = "产品类型")
    private String productType;
    /**
     * 状态.
     */
    @ApiModelProperty(value = "")
    private String status;
    /**
     *
     */
    @ApiModelProperty(value = "")
    private String tags;
    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    private LocalDateTime updateTime;
    /**
     * 商户系统创建时间
     */
    @ApiModelProperty(value = "商户系统创建时间")
    private LocalDateTime createdAt;
    /**
     * 商户系统修改时间
     */
    @ApiModelProperty(value = "商户系统修改时间")
    private LocalDateTime updatedAt;
    /**
     * 商户系统创建时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统创建时间(北京时间)")
    private LocalDateTime bjCreatedAt;
    /**
     * 商户系统修改时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统修改时间(北京时间)")
    private LocalDateTime bjUpdatedAt;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    private String delFlag;

    @ApiModelProperty(value = "产品描述")
    @TableField(exist = false)
    private String bodyHtml;

    @TableField(exist = false)
    @ApiModelProperty(value = "变体")
    private List<AppStoreProductVariant> variants;

    @TableField(exist = false)
    @ApiModelProperty(value = "options")
    private List<AppStoreProductAttr> options;

    @TableField(exist = false)
    @ApiModelProperty(value = "图片")
    private List<AppStoreProductImgDepot> images;

}
