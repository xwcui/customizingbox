package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;

/**
 * 产品图片(最多9张)
 */
public class ApiImportProductImage {

    /**
     * 是否封面（每个SKU只能由一张封面）
     */
    Integer isCover;

    /**
     * 图片URL
     */
    String originalImageUrl;

    /**
     * 排序
     */
    Integer sortBy;

    @XmlElement(name="IsCover")
    public Integer getIsCover() {
        return isCover;
    }

    public void setIsCover(Integer isCover) {
        this.isCover = isCover;
    }

    @XmlElement(name="OriginalImageUrl")
    public String getOriginalImageUrl() {
        return originalImageUrl;
    }

    public void setOriginalImageUrl(String originalImageUrl) {
        this.originalImageUrl = originalImageUrl;
    }

    @XmlElement(name="SortBy")
    public Integer getSortBy() {
        return sortBy;
    }

    public void setSortBy(Integer sortBy) {
        this.sortBy = sortBy;
    }
}
