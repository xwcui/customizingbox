package com.customizingbox.cloud.common.datasource.model.app.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * shopify 产品详情表
 *
 * @author Y
 * @date 2022-03-26 18:20:38
 */
@Data
@TableName("app_store_product_description")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "shopify 产品详情表")
public class AppStoreProductDescription extends Model<AppStoreProductDescription> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "商户类型")
    private Integer platformType;
    /**
     * 产品详情
     */
    @ApiModelProperty(value = "产品详情")
    private String bodyHtml;
    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标记")
    @TableLogic
    private String delFlag;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
