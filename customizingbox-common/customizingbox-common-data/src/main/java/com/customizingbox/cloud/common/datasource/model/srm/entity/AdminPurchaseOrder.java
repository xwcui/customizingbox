package com.customizingbox.cloud.common.datasource.model.srm.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * <p>
 * 采购订单表
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_purchase_order")
@ApiModel(value = "采购订单表")
public class AdminPurchaseOrder extends Model<AdminPurchaseOrder> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "产品变体id")
    private Long variantId;

    @ApiModelProperty(value = "唯一码")
    private String onlyCode;

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "订单itemid")
    private Long orderItemId;

    @ApiModelProperty(value = "属性值")
    private String attrValues;

    @ApiModelProperty(value = "客户自定义商品信息")
    private String properties;

    @ApiModelProperty(value = "产品图, 多图用json数据格式保存")
    private String imgSrc;

    @ApiModelProperty(value = "采购数量")
    private Integer quantity;

    @ApiModelProperty(value = "采购价(就是我们的成本价)")
    private BigDecimal costPrice;

    @ApiModelProperty(value = "采购总价")
    private BigDecimal totalCostPrice;

    @ApiModelProperty(value = "采购状态 1: 待下单, 2: 已下单, 3: 供应商打回, 4:生产中，5:已发货，6:质检不通过，7:已入库，8:订单已取消")
    private Integer status;

    @ApiModelProperty(value = "采购员(admin用户id)")
    private Long adminPurchaseUserId;

    @ApiModelProperty(value = "供应商id")
    private Long supplierId;

    @ApiModelProperty(value = "下单时间")
    private LocalDateTime orderTime;

    @ApiModelProperty(value = "生产时间")
    private LocalDateTime productionTime;

    @ApiModelProperty(value = "打回原因")
    private String refusedMsg;

    @ApiModelProperty(value = "质检备注")
    private String qualityMsg;

    @ApiModelProperty(value = "生产批次")
    private String productionBatch;

    @ApiModelProperty(value = "发货批次")
    private String deliveryBatch;

    @ApiModelProperty(value = "发货物流商")
    private String deliveryMethod;

    @ApiModelProperty(value = "物流单号")
    private String deliveryCode;

    @ApiModelProperty(value = "不良品数量")
    private Integer rejectsQuantity;

    @ApiModelProperty(value = "良品数量")
    private Integer acceptQuantity;

    @ApiModelProperty(value = "遗失数量")
    private Integer loseQuantity;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建用户")
    private Long createId;
}
