package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.ApiGetTransportResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by cjq on 2018/12/4.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "soap12:Body")
public class RequestBody {


    /**
     * 请求运输方式参数实体类
     */
    @XmlElement(name="GetTransportList")
    ApiGetTransportRequest apiGetTransportRequest;

    /**
     * 返回运输方式结果实体类
     */
    @XmlElement(name="GetTransportListResponse")
    ApiGetTransportResponse apiGetTransportResponse;


    @XmlElement(name="UpLoadOrderV2")
    ApiUploadOrderRequest apiUploadOrderRequest;


    public ApiGetTransportRequest getApiGetTransportRequest() {
        return apiGetTransportRequest;
    }

    public void setApiGetTransportRequest(ApiGetTransportRequest apiGetTransportRequest) {
        this.apiGetTransportRequest = apiGetTransportRequest;
    }

    public ApiGetTransportResponse getApiGetTransportResponse() {
        return apiGetTransportResponse;
    }

    public void setApiGetTransportResponse(ApiGetTransportResponse apiGetTransportResponse) {
        this.apiGetTransportResponse = apiGetTransportResponse;
    }

    public ApiUploadOrderRequest getApiUploadOrderRequest() {
        return apiUploadOrderRequest;
    }

    public void setApiUploadOrderRequest(ApiUploadOrderRequest apiUploadOrderRequest) {
        this.apiUploadOrderRequest = apiUploadOrderRequest;
    }
}
