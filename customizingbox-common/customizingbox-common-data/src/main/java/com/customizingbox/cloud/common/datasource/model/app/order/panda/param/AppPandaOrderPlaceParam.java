package com.customizingbox.cloud.common.datasource.model.app.order.panda.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(description = "接收下单之前的参数")
@Data
public class AppPandaOrderPlaceParam {

    @ApiModelProperty(value = "订单id列表")
    @NotNull(message = "订单id不能为空")
    private List<Long> orderId;
}
