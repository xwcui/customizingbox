package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/15 10:07
 */
@Data
@ApiModel("客户信息")
public class AdminCustomerResponseVo {
    @ApiModelProperty("客户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appUserId;

    @ApiModelProperty("客户名称")
    private String username;

    @ApiModelProperty("客户邮箱")
    private String email;

    @ApiModelProperty("注册时ip国家")
    private String country;

    @ApiModelProperty("注册日期")
    private String createTime;

    @ApiModelProperty("账户余额")
    private String balance;

    @ApiModelProperty("客户经理")
    private String adminUserName;
}
