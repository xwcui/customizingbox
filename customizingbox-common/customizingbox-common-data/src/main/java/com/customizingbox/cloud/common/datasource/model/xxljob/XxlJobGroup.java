package com.customizingbox.cloud.common.datasource.model.xxljob;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Data
public class XxlJobGroup {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String appname;
    private String title;
    private int addressType;
    private String addressList;
    private Date updateTime;


    private List<String> registryList;

    public List<String> getRegistryList() {
        if (addressList != null && addressList.trim().length() > 0) {
            registryList = new ArrayList<>(Arrays.asList(addressList.split(",")));
        }
        return registryList;
    }

}
