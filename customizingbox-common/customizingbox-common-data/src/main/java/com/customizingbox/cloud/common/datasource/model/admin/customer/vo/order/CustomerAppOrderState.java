package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/20 16:12
 */
@Data
@ApiModel("app状态")
public class CustomerAppOrderState {
    @ApiModelProperty("订单状态 1：正常 2：取消")
    private String orderState;

    @ApiModelProperty("支付状态 1：未支付 2：已支付")
    private String payState;

    @ApiModelProperty("发货状态 1：未发货 2：部分发货 3：全部发货")
    private String shippingState;

    @ApiModelProperty("退款状态 0：无退款 3：部分退款 4：全部退款")
    private String refundState;
}
