package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysMenu;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppMenuVO;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 */
public interface AppSysMenuMapper extends BaseMapper<AppSysMenu> {

    /**
     * 通过角色编号查询菜单
     *
     * @param roleId 角色ID
     */
    List<AppMenuVO> listMenusByRoleId(String roleId);

    /**
     * 通过角色ID查询权限
     *
     * @param roleIds Ids
     */
    List<String> listPermissionsByRoleIds(String roleIds);
}
