package com.customizingbox.cloud.common.datasource.mapper.app.order.source;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * panda 订单item表
 *
 * @author Y
 * @date 2022-03-30 13:48:16
 */
public interface AppOrderItemMapper extends BaseMapper<AppOrderItem> {

    /**
     * 根据订单id查询item列表
     * @param orderId
     * @param usRate 美元汇率
     * @return
     */
    List<AppQuotationOrderVO> queryByOrderId(@Param(value = "orderId") String orderId, @Param(value = "usRate") BigDecimal usRate);

//    /**
//     * 修改订单中产品映射关系(未支付)
//     * @param storeId 商户店铺id
//     * @param sourceProductId 商户产品id
//     * @param sourceVariantId 商户产品变体id
//     * @param adminProductId admin 产品id
//     * @param adminVariantId admin 产品变体id
//     */
//    void updateOrderItemProductRelevancy(@Param(value = "storeId") Long storeId
//            , @Param(value = "sourceProductId") Long sourceProductId
//            , @Param(value = "sourceVariantId") Long sourceVariantId
//            , @Param(value = "adminProductId") Long adminProductId
//            , @Param(value = "adminVariantId") Long adminVariantId);

}
