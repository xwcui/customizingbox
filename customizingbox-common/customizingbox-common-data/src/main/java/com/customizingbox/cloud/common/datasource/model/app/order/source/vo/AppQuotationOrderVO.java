package com.customizingbox.cloud.common.datasource.model.app.order.source.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 订单列表关联变体
 */
@Data
@ApiModel(description = "订单表")
public class AppQuotationOrderVO {

    @ApiModelProperty(value = "app订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "商铺名称")
    private String storeName;

    @ApiModelProperty(value = "店铺订单id")
    private String sourceId;

    @ApiModelProperty(value = "支付状态")
    private String financialStatus;

    @ApiModelProperty(value = "订单发货状态（1：未发货；2：部分发货；3：全部发货; 4: 已收货）")
    private String freightStatus;

    @ApiModelProperty(value = "收货地址")
    private String address;

    @ApiModelProperty(value = "收件人")
    private String username;

    @ApiModelProperty(value = "店铺订单备注")
    private String sourceMark;

    @ApiModelProperty(value = "店铺运输方式")
    private String transport;

    @ApiModelProperty(value = "店铺运费")
    private String freightPrice;

    @ApiModelProperty(value = "和admin产品关联状态; 1: 全部关联 2: 部分关联 3: 全部未关联")
    private Integer relevanceStatus = 3;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "变体列表")
    private List<AppQuotationOrderItemVO> orderItems = new ArrayList<>();


    @ApiModelProperty(value = "变体图片")
    @JsonIgnore
    private String appImgSrc;

    @ApiModelProperty(value = "变体属性值")
    @JsonIgnore
    private String appAttrValues;

    @ApiModelProperty(value = "变体数量")
    @JsonIgnore
    private String appFulfillableQuantity;

    @ApiModelProperty(value = "关联(有值就说明已经关联了)")
    @JsonIgnore
    private String adminVariantId;

    @ApiModelProperty(value = "admin变体图片")
    @JsonIgnore
    private String adminImgSrc;

    @ApiModelProperty(value = "admin变体属性值")
    @JsonIgnore
    private String adminAttrValues;

    @ApiModelProperty(value = "给客户的价格")
    @JsonIgnore
    private BigDecimal quotePrice;

}
