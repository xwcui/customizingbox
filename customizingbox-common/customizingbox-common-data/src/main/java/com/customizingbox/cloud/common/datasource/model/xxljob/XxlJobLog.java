package com.customizingbox.cloud.common.datasource.model.xxljob;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class XxlJobLog {

	@TableId(type = IdType.AUTO)
	private Long id;

	private Integer jobGroup;
	private Integer jobId;

	private String executorAddress;
	private String executorHandler;
	private String executorParam;
	private String executorShardingParam;
	private int executorFailRetryCount;

	private Date triggerTime;
	private int triggerCode;
	private String triggerMsg;

	private Date handleTime;
	private int handleCode;
	private String handleMsg;

	private int alarmStatus;


}
