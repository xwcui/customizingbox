package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/4/23 13:55
 */
@Data
@ApiModel("客户退款信息")
public class UserRefundResponseVo {
    @ApiModelProperty("退款单号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long refundId;

    @ApiModelProperty("app订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pandaOrderId;

    @ApiModelProperty("客户名")
    private String appUserName;

    @ApiModelProperty("客户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appUserId;

    @ApiModelProperty("客户经理")
    private String adminUserName;

    @ApiModelProperty("退款状态 1 申请中 2 已退款(同意退款) 3 拒绝退款 ")
    private Integer refundState;

    @ApiModelProperty("申请人")
    private String supplyUser;

    @ApiModelProperty("审核人")
    private String  checkUser;

    @ApiModelProperty("申请时间")
    private LocalDateTime supplyTime;

    @ApiModelProperty("审核时间")
    private LocalDateTime checkTime;
}
