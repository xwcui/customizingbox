package com.customizingbox.cloud.common.datasource.mapper.app.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOauthClient;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface AppSysOauthClientMapper extends BaseMapper<AppSysOauthClient> {

}
