package com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * shopify 订单item表
 *
 * @author Y
 * @date 2022-03-30 14:19:53
 */
@Data
@TableName("app_shopify_order_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "shopify 订单item表")
public class AppShopifyOrderItem extends Model<AppShopifyOrderItem> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String adminGraphqlApiId;
    /**
     * 地址信息
     */
    @ApiModelProperty(value = "地址信息")
    private String destinationLocation;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer fulfillableQuantity;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String fulfillmentService;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String fulfillmentStatus;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer giftCard;
    /**
     * 重量
     */
    @ApiModelProperty(value = "重量")
    private Long grams;
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String originLocation;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal price;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String priceSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer productExists;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    private Long productId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String properties;
    /**
     * 购买数量
     */
    @ApiModelProperty(value = "购买数量")
    private Integer quantity;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer requiresShipping;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String sku;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer taxable;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String title;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalDiscount;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalDiscountSet;
    /**
     * 变体id
     */
    @ApiModelProperty(value = "变体id")
    private Long variantId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String variantInventoryManagement;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String variantTitle;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String vendor;
    /**
     * 税率
     */
    @ApiModelProperty(value = "税率")
    private String taxLines;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String duties;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String discountAllocations;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

}
