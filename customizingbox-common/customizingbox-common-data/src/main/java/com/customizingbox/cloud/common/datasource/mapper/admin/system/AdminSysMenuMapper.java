package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysMenu;
import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminMenuVO;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 */
public interface AdminSysMenuMapper extends BaseMapper<AdminSysMenu> {

    /**
     * 通过角色编号查询菜单
     *
     * @param roleId 角色ID
     */
    List<AdminMenuVO> listMenusByRoleId(String roleId);

    /**
     * 通过角色ID查询权限
     *
     * @param roleIds Ids
     */
    List<String> listPermissionsByRoleIds(String roleIds);
}
