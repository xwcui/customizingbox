package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysConfigStorage;

/**
 * 存储配置
 */
public interface AppSysConfigStorageMapper extends BaseMapper<AppSysConfigStorage> {

}
