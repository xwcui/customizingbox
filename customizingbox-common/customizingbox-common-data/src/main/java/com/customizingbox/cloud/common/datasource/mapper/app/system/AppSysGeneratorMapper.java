package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 代码生成器
 */
public interface AppSysGeneratorMapper {

    /**
     * 分页查询表格
     */
    @InterceptorIgnore(tenantLine="true")
    IPage<List<Map<String, Object>>> queryList(Page<Map<String, Object>> page, @Param("tableName") String tableName);

    /**
     * 查询表信息
     *
     * @param tableName 表名称
     */
    @InterceptorIgnore(tenantLine="true")
    Map<String, String> queryTable(String tableName);

    /**
     * 查询表列信息
     *
     * @param tableName 表名称
     */
    @InterceptorIgnore(tenantLine="true")
    List<Map<String, Object>> queryColumns(String tableName);
}
