package com.customizingbox.cloud.common.datasource.mapper.admin.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysRoleMenu;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 */
public interface AdminSysRoleMenuMapper extends BaseMapper<AdminSysRoleMenu> {

}
