package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;

/**
 * 产品供应商
 */
public class ApiImportProductSupplier {

    /**
     * 供应商名称
     */
    String supplierName;

    /**
     * 采购类别
     * (1. 市场采购,2. 网络采购,3. 工厂采购)
     */
    Integer supplierType;

    @XmlElement(name="SupplierName")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @XmlElement(name="SupplierType")
    public Integer getSupplierType() {
        return supplierType;
    }

    public void setSupplierType(Integer supplierType) {
        this.supplierType = supplierType;
    }
}
