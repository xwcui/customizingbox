package com.customizingbox.cloud.common.datasource.model.app.system.dto;

import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppMenuVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;


@Data
@EqualsAndHashCode(callSuper = true)
public class AppSysMenuTree extends AppSysTreeNode {

	private String icon;

	private String name;

	private boolean spread = false;

	private String path;

	private String component;

	private String authority;

	private String redirect;

	private String keepAlive;

	private String code;

	private String type;

	private String label;

	private Integer sort;

	private String permission;

	private LocalDateTime createTime;

	private LocalDateTime updateTime;

	public AppSysMenuTree() {
	}

	public AppSysMenuTree(String id, String name, String parentId) {
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.label = name;
	}

	public AppSysMenuTree(String id, String name, AppSysMenuTree parent) {
		this.id = id;
		this.parentId = parent.getId();
		this.name = name;
		this.label = name;
	}

	public AppSysMenuTree(AppMenuVO menuVo) {
		this.id = menuVo.getId();
		this.parentId = menuVo.getParentId();
		this.icon = menuVo.getIcon();
		this.name = menuVo.getName();
		this.path = menuVo.getPath();
		this.component = menuVo.getComponent();
		this.type = menuVo.getType();
		this.label = menuVo.getName();
		this.sort = menuVo.getSort();
		this.keepAlive = menuVo.getKeepAlive();
		this.permission = menuVo.getPermission();
		this.createTime = menuVo.getCreateTime();
		this.updateTime = menuVo.getUpdateTime();
	}
}
