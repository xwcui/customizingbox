package com.customizingbox.cloud.common.datasource.mapper.app.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductImgDepot;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-26 10:32:19
 */
public interface AppStoreProductImgDepotMapper extends BaseMapper<AppStoreProductImgDepot> {

}
