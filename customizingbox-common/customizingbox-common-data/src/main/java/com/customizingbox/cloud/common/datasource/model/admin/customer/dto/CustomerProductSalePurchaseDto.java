package com.customizingbox.cloud.common.datasource.model.admin.customer.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/23 16:34
 */
@Data
@ApiModel
public class CustomerProductSalePurchaseDto {
    /**
     *客户产品id
     */
    private Long productId;

    /**
     * 销售量
     */
    private Integer sale;

    /**
     * 采购量
     */
    private Integer purchase;
}
