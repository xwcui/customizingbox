package com.customizingbox.cloud.common.datasource.model.app.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-26 10:32:21
 */
@Data
@TableName("app_store_product_attr")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品属性表")
public class AppStoreProductAttr extends Model<AppStoreProductAttr> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "原商户数据id")
    private Long sourceId;
    @ApiModelProperty(value = "商户类型")
    private Integer platformType;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    private Long productId;
    /**
     * shopify 产品id
     */
    @ApiModelProperty(value = "shopify 产品id")
    private Long sourceProductId;
    /**
     * 属性名称
     */
    @ApiModelProperty(value = "属性名称")
    private String name;
    /**
     * 属性值.  json 数组
     */
    @ApiModelProperty(value = "属性值.  json 数组")
    private String values;
    /**
     * 排名
     */
    @ApiModelProperty(value = "排名")
    private String position;
    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标记")
    @TableLogic
    private String delFlag;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
