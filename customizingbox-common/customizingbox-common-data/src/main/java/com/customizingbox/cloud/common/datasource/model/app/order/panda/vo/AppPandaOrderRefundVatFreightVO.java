package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "退款订单Vat总金额, 和运费总金额")
public class AppPandaOrderRefundVatFreightVO {

    @ApiModelProperty(value = "vat总金额")
    private BigDecimal totalVatAmount;

    @ApiModelProperty(value = "运费总金额")
    private BigDecimal totalFreightAmount;

}