package com.customizingbox.cloud.common.datasource.mapper.app.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductVariantListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
public interface AppStoreProductVariantMapper extends BaseMapper<AppStoreProductVariant> {

    IPage<List<AppStoreProductVariantListVO>> appAdminVariantByPId(Page page, @Param("productId") Long productId);

    /**
     * 根据变体id, 查询关联admin里的商品
     * @param variantId
     * @return
     */
    AdminStoreProductVariant getAdminProductByAppVariantId(@Param("variantId") Long variantId);
}
