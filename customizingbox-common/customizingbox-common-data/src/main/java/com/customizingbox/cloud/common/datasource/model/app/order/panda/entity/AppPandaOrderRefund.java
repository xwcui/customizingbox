package com.customizingbox.cloud.common.datasource.model.app.order.panda.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 订单退款表
 *
 * @author Y
 * @date 2022-04-23 09:55:23
 */
@Data
@TableName("app_panda_order_refund")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "订单退款表")
public class AppPandaOrderRefund extends Model<AppPandaOrderRefund> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id")
    private Long orderId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long appUserId;
    /**
     * 交易号
     */
    @ApiModelProperty(value = "交易记录id, 交易号")
    private Long transactionRecordId;
    /**
     * 退款总金额
     */
    @ApiModelProperty(value = "退款总金额")
    private BigDecimal amount;
    /**
     * 运费
     */
    @ApiModelProperty(value = "运费")
    private BigDecimal freightAmount;
    /**
     * VAT 费用
     */
    @ApiModelProperty(value = "VAT 费用")
    private BigDecimal vatAmount;
    /**
     * 退款状态. 1 申请中 4 已退款(同意退款) 5 拒绝退款
     */
    @ApiModelProperty(value = "退款状态. 1 申请中 4 已退款(同意退款) 5 拒绝退款")
    private Integer status;
    /**
     * 申请原因
     */
    @ApiModelProperty(value = "申请原因")
    private String applyRemark;
    /**
     * 拒绝理由
     */
    @ApiModelProperty(value = "拒绝理由")
    private String rejectRemark;
    /**
     * 审核员id
     */
    @ApiModelProperty(value = "审核员id")
    private Long adminAssessorId;
    /**
     * 更新版本号
     */
    @ApiModelProperty(value = "更新版本号")
    @Version
    private Integer version;

    @ApiModelProperty(value = "审核时间")
    private LocalDateTime checkTime;
    /**
     * 创建者id
     */
    @ApiModelProperty(value = "创建者id")
    private Long createId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改者id
     */
    @ApiModelProperty(value = "修改者id")
    private Long updateId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


}
