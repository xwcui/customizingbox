package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;

/**
 * 产品库存信息集合
 */
public class ProductStock {

    /**
     * 仓库id
     */
    private Integer wareHouseID;

    /**
     * 库存数量
     */
    private Integer stockNumber;

    /**
     * 系统SKU
     */
    private String sku;

    /**
     * 自定义SKU
     */
    private String  clientSKU;

    /**
     * 海外仓SKU
     */
    private String  orderSourceSKU;

    public ProductStock() {
    }


    @XmlElement(name="WareHouseID")
    public Integer getWareHouseID() {
        return wareHouseID;
    }

    public void setWareHouseID(Integer wareHouseID) {
        this.wareHouseID = wareHouseID;
    }

    @XmlElement(name="StockNumber")
    public Integer getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(Integer stockNumber) {
        this.stockNumber = stockNumber;
    }

    @XmlElement(name="SKU")
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @XmlElement(name="ClientSKU")
    public String getClientSKU() {
        return clientSKU;
    }

    public void setClientSKU(String clientSKU) {
        this.clientSKU = clientSKU;
    }

    @XmlElement(name="OrderSourceSKU")
    public String getOrderSourceSKU() {
        return orderSourceSKU;
    }

    public void setOrderSourceSKU(String orderSourceSKU) {
        this.orderSourceSKU = orderSourceSKU;
    }
}
