package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderToSaiheOrderItemVo {

    /**
     * 卖家SKU
     */
    String sellerSKU;

    /**
     * admin产品id
     */
    Long adminProductId;
    /**
     * 订单产品数量
     */
    Integer productNum;

    /**
     * 订单产品销售单价
     */
    BigDecimal productPrice;

    /**
     * 订单产品运费单价
     */
    BigDecimal shippingPrice;

    /**
     * orderItemId
     */
    String orderItemId;

    /**
     *  赛盒sku
     */
    String shSku;

    /**
     * 定制产品; 1: 定制; 2: 非定制
     */
    String  customMode;

}
