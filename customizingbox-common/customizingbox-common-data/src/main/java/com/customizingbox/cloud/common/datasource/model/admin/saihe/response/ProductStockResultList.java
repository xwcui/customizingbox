package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * 产品库存信息集合
 *
 */
public class ProductStockResultList {

    List<ApiProductStockResult> apiProductStockResult;

    public ProductStockResultList() {
    }
    @XmlElement(name="ApiProductStockResult")
    public List<ApiProductStockResult> getApiProductStockResult() {
        return apiProductStockResult;
    }

    public void setApiProductStockResult(List<ApiProductStockResult> apiProductStockResult) {
        this.apiProductStockResult = apiProductStockResult;
    }
}
