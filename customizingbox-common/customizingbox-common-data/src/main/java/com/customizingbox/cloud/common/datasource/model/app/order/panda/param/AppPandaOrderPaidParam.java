package com.customizingbox.cloud.common.datasource.model.app.order.panda.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@ApiModel(description = "支付订单需要的参数")
@Data
public class AppPandaOrderPaidParam {

    @ApiModelProperty(value = "订单id")
    @NotNull(message = "订单id不能为空")
    private Long orderId;

    @ApiModelProperty(value = "运输方式id")
    @NotNull(message = "运输方式id能为空")
    private Long transportId;

    @ApiModelProperty(value = "product App 产品总金额")
    @NotNull(message = "产品总金额不能为空")
    private BigDecimal productAmount;

    @ApiModelProperty(value = "运费金额")
    @NotNull(message = "运费金额不能为空")
    private BigDecimal freightAmount;

    @ApiModelProperty(value = "vat金额")
    @NotNull(message = "vat金额不能为空")
    private BigDecimal vatAmount;

    @ApiModelProperty(value = "总支付金额")
    @NotNull(message = "总支付金额不能为空")
    private BigDecimal paidAmount;
}
