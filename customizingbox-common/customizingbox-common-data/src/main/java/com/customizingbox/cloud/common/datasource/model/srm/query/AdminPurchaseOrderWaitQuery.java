package com.customizingbox.cloud.common.datasource.model.srm.query;


import com.customizingbox.cloud.common.datasource.util.EnumValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;


/**
 * <p>
 * 待下单页面参数
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
@Data
@ApiModel(value = "待下单页面参数")
public class AdminPurchaseOrderWaitQuery {

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "客户id/邮箱")
    private String customInfo;

    @ApiModelProperty(value = "唯一码")
    private String onlyCode;

    @ApiModelProperty(value = "产品spu")
    private String spu;

    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(value = "采购员名称")
    private String purchaseName;

    @ApiModelProperty(value = "采购状态(-1: 全部;  1: 待下单,  3: 供应商打回)")
    @EnumValue(intValues = { -1, 1, 3}, message = "采购状态异常")
    private Integer purchaseStatus;

    @Null
    @ApiModelProperty(value = "订单状态, 1: 正常, 2 取消", hidden = true)
    private Integer status;

    @Null
    @ApiModelProperty(hidden = true)
    private Integer payStatus;
}
