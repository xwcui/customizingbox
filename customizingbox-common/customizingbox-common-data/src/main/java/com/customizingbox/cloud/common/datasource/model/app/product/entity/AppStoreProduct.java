package com.customizingbox.cloud.common.datasource.model.app.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-26 10:32:23
 */
@Data
@TableName("app_store_product")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "产品表")
public class AppStoreProduct extends Model<AppStoreProduct> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 原平台产品id
     */
    @ApiModelProperty(value = "原平台产品id")
    private Long sourceProductId;
    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private Long storeId;
    /**
     * 产品描述id
     */
    @ApiModelProperty(value = "产品描述id")
    private Long descriptionId;
    /**
     * 平台类型. 1: shopify
     */
    @ApiModelProperty(value = "平台类型. 1: shopify  ")
    private Integer platformType;
    /**
     * 标题
     */
    @ApiModelProperty(value = "标题")
    private String title;
    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "店铺名称")
    private String vendor;
    /**
     * 产品类型
     */
    @ApiModelProperty(value = "产品类型")
    private String productType;
    /**
     * 状态. 1: 设计阶段
     */
    @ApiModelProperty(value = "状态. 1: 设计阶段")
    private Integer status;
    /**
     *
     */
    @ApiModelProperty(value = "")
    private String tags;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    private LocalDateTime updateTime;
    /**
     * 商户系统创建时间
     */
    @ApiModelProperty(value = "商户系统创建时间")
    private LocalDateTime createdAt;
    /**
     * 商户系统修改时间
     */
    @ApiModelProperty(value = "商户系统修改时间")
    private LocalDateTime updatedAt;
    /**
     * 商户系统创建时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统创建时间(北京时间)")
    private LocalDateTime bjCreatedAt;
    /**
     * 商户系统修改时间(北京时间)
     */
    @ApiModelProperty(value = "商户系统修改时间(北京时间)")
    private LocalDateTime bjUpdatedAt;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    private String delFlag;

}
