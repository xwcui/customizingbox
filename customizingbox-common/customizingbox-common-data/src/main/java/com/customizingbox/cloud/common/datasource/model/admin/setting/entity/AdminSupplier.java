package com.customizingbox.cloud.common.datasource.model.admin.setting.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 供应商表
 * </p>
 *
 * @author DengChao_Li
 * @since 2022-04-13
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_supplier")
@ApiModel(value = "供应商")
public class AdminSupplier extends Model<AdminSupplier> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "供应商名称")
    private String name;

    @ApiModelProperty(value = "联系人")
    private String contacts;

    @ApiModelProperty(value = "登录手机号码")
    private String phone;


    @ApiModelProperty(value = "收款银行账户")
    private String bankAccount;

    @ApiModelProperty(value = "收款银行信息")
    private String bankInfo;

    @ApiModelProperty(value = "收款人")
    private String payee;

    @ApiModelProperty(value = "采购员,admin用户名")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminPurchaseUserId;

    @ApiModelProperty(value = "供应商状态. 1: 正常, 2: 禁用")
    private Integer status;

    @ApiModelProperty(value = "创建者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateId;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "供应商在admin中的用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminUserId;
}
