package com.customizingbox.cloud.common.datasource.model.admin.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/15 10:29
 */
@Data
@ApiModel("接收查询参数")
public class AdminCustomerDto {
    @ApiModelProperty("客户id")
    private Long appUserId;

    @ApiModelProperty("客户email")
    private String appUserEmail;
}
