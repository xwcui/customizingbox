package com.customizingbox.cloud.common.datasource.model.admin.system.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class AdminSysTreeNode {

	protected String id;

	protected String parentId;

	private Integer sort;

	protected List<AdminSysTreeNode> children = new ArrayList<>();

	public void addChildren(AdminSysTreeNode adminSysTreeNode) {
		children.add(adminSysTreeNode);
	}

	public List<AdminSysTreeNode> getChildren() {
		if (children.size() <= 0) {
			return null;
		}
		return children;
	}
}
