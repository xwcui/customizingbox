package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "订单退款填写页数据")
public class AppPandaOrderRefundDetailVO {

    @ApiModelProperty(value = "客户名")
    private String username;

    @ApiModelProperty(value = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @ApiModelProperty(value = "当前客户经理")
    private String currentUserName;

    @ApiModelProperty(value = "下单客户经理")
    private String orderUserName;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "店铺名称")
    private String storeName;

    @ApiModelProperty(value = "店铺订单id")
    private String sourceOrderId;

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "支付状态 1：未支付 2：已支付")
    private String payStatus;

    @ApiModelProperty(value = "订单发货状态（1：未发货；2：部分发货；3：全部发货; 4: 已收货）")
    private String freightStatus;

    @ApiModelProperty(value = "支付运费")
    private BigDecimal freightAmount;

    @ApiModelProperty(value = "支付vat费用")
    private BigDecimal vatAmount;


    @ApiModelProperty(value = "已退款运费")
    private BigDecimal refundFreightAmount;

    @ApiModelProperty(value = "已退款vat费用")
    private BigDecimal refundVatAmount;

    @ApiModelProperty(value = "申请中运费")
    private BigDecimal waitFreightAmount;

    @ApiModelProperty(value = "申请中vat费用")
    private BigDecimal waitVatAmount;

    @ApiModelProperty(value = "item订单数据")
    private List<AppPandaOrderRefundItemDetailVO> itemDetailVOS;
}