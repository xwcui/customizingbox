package com.customizingbox.cloud.common.datasource.model.xxljob;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class XxlJobLogGlue {

	@TableId(type = IdType.AUTO)
	private Integer id;
	private Integer jobId;
	private String glueType;
	private String glueSource;
	private String glueRemark;
	private Date addTime;
	private Date updateTime;

}
