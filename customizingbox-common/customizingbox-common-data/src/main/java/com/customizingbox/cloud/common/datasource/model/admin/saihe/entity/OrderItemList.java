package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by cjq on 2019/1/15.
 */
public class OrderItemList {
    //订单产品列表
    List<ApiUploadOrderList> apiUploadOrderLists;


    public OrderItemList() {
    }

    @XmlElement(name="ApiUploadOrderList")
    public List<ApiUploadOrderList> getApiUploadOrderLists() {
        return apiUploadOrderLists;
    }

    public void setApiUploadOrderLists(List<ApiUploadOrderList> apiUploadOrderLists) {
        this.apiUploadOrderLists = apiUploadOrderLists;
    }
}
