package com.customizingbox.cloud.common.datasource.model.srm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/5/5 14:33
 */
@Data
@ApiModel("质检记录")
public class QualityCheckRecordVo {
    @ApiModelProperty("质检员")
    private String adminQualityUserName;

    @ApiModelProperty("不良品数")
    private Integer rejectsQuantity;

    @ApiModelProperty("遗失数")
    private Integer loseQuantity;

    @ApiModelProperty("质检备注")
    private String detectionMark;

    @ApiModelProperty("质检时间")
    private LocalDateTime detectionTime;

    @ApiModelProperty("供应商填写进度物流商")
    private String progressDeliveryMethod;

    @ApiModelProperty("供应商填写进度物流单号")
    private String progressDeliveryCode;

    @ApiModelProperty("供应商填写时间")
    private LocalDateTime progressTime;

    @ApiModelProperty("质检状态 1：通过 2：不通过")
    private Integer status;

}
