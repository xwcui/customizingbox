package com.customizingbox.cloud.common.datasource.model.xxljob;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class XxlJobLogReport {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Date triggerDay;

    private int runningCount;
    private int sucCount;
    private int failCount;


}
