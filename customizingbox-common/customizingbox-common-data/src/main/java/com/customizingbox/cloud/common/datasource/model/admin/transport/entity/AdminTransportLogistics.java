package com.customizingbox.cloud.common.datasource.model.admin.transport.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 运输方式和物流属性关联表
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_transport_logistics")
@ApiModel(value = "运输方式和物流属性关联表")
public class AdminTransportLogistics extends Model<AdminTransportLogistics> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输方式表id")
    private Long transportId;

    @ApiModelProperty(value = "物流属性表")
    private Long logisticsId;

    @ApiModelProperty(value = "创建者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateId;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}