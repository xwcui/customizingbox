package com.customizingbox.cloud.common.datasource.model.app.order.source.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * panda 订单item表
 *
 * @author Y
 * @date 2022-03-30 13:48:16
 */
@Data
@TableName("app_order_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "panda 订单item表")
public class AppOrderItem extends Model<AppOrderItem> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 商户订单item表id
     */
    @ApiModelProperty(value = "商户订单item表id")
    private Long sourceId;
    /**
     * 订单表id
     */
    @ApiModelProperty(value = "订单表id")
    private Long orderId;
    /**
     * 商户订单表id
     */
    @ApiModelProperty(value = "商户订单表id")
    private Long sourceOrderId;
    /**
     * 平台类型. 1: shopify  
     */
    @ApiModelProperty(value = "平台类型. 1: shopify  ")
    private Integer platformType;
    /**
     * 产品表id
     */
    @ApiModelProperty(value = "产品表id")
    private Long productId;
    /**
     * 产品变体id
     */
    @ApiModelProperty(value = "产品变体id")
    private Long variantId;
    /**
     * 原商户产品id
     */
    @ApiModelProperty(value = "原商户产品id")
    private Long sourceProductId;
    /**
     * 原商户变体id
     */
    @ApiModelProperty(value = "原商户变体id")
    private Long sourceVariantId;
    /**
     * 总产品产品数量
     */
    @ApiModelProperty(value = "总产品产品数量")
    private Integer quantity;
    /**
     * 客户自定义商品信息
     */
    @ApiModelProperty(value = "客户自定义商品信息")
    private String properties;
    /**
     * 实际产品数据 (总产品 - 退款)
     */
    @ApiModelProperty(value = "实际产品数据 (总产品 - 退款)")
    private Integer fulfillableQuantity;
    /**
     * 退款产品数量
     */
    @ApiModelProperty(value = "退款产品数量")
    private Integer refundQuantity;
    /**
     * 产品快照id
     */
    @ApiModelProperty(value = "产品快照id")
    private Long productSnapshootId;
    /**
     * 数据版本号.  乐观锁
     */
    @ApiModelProperty(value = "数据版本号.  乐观锁")
    private Integer version;

    @ApiModelProperty(value = "APP下单状态(1: 未下单  3: 全部下单)")
    private Integer placeStatus;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    @ApiModelProperty(value = "和admin产品关联状态; 1: 全部关联 2: 部分关联 3: 全部未关联")
    private Integer relevancyStatus;

    @ApiModelProperty(value = "关联admin产品id")
    private Long adminProductId;

    @ApiModelProperty(value = "关联admin产品item id")
    private Long adminVariantId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;
}
