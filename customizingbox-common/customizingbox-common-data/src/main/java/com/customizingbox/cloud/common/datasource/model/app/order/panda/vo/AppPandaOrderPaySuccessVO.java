package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(description = "panda 支付成功返回值")
public class AppPandaOrderPaySuccessVO {

    @ApiModelProperty(value = "交易id")
    @JsonSerialize(using = ToStringSerializer.class)
    private String transactionId;

    @ApiModelProperty(value = "支付的总金额")
    private BigDecimal paidTotalAmount;

}
