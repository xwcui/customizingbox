package com.customizingbox.cloud.common.datasource.mapper.app.order.shopify;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrderRefund;

/**
 * shopify 订单退款表
 *
 * @author Y
 * @date 2022-03-30 14:19:52
 */
public interface AppShopifyOrderRefundMapper extends BaseMapper<AppShopifyOrderRefund> {

}
