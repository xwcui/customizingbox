package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;

import javax.xml.bind.annotation.XmlElement;

/**
 * 上传赛盒后sku修改或新增的返回值
 */
public class SkuResult {

    String sku;

    String clientSku;

    public SkuResult() {
    }

    @XmlElement(name="Sku")
    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @XmlElement(name="ClientSku")
    public String getClientSku() {
        return clientSku;
    }

    public void setClientSku(String clientSku) {
        this.clientSku = clientSku;
    }
}
