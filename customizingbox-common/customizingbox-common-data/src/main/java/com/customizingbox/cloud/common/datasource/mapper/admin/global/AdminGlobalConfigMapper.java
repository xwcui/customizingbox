package com.customizingbox.cloud.common.datasource.mapper.admin.global;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.global.AdminGlobalConfig;

/**
 * <p>
 * admin全局配置 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-05-09
 */
public interface AdminGlobalConfigMapper extends BaseMapper<AdminGlobalConfig> {

}
