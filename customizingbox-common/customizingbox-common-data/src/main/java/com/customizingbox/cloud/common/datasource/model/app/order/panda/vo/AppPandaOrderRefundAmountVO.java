package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "查询退款金额")
public class AppPandaOrderRefundAmountVO {

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "运费")
    private BigDecimal freightAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "vat金额")
    private BigDecimal vatAmount = BigDecimal.ZERO;
}