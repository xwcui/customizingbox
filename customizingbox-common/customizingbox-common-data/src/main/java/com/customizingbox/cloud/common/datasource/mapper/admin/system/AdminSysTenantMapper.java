package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysTenant;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
 * <p>
 * 租户管理 Mapper 接口
 * </p>
 */
public interface AdminSysTenantMapper extends BaseMapper<AdminSysTenant> {

    /**
     * 通过租户ID删除租户
     * 包括base_upms库所有表的当前租户数据
     */
    void deleteSysTenantById(@Param("id") Serializable id);
}
