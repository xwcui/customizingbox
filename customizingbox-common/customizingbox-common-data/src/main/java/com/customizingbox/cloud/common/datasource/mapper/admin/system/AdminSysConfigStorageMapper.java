package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysConfigStorage;

/**
 * 存储配置
 */
public interface AdminSysConfigStorageMapper extends BaseMapper<AdminSysConfigStorage> {

}
