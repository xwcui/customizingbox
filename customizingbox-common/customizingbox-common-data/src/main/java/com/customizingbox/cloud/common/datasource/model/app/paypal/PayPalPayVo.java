package com.customizingbox.cloud.common.datasource.model.app.paypal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel(description = "paypal支付vo")
public class PayPalPayVo {

    @ApiModelProperty(value = "充值金额")
    @NotNull(message = "充值金额不能为空")
    private BigDecimal amount;
}
