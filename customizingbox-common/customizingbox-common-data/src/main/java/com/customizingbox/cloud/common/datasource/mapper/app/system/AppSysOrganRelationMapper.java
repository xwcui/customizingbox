package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrganRelation;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface AppSysOrganRelationMapper extends BaseMapper<AppSysOrganRelation> {

    /**
     * 删除机构关系表数据
     *
     * @param id 机构ID
     */
    int deleteOrganRelationsById(String id);

    /**
     * 更改部分关系表数据
     */
    int deleteOrganRelations(AppSysOrganRelation appSysOrganRelation);

    /**
     * 更改部分关系表数据
     */
    List<AppSysOrganRelation> listOrganRelations(AppSysOrganRelation appSysOrganRelation);

}
