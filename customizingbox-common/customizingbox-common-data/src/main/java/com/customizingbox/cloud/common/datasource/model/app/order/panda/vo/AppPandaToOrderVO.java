package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderItemVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 订单列表关联变体
 *
 */
@Data
@ApiModel(description = "订单表")
public class AppPandaToOrderVO {

     @ApiModelProperty(value = "app订单id")
     @JsonSerialize(using = ToStringSerializer.class)
     private Long id;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "商铺名称")
    private String storeName;

    @ApiModelProperty(value = "店铺订单id")
    private String sourceId;

    @ApiModelProperty(value = "支付状态")
    private String financialStatus;

    @ApiModelProperty(value = "订单发货状态（1：未发货；2：部分发货；3：全部发货; 4: 已收货）")
    private String freightStatus;

    @ApiModelProperty(value = "收货地址")
    private String address;

    @ApiModelProperty(value = "收货地址id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long addressId;

    @ApiModelProperty(value = "收件人")
    private String username;

    @ApiModelProperty(value = "店铺订单备注")
    private String sourceMark;

    @ApiModelProperty(value = "原平台店铺运输方式")
    private String sourceTrackingCompany;

    @ApiModelProperty(value = "原平台店铺运费")
    private String sourceFreightAmount;

    @ApiModelProperty(value = "app订单创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "变体列表")
    private List<AppPandaToOrderItemVO> orderItems = new ArrayList<>();

    @ApiModelProperty(value = "panda订单itemId")
    @JsonIgnore
    private Long pandaOrderItemId;

    @ApiModelProperty(value = "关联(有值就说明已经关联了)")
    @JsonIgnore
    private Long adminVariantId;

    @ApiModelProperty(value = "app产品变体id")
    @JsonIgnore
    private Long variantId;

    @ApiModelProperty(value = "变体图片")
    @JsonIgnore
    private String appImgSrc;

    @ApiModelProperty(value = "变体属性值")
    @JsonIgnore
    private String appAttrValues;

    @ApiModelProperty(value = "变体数量")
    @JsonIgnore
    private Integer fulfillableQuantity;

    @ApiModelProperty(value = "admin变体图片")
    @JsonIgnore
    private String adminImgSrc;

    @ApiModelProperty(value = "admin变体属性值")
    @JsonIgnore
    private String adminAttrValues;

    @ApiModelProperty(value = "给客户的价格")
    @JsonIgnore
    private BigDecimal quotePrice;

}
