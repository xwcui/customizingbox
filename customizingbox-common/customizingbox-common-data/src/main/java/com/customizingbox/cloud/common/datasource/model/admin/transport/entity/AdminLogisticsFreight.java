package com.customizingbox.cloud.common.datasource.model.admin.transport.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * <p>
 * 物流费用
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_logistics_freight")
@ApiModel(value = "物流费用")
public class AdminLogisticsFreight extends Model<AdminLogisticsFreight> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输方式id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long transportId;

    @ApiModelProperty(value = "运输方式名称")
    private String transportName;

    @ApiModelProperty(value = "始发地id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long beginCountryId;

    @ApiModelProperty(value = "目的地id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long endCountryId;

    @ApiModelProperty(value = "折扣")
    private BigDecimal discount;

    @ApiModelProperty(value = "区间开始重量 单位克")
    private Integer beginWeight;

    @ApiModelProperty(value = "区间结束重量 单位克")
    private Integer endWeight;

    @ApiModelProperty(value = "固定费+挂号费")
    private BigDecimal fixedPrice;

    @ApiModelProperty(value = "克重费")
    private BigDecimal gramPrice;

    @ApiModelProperty(value = "最早到达时间(天)")
    private Integer earliestDay;

    @ApiModelProperty(value = "最晚到达时间(天)")
    private Integer longestDay;

    @ApiModelProperty(value = "备注")
    private String mark;

    @ApiModelProperty(value = "启动时间")
    private LocalDateTime enableTime;

    @ApiModelProperty(value = "启用结束时间")
    private LocalDateTime notEnableTime;

    @ApiModelProperty(value = "创建者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateId;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}