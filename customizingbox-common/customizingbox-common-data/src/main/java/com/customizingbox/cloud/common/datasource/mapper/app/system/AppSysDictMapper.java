package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 */
public interface AppSysDictMapper extends BaseMapper<AppSysDict> {

}
