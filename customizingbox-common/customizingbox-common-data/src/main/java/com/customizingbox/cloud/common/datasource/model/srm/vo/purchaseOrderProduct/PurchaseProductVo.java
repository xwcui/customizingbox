package com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 采购订单产品父类vo
 *
 * @author DC_Li
 * @date 2022/4/25 15:55
 */
@Data
@ApiModel("待生产订单产品响应数据")
public class PurchaseProductVo {
    @JsonIgnore
    private Long productId;

    @ApiModelProperty(value = "采购订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaseOrderId;

    @ApiModelProperty(value = "产品变体图地址")
    private String variantImgUrl;

    @ApiModelProperty("产品名称")
    private String productTitle;

    @ApiModelProperty("SPU")
    private String spu;

    @ApiModelProperty("SKU")
    private String sku;

    @ApiModelProperty("唯一码")
    private String onlyCode;

    @ApiModelProperty("规格值描述")
    private String attrValues;

    @ApiModelProperty("规格名描述")
    private String attrNames;

    @ApiModelProperty("自定义信息")
    private String properties;

    @ApiModelProperty("数量")
    private Integer quantity;

    @ApiModelProperty("采购状态 1: 待下单, 2: 已下单, 3: 供应商打回, 4:生产中，5:已发货，6:质检不通过，7:已入库，8:订单已取消")
    private Integer purchaseState;

    @ApiModelProperty("下单采购员")
    private String purchaseName;

    @ApiModelProperty("下单供应商")
    private String supplierName;

    @ApiModelProperty("下单采购价")
    private BigDecimal price;
}
