package com.customizingbox.cloud.common.datasource.mapper.app.order.shopify;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrderRefundItem;

/**
 * shopify 订单item退款表
 *
 * @author Y
 * @date 2022-03-30 14:19:51
 */
public interface AppShopifyOrderRefundItemMapper extends BaseMapper<AppShopifyOrderRefundItem> {

}
