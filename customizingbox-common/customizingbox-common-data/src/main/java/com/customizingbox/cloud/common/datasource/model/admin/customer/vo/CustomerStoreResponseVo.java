package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/4/15 17:02
 */
@Data
@ApiModel
public class CustomerStoreResponseVo {
        @ApiModelProperty("店铺类型 1:shopify")
        private Integer platformType;

        @ApiModelProperty("店铺名")
        private String storeName;

        @ApiModelProperty("店铺id")
        @JsonSerialize(using = ToStringSerializer.class)
        private Long storeId;

        @ApiModelProperty("客户名")
        private String username;

        @ApiModelProperty("客户id")
        @JsonSerialize(using = ToStringSerializer.class)
        private Long appUserId;

        @ApiModelProperty("客户经理")
        private String adminUserName;

        @ApiModelProperty("客户平台店铺订单总量")
        private Integer platformOrderCount;

        @ApiModelProperty("店铺创建时间")
        // 未转时区
        private LocalDateTime platformStoreCreateTime;

        @ApiModelProperty("app添加时间")
        private LocalDateTime appAddTime;

        @ApiModelProperty("授权状态,1:正常,2:已解除授权,3已删除:")
        private Integer storeState;

        @JsonIgnore
        private String token;
}
