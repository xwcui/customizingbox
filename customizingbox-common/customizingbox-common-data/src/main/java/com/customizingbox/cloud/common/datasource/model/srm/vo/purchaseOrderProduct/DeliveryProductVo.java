package com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 已发货订单产品vo
 * @author DC_Li
 * @date 2022/5/3 9:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DeliveryProductVo extends PurchaseProductVo {
    @ApiModelProperty("生产批次")
    private String productionBatch;

    @ApiModelProperty("发货批次")
    private String deliveryBatch;

    @ApiModelProperty("物流单号")
    private String deliveryCode;

    // @ApiModelProperty("质检不通过信息")
    // private String qualityFailedMsg;

    @ApiModelProperty("结算状态 0：未结算,1：已结算")
    private Boolean settlementState;
}
