package com.customizingbox.cloud.common.datasource.model.admin.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 机构关系表
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "admin_sys_organ_relation")
public class AdminSysOrganRelation extends Model<AdminSysOrganRelation> {

    private static final long serialVersionUID = 1L;

    /**
     * 祖先节点
     */
    @ApiModelProperty(value = "祖先节点")
    private String ancestor;

    /**
     * 后代节点
     */
    @ApiModelProperty(value = "后代节点")
    private String descendant;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
