package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ApiImportProductImage;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by cjq on 2019/3/29.
 */

public class ImagesList {

    /**
     * 产品图片(最多9张)
     */
    List<ApiImportProductImage> imagesList;

    @XmlElement(name="ApiImportProductImage")
    public List<ApiImportProductImage> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<ApiImportProductImage> imagesList) {
        this.imagesList = imagesList;
    }
}
