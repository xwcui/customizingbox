package com.customizingbox.cloud.common.datasource.model.xxljob;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.util.StringUtils;

@Data
public class XxlJobUser {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String username;
    private String password;
    private int role;
    private String permission;


    public boolean validPermission(int jobGroup) {
        if (this.role == 1) {
            return true;
        } else {
            if (StringUtils.hasText(this.permission)) {
                for (String permissionItem : this.permission.split(",")) {
                    if (String.valueOf(jobGroup).equals(permissionItem)) {
                        return true;
                    }
                }
            }
            return false;
        }

    }

}
