package com.customizingbox.cloud.common.datasource.model.app.paypal;

import java.math.BigDecimal;


public class PaypalAmount {

    private BigDecimal amount;
    private BigDecimal payAmount;
    private BigDecimal paypalFee;

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public BigDecimal getPaypalFee() {
        return paypalFee;
    }

    public void setPaypalFee(BigDecimal paypalFee) {
        this.paypalFee = paypalFee;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public PaypalAmount(BigDecimal amount, Integer i) {
        if(i == 0)
            this.payAmount = amount.add(new BigDecimal("0.3")).divide(new BigDecimal("0.971"),2,BigDecimal.ROUND_HALF_UP);
        else
            this.payAmount = amount.divide(new BigDecimal("0.971"),2,BigDecimal.ROUND_HALF_UP);
        this.paypalFee = this.payAmount.subtract(amount);
        this.amount = amount;
    }

    public PaypalAmount(BigDecimal amount){
        this.payAmount = amount.add(new BigDecimal("0.3")).divide(new BigDecimal("0.971"),2,BigDecimal.ROUND_HALF_UP);
        this.paypalFee = this.payAmount.subtract(amount);
        this.amount = amount;
    }
}
