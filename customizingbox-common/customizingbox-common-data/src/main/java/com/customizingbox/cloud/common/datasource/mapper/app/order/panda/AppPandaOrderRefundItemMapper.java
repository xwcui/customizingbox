package com.customizingbox.cloud.common.datasource.mapper.app.order.panda;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderRefundItem;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundItemAmountVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 退款item表
 *
 * @author Y
 * @date 2022-04-23 09:55:24
 */
public interface AppPandaOrderRefundItemMapper extends BaseMapper<AppPandaOrderRefundItem> {

    /**
     * 查询待退款订单总金额
     * @param orderId 订单id
     * @param status 退款状态. 1 申请中 4 已退款(同意退款) 5 拒绝退款  -1: 申请中和已退款
     * @return
     */
    List<AppPandaOrderRefundItemAmountVO> applyRefundAmount(@Param(value = "orderId") Long orderId, @Param(value = "status") Integer status);


}
