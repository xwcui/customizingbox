package com.customizingbox.cloud.common.datasource.model.admin.customer.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author DC_Li
 * @date 2022/5/10 15:26
 */
@Data
public class FreightAndVatTotalDto {
    /**
     * 退款表id,退款交易号
     */
    private String refundIds;
    /**
     * 退款运费总和
     */
    private BigDecimal freightAmount;
    /**
     * 退款vat总和
     */
    private BigDecimal vatAmount;
}
