package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDictValue;

/**
 * 字典项
 */
public interface AppSysDictValueMapper extends BaseMapper<AppSysDictValue> {

}
