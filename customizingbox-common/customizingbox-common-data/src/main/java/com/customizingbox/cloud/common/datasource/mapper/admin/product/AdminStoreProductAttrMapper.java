package com.customizingbox.cloud.common.datasource.mapper.admin.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import com.customizingbox.cloud.common.datasource.model.srm.ProductIdNames;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-31 09:34:36
 */
public interface AdminStoreProductAttrMapper extends BaseMapper<AdminStoreProductAttr> {
    /**
     * 获取产品规格名
     * @param productIds
     * @return
     */
    List<ProductIdNames> getName(@Param("productIds") Collection<Long> productIds);

    /**
     * 获取产品规格名
     * @param productId
     * @return
     */
    String getNameById(@Param("productId") Long productId);
}
