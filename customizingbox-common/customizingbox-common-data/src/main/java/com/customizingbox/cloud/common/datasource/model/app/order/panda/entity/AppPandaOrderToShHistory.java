package com.customizingbox.cloud.common.datasource.model.app.order.panda.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;


/**
 * <p>
 *
 * </p>
 *
 * @author Z
 * @since 2022-05-09
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("app_panda_order_to_sh_history")
@ApiModel(value = "")
public class AppPandaOrderToShHistory extends Model<AppPandaOrderToShHistory> {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "id")
    private Long id;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "panda订单id")
    private Long orderId;

    @ApiModelProperty(value = "订单产品上传赛盒状态, 1: 已上传, 2 未上传, 3:部分上传")
    private Integer productUploadShStatus;

    @ApiModelProperty(value = "订单上传赛盒状态, 1: 已上传, 2 未上传, 3:上传失败")
    private Integer uploadShStatus;

    @ApiModelProperty(value = "上传赛盒信息")
    private String msg;

    @ApiModelProperty(value = "赛盒订单号")
    private String shOrderCode;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}