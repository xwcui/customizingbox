package com.customizingbox.cloud.common.datasource.mapper.transaction;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.paypal.AppPaypalPayment;

/**
 * <p>
 * payapl交易记录 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-22
 */
public interface AppPaypalPaymentMapper extends BaseMapper<AppPaypalPayment> {

}
