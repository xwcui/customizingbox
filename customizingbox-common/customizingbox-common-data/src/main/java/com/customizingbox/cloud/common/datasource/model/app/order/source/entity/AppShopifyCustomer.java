package com.customizingbox.cloud.common.datasource.model.app.order.source.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 商户客户表
 *
 * @author Y
 * @date 2022-03-30 13:48:14
 */
@Data
@TableName("app_shopify_customer")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商户客户表")
public class AppShopifyCustomer extends Model<AppShopifyCustomer> {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "")
    private Long id;

    @ApiModelProperty(value = "原商户的客户id")
    private Long sourceId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String email;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer acceptsMarketing;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime createdAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime updatedAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String firstName;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String lastName;
    /**
     * 订单数量
     */
    @ApiModelProperty(value = "订单数量")
    private Integer ordersCount;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String state;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private BigDecimal totalSpent;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long lastOrderId;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String note;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer verifiedEmail;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String multipassIdentifier;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer taxExempt;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String tags;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String lastOrderName;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String currency;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime acceptsMarketingUpdatedAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String marketingOptInLevel;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String taxExemptions;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String smsMarketingConsent;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String adminGraphqlApiId;
    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
