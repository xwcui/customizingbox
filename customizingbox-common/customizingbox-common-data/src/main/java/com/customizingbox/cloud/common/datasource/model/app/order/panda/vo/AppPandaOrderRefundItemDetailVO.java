package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "item订单退款填写页数据")
public class AppPandaOrderRefundItemDetailVO {

    @ApiModelProperty(value = "orderItemId")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderItemId;

    @ApiModelProperty(value = "变体图")
    private String src;

    @ApiModelProperty(value = "变体图属性值")
    private String title;

    @ApiModelProperty(value = "数量")
    private Integer quantity;

    @ApiModelProperty(value = "给客户的价格")
    private BigDecimal quotePrice;

    @ApiModelProperty(value = "支付金额")
    private BigDecimal payAmount;

    @ApiModelProperty(value = "已退款金额")
    private BigDecimal successRefundAmount;

    @ApiModelProperty(value = "申请中退款金额")
    private BigDecimal waitRefundAmount;

    @ApiModelProperty(value = "本次申请退款金额")
    private BigDecimal applyRefundAmount;


}
