package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author DC_Li
 * @date 2022/4/18 15:14
 */
@Data
@ApiModel
public class CustomerProductVariant {
    @ApiModelProperty("变体id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long variantId;

    @ApiModelProperty("变体sku")
    private String sku;

    @ApiModelProperty("变体规格")
    private String option;

    @ApiModelProperty("报价状态")
    private Boolean quoteState;

    @ApiModelProperty("产品库变体信息")
    private AdminQuoteVariantResponseVo adminVariant;

    @ApiModelProperty("倍率")
    private BigDecimal supplyRate;
    /**
     * 用来判断是否报价 未报价为空，已报价则不为空
     */
    @JsonIgnore
    private Long adminVariantId;
}
