package com.customizingbox.cloud.common.datasource.model.admin.transport.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 *
 * 运输单元（费用） 查询vo
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Api("运输单元（费用） 查询vo")
public class AdminLogisticsFreightSearchVo {

    @ApiModelProperty(value = "目的地id")
    private Long endCountryId;

    @ApiModelProperty(value = "运输方式id")
    private Long transportId;

    @ApiModelProperty(value = "启动时间")
    private LocalDateTime enableTime;
}
