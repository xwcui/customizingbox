package com.customizingbox.cloud.common.datasource.model.admin.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 接收客户店铺查询条件
 * @author DC_Li
 * @date 2022/4/15 17:19
 */
@Data
@ApiModel("客户列表店铺查询参数")
public class CustomerStoreDto {
    @ApiModelProperty("店铺id")
    private Long storeId;
    @ApiModelProperty("店铺名称")
    private String storeName;
    @ApiModelProperty("客户id")
    private Long appUserId;
    @ApiModelProperty("客户邮箱")
    private String appUserEmail;
}
