package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by cjq on 2019/1/11.
 */
public class ApiUploadOrderInfo {

    /**
     * 商户号
     */
    Integer customerID;


    /**
     * 来源渠道ID(需在ERP系统中创建)
     */
    Integer orderSourceID;

    /**
     * 支付时间
     */
    Date payTime;

    /**
     * 客户订单号
     */
    String clientOrderCode;

    /**
     * 邮箱
     */
    String email;

    /**
     * 交易号
     */
    String transactionId;

    /**
     * 币种
     */
    String currency;

    /**
     * 订单总金额
     */
    BigDecimal totalPrice;

    /**
     * 订单优惠金额
     */
    BigDecimal promotionDiscountAmount;

    /**
     * 运费收入
     */
    BigDecimal transportPay;

    /**
     * 买家留言
     */
    String orderDescription;

    /**
     * 买家姓氏
     */
    String firstName;

    /**
     * 买家名字
     */
    String lastName;

    /**
     * 收货国家
     */
    String country;

    /**
     * 证件/税号
     */
    String taxNumber;

    /**
     * 寄件人税号
     */
    String senderTaxNumber;


    /**
     * 收货省份
     */
    String province;

    /**
     * 收货城市
     */
    String city;

    /**
     * District 收货人区/县
     */
    String  district;

    /**
     * 邮编
     */
    String postCode;

    /**
     * 电话
     */
    String telephone;

    /**
     * 收货地址一
     */
    String address1;

    /**
     * 收货地址二
     */
    String address2;

    /**
     * CabinetNO  柜号
     */
    String cabinetNO;

    /**
     * 追踪号
     */
    String trackNumbers;

    /**
     * 订单产品列表
     */
    OrderItemList orderItemList=new OrderItemList();

    /**
     * 发货仓库ID
     */
    Integer wareHouseID;

    /**
     * 运输方式ID
     */
    Integer transportID;

    /**
     * 是否执行订单策略(默认为:true；如果为false，则必须传入WareHouseID和TransportID参数)
     */
    Boolean isOperateMatch;

    /**
     * 客户订单对应平台账号, 判断该平台的卖家唯一ID
     */
    String clientUserAccount;

    /**
     * HouseNumber  门牌号
     */
    String houseNumber;


    public ApiUploadOrderInfo() {
    }

    @XmlElement(name="CustomerID")
    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }
    @XmlElement(name="OrderSourceID")
    public Integer getOrderSourceID() {
        return orderSourceID;
    }

    public void setOrderSourceID(Integer orderSourceID) {
        this.orderSourceID = orderSourceID;
    }

    @XmlElement(name="PayTime")
    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    @XmlElement(name="ClientOrderCode")
    public String getClientOrderCode() {
        return clientOrderCode;
    }

    public void setClientOrderCode(String clientOrderCode) {
        this.clientOrderCode = clientOrderCode;
    }

    @XmlElement(name="Email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlElement(name="TransactionId")
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @XmlElement(name="Currency")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @XmlElement(name="TotalPrice")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @XmlElement(name="PromotionDiscountAmount")
    public BigDecimal getPromotionDiscountAmount() {
        return promotionDiscountAmount;
    }

    public void setPromotionDiscountAmount(BigDecimal promotionDiscountAmount) {
        this.promotionDiscountAmount = promotionDiscountAmount;
    }

    @XmlElement(name="TransportPay")
    public BigDecimal getTransportPay() {
        return transportPay;
    }

    public void setTransportPay(BigDecimal transportPay) {
        this.transportPay = transportPay;
    }

    @XmlElement(name="OrderDescription")
    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }

    @XmlElement(name="FirstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @XmlElement(name="LastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @XmlElement(name="Country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @XmlElement(name="TaxNumber")
    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    @XmlElement(name="SenderTaxNumber")
    public String getSenderTaxNumber() {
        return senderTaxNumber;
    }

    public void setSenderTaxNumber(String senderTaxNumber) {
        this.senderTaxNumber = senderTaxNumber;
    }

    @XmlElement(name="Province")
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @XmlElement(name="City")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @XmlElement(name="District")
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @XmlElement(name="PostCode")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @XmlElement(name="Telephone")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @XmlElement(name="Address1")
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @XmlElement(name="Address2")
    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @XmlElement(name="CabinetNO")
    public String getCabinetNO() {
        return cabinetNO;
    }

    public void setCabinetNO(String cabinetNO) {
        this.cabinetNO = cabinetNO;
    }

    @XmlElement(name="TaxNumber")
    public String getTrackNumbers() {
        return trackNumbers;
    }

    public void setTrackNumbers(String trackNumbers) {
        this.trackNumbers = trackNumbers;
    }

    @XmlElement(name="OrderItemList")
    public OrderItemList getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(OrderItemList orderItemList) {
        this.orderItemList = orderItemList;
    }

    @XmlElement(name="WareHouseID")
    public Integer getWareHouseID() {
        return wareHouseID;
    }

    public void setWareHouseID(Integer wareHouseID) {
        this.wareHouseID = wareHouseID;
    }

    @XmlElement(name="TransportID")
    public Integer getTransportID() {
        return transportID;
    }

    public void setTransportID(Integer transportID) {
        this.transportID = transportID;
    }

    @XmlElement(name="IsOperateMatch")
    public Boolean getOperateMatch() {
        return isOperateMatch;
    }

    public void setOperateMatch(Boolean operateMatch) {
        isOperateMatch = operateMatch;
    }

    @XmlElement(name="ClientUserAccount")
    public String getClientUserAccount() {
        return clientUserAccount;
    }

    public void setClientUserAccount(String clientUserAccount) {
        this.clientUserAccount = clientUserAccount;
    }

    @XmlElement(name="HouseNumber")
    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
}
