package com.customizingbox.cloud.common.datasource.model.admin.system.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *  角色列表
 * </p>
 */
@Data
@ApiModel(description = "角色新增参数")
public class AdminSysRoleIdsParam {

	@ApiModelProperty(value = "PK")
	private List<String> roleIds;
}
