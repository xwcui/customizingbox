package com.customizingbox.cloud.common.datasource.model.admin.system.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 角色表
 * </p>
 */
@Data
@ApiModel(description = "角色")
public class AdminSysRoleDetailVO extends Model<AdminSysRoleDetailVO> {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "id")
	private String id;


	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "角色标识")
	private String roleCode;

	@ApiModelProperty(value = "角色描述")
	private String roleDesc;


	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	@ApiModelProperty(value = "更新时间")
	private LocalDateTime updateTime;

//	@TableLogic
	@ApiModelProperty(value = "删除标识")
	private String delFlag;

	@ApiModelProperty(value = "角色菜单id")
	List<String> menuIds;

	@Override
	public Serializable pkVal() {
		return this.id;
	}

}
