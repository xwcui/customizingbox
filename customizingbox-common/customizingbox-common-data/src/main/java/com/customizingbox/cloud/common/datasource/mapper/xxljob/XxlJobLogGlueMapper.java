package com.customizingbox.cloud.common.datasource.mapper.xxljob;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.xxljob.XxlJobLogGlue;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface XxlJobLogGlueMapper extends BaseMapper<XxlJobLogGlue> {

    int save(XxlJobLogGlue xxlJobLogGlue);

    List<XxlJobLogGlue> findByJobId(@Param("jobId") int jobId);

    int removeOld(@Param("jobId") int jobId, @Param("limit") int limit);

    int deleteByJobId(@Param("jobId") int jobId);

}
