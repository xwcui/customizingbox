package com.customizingbox.cloud.common.datasource.model.admin.transport.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 国家和地区表信息
 * </p>
 *
 * @author Z
 * @since 2022-04-14
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_country_info")
@ApiModel(value = "国家和地区表信息")
public class AdminCountryInfoDto {


    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "中文名称")
    @NotBlank(message = "中文名称不能为null")
    private String nameCn;

    @ApiModelProperty(value = "英文名称")
    @NotBlank(message = "英文名称不能为null")
    private String nameEn;

    @ApiModelProperty(value = "地区代码")
    @NotBlank(message = "地区代码不能为null")
    private String code;
}