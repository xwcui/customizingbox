package com.customizingbox.cloud.common.datasource.model.srm.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.enums.BooleanEnum;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;

/**
 * @author DC_Li
 * @date 2022/5/7 11:42
 */
@Data
@ColumnWidth(value = 20)
@ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER,verticalAlignment = VerticalAlignmentEnum.CENTER,wrapped = BooleanEnum.TRUE)
public class ExportProductionMeans implements Serializable {
    @ExcelProperty(value ="唯一码")
    @ColumnWidth(20)
    private String onlyCode;

    @ExcelProperty(value = "产品变体图地址")
    private String variantImgUrl;

    @ExcelProperty(value = "产品变体图")
    private URL variantImg;

    @ExcelProperty(value = "产品名称")
    private String productName;

    @ExcelProperty(value = "SPU")
    @ColumnWidth(10)
    private String spu;

    @ExcelProperty(value ="SKU")
    @ColumnWidth(10)
    private String sku;

    @ExcelIgnore
    private String attrValues;

    @ExcelProperty(value ="属性值1")
    @ColumnWidth(12)
    private String attrValue1;

    @ExcelProperty(value ="属性值2")
    @ColumnWidth(12)
    private String attrValue2;

    @ExcelProperty(value ="属性值3")
    @ColumnWidth(12)
    private String attrValue3;

    @ExcelProperty(value ="自定义图片")
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT,wrapped = BooleanEnum.TRUE)
    private String customPic;

    @ExcelProperty(value ="自定义文本")
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.LEFT,wrapped = BooleanEnum.TRUE)
    private String customText;

    @ExcelProperty(value ="数量")
    @ColumnWidth(10)
    private Integer quantity;

    @ExcelProperty(value ="下单采购价")
    @NumberFormat("0.00")
    private BigDecimal purchasePrice;
}
