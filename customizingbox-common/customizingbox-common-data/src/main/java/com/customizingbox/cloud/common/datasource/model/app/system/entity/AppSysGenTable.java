package com.customizingbox.cloud.common.datasource.model.app.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 代码生成配置表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "代码生成配置表")
@TableName("app_sys_gen_table")
public class AppSysGenTable extends Model<AppSysGenTable> {
    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "PK")
    private String id;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名")
    private String tableName;

    /**
     * 表描述
     */
    @ApiModelProperty(value = "表描述")
    private String tableComment;

    /**
     * 生成包路径
     */
    @ApiModelProperty(value = "生成包路径")
    private String packageName;

    /**
     * 生成模块名
     */
    @ApiModelProperty(value = "生成模块名")
    private String moduleName;

    /**
     * 作者
     */
    @ApiModelProperty(value = "作者")
    private String author;

    /**
     * 表前缀
     */
    @ApiModelProperty(value = "表前缀")
    private String tablePrefix;

    /**
     * 服务的路由key
     */
    @ApiModelProperty(value = "服务的路由key")
    private String genKey;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 最后更新时间
     */
    @ApiModelProperty(value = "最后更新时间")
    private LocalDateTime updateTime;

    /**
     * 创建者ID
     */
    @ApiModelProperty(value = "创建者ID")
    private String createId;

    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    @ApiModelProperty(value = "逻辑删除标记（0：显示；1：隐藏）")
    private String delFlag;
}
