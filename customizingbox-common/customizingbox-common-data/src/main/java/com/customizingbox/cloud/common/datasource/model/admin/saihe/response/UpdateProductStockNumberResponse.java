package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * 赛盒创建库存返回对象
 * 请求库存产品操作结果实体类
 */
public class UpdateProductStockNumberResponse {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";

    /**
     * 产品库存信息集合
     */
    UpdateProductStockNumberResult updateProductStockNumberResult;

    public UpdateProductStockNumberResponse() {
    }

    @XmlElement(name="UpdateProductStockNumberResult")
    public UpdateProductStockNumberResult getUpdateProductStockNumberResult() {
        return updateProductStockNumberResult;
    }

    public void setUpdateProductStockNumberResult(UpdateProductStockNumberResult updateProductStockNumberResult) {
        this.updateProductStockNumberResult = updateProductStockNumberResult;
    }
}
