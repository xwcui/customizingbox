package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.SkuResult;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * 产品上传赛盒 新增的产品集合
 */
public class SkuAddList {

    List<SkuResult> skuResults;

    public SkuAddList() {
    }

    @XmlElement(name="SkuResult")
    public List<SkuResult> getSkuResults() {
        return skuResults;
    }

    public void setSkuResults(List<SkuResult> skuResults) {
        this.skuResults = skuResults;
    }
}
