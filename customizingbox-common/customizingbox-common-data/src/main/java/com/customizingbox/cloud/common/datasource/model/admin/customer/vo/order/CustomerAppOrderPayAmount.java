package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author DC_Li
 * @date 2022/4/20 16:12
 */
@Data
@ApiModel("app支付金额")
public class CustomerAppOrderPayAmount {
    @ApiModelProperty("产品金额")
    private BigDecimal productPrice;

    @ApiModelProperty("运费")
    private BigDecimal shippingPrice;

    @ApiModelProperty("VAT")
    private BigDecimal vatAmount;

    @ApiModelProperty("总支付金额")
    private BigDecimal totalPayMoney;

    @ApiModelProperty("支付交易号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long payTransactionNumber;

    @ApiModelProperty("产品退款金额")
    private BigDecimal productRefundMoney;

    @ApiModelProperty("运费退款金额")
    private BigDecimal shippingRefundMoney;

    @ApiModelProperty("VAT退款金额")
    private BigDecimal vatRefundMoney;

    @ApiModelProperty("退款交易号")
    @JsonSerialize(using = ToStringSerializer.class)
    private List<Long> refundTransactionNumbers;
}
