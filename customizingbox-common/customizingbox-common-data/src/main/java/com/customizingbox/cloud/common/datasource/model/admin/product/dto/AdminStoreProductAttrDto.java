package com.customizingbox.cloud.common.datasource.model.admin.product.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AdminStoreProductAttrDto {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性名
     */
    @ApiModelProperty(value = "属性名")
    private String attrName;
    /**
     * 属性值
     */
    @ApiModelProperty(value = "属性值")
    private List<String> attrValues;

    @ApiModelProperty(value = "排序,与变体中的规格值顺序一致")
    private Integer sort;

}
