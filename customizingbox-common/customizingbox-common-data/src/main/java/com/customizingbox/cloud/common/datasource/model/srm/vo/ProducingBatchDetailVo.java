package com.customizingbox.cloud.common.datasource.model.srm.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/4/29 9:25
 */
@Data
@ApiModel("生产中批次详情")
public class ProducingBatchDetailVo {
    @ApiModelProperty("批次号")
    private String batchId;

    @ApiModelProperty("SPU")
    private String spu;

    @ApiModelProperty("开始生产日期")
    private LocalDateTime startProduceTime;

    @ApiModelProperty("该批次总量")
    private Integer batchProductNum;

    @ApiModelProperty("未发货数")
    private Integer unFulfillNum;
}
