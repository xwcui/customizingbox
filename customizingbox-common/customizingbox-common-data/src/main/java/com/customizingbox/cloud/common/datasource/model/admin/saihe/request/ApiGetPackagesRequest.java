package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by cjq on 2019/5/15.
 */
public class ApiGetPackagesRequest {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";

    GetPackageRequest request;

    @XmlElement(name="request")
    public GetPackageRequest getRequest() {
        return request;
    }

    public void setRequest(GetPackageRequest request) {
        this.request = request;
    }


}
