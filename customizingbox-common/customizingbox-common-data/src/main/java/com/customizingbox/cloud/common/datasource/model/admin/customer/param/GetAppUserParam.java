package com.customizingbox.cloud.common.datasource.model.admin.customer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/5/4 11:56
 */
@Data
@ApiModel("查询app客户参数")
public class GetAppUserParam {
    @ApiModelProperty("客户id")
    private Long appUserId;

    @ApiModelProperty("客户邮箱")
    private String email;
}
