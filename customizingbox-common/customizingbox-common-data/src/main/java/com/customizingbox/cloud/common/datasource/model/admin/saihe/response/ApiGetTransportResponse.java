package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;


import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ApiTransport;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by xiwu
 * 返回运输方式结果实体类
 */
public class ApiGetTransportResponse {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";

    private GetTransportListResult getTransportListResult;


    @XmlElement(name="GetTransportListResult")
    public GetTransportListResult getGetTransportListResult() {
        return getTransportListResult;
    }

    public ApiGetTransportResponse(String Status) {
        this.getTransportListResult=new GetTransportListResult(Status);
    }

    public ApiGetTransportResponse() {
    }

    public void setGetTransportListResult(GetTransportListResult getTransportListResult) {
        this.getTransportListResult = getTransportListResult;
    }
}
