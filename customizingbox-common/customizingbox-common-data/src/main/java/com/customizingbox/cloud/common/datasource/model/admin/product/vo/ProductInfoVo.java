package com.customizingbox.cloud.common.datasource.model.admin.product.vo;

import com.customizingbox.cloud.common.datasource.model.admin.product.dto.AdminStoreProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "产品保存或修改实体")
public class ProductInfoVo {

    @ApiModelProperty(value = "产品信息")
    @NotNull
    private AdminStoreProduct adminStoreProduct;

    @ApiModelProperty(value = "产品规格list")
    private List<AdminStoreProductAttr> adminStoreProductAttrs;

    @ApiModelProperty(value = "变体数据")
    private List<AdminStoreProductVariantDto> adminStoreProductVariantDtos;

    @ApiModelProperty(value = "图片数据")
    @Size(min = 1)
    private List<AdminStoreProductImgDepot> adminStoreProductImgDepots;

}
