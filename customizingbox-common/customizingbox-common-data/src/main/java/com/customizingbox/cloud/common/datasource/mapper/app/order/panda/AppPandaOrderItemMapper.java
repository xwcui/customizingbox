package com.customizingbox.cloud.common.datasource.mapper.app.order.panda;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.product.vo.UniqueProductSearchVo;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.OrderToSaiheOrderItemVo;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.ProductStock;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.SkuResult;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 原平台订单item表
 *
 * @author Y
 * @date 2022-04-12 16:41:47
 */
public interface AppPandaOrderItemMapper extends BaseMapper<AppPandaOrderItem> {

    /**
     * 根据订单获取总产品体积和重量
     * @param pandaOrderId
     * @return
     */
    AdminTransportPriceDTO getOrderWeightAndVolume(@Param(value = "pandaOrderId") Long pandaOrderId);

//    /**
//     * 修改订单中产品映射关系(未支付)
//     * @param storeId 商户店铺id
//     * @param sourceProductId 商户产品id
//     * @param sourceVariantId 商户产品变体id
//     * @param adminProductId admin 产品id
//     * @param adminVariantId admin 产品变体id
//     */
//    void updateOrderItemProductRelevancy(@Param(value = "storeId") Long storeId
//            , @Param(value = "sourceProductId") Long sourceProductId
//            , @Param(value = "sourceVariantId") Long sourceVariantId
//            , @Param(value = "adminProductId") Long adminProductId
//            , @Param(value = "adminVariantId") Long adminVariantId);

    /**
     * 修改订单item 上传赛盒状态和赛盒sku
     * @param skuUpdateList
     * @param date
     * @return
     */
    int updateSaiheSatusAndSaiheSku(@Param("skus") List<SkuResult> skuUpdateList, @Param("date") LocalDateTime date);

    /**
     * 分页
     * @param uniqueProductSearchVo
     * @param page
     * @return
     */
    IPage<UniqueProductPageVo> uniqueProductPage(@Param("searchVo") UniqueProductSearchVo uniqueProductSearchVo, @Param("page") Page page);

    /**
     * 获取上传赛盒库存需要的数据
     * @param onlyCodes
     * @return
     */
    List<ProductStock> getProcessUniqueProductStockNumberToSaiheList(@Param("onlyCodes") List<String> onlyCodes);

    /**
     * 修改上传赛盒产品库存数量状态
     * @param onlyCodesResult
     * @return
     */
    int updateSaiheSatus(@Param("skus") ArrayList<String> onlyCodesResult);

    /**
     * 获取订单上传赛盒的item 只获取 fulfillable_quantity > 0 的
     * @param orderId
     * @return
     */
    List<OrderToSaiheOrderItemVo> getOrderItemByOrderId(@Param("orderId") Long orderId);
}
