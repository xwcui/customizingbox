package com.customizingbox.cloud.common.datasource.model.admin.customer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/23 15:34
 */
@Data
@ApiModel("查询客户产品销量参数")
public class CustomerProductSaleParam {
    @ApiModelProperty(value = "店铺id")
    private Long storeId;

    @ApiModelProperty("店铺名")
    private String storeName;

    @ApiModelProperty(value = "客户id")
    private Long appUserId;

    @ApiModelProperty("客户邮箱")
    private String appUserEmail;
}
