package com.customizingbox.cloud.common.datasource.mapper.admin.transport;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminShTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminShTransportSearchVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 赛合运输方式 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-13
 */
public interface AdminShTransportMapper extends BaseMapper<AdminShTransport> {

    /**
     *  1. 根据赛盒物流修改本地赛盒物流
     *  2. 将本地赛盒物流没有的   satate标记为 0
     * @param shTransports
     */
    int updaAdminShTransport(@Param("shTransports") List<AdminShTransport> shTransports);

    /**
     * 查询分页
     * @param page
     * @param searchVo
     * @return
     * Page<AppSysUserDTO> page, @Param("query") AppSysUserDTO appSysUserDTO
     */
    IPage<AdminShTransport> pageAll(Page<AdminShTransportSearchVo> page, @Param("searchVo") AdminShTransportSearchVo searchVo);
}
