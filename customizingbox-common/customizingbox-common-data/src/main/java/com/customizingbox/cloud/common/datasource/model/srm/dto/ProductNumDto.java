package com.customizingbox.cloud.common.datasource.model.srm.dto;

import lombok.Data;

/**
 * 生产中批次未发货数量或彼此总数
 * @author DC_Li
 * @date 2022/4/29 11:49
 */
@Data
public class ProductNumDto {
    /**
     * 生产批次
     */
    private String batchId;

    /**
     * 产品数量
     */
    private Integer productNum;
}
