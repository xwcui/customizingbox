package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 * 上传订单至ERP系统vo
 */
public class ApiUploadOrderRequest {


    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";


    Request request;

    public ApiUploadOrderRequest() {
    }

    @XmlElement(name="request")
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }



}
