package com.customizingbox.cloud.common.datasource.mapper.admin.customer;

import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerAppOrderQuery;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.CustomerProductSalePurchaseDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.CustomerStoreDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.CustomerStoreProductsDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.param.CustomerProductSaleParam;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.*;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerAppPandaOrderResponseVo;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order.CustomerAppOrderVariants;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author DC_Li
 * @date 2022/4/15 18:06
 */
public interface AdminCustomerManageMapper {
    /**
     * 查询客户店铺信息
     * @param start
     * @param size
     * @param userId
     * @param customerStoreDto
     * @return
     */
    List<CustomerStoreResponseVo> getCustomerStores(@Param("start") long start, @Param("size") long size,
                                                    @Param("userId") Long userId, @Param("customerStoreDto") CustomerStoreDto customerStoreDto);

    /**
     * 查询总数
     * @param start
     * @param size
     * @param userId
     * @param customerStoreDto
     * @return
     */
    Long getCount(@Param("start") long start, @Param("size") long size, @Param("userId") Long userId, @Param("customerStoreDto") CustomerStoreDto customerStoreDto);

    /**
     * 查询客户店铺报价产品id
     * @param start
     * @param size
     * @param userId
     * @param customerStoreProductsDto
     * @return
     */
    List<Long> getCustomerProductIds(@Param("start") long start, @Param("size") long size, @Param("userId") Long userId,
                                     @Param("searchObject") CustomerStoreProductsDto customerStoreProductsDto);

    /**
     * 查询客户报价产品
     * @param customerProductIds  客户店铺产品id
     * @param customerStoreProductsDto
     * @return
     */
    List<CustomerProductResponseVo> getCustomerProducts(@Param("customerProductIds") List<Long> customerProductIds,
                                                        @Param("searchObject") CustomerStoreProductsDto customerStoreProductsDto);

    /**
     * 查询客户报价产品总数
     * @param userId
     * @param customerStoreProductsDto
     * @return
     */
    Long getCustomerProductsCount(@Param("userId") Long userId,
                                                             @Param("searchObject") CustomerStoreProductsDto customerStoreProductsDto);

    /**
     * 查询一个产品的图片url
     * @param storeProductId
     * @return
     */
    String getProductImgSrc(@Param("storeProductId") Long storeProductId);

    /**
     * 查询一个产品的所有变体信息
     * @param productId
     * @return
     */
    List<CustomerProductVariantsResponseVo> getCustomerProductDetail(@Param("productId") Long productId);

    /**
     * 查询报价时已报价产品id
     * @param productId 店铺产品id
     * @return
     */
    List<Long> getQuoteAdminProductIds(@Param("productId") Long productId);

    /**
     * 查询报价时已报价产品详情
     * @param adminProductIds
     * @return
     */
    List<AdminQuoteProductVo> getQuoteAdminProductDetail(@Param("adminProductIds") List<Long> adminProductIds);

    /**
     * 报价时查询admin产品信息
     * @param spu
     * @return
     */
    AdminQuoteProductVo getAdminProduct(@Param("spu") String spu);

    /**
     * 报价时查询admin变体信息
     * @param spu
     * @return
     */
    List<AdminQuoteVariantResponseVo> getAdminVariants(@Param("spu") String spu);

    /**
     * 查询一个产品的所有规格名
     * @param spu
     * @return
     */
    List<String> getProductAttrNames(@Param("spu") String spu);

    /**
     * 查询客户店铺订单
     * @param start
     * @param size
     * @param userId
     * @param query
     * @return
     */
    List<CustomerAppPandaOrderResponseVo> getCustomerStorePandaOrders(@Param("start") long start, @Param("size") long size, @Param("userId") Long userId,
                                                                      @Param("query") CustomerAppOrderQuery query);

    /**
     * 查询客户店铺订单总数
     *
     * @param userId
     * @param query
     * @return
     */
    Long getCustomerStorePandaOrdersCount(@Param("userId") Long userId, @Param("query") CustomerAppOrderQuery query);

    /**
     * 查询一个客户店铺潘达订单
     * @param pandaOrderId
     * @return
     */
    CustomerAppPandaOrderResponseVo getCustomerStorePandaOrder(@Param("pandaOrderId") Long pandaOrderId);

    /**
     * 查询潘达订单的变体
     * @param orderId
     * @param usdRate
     * @return
     */
    List<CustomerAppOrderVariants> getPandaOrderItems(@Param("orderId") Long orderId, @Param("usdRate") BigDecimal usdRate);

    /**
     * 保存报价信息
     * @param requestVo
     * @return
     */
    Boolean saveQuote(@Param("requestVo") List<SaveQuoteRequestVo> requestVo);

    /**
     * 移交客户
     * @param appUserId
     * @param adminUserId
     * @return
     */
    Boolean transferCustomer(@Param("appUserId") Long appUserId, @Param("adminUserId") Long adminUserId);


    /**
     * 查询客户产品总数
     * @param userId
     * @param param
     * @return
     */
    Long getCustomerProductSaleCount(@Param("userId") Long userId, @Param("param") CustomerProductSaleParam param);

    /**
     * 查询客户产品，无销售信息
     * @param start
     * @param size
     * @param userId
     * @param param
     * @return
     */
    List<CustomerProductSaleResponseVo> getCustomerProductSale(@Param("start") long start, @Param("size") long size,
                                                               @Param("userId") Long userId, @Param("param") CustomerProductSaleParam param);

    /**
     * 查询店铺产品来源平台上的销售量
     * @param productIds 客户产品id集合
     * @param time 开始时间
     * @return
     */
    List<CustomerProductSalePurchaseDto> getSaleCondition(@Param("productIds") List<Long> productIds, @Param("time") LocalDateTime time);

    /**
     * 查询客户在我们这里下单的采购量
     * @param productIds 客户产品id集合
     * @param time 开始时间
     * @return
     */
    List<CustomerProductSalePurchaseDto> getPurchaseCondition(@Param("productIds") List<Long> productIds, @Param("time") LocalDateTime time);


    /**
     * 获取产品库一个产品的第一个变体图片
     * @param adminProductId 产品库产品id
     * @return
     */
    String getProductImg(@Param("adminProductId") Long adminProductId);
}
