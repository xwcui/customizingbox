package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

/**
 *
 */
public class ApiUploadOrderList {


    /**
     * 卖家SKU
     */
    String sellerSKU;

    /**
     * 订单产品数量
     */
    Integer productNum;

    /**
     * 订单产品销售单价
     */
    BigDecimal productPrice;

    /**
     * 订单产品运费单价
     */
    BigDecimal shippingPrice;

    /**
     * orderItemId
     */
    String orderItemId;

    @XmlElement(name="SellerSKU")
    public String getSellerSKU() {
        return sellerSKU;
    }

    public void setSellerSKU(String sellerSKU) {
        this.sellerSKU = sellerSKU;
    }

    @XmlElement(name="ProductNum")
    public Integer getProductNum() {
        return productNum;
    }

    public void setProductNum(Integer productNum) {
        this.productNum = productNum;
    }

    @XmlElement(name="ProductPrice")
    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    @XmlElement(name="ShippingPrice")
    public BigDecimal getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(BigDecimal shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    @XmlElement(name="OrderItemId")
    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }
}
