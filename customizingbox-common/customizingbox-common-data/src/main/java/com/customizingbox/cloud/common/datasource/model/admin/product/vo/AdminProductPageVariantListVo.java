package com.customizingbox.cloud.common.datasource.model.admin.product.vo;

import com.customizingbox.cloud.common.datasource.model.admin.product.dto.AdminStoreProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 产品库列表下的变体列表
 */
@Data
@ApiModel(description = "产品下变体信息")
public class AdminProductPageVariantListVo {

    @ApiModelProperty(value = "产品下变体列表")
    private List<AdminStoreProductVariantDto> adminStoreProductVariantDtos;

    @ApiModelProperty(value = "产品规格list")
    private List<AdminStoreProductAttr> adminStoreProductAttrs;

}