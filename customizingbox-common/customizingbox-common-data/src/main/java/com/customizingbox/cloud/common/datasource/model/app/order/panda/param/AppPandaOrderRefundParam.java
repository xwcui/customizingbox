package com.customizingbox.cloud.common.datasource.model.app.order.panda.param;

import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderRefundItemAmountVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(description = "退款管理页面参数")
public class AppPandaOrderRefundParam {

    @ApiModelProperty(value = "退款id")
    private Long id;

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "客户id/邮箱")
    private String customer;

    @ApiModelProperty(value = "申请人")
    private String applyUser;

    @ApiModelProperty(value = "状态:  1 申请中 4 已退款(同意退款) 5 拒绝退款")
    private Integer status;

    @ApiModelProperty(value = "当前登录的用户id, 这个值如果存在, 则只查登录用户申请的数据", hidden = true)
    @Null
    private Long loginUserId;
}