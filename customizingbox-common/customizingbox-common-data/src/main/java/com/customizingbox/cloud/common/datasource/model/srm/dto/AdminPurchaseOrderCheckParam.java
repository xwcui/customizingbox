package com.customizingbox.cloud.common.datasource.model.srm.dto;

import com.customizingbox.cloud.common.datasource.util.EnumValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "退款审核页面参数")
public class AdminPurchaseOrderCheckParam {

    @ApiModelProperty(value = "退款订单id")
    @NotNull
    private Long id;

    @ApiModelProperty(value = "状态:  4:同意退款 5: 拒绝退款")
    @EnumValue(intValues = {4, 5})
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String mark;
}