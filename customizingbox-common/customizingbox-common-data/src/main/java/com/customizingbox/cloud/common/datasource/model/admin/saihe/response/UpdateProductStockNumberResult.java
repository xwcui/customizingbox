package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;

import javax.xml.bind.annotation.XmlElement;

public class UpdateProductStockNumberResult {

    String Status;

    Integer totalSuccess;

    String msg;
    /**
     * 产品库存信息集合
     */
    ProductStockResultList productStockResultList;

    public UpdateProductStockNumberResult() {
    }

    @XmlElement(name="Status")
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @XmlElement(name="ProductStockResultList")
    public ProductStockResultList getProductStockResultList() {
        return productStockResultList;
    }

    public void setProductStockResultList(ProductStockResultList productStockResultList) {
        this.productStockResultList = productStockResultList;
    }

    @XmlElement(name = "TotalSuccess")
    public Integer getTotalSuccess() {
        return totalSuccess;
    }

    public void setTotalSuccess(Integer totalSuccess) {
        this.totalSuccess = totalSuccess;
    }

    @XmlElement(name = "Msg")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
