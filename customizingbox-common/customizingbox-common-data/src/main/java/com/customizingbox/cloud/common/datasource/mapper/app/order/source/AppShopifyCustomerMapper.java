package com.customizingbox.cloud.common.datasource.mapper.app.order.source;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShopifyCustomer;

/**
 * 商户客户表
 *
 * @author Y
 * @date 2022-03-30 13:48:14
 */
public interface AppShopifyCustomerMapper extends BaseMapper<AppShopifyCustomer> {

}
