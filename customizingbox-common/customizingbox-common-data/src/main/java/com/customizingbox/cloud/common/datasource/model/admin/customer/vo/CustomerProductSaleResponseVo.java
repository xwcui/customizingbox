package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/23 15:31
 */
@Data
@ApiModel("客户产品销量数据")
public class CustomerProductSaleResponseVo {
    @ApiModelProperty("产品图片")
    private String productImg;

    @ApiModelProperty("客户名")
    private String appUserName;

    @ApiModelProperty("客户id")
    private String appUserId;

    @ApiModelProperty("客户经理")
    private String adminUserName;

    @ApiModelProperty("店铺类型 1:shopify")
    private Integer storeType;

    @ApiModelProperty("店铺名")
    private String storeName;

    @ApiModelProperty("店铺id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeId;

    @ApiModelProperty("店铺产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appProductId;

    @ApiModelProperty("近1天销量")
    private Integer oneDaySale;

    @ApiModelProperty("近1天采购量")
    private Integer oneDayPurchase;

    @ApiModelProperty("近7天销量")
    private Integer sevenDaySale;

    @ApiModelProperty("近7天采购量")
    private Integer sevenDayPurchase;

    @ApiModelProperty("近15天销量")
    private Integer fifteenDaySale;

    @ApiModelProperty("近15天采购量")
    private Integer fifteenDayPurchase;

}
