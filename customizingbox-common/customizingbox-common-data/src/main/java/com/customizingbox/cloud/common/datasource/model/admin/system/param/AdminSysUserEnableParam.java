package com.customizingbox.cloud.common.datasource.model.admin.system.param;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class AdminSysUserEnableParam {

	@ApiModelProperty(value = "id")
	private String id;

	@ApiModelProperty(value = "状态: 0: 启用, 9: 禁用")
	private String status;
}
