package com.customizingbox.cloud.common.datasource.model.app.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "用户")
@TableName(value = "app_sys_user")
public class AppSysUser extends Model<AppSysUser> {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@ApiModelProperty(value = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;
	/**
	 * 用户名
	 */
	@ApiModelProperty(value = "用户名")
	private String username;

	/**
	 * 邮箱
	 */
	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "密码")
	private String password;
	/**
	 * 随机盐
	 */
	@ApiModelProperty(value = "随机盐")
	private String salt;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	private LocalDateTime updateTime;
	/**
	 * 0-正常，1-删除
	 */
	@ApiModelProperty(value = "0-正常，1-删除")
	@TableLogic
	private String delFlag;

	/**
	 * 锁定标记
	 */
	@ApiModelProperty(value = "锁定标记")
	private String lockFlag;

	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号")
	private String phone;
	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	private String avatar;

	/**
	 * 机构ID
	 */
	@ApiModelProperty(value = "机构ID")
	private String organId;

	@ApiModelProperty(value = "用户余额")
	private BigDecimal balance;

	/**
	 * 租户ID
	 */
	@ApiModelProperty(value = "租户ID")
	private String tenantId;

	@ApiModelProperty("客户经理id")
	private Long adminUserId;

	@ApiModelProperty("ioss")
	private String ioss;

	@ApiModelProperty("字段版本")
	@Version
	private Integer version;
}
