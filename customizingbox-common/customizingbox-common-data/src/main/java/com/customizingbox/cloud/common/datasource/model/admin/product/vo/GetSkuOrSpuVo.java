package com.customizingbox.cloud.common.datasource.model.admin.product.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("获取sku或者spu")
public class GetSkuOrSpuVo {
    /**
     * {type}/{number}
     */
    @ApiModelProperty(value = "需要生成sku时将spu传过来,生成spu不用传")
    @NotNull(message = "spu is null")
    private String spu;

    @ApiModelProperty(value = "number 需要几个数量")
    @NotNull(message = "number is null")
    private Integer number;
}
