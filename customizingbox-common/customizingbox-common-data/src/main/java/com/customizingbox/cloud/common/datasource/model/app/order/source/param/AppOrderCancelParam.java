package com.customizingbox.cloud.common.datasource.model.app.order.source.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Null;

@Data
@ApiModel(value = "app订单查询基础参数")
public class AppOrderCancelParam extends AppOrderBaseParam {


    @ApiModelProperty(hidden = true, value = "app下单状态状态")
    @Null
    private Integer placeStatus;

    @ApiModelProperty(hidden = true, value = "订单状态(1: 正常, 2: 取消)")
    @Null
    private Integer orderStatus;
}
