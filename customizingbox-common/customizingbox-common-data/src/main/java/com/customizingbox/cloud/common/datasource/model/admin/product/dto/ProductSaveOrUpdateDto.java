package com.customizingbox.cloud.common.datasource.model.admin.product.dto;

import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@ApiModel(description = "产品保存或修改实体")
public class ProductSaveOrUpdateDto {

    @ApiModelProperty(value = "产品信息")
    @NotNull
    private AdminStoreProductDto adminStoreProductDto;

    @ApiModelProperty(value = "产品规格list")
    @Size(min = 1)
    private List<AdminStoreProductAttrDto> adminStoreProductAttrDtos;

    @ApiModelProperty(value = "变体数据")
    @Size(min = 1)
    private List<AdminStoreProductVariantDto> adminStoreProductVariantDtos;

    @ApiModelProperty(value = "图片数据")
    @Size(min = 1)
    private List<AdminStoreProductImgDepot> adminStoreProductImgDepots;
}
