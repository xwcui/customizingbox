package com.customizingbox.cloud.common.datasource.mapper.admin.transport;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransport;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportPageVo;
import com.customizingbox.cloud.common.datasource.model.admin.transport.vo.AdminTransportSearchVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 运输方式 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminTransportMapper extends BaseMapper<AdminTransport> {


    IPage<AdminTransportPageVo> pageAll(Page page, @Param("searchVo") AdminTransportSearchVo searchVo);

    List<AdminTransport> getListBySearchVo(@Param("searchVo") AdminTransportSearchVo searchVo);
}
