package com.customizingbox.cloud.common.datasource.model.admin.product.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-31 09:34:38
 */
@Data
@ApiModel(description = "产品属性值表")
public class AdminStoreProductVariantDto {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long imageId;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String imageUrl;


    /**
     * 属性json对象
     */
    @ApiModelProperty(value = "属性json对象")
    private String attrValues;
    /**
     * 成本价
     */
    @ApiModelProperty(value = "成本价")
    private BigDecimal costPrice;
    /**
     * 供货价
     */
    @ApiModelProperty(value = "供货价")
    private BigDecimal supplyPrice;
    /**
     * sku. 系统生成
     */
    @ApiModelProperty(value = "sku. 系统生成")
    private String sku;
    /**
     * 创建者id
     */
    @ApiModelProperty(value = "创建者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改者id
     */
    @ApiModelProperty(value = "修改者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    @ApiModelProperty(value = "逻辑删除标记")
    private String delFlag;

    @ApiModelProperty(value = "上传赛和状态; 1: 已上传, 2: 未上传, 3: 部分上传")
    private Integer upLoadShStatus;

    @ApiModelProperty(value = "上传赛和时间")
    private LocalDateTime upLoadShTime;

    @ApiModelProperty(value = "是否报过价 ture 报过价  false 未报价")
    private Boolean quoteFlag;

    @ApiModelProperty(value = "赛盒sku")
    private String shSku;

}
