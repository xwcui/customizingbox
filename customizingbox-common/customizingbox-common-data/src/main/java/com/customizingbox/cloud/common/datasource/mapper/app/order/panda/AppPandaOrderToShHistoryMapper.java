package com.customizingbox.cloud.common.datasource.mapper.app.order.panda;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderToShHistory;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-05-09
 */
public interface AppPandaOrderToShHistoryMapper extends BaseMapper<AppPandaOrderToShHistory> {

}
