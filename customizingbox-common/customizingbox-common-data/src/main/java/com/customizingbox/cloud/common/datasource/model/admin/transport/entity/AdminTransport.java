package com.customizingbox.cloud.common.datasource.model.admin.transport.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * <p>
 * 运输方式
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_transport")
@ApiModel(value = "运输方式")
public class AdminTransport extends Model<AdminTransport> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输方式名称")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "体积因子")
    private BigDecimal volumeFactor;

    @ApiModelProperty(value = "重量计算方式")
    private Integer completeMode;

    @ApiModelProperty(value = "追踪类型 0 真实追踪号 1   物流商单号")
    private Integer traceType;

    @ApiModelProperty(value = "关联赛和")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer shTransportId;

    @ApiModelProperty(value = "创建者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateId;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "状态. 1: 启用; 0: 禁用")
    private Integer status;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}