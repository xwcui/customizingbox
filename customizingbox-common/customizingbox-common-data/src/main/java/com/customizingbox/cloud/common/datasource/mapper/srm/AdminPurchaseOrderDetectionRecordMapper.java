package com.customizingbox.cloud.common.datasource.mapper.srm;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrderDetectionRecord;
import com.customizingbox.cloud.common.datasource.model.srm.vo.AdminPurchaseOrderDetectionRecordVO;
import com.customizingbox.cloud.common.datasource.model.srm.vo.QualityCheckRecordVo;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 质检记录表 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-05-03
 */
public interface AdminPurchaseOrderDetectionRecordMapper extends BaseMapper<AdminPurchaseOrderDetectionRecord> {
    /**
     * srm不良品查看进度时查询质检历史
     * @param purchaseOrderId
     * @return
     */
    List<QualityCheckRecordVo> selectCheckRecordVos(@Param("purchaseOrderId") Long purchaseOrderId);

    /**
     * 更新进度
     * @param recordId 质检记录id
     * @param progressDeliveryMethod 物流商
     * @param progressDeliveryCode 物流单号
     * @param now 当前时间
     * @param userId
     * @return
     */
    Boolean updateProgress(@Param("recordId") Long recordId, @Param("progressDeliveryMethod") String progressDeliveryMethod,
                           @Param("progressDeliveryCode") String progressDeliveryCode, @Param("now") LocalDateTime now, Long userId);

    /**
     * 查历史质检记录
     * @param purchaseOrderId
     * @return
     */
    List<AdminPurchaseOrderDetectionRecordVO> historyRecord(Long purchaseOrderId);

}
