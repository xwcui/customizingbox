package com.customizingbox.cloud.common.datasource.model.app.order.source.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 用户地址表
 *
 * @author Y
 * @date 2022-03-30 13:48:15
 */
@Data
@TableName("app_shipping_address")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户地址表")
public class AppShippingAddress extends Model<AppShippingAddress> {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主鍵id")
    private Long id;
    /**
     * 
     */
    @ApiModelProperty(value = "全名")
    private String name;
    /**
     * 
     */
    @ApiModelProperty(value = "客户名")
    private String lastName;
    /**
     * 
     */
    @ApiModelProperty(value = "客户姓")
    private String firstName;
    /**
     * 
     */
    @ApiModelProperty(value = "国家")
    private String country;
    /**
     * 
     */
    @ApiModelProperty(value = "国家code")
    private String countryCode;
    /**
     * 
     */
    @ApiModelProperty(value = "省份")
    private String province;
    /**
     * 
     */
    @ApiModelProperty(value = "省code")
    private String provinceCode;
    /**
     * 
     */
    @ApiModelProperty(value = "城市")
    private String city;
    /**
     * 
     */
    @ApiModelProperty(value = "手机号")
    private String phone;
    /**
     * 
     */
    @ApiModelProperty(value = "邮编")
    private String zip;
    /**
     * 
     */
    @ApiModelProperty(value = "地址1")
    private String address1;
    /**
     * 
     */
    @ApiModelProperty(value = "地址2")
    private String address2;
    /**
     * 
     */
    @ApiModelProperty(value = "公司")
    private String company;
    /**
     * 
     */
    @ApiModelProperty(value = "经度")
    private String latitude;
    /**
     * 
     */
    @ApiModelProperty(value = "纬度")
    private String longitude;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 本地创建时间
     */
    @ApiModelProperty(value = "本地创建时间")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime createTime;
    /**
     * 本地修改时间
     */
    @ApiModelProperty(value = "本地修改时间")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime updateTime;
}
