package com.customizingbox.cloud.common.datasource.mapper.app.store;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商户表
 *
 * @author Y
 * @date 2022-03-23 09:51:00
 */
public interface AppStoreMapper extends BaseMapper<AppStore> {

    /**
     * 根据店铺名和平台类型查询店铺(不含租户)
     * @param shop
     * @param platformType
     * @return
     */
    AppStore queryStoreIfExit(@Param(value = "shop") String shop, @Param(value = "platformType") Integer platformType);

    void queryById(@Param(value = "id") Long id);
}
