package com.customizingbox.cloud.common.datasource.mapper.transaction;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.transaction.dto.AppSysUserTransactionMoneyDTO;
import com.customizingbox.cloud.common.datasource.model.app.transaction.entity.AppSysUserTransactionRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 交易记录表
 *
 * @author Y
 * @date 2022-04-21 16:09:21
 */
public interface AppSysUserTransactionRecordMapper extends BaseMapper<AppSysUserTransactionRecord> {

    /**
     * 查询用户总收入, 总支出
     */
    AppSysUserTransactionMoneyDTO queryTotalPayInPayOut(@Param(value = "userId") Long userId);
}
