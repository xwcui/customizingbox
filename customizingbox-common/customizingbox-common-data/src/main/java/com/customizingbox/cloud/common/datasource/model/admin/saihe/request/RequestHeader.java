package com.customizingbox.cloud.common.datasource.model.admin.saihe.request;

/**
 * Created by cjq on 2019/1/16.
 */



import com.customizingbox.cloud.common.datasource.model.admin.saihe.entity.HeaderUserSoapHeader;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "soap:Header")
public class RequestHeader {


    @XmlElement(name="HeaderUserSoapHeader")
    HeaderUserSoapHeader headerUserSoapHeader;

    public HeaderUserSoapHeader getHeaderUserSoapHeader() {
        return headerUserSoapHeader;
    }

    public void setHeaderUserSoapHeader(HeaderUserSoapHeader headerUserSoapHeader) {
        this.headerUserSoapHeader = headerUserSoapHeader;
    }
}
