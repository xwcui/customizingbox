package com.customizingbox.cloud.common.datasource.model.app.order.panda.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@ApiModel(description = "支付订单需要的参数(最外层)")
@Data
public class AppPandaOrderParentPaidParam {

    @ApiModelProperty(value = "product App 产品总金额")
    @NotNull(message = "产品总金额不能为空")
    private BigDecimal productTotalAmount;

    @ApiModelProperty(value = "运费金额")
    @NotNull(message = "运费金额不能为空")
    private BigDecimal freightTotalAmount;

    @ApiModelProperty(value = "vat金额")
    @NotNull(message = "vat金额不能为空")
    private BigDecimal vatTotalAmount;

    @ApiModelProperty(value = "总支付金额")
    @NotNull(message = "总支付金额不能为空")
    private BigDecimal paidTotalAmount;

    @ApiModelProperty(value = "订单详情")
    private List<AppPandaOrderPaidParam> orderPaidParams;

}
