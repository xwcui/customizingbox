package com.customizingbox.cloud.common.datasource.model.admin.transport.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 运输方式page
 */
@Data
public class AdminTransportPageVo {

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "运输方式名称")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "体积因子")
    private BigDecimal volumeFactor;

    @ApiModelProperty(value = "重量计算方式")
    private Integer completeMode;

    @ApiModelProperty(value = "追踪类型 0 真实追踪号 1   物流商单号")
    private Integer traceType;

    @ApiModelProperty(value = "关联赛和")
    private Integer shTransportId;

    @ApiModelProperty(value = "赛和运输方式中文名称")
    private String shTransportName;

    @ApiModelProperty(value = "赛和运输方式英文名称")
    private String shTransportNameEn;

    @ApiModelProperty(value = "运输单元数量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logisticsFreightNumber;

    @ApiModelProperty(value = "创建者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改者id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long updateId;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "状态. 1: 启用; 0: 禁用")
    private Integer status;

    @ApiModelProperty(value = "物流属性(模板)数量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logisticsNumber;

    @ApiModelProperty(value = "物流属性数组")
    private List<String> logisticsIds;


}
