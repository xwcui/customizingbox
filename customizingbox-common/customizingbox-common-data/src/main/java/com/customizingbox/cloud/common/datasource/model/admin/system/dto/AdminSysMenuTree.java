package com.customizingbox.cloud.common.datasource.model.admin.system.dto;

import com.customizingbox.cloud.common.datasource.model.admin.system.vo.AdminMenuVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;


@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "菜单")
public class AdminSysMenuTree extends AdminSysTreeNode {

	@ApiModelProperty(value = "图片")
	private String icon;

	@ApiModelProperty(value = "菜单名称")
	private String name;

	private boolean spread = false;

	@ApiModelProperty(value = "前端URL(这个弃用)")
	private String path;

	@ApiModelProperty(value = "菜单路径")
	private String component;

	@ApiModelProperty(value = "(这个弃用)")
	private String authority;

	@ApiModelProperty(value = "(这个弃用)")
	private String redirect;

	@ApiModelProperty(value = "0-开启，1- 关闭")
	private String keepAlive;

	@ApiModelProperty(value = "")
	private String code;

	@ApiModelProperty(value = "菜单类型 （0菜单 1按钮）")
	private String type;

	@ApiModelProperty(value = "")
	private String label;

	@ApiModelProperty(value = "排序")
	private Integer sort;

	@ApiModelProperty(value = "菜单权限标识")
	private String permission;

	@ApiModelProperty(value = "菜单权限标识编码")
	private String permissionCode;

	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	@ApiModelProperty(value = "修改时间")
	private LocalDateTime updateTime;

	public AdminSysMenuTree() {
	}

	public AdminSysMenuTree(String id, String name, String parentId) {
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.label = name;
	}

	public AdminSysMenuTree(String id, String name, AdminSysMenuTree parent) {
		this.id = id;
		this.parentId = parent.getId();
		this.name = name;
		this.label = name;
	}

	public AdminSysMenuTree(AdminMenuVO menuVo) {
		this.id = menuVo.getId();
		this.parentId = menuVo.getParentId();
		this.icon = menuVo.getIcon();
		this.name = menuVo.getName();
		this.path = menuVo.getPath();
		this.component = menuVo.getComponent();
		this.type = menuVo.getType();
		this.label = menuVo.getName();
		this.sort = menuVo.getSort();
		this.keepAlive = menuVo.getKeepAlive();
		this.permission = menuVo.getPermission();
		this.permissionCode = menuVo.getPermissionCode();
		this.createTime = menuVo.getCreateTime();
		this.updateTime = menuVo.getUpdateTime();
	}
}
