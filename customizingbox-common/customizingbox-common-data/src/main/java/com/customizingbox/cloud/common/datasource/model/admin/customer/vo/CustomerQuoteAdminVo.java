package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 客户报价时产品库的产品信息和变体信息
 * @author DC_Li
 * @date 2022/4/19 14:27
 */
@Data
@ApiModel
public class CustomerQuoteAdminVo {
    @ApiModelProperty("变体信息")
    private List<AdminQuoteVariantResponseVo> variants;

    @ApiModelProperty("产品信息")
    private AdminQuoteProductVo product;

    @ApiModelProperty("美元汇率")
    private BigDecimal usdRate;
}
