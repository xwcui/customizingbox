package com.customizingbox.cloud.common.datasource.util;

import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysMenuTree;
import com.customizingbox.cloud.common.datasource.model.admin.system.dto.AdminSysTreeNode;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysMenu;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;


@UtilityClass
public class AdminSysTreeUtil {
    /**
     * 两层循环实现建树
     *
     * @param treeNodes 传入的树节点列表
     */
    public <T extends AdminSysTreeNode> List<T> build(List<T> treeNodes, Object root) {
        List<T> trees = new ArrayList<>();
        for (T treeNode : treeNodes) {
            if (root.equals(treeNode.getParentId())) {
                trees.add(treeNode);
//				trees.sort(Comparator.comparing(TreeNode::getSort));
            }
            for (T it : treeNodes) {
                if (it.getParentId().equals(treeNode.getId())) {
                    treeNode.addChildren(it);
//					treeNode.getChildren().sort(Comparator.comparing(TreeNode::getSort));
                }
            }
        }
        return trees;
    }

    /**
     * 使用递归方法建树
     */
    public <T extends AdminSysTreeNode> List<T> buildByRecursive(List<T> treeNodes, Object root) {
        List<T> trees = new ArrayList<T>();
        for (T treeNode : treeNodes) {
            if (root.equals(treeNode.getParentId())) {
                trees.add(findChildren(treeNode, treeNodes));
            }
        }
        return trees;
    }

    /**
     * 递归查找子节点
     */
    public <T extends AdminSysTreeNode> T findChildren(T treeNode, List<T> treeNodes) {
        for (T it : treeNodes) {
            if (treeNode.getId().equals(it.getParentId())) {
                if (treeNode.getChildren() == null) {
                    treeNode.setChildren(new ArrayList<>());
                }
                treeNode.addChildren(findChildren(it, treeNodes));
            }
        }
        return treeNode;
    }

    /**
     * 通过sysMenu创建树形节点
     */
    public List<AdminSysMenuTree> buildTree(List<AdminSysMenu> menus, String root) {
        List<AdminSysMenuTree> trees = new ArrayList<>();
        AdminSysMenuTree node;
        for (AdminSysMenu menu : menus) {
            node = new AdminSysMenuTree();
            node.setType(menu.getType());
            node.setId(menu.getId());
            node.setParentId(menu.getParentId());
            node.setName(menu.getName());
            node.setPath(menu.getPath());
            node.setPermission(menu.getPermission());
            node.setPermissionCode(menu.getPermissionCode());
            node.setLabel(menu.getName());
            node.setComponent(menu.getComponent());
            node.setIcon(menu.getIcon());
            node.setSort(menu.getSort());
            node.setKeepAlive(menu.getKeepAlive());
            trees.add(node);
        }
        return AdminSysTreeUtil.build(trees, root);
    }
}
