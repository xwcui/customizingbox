package com.customizingbox.cloud.common.datasource.model.xxljob;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class XxlJobRegistry {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String registryGroup;
    private String registryKey;
    private String registryValue;
    private Date updateTime;


}
