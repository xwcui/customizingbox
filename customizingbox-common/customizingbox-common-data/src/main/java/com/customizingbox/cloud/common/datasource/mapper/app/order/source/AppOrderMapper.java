package com.customizingbox.cloud.common.datasource.mapper.app.order.source;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderQuotationParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderVO;
import org.apache.ibatis.annotations.Param;

/**
 * 原平台订单表(用于给客户展示, 客户可下单至panda订单表)
 *
 * @author Y
 * @date 2022-03-30 13:48:18
 */
public interface AppOrderMapper extends BaseMapper<AppOrder> {

    /**
     * 查询没有下单的订单
     * @param page
     * @return
     */
    IPage<AppQuotationOrderVO> pageNotPlaceOrderAll(Page page, @Param(value = "param") AppOrderQuotationParam param);
}
