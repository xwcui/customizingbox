package com.customizingbox.cloud.common.datasource.mapper.admin.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.AdminStoreProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.dto.ToSaiheProductVariantDto;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.admin.saihe.response.SkuResult;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-31 09:34:38
 */
public interface AdminStoreProductVariantMapper extends BaseMapper<AdminStoreProductVariant> {

    List<AdminStoreProductVariantDto> selectAdminStoreProductVariantDtos(@Param("productId") Long productId);

    int delAdminProductVariantByIds(@Param("ids") List<Long> oldVariantIds);

    List<ToSaiheProductVariantDto> selectToSaiheProductVariantDtos(@Param("productId") Long productId);

    int updateSaiheState(@Param("skus") List<SkuResult> skuUpdateResults, @Param("date") Date date);

    List<ToSaiheProductVariantDto> selectToSaiheUniqueProductVariantDtos(@Param("onlyCodes") List<String> onlyCodes);
}
