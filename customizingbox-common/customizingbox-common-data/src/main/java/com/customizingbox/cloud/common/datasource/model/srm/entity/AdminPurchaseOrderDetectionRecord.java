package com.customizingbox.cloud.common.datasource.model.srm.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 质检记录表
 * </p>
 *
 * @author Z
 * @since 2022-05-03
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("admin_purchase_order_detection_record")
@ApiModel(value = "质检记录表")
public class AdminPurchaseOrderDetectionRecord extends Model<AdminPurchaseOrderDetectionRecord> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "供应商订单id")
    private Long purchaseOrderId;

    @ApiModelProperty(value = "panda订单id")
    private Long orderId;

    @ApiModelProperty(value = "唯一码")
    private String onlyCode;

    @ApiModelProperty(value = "pamda订单itemid")
    private Long orderItemId;

    @ApiModelProperty(value = "质检状态: 1: 通过, 2 不通过")
    private Integer status;

    @ApiModelProperty(value = "采购数量")
    private Integer quantity;

    @ApiModelProperty(value = "不良品数量")
    private Integer rejectsQuantity;

    @ApiModelProperty(value = "良品数量")
    private Integer acceptQuantity;

    @ApiModelProperty(value = "遗失数量")
    private Integer loseQuantity;

    @ApiModelProperty(value = "质检备注")
    private String detectionMark;

    @ApiModelProperty(value = "质检时间")
    private LocalDateTime detectionTime;

    @ApiModelProperty(value = "srm用户填写进度物流商")
    private String progressDeliveryMethod;

    @ApiModelProperty(value = "srm用户填写进度物流单号")
    private String progressDeliveryCode;

    @ApiModelProperty(value = "srm用户填写进度时间")
    private LocalDateTime progressTime;

    @ApiModelProperty(value = "质检员id(admin用户)")
    private Long adminQualityUserId;

    @ApiModelProperty(value = "填写进度用户id")
    private Long srmUserId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
