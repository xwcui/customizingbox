package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author DC_Li
 * @date 2022/4/18 14:39
 */
@Data
@ApiModel("客户店铺产品")
public class CustomerProductResponseVo {
    @ApiModelProperty("产品图")
    private String productPicture;

    @ApiModelProperty("客户名")
    private String customerName;

    @ApiModelProperty("客户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long customerId;

    @ApiModelProperty("客户经理")
    private String customerManager;

    // 1 shopify
    @ApiModelProperty("店铺类型 1:shopify")
    private Integer platformType;

    @ApiModelProperty("店铺名")
    private String storeName;

    @ApiModelProperty("店铺id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeId;

    @ApiModelProperty("店铺商品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long storeProductId;

    @ApiModelProperty("客户店铺变体信息")
    private List<CustomerProductVariant> storeVariants;
}
