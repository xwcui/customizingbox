package com.customizingbox.cloud.common.datasource.model.admin.product.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "产品查询")
public class ProductSearchVo {

    /**
     * 产品报关英文名
     */
    @ApiModelProperty(value = "产品报关英文名")
    private String nameEn;

    /**
     * 产品报关中文名
     */
    @ApiModelProperty(value = "产品报关中文名")
    private String nameCn;

    /**
     * spu
     */
    @ApiModelProperty(value = "spu")
    private String spu;

    /**
     * supplier_id
     */
    @ApiModelProperty(value = "供货商id")
    private Long supplierId;

    /**
     * custom_mode  定制产品; 1: 定制; 2: 非定制
     */
    @ApiModelProperty(value = " custom_mode  定制产品; 1: 定制; 2: 非定制")
    private Integer customMode;
}
