package com.customizingbox.cloud.common.datasource.model.app.product.vo;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *  产品变体列表
 */
@Data
@ApiModel(description = "产品属性值表")
public class AppStoreProductVariantListVO  {

    @ApiModelProperty(value = "app变体id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appVariantId;

    @ApiModelProperty(value = "app变体图片")
    private String appVariantImgSrc;

    @ApiModelProperty(value = "app变体属性值")
    private String appOptions;


    @ApiModelProperty(value = "admin变体id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminVariantId;

    @ApiModelProperty(value = "app变体图片")
    private String adminVariantImgSrc;

    @ApiModelProperty(value = "app变体属性值")
    private String adminOptions;

    @ApiModelProperty(value = "给客户的价格")
    private String cost;
}
