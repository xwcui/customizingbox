package com.customizingbox.cloud.common.datasource.model.admin.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 邮件
 * </p>
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "邮件")
@TableName("admin_sys_email")
public class AdminSysEmail extends Model<AdminSysEmail> {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "PK")
	private String id;

	@ApiModelProperty(value = "收件人")
	private String to;

	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "内容")
	private String content;

}
