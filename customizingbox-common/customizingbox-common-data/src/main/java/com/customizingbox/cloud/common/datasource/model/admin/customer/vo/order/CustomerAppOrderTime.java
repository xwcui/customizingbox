package com.customizingbox.cloud.common.datasource.model.admin.customer.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/4/20 16:12
 */
@Data
@ApiModel("app时间")
public class CustomerAppOrderTime {
    @ApiModelProperty("添加时间")
    private LocalDateTime createTime;

    @ApiModelProperty("付款时间")
    private LocalDateTime payTime;

    @ApiModelProperty("发货时间")
    private LocalDateTime shipTime;
}
