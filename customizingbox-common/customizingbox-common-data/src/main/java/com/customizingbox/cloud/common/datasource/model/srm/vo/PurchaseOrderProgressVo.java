package com.customizingbox.cloud.common.datasource.model.srm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author DC_Li
 * @date 2022/5/5 10:34
 */
@Data
@ApiModel("查看不良品采购单进度响应数据，根据采购单状态判断进度到哪里")
public class PurchaseOrderProgressVo {
    @ApiModelProperty("采购订单状态 1: 待下单, 2: 已下单, 3: 供应商打回, 4:生产中，5:已发货，6:质检不通过，7:已入库，8:订单已取消")
    private Integer purchaseState;

    @ApiModelProperty("采购员")
    private String adminPurchaseUserName;

    @ApiModelProperty("供应商")
    private String supplier;

    @ApiModelProperty("打回原因")
    private String refusedReason;

    @ApiModelProperty("质检员")
    private String qualityCheckUser;

    @ApiModelProperty("开始下单时间")
    private LocalDateTime placePurchaseOrderTime;

    @ApiModelProperty("供应商打回时间")
    private LocalDateTime refusedTime;

    @ApiModelProperty("开始生产时间")
    private LocalDateTime startProduceTime;

    @ApiModelProperty("发货时间")
    private LocalDateTime deliveryTime;

    @ApiModelProperty("质检时间")
    private LocalDateTime qualityCheckTime;

    @ApiModelProperty("不良品记录集合，最后一个的对象没有供应商填写时，采购状态显示质检不通过，有供应商填写时采购状态显示已重发")
    private List<QualityCheckRecordVo> qualityCheckRecords;
}
