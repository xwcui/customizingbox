package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;

import javax.xml.bind.annotation.XmlElement;


public class Result {

    ApiUploadResult apiUploadResult;

    @XmlElement(name="ApiUploadResult")
    public ApiUploadResult getApiUploadResult() {
        return apiUploadResult;
    }

    public void setApiUploadResult(ApiUploadResult apiUploadResult) {
        this.apiUploadResult = apiUploadResult;
    }

}
