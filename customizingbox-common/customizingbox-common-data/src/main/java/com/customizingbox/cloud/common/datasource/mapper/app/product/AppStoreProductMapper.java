package com.customizingbox.cloud.common.datasource.mapper.app.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductListVO;

import java.util.List;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-26 10:32:23
 */
public interface AppStoreProductMapper extends BaseMapper<AppStoreProduct> {

    /**
     * 商品列表
     * @return
     */
    IPage<List<AppStoreProductListVO>> listAll(Page page, String tenantId);

    /**
     * 查询已关联admin的产品列表
     * @param tenantId
     * @return
     */
    List<AppStoreProductListVO> listRelevancyAll(String tenantId);

}
