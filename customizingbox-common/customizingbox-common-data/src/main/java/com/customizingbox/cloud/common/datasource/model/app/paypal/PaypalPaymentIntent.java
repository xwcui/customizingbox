package com.customizingbox.cloud.common.datasource.model.app.paypal;

/**
 * @program:
 * @description:
 * @author: 
 * @create: 
 **/
public enum PaypalPaymentIntent {

    sale, authorize, order
}

