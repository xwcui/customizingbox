package com.customizingbox.cloud.common.datasource.mapper.app.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRoleMenu;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 */
public interface AppSysRoleMenuMapper extends BaseMapper<AppSysRoleMenu> {

}
