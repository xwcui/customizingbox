package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(description = "panda 预下单订单item表")
public class AppPandaPaidOrderVO extends Model<AppPandaPaidOrderVO> {

    @ApiModelProperty(value = "app订单id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "商铺名称")
    private String storeName;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "支付状态")
    private String financialStatus;

    @ApiModelProperty(value = "订单发货状态（1：未发货；2：部分发货；3：全部发货; 4: 已收货）")
    private String freightStatus;

    @ApiModelProperty(value = "收货地址")
    private String address;

    @ApiModelProperty(value = "收货地址id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long addressId;

    @ApiModelProperty(value = "收件人")
    private String username;

    @ApiModelProperty(value = "店铺订单备注")
    private String sourceMark;

    @ApiModelProperty(value = "原平台店铺运输方式")
    private String sourceTrackingCompany;

    @ApiModelProperty(value = "原平台店铺运费")
    private String sourceFreightAmount;

    @ApiModelProperty(value = "可选运输方式下拉列表")
    private List<AdminTransportPriceResultDto> transportPrice;

    @ApiModelProperty(value = "产品总价")
    private BigDecimal productAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "运费")
    private BigDecimal freightAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "vat金额")
    private BigDecimal vatAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "总支付金额")
    private BigDecimal paidAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "变体列表")
    private List<AppPandaPaidOrderItemVO> orderItems = new ArrayList<>();

    @ApiModelProperty(value = "panda订单itemId")
    @JsonIgnore
    private Long pandaOrderItemId;

    @ApiModelProperty(value = "admin产品变体id")
    @JsonIgnore
    private Long adminProductId;

    @ApiModelProperty(value = "关联(有值就说明已经关联了)")
    @JsonIgnore
    private Long adminVariantId;

    @ApiModelProperty(value = "app产品变体id")
    @JsonIgnore
    private Long variantId;

    @ApiModelProperty(value = "发货数量")
    @JsonIgnore
    private Integer fulfillableQuantity;

    @ApiModelProperty(value = "变体图片")
    @JsonIgnore
    private String adminImgSrc;

    @ApiModelProperty(value = "变体属性值")
    @JsonIgnore
    private String adminAttrValues;

    @ApiModelProperty(value = "采购价")
    @JsonIgnore
    private BigDecimal costPrice;

    @ApiModelProperty(value = "供货价")
    @JsonIgnore
    private BigDecimal supplyPrice;

    @ApiModelProperty(value = "报价倍率")
    @JsonIgnore
    private BigDecimal supplyRate;

    @ApiModelProperty(value = "给客户的商品价格")
    @JsonIgnore
    private BigDecimal quotePrice;

    @ApiModelProperty(value = "物流属性id列表")
    @TableField(exist = false)
    private List<Long> logisticsId;


}
