package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 客户报价时查询产品库的产品信息
 * @author DC_Li
 * @date 2022/4/19 14:30
 */
@Data
@ApiModel("产品库产品信息")
public class AdminQuoteProductVo {
    @ApiModelProperty("admin产品id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminProductId;

    @ApiModelProperty("SPU")
    private String spu;

    @ApiModelProperty("产品图")
    private String productImg;

    @ApiModelProperty("中文名")
    private String productCNName;

    @ApiModelProperty("供货信息")
    private String supplierName;

}
