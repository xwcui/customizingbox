package com.customizingbox.cloud.common.datasource.model.app.order.panda.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * 退款item表
 *
 * @author Y
 * @date 2022-04-23 09:55:24
 */
@Data
@TableName("app_panda_order_refund_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退款item表")
public class AppPandaOrderRefundItem extends Model<AppPandaOrderRefundItem> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 退款id
     */
    @ApiModelProperty(value = "退款id")
    private Long refundId;
    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id")
    private Long orderId;
    /**
     * 订单item表id
     */
    @ApiModelProperty(value = "订单item表id")
    private Long orderItemId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long appUserId;
    /**
     * 退款金额
     */
    @ApiModelProperty(value = "退款金额")
    private BigDecimal amount;
    /**
     * 更新版本号
     */
    @ApiModelProperty(value = "更新版本号")
    @Version
    private Integer version;
    /**
     * 创建者id
     */
    @ApiModelProperty(value = "创建者id")
    private Long createId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改者id
     */
    @ApiModelProperty(value = "修改者id")
    private Long updateId;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

}
