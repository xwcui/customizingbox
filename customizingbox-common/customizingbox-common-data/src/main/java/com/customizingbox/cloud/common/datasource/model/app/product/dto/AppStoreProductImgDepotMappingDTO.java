package com.customizingbox.cloud.common.datasource.model.app.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "原数据与本地图片数据映射关系")
public class AppStoreProductImgDepotMappingDTO {

    @ApiModelProperty(value = "主键id")
    private Long sourceId;

    @ApiModelProperty(value = "主键id")
    private Long localId;

}
