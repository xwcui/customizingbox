package com.customizingbox.cloud.common.datasource.model.srm.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author DC_Li
 * @date 2022/4/25 15:12
 */
@Data
@ApiModel("待生产订单产品查询")
public class ReadyProduceQuery {
    @ApiModelProperty("SPU")
    private String spu;

    @ApiModelProperty("唯一码")
    private String onlyCode;

    @ApiModelProperty("批次数量")
    private Integer batchNum;
}
