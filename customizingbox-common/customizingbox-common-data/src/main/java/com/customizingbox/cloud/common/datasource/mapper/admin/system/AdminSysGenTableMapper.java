package com.customizingbox.cloud.common.datasource.mapper.admin.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.system.entity.AdminSysGenTable;


/**
 * 代码生成配置表
 */
public interface AdminSysGenTableMapper extends BaseMapper<AdminSysGenTable> {

}
