package com.customizingbox.cloud.common.datasource.model.admin.setting.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * @author DC_Li
 * @date 2022/4/21 17:36
 */
@Data
@ApiModel("供应商详情")
public class AdminSupplierResponseVoForEdit {
    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "供应商名称")
    private String name;

    @ApiModelProperty(value = "联系人")
    private String contacts;

    @ApiModelProperty(value = "登录手机号码")
    private String phone;

    @ApiModelProperty(value = "收款银行账户")
    private String bankAccount;

    @ApiModelProperty(value = "收款银行信息")
    private String bankInfo;

    @ApiModelProperty(value = "收款人")
    private String payee;

    @ApiModelProperty(value = "采购员,admin用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminPurchaseUserId;
}
