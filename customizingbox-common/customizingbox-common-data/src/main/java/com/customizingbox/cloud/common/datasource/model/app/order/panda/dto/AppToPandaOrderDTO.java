package com.customizingbox.cloud.common.datasource.model.app.order.panda.dto;

import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AppToPandaOrderDTO {

    @ApiModelProperty(value = "订单信息")
    private AppOrder appOrder;

    @ApiModelProperty(value = "订单item信息")
    private List<AppOrderItem> appOrderItems;
}
