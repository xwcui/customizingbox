package com.customizingbox.cloud.common.datasource.model.admin.customer.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author DC_Li
 * @date 2022/4/18 17:44
 */
@Data
@ApiModel("客户商品变体")
public class CustomerProductVariantsResponseVo {
    @ApiModelProperty("客户商品变体图")
    private String variantPic;

    @ApiModelProperty("app店铺变体id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long appVariantId;

    @ApiModelProperty("app店铺变体sku")
    private String appVariantSku;

    @ApiModelProperty("app店铺变体规格")
    private String appVariantOption;

    @ApiModelProperty("报价倍率")
    private BigDecimal quoteRate;

    @ApiModelProperty("上次报价的产品库sku")
    private String sku;

    @ApiModelProperty("上次报价的产品库变体属性值")
    private String attrValues;

    @ApiModelProperty("上次报价的变体的供货价")
    private String supplyPrice;
}
