package com.customizingbox.cloud.common.datasource.mapper.app.order.shopify;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrderItem;

/**
 * shopify 订单item表
 *
 * @author Y
 * @date 2022-03-30 14:19:53
 */
public interface AppShopifyOrderItemMapper extends BaseMapper<AppShopifyOrderItem> {

}
