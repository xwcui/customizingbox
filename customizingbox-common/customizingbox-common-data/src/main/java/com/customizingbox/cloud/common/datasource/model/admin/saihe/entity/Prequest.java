package com.customizingbox.cloud.common.datasource.model.admin.saihe.entity;

import javax.xml.bind.annotation.XmlElement;

public class Prequest {


    String userName;


    String password;


    Integer customerID;

    ImportProductList importProductList;

    public Prequest() {
    }

    @XmlElement(name = "UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlElement(name = "Password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlElement(name = "CustomerID")
    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    @XmlElement(name = "ImportProductList")
    public ImportProductList getImportProductList() {
        return importProductList;
    }

    public void setImportProductList(ImportProductList importProductList) {
        this.importProductList = importProductList;
    }
}
