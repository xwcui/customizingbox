package com.customizingbox.cloud.common.datasource.mapper.app.system;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.admin.customer.dto.AdminCustomerDto;
import com.customizingbox.cloud.common.datasource.model.admin.customer.vo.AdminCustomerResponseVo;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppSysUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 */
public interface AppSysUserMapper extends BaseMapper<AppSysUser> {

    /**
     * 无租户查询
     *
     * @return SysUser
     */
    @InterceptorIgnore(tenantLine="true")
    AppSysUser getByNoTenant(AppSysUser sysUser);

    /**
     * 分页查询用户信息（含角色）
     *
     * @param page    分页
     * @param appSysUserDTO 查询参数
     * @return list
     */
    IPage<List<AppSysUserVO>> getUserVosPage(Page<AppSysUserDTO> page, @Param("query") AppSysUserDTO appSysUserDTO);

    /**
     * 通过ID查询用户信息
     *
     * @param id 用户ID
     * @return userVo
     */
    AppSysUserVO getUserVoById(String id);

    /**
     *
     * @param adminCustomerDto
     * @param userId
     * @return
     */
    /**
     * 查询客户经理的客户
     * @param start 开始
     * @param size 每页条数
     * @param adminCustomerDto
     * @param userId admin用户id，不为空则表示角色为客户经理，只查自己；为空代表是管理员，查所有
     * @return
     */
    List<AdminCustomerResponseVo> pageCustomers(@Param("start") long start, @Param("size") long size,
                                                       @Param("query") AdminCustomerDto adminCustomerDto,
                                                       @Param("userId") Long userId);

    /**
     * 计算客户总数
     * @param query
     * @param userId
     * @return
     */
    Long pageCustomersCount(@Param("query") AdminCustomerDto query, @Param("userId") Long userId);

    /**
     * 查询客户信息
     * @param email
     * @param appUserId
     * @return
     */
    AdminCustomerResponseVo getOneAppUser(@Param("email") String email, @Param("appUserId") Long appUserId);
}
