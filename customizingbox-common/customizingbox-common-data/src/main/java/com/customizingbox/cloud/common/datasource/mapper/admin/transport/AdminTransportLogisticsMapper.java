package com.customizingbox.cloud.common.datasource.mapper.admin.transport;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.admin.transport.entity.AdminTransportLogistics;

/**
 * <p>
 * 运输方式和物流属性关联表 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-15
 */
public interface AdminTransportLogisticsMapper extends BaseMapper<AdminTransportLogistics> {

}
