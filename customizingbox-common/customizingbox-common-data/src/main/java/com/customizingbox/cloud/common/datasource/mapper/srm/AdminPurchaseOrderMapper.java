package com.customizingbox.cloud.common.datasource.mapper.srm;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.srm.dto.ProductNumDto;
import com.customizingbox.cloud.common.datasource.model.srm.entity.AdminPurchaseOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customizingbox.cloud.common.datasource.model.srm.query.*;
import com.customizingbox.cloud.common.datasource.model.srm.vo.*;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.DeliveryProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.ProducingProductVo;
import com.customizingbox.cloud.common.datasource.model.srm.vo.purchaseOrderProduct.PurchaseProductVo;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 采购订单表 Mapper 接口
 * </p>
 *
 * @author Z
 * @since 2022-04-25
 */
public interface AdminPurchaseOrderMapper extends BaseMapper<AdminPurchaseOrder> {
    /**
     * 查询待生产订单数据
     *
     * @param userId
     * @param query
     * @return
     */
    List<PurchaseProductVo> listReadyProduce(@Param("userId") Long userId, @Param("query") ReadyProduceQuery query);

    /**
     * 查询待生产订单数据分页
     *
     * @param page
     * @return
     */
    IPage<PurchaseProductVo> pageReadyProduce(@Param("page") Page page);

    /**
     * 开始生产
     *
     * @param purchaseOrderIds
     * @param batchId
     * @param status           状态
     * @param now              开始生产时间
     * @return
     */
    Boolean startProduce(@Param("purchaseOrderIds") List<Long> purchaseOrderIds, @Param("batchId") Long batchId,
                         @Param("status") Integer status, @Param("now") LocalDateTime now);

    /**
     * 打回
     *
     * @param purchaseOrderId
     * @param refuseReason
     * @param status          状态
     * @param now             打回时间
     * @return
     */
    Boolean refuseProduce(@Param("purchaseOrderId") Long purchaseOrderId, @Param("refuseReason") String refuseReason,
                          @Param("status") Integer status, @Param("now") LocalDateTime now);

    /**
     * 分页查询生产中产品
     *
     * @param page
     * @param userId
     * @param query
     * @return
     */
    IPage<ProducingProductVo> pageProducingProduct(@Param("page") Page page, @Param("userId") Long userId, @Param("query") ProducingProductQuery query);

    /**
     * 查询生产中产品列表
     * @param userId
     * @param query
     * @return
     */
    List<ProducingProductVo> listProducingProduct(@Param("userId") Long userId, @Param("query") ProducingProductQuery query);

    /**
     * 查询批量打印唯一码数据
     *
     * @param userId 当前用户id
     * @param query 查询条件
     * @return
     */
    List<PrintOnlyCodeVo> printOnlyCodeBatch(@Param("userId") Long userId, @Param("query") ProducingProductQuery query);

    /**
     * 查询生产资料
     *
     * @param purchaseOrderIds 采购单id集合
     * @return
     */
    List<ProductionMeans> exportProductionMeansBatch(@Param("purchaseOrderIds") List<Long> purchaseOrderIds);

    /**
     * 生产批次详情
     *
     * @param record
     * @return
     */
    ProducingBatchDetailVo getPorducingBatchDetail(@Param("record") ProducingBatchDetailVo record);
    /**
     * 查询该供应商的包含至少一个生产中订单的批次id
     * @param page
     * @param userId
     * @return
     */
    IPage<ProducingBatchDetailVo> selectBatchIds(@Param("page") Page page, @Param("userId") Long userId);

    /**
     * 根据状态获取生产批次产品数
     *
     * @param batchIds 包含至少一个生产中订单的批次id
     * @param status 4:生产中
     * @return
     */
    List<ProductNumDto> getProductNum(@Param("batchIds") List<String> batchIds, @Param("status") Integer status);

    /**
     * 已发货和已入库采购产品分页查询
     *
     * @param page
     * @param userId
     * @param query
     * @return
     */
    IPage<DeliveryProductVo> pageFulfilled(@Param("page") Page page, @Param("userId") Long userId, @Param("query") FulfilledProductQuery query);

    /**
     * 获取采购单产品信息
     *
     * @param onlyCode
     * @return
     */
    ReadyDeliveryProductVo getProductByOnlyCode(@Param("onlyCode") String onlyCode);

    /**
     * 确认发货
     *
     * @param commitDeliveryVo  发货信息
     * @param deliveryBatchCode 发货批次
     * @param status            状态
     * @param now
     * @return
     */
    Boolean commitDelivery(@Param("commitDeliveryVo") CommitDeliveryRequestVo commitDeliveryVo, @Param("deliveryBatchCode") Long deliveryBatchCode,
                           @Param("status") Integer status, @Param("now") LocalDateTime now);

    /**
     * 不良品列表分页查询
     *
     * @param page
     * @param query
     * @param userId
     * @return
     */
    IPage<DefectiveProductVo> pageDefectiveProducts(@Param("page") Page page, @Param("query") DefectiveProductQuery query, @Param("userId") Long userId);

    /**
     * 分页查询待下单接口
     *
     * @param page
     * @param query
     * @return
     */
    IPage<AdminPurchaseOrderWaitVO> waitOrderPage(@Param("page") Page page, @Param("query") AdminPurchaseOrderWaitQuery query);

    /**
     * 查询进度
     *
     * @param purchaseOrderId 供应商采购单id
     * @return
     */
    PurchaseOrderProgressVo getProgress(@Param("purchaseOrderId") Long purchaseOrderId);

    /**
     * 采购进度汇总
     * @param page
     * @param query
     * @return
     */
    IPage<AdminPurchaseOrderCollectVO> purchaseCollect(@Param("page") Page page, AdminPurchaseOrderCollectQuery query);

    /**
     * 查询质检操作台数据
     * @param onlyCode
     * @return
     */
    AdminPurchaseOrderCheckConsoleVO checkConsole(@Param("onlyCode") String onlyCode);


}
