package com.customizingbox.cloud.common.datasource.model.app.order.panda.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(description = "panda 预下单多订单金额")
public class AppPandaPaidOrderParentVO extends Model<AppPandaPaidOrderParentVO> {

    @ApiModelProperty(value = "总产品价")
    private BigDecimal productTotalAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "运费")
    private BigDecimal freightTotalAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "vat金额")
    private BigDecimal vatTotalAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "总支付金额")
    private BigDecimal totalPaidAmount = BigDecimal.ZERO;

    @ApiModelProperty(value = "订单列表")
    private List<AppPandaPaidOrderVO> paidOrders = new ArrayList<>();
}
