package com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;

/**
 * shopify 订单退款表
 *
 * @author Y
 * @date 2022-03-30 14:19:52
 */
@Data
@TableName("app_shopify_order_refund")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "shopify 订单退款表")
public class AppShopifyOrderRefund extends Model<AppShopifyOrderRefund> {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String adminGraphqlApiId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime createdAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String note;
    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id")
    private Long orderId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private LocalDateTime processedAt;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer restock;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String totalDutiesSet;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Long userId;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String orderAdjustments;
    /**
     * 
     */
    @ApiModelProperty(value = "")
    private String transactions;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


    @TableField(exist=false)
    @ApiModelProperty(value = "订单退款item数据")
    private List<AppShopifyOrderRefund> refundLineItems;

}
