package com.customizingbox.cloud.common.datasource.model.admin.saihe.response;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * 产品新增编辑返回对象
 */
public class ApiUploadProductsResponse {

    @XmlAttribute(name="xmlns")
    protected String xmlns="http://tempuri.org/";

    private ProcessUpdateProductResult processUpdateProductResult;

    public ApiUploadProductsResponse() {
    }

    public ApiUploadProductsResponse(String status) {
        this.processUpdateProductResult=new ProcessUpdateProductResult(status);
    }

    @XmlElement(name="ProcessUpdateProductResult")
    public ProcessUpdateProductResult getProcessUpdateProductResult() {
        return processUpdateProductResult;
    }

    public void setProcessUpdateProductResult(ProcessUpdateProductResult processUpdateProductResult) {
        this.processUpdateProductResult = processUpdateProductResult;
    }
}
