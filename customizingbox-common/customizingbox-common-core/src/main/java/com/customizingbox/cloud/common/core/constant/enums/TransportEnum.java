package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class TransportEnum {

    /**
     * 赛盒获取到的运输方式是否包含此运输方式 0 不包含 1 包含
     */
    @Getter
    @AllArgsConstructor
    public enum SaiheTransportState {
        NOT_INCLUDED(0, "不包含"),
        CONTAIN(1, "包含"),
        ;
        private final Integer code;
        private final String msg;
    }

    /**
     * 物流模块开启或禁用美剧
     */
    @Getter
    @AllArgsConstructor
    public enum TransportStatusEnum {
        SHUT(0, "关闭"),
        OPEN(1, "开启"),
        ;
        private final Integer code;
        private final String msg;
    }

    /**
     * 体积因子
     */
    @Getter
    @AllArgsConstructor
    public enum TransportVolumeFactor {
        SHUT(0, "体积重"),
        OPEN(1, "实重"),
        ;
        private final Integer code;
        private final String msg;
    }

    /**
     * 追踪类型
     */
    @Getter
    @AllArgsConstructor
    public enum TransportTraceType {
        SHUT(0, "真实追踪号"),
        OPEN(1, "物流商单号"),
        ;
        private final Integer code;
        private final String msg;
    }

}
