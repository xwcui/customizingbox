package com.customizingbox.cloud.common.core.util;


import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.TreeMap;

@Slf4j
public class HttpParamUtil {

    private static final String UNKNOWN = "unknown";

    public static TreeMap<String, String> getFormParam(HttpServletRequest request) {
        TreeMap<String, String> param = new TreeMap<>();
        try {
            for (Map.Entry<String, String[]> requestParamsEntry : request.getParameterMap().entrySet()) {
                param.put(requestParamsEntry.getKey(), requestParamsEntry.getValue()[0]);
            }
        } catch (Exception e) {
            log.error("error", e);
        }
        return param;
    }

    public static String getJsonParam(HttpServletRequest request) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            log.error("error", e);
        } finally {
            try {
                if (null != reader) {
                    reader.close();
                }
            } catch (Exception e) {
                log.error("error", e);
            }
        }
        return sb.toString();
    }

}
