package com.customizingbox.cloud.common.core.constant.enums;

public enum ApiResStatus {
    /**
     * 成功
     */
    OK(200, "操作成功"),
    FAIL(500, "操作失败"),

    INVALID_TOKEN(401, "登录过期, 请重新登录"),

    TO_ORDER_ERROR(600, "下单异常, 请重新下单"),

    NOT_UFFICIENT_FUNDS(1001, "余额不足"),
    BALANCE_ERROR(1002, "余额相关异常"),

    ORDER_DATA_ANALYSIS_ERROR(500, "订单数据解析失败")

    ;


    private final Integer status;

    private final String message;

    ApiResStatus(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    ApiResStatus(String message) {
        this.status = 500;
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
