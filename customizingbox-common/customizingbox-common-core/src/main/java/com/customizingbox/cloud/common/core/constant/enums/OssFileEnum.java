package com.customizingbox.cloud.common.core.constant.enums;

public enum OssFileEnum {

    /**
     * 文件对应 ossPath
     * 1：产品
     */
    PRODUCT(1, "product"),
    ;

    private final Integer code;

    private final String path;

    OssFileEnum(Integer code, String path) {
        this.code = code;
        this.path = path;
    }

    public Integer getCode() {
        return code;
    }

    public String getPath() {
        return path;
    }


    public static String getMsg(Integer code) {
        for (OssFileEnum ossFileEnum : OssFileEnum.values()) {
            if (ossFileEnum.code.equals(code)) {
                return ossFileEnum.getPath();
            }
        }
        return "default";
    }


}
