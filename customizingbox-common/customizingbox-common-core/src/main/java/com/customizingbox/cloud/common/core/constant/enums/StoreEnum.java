package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class StoreEnum {

    @Getter
    @AllArgsConstructor
    public enum State {
        NORMAL(1, "正常"),
        DISABLED(2, "禁用"),
        ;
        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum Type {
        SHOPIFY(1, "shopify"),
        ;
        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum Topic {
        APP_UNINSTALLED("app/uninstalled", ""),
        ORDERS_CANCELLED("orders/cancelled", ""),
        ORDERS_CREATE("orders/create", "订单创建"),
        ORDERS_DELETE("orders/delete", "订单删除"),
        ORDERS_FULFILLED("orders/fulfilled", ""),
        ORDERS_PAID("orders/paid", "支付订单"),
        ORDERS_PARTIALLY_FULFILLED("orders/partially_fulfilled", ""),
        ORDERS_UPDATED("orders/updated", ""),
        PRODUCTS_CREATE("products/create", "产品创建"),
        PRODUCTS_UPDATE("products/update", "产品修改"),
        PRODUCTS_DELETE("products/delete", "产品删除"),
        REFUNDS_CREATE("refunds/create", ""),
        ORDER_TRANSACTIONS_CREATE("order_transactions/create", ""),
        LOCATIONS_CREATE("locations/create", ""),
        LOCATIONS_DELETE("locations/delete", ""),
        LOCATIONS_UPDATE("locations/update", ""),

        ;
        private final String type;
        private final String msg;
    }
}
