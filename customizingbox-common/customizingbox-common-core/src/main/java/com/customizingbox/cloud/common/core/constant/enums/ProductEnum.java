package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

public class ProductEnum {

    @Getter
    @AllArgsConstructor
    public enum Status {
        DRAFT(1, "draft", "未上架, 草稿"),
        ACTIVE(2, "active", "上架"),
        UNKNOWN(-1, "unknown", "未知");
        ;
        private final Integer code;
        private final String name;
        private final String msg;

        public static Status getStatus(String key) {
            return Arrays.asList(Status.values()).stream()
                    .filter(status -> status.getName().equals(key))
                    .findFirst().orElse(Status.UNKNOWN);
        }

    }

    /**
     * 是否报价
     */
    @Getter
    @AllArgsConstructor
    public enum quoteFlag {
        QUOTED(true, "已报价"),
        NO_QUOTATION(false, "未报价"),
        ;
        private final Boolean code;
        private final String msg;
    }

    /**
     * 是否逻辑删除
     */
    @Getter
    @AllArgsConstructor
    public enum delFlag {
        DEL(1, "删除"),
        SHOW(0, "展示"),
        ;
        private final Integer code;
        private final String msg;
    }

    /**
     * 是否逻辑删除
     */
    @Getter
    @AllArgsConstructor
    public enum uploadShStatus {
        UPLOAD(1, "已上传"),
        NO_UPLOAD(2, "未上传"),
        PART_UPLOAD(3, "部分上传"),
        ;
        private final Integer code;
        private final String msg;
    }

    /**
     * 定制产品状态
     */
    @Getter
    @AllArgsConstructor
    public enum CUSTOM_MODE {
        CUSTOMIZATION(1, "定制产品"),
        GENERAL(2, "非定制/普通产品"),
        ;
        private final Integer code;
        private final String msg;
    }
}
