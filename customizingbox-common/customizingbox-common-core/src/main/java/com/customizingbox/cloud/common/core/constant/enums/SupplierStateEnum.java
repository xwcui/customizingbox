package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author DC_Li
 * @date 2022/5/10 17:49
 */
@Getter
@AllArgsConstructor
public enum  SupplierStateEnum {
    ENABLE(1,"正常"),
    DISABLE(2,"禁用");

    private final Integer status;
    private final String msg;
}
