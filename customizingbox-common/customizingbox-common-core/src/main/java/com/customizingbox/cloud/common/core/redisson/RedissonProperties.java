package com.customizingbox.cloud.common.core.redisson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * @Description: 读取redis配置信息，封装到当前实体中
 */
@PropertySource({"classpath:redisson.properties"})
@ConfigurationProperties(prefix = "spring.redis")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RedissonProperties {

    /**
     * redis主机地址，ip：port，有多个用半角逗号分隔
     */
    private String host;
    /**
     * redis 连接密码
     */
    private String password;

    /**
     * redis 连接密码
     */
    private String port;

    /**
     * 选取那个数据库
     */
    private int database;

    public RedissonProperties setPassword(String password) {
        this.password = password;
        return this;
    }

    public RedissonProperties setDatabase(int database) {
        this.database = database;
        return this;
    }

}
