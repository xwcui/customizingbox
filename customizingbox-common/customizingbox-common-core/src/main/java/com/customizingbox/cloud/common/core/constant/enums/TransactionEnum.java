package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

public class TransactionEnum {

    // 交易类型(1: 充值; 2: 支出)
    @Getter
    @AllArgsConstructor
    public enum TYPE {
        PAY_IN(1, "充值/收入"),
        PAY_OUT(2, "支出"),
        ;
        private final Integer code;
        private final String msg;

        public static TransactionEnum.TYPE getCode(Integer code) {
            return Arrays.asList(TransactionEnum.TYPE.values()).stream()
                    .filter(status -> status.getCode().equals(code))
                    .findFirst().orElse(null);
        }
    }

    // 来源类型, 1: 充值, 2: 订单支付, 3: 订单退款
    @Getter
    @AllArgsConstructor
    public enum SOURCE_TYPE {
        PAY_BALANCE(1, "充值"),
        ORDER_PAY(2, "订单支付"),
        ORDER_REFUND(3, "订单退款"),
        ;
        private final Integer code;
        private final String msg;

        public static TransactionEnum.SOURCE_TYPE getCode(Integer code) {
            return Arrays.asList(TransactionEnum.SOURCE_TYPE.values()).stream()
                    .filter(status -> status.getCode().equals(code))
                    .findFirst().orElse(null);
        }
    }

    // 货币代码和描述
    @Getter
    @AllArgsConstructor
    public enum CURRENCY {
         USD("USD","美元");
         private final String code;
         private final String description;
    }
}
