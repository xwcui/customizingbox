package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author DC_Li
 * @date 2022/4/18 11:09
 */
@Getter
@AllArgsConstructor
public enum ShopifyRestApiEnum {
    // 店铺属性
    SHOP_PROPERTY("https://%s/admin/api/2022-04/shop.json", "店铺属性"),
    // 订单总数
    ORDER_COUNT("https://%s/admin/api/2022-04/orders/count.json?status=any", "订单总数");
    private final String url;
    private final String comment;
}
