package com.customizingbox.cloud.common.core.constant;


public interface SecurityConstants {

    /**
     * 短信登录验证码
     */
    String SMS_LOGIN = "sms_login";

    /**
     * 刷新
     */
    String REFRESH_TOKEN = "refresh_token";

    /**
     * 验证码有效期（5分钟）
     */
    int CODE_TIME = 300;

    /**
     * 角色前缀
     */
    String ROLE = "ROLE_";

    /**
     * 前缀
     */
    String BASE_PREFIX = "base_";

    /**
     * oauth 相关前缀
     */
    String OAUTH_PREFIX = "oauth:";

    /**
     * 项目的 license
     */
    String LICENSE = "Copyright © customizingbox";

    /**
     * OAUTH URL
     */
    String OAUTH_TOKEN_URL = "/oauth/token";

    /**
     * {bcrypt} 加密的特征码
     */
    String BCRYPT = "{bcrypt}";

    /**
     * sys_oauth_client 表的字段，id、client_secret
     */
    String CLIENT_FIELDS = "id, CONCAT('{noop}',client_secret) as client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    /**
     * JdbcClientDetailsService 查询语句
     */
    String BASE_FIND_STATEMENT_ADMIN = "select " + CLIENT_FIELDS + " from admin_sys_oauth_client";

    String BASE_FIND_STATEMENT_APP = "select " + CLIENT_FIELDS + " from app_sys_oauth_client";

    /**
     * 默认的查询语句
     */
    String DEFAULT_FIND_STATEMENT_ADMIN = BASE_FIND_STATEMENT_ADMIN + " order by id";
    String DEFAULT_FIND_STATEMENT_APP = BASE_FIND_STATEMENT_APP + " order by id";

    /**
     * 按条件client_id 查询
     */
    String DEFAULT_SELECT_STATEMENT_ADMIN = BASE_FIND_STATEMENT_ADMIN + " where id = ?";
    String DEFAULT_SELECT_STATEMENT_APP = BASE_FIND_STATEMENT_APP + " where id = ?";


    /**
     * 客户端模式
     */
    String CLIENT_CREDENTIALS = "client_credentials";

    /**
     * 用户ID字段
     */
    String DETAILS_USER_ID = "user_id";

    /**
     * 用户名字段
     */
    String DETAILS_USERNAME = "username";

    /**
     * 用户机构字段
     */
    String DETAILS_ORGAN_ID = "organ_id";

    /**
     * 租户ID 字段
     */
    String DETAILS_TENANT_ID = "tenant_id";

    /**
     * 协议字段
     */
    String DETAILS_LICENSE = "license";

    String DETAILS_ROLE_ADMINISTRATOR  = "isAdministrator";

    /**
     * 查看客户权限.
     */
    String IS_CUSTOMER_MANAGER = "customerDataAuth";

    /**
     * 查看客户权限.
     */
    String IS_PURCHASE_MANAGER = "purchaseDataAuth";

    /**
     * shCode
     */
    String SAI_HE_CODE = "shCode";

}
