package com.customizingbox.cloud.common.core.util;

import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


@ApiModel(value = "响应包装类")
public class ApiResponse<T> implements Serializable {

    /**
     * 状态代码
     */
    @ApiModelProperty("状态代码")
    protected Integer status;

    /**
     * 消息内容
     */
    @ApiModelProperty("消息内容")
    protected String message;

    /**
     * 消息主体业务对象
     */
    @ApiModelProperty("消息主体业务对象")
    protected T data;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
        this.status = ApiResStatus.OK.getStatus();
        this.message = ApiResStatus.OK.getMessage();
    }

    /**
     * 简单构造，返回一个成功信息，不带业务对象
     */
    public ApiResponse() {
    }

    /**
     * 简单构造，返回一个成功信息，不带业务对象
     */
    public ApiResponse(ApiResStatus status) {
        this.status = status.getStatus();
        this.message = status.getMessage();
    }

    /**
     * 简单构造，返回执行结果信息，不带业务对象g
     */
    public ApiResponse(Integer status, String msg) {
        this.status = status;
        this.message = msg;
    }

    /**
     * 简单构造，返回执行结果信息，带业务对象
     */
    public ApiResponse(Integer status, String msg, T data) {
        this.status = status;
        this.message = msg;
        this.data = data;
    }

    private ApiResponse(T data, Integer status, String msg) {
        this.data = data;
        this.status = status;
        this.message = msg;
    }

    public synchronized static <T> ApiResponse<T> ok(T data) {
        ApiResponse<T> res = new ApiResponse<>();
        res.setStatus(ApiResStatus.OK.getStatus());
        res.setMessage(ApiResStatus.OK.getMessage());
        res.setData(data);
        return res;
    }

    public synchronized static <T> ApiResponse<T> ok(T data, String message) {
        ApiResponse<T> res = new ApiResponse<>();
        res.setStatus(ApiResStatus.OK.getStatus());
        res.setMessage(message);
        res.setData(data);
        return res;
    }

    public synchronized static <T> ApiResponse<T> ok() {
        ApiResponse<T> res = new ApiResponse<>();
        res.setStatus(ApiResStatus.OK.getStatus());
        res.setMessage(ApiResStatus.OK.getMessage());
        res.setData(null);
        return res;
    }

    public synchronized static <T> ApiResponse<T> failed(String msg) {
        ApiResponse<T> res = new ApiResponse<>();
        res.setStatus(ApiResStatus.FAIL.getStatus());
        res.setMessage(msg);
        return res;
    }

    public synchronized static <T> ApiResponse<T> orderFailed(String msg) {
        ApiResponse<T> res = new ApiResponse<>();
        res.setStatus(ApiResStatus.TO_ORDER_ERROR.getStatus());
        res.setMessage(msg);
        return res;
    }

    public synchronized static <T> ApiResponse<T> over(T data, Integer status, String msg) {
        ApiResponse<T> res = new ApiResponse<>();
        res.setStatus(status);
        res.setMessage(msg);
        res.setData(data);
        return res;
    }

    public synchronized static <T> ApiResponse<T> over(T data, ApiResStatus status) {
        ApiResponse<T> res = new ApiResponse<>();
        res.setData(data);
        res.setStatus(status.getStatus());
        res.setMessage(status.getMessage());
        return res;
    }

    public synchronized static <T> ApiResponse<T> over(ApiResStatus status) {
        ApiResponse<T> res = new ApiResponse<>();
        res.setStatus(status.getStatus());
        res.setMessage(status.getMessage());
        return res;
    }

    public synchronized static <T> ApiResponse<T> execute(boolean result) {
        if (result) {
            return ApiResponse.ok();
        } else {
            return ApiResponse.failed("操作失败");
        }
    }

    public synchronized static <T> ApiResponse<T> execute(boolean result, String msg) {
        if (result) {
            return ApiResponse.ok();
        } else {
            return ApiResponse.failed(msg);
        }
    }

}
