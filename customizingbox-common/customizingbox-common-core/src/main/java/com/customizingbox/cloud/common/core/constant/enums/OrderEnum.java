package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class OrderEnum {

    // 订单状态(1: 正常, 2: 取消)
    @Getter
    @AllArgsConstructor
    public enum STATUS {
        ACTIVITY(1, "正常"),
        CANCELLED(2, " 取消"),
        ;

        private final Integer code;
        private final String msg;
    }

    // 和admin产品关联状态; 1: 全部关联 2: 存在未关联
    @Getter
    @AllArgsConstructor
    public enum RELEVANCY_STATUS {
        ALL_RELEVANCY(1, "全部关联"),
        PORTION_RELEVANCY(2, "部分关联"),
        ALL_NOT_RELEVANCY(3, "全部未关联"),
        ;
        private final Integer code;
        private final String msg;
    }
    @Getter
    @AllArgsConstructor
    public enum PAY_STATUS {
        NOT_PAY(1, "未支付"),
        SUCCESS_PAY(2, " 已支付"),
        ;

        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum PLACE_STATUS {
        NOT_PLACE(1, "未下单"),
        PORTION_PLACE(2, "部分下单"),
        SUCCESS_PLACE(3, "全部下单"),
        ;

        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum FREIGHT_STATUS {
        WAIT_OUT(1, "待发货"),
        WAIT_IN(2, "待收货"),
        SUCCESS(3, "已收货"),
        ;

        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum REFUND_STATUS {

        NOT(0, "未退款(没有退款)"),
        APPLY(1, "申请中"),
        UNDERWAY(2, "退款中"),
        PORTION(3, "部分退款"),
        ALL_REFUND(4, "全部退款, 退款成功"),
        REJECT_REFUND(5, "拒绝退款"),
        ;

        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum PURCHASE_ORDER_STATUS {
        WAIT_ORDER(1, "待下单"),
        PLACE_ORDER(2, "已下单"),
        PURCHASE_REJECT(3, "供应商打回"),
        PRODUCTION(4, "生产中"),
        SHIPMENT(5, "发货"),
        QUALITY_FAIL(6, "质检不通过"),
        SUCCESS(7, "已入库"),
        CANCEL(8, "订单取消")
        ;

        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum SH_UPLOAD_STATUS {
        SUCCESS(1, "上传成功"),
        NOT(2, "未上传"),
        FAILED(3, "上传失败"),
        ;

        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum ORDER_PRODUCT_SH_UPLOAD_STATUS {
        SUCCESS(1, "已上传"),
        NOT(2, "未上传"),
        PART(3, "部分上传"),
        ;

        private final Integer code;
        private final String msg;
    }

    @Getter
    @AllArgsConstructor
    public enum PAY_TYPE {
        BALANCE(1, "余额支付")
        ;

        private final Integer code;
        private final String msg;
    }



}
