package com.customizingbox.cloud.common.core.constant;


public interface CommonConstants {

    /**
     * 租户1的ID
     */
    String TENANT_ID_1 = "1";

    /**
     * header 中 Authorization
     */
    String AUTHORIZATION = "Authorization";


    /**
     * 正常
     */
    String STATUS_NORMAL = "0";

    /**
     * 锁定
     */
    String STATUS_LOCK = "9";

    /**
     * 日志类型：正常操作日志
     */
    String LOG_TYPE_0 = "0";

    /**
     * 日志类型：异常操作日志
     */
    String LOG_TYPE_9 = "9";

    /**
     * 菜单
     */
    String MENU = "0";

    /**
     * 按钮
     */
    String BUTT = "1";

    /**
     * 编码
     */
    String UTF8 = "UTF-8";

    /**
     * 前端工程名
     */
    String FRONT_END_PROJECT = "customizingbox-web";

    /**
     * 后端工程名
     */
    String BACK_END_PROJECT = "customizingbox-app";

    /**
     * 树形父类ID
     */
    String PARENT_ID = "0";

    /**
     * 数据权限类型
     * 0全部，1自定义，2本级及子级，3本级
     */
    Integer DS_TYPE_0 = 0;
    Integer DS_TYPE_1 = 1;
    Integer DS_TYPE_2 = 2;
    Integer DS_TYPE_3 = 3;

    /**
     * 管理员角色编码
     */
    String ROLE_CODE_ADMIN = "ROLE_ADMIN";


    String CONFIG_DATA_ID = "dynamic_routes";

    String CONFIG_GROUP = "DEFAULT_GROUP";

    long CONFIG_TIMEOUT_MS = 5000;

    String ONLY_CODE_PREFIX = "div";

}
