package com.customizingbox.cloud.common.core.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.customizingbox.cloud.common.core.constant.enums.OssFileEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

@Slf4j
public class OSSFileUtil {

    private static final String BUCKET_NAME = "customizingbox-app";

    private static final String ENDPOINT = "oss-us-west-1.aliyuncs.com";

    private static final String ACCESS_KEY_ID = "LTAI5tRP8aGeb31JnR9jHaea";

    private static final String ACCESS_KEY_SECRET = "mmC0EuiD6lclpFJVXiwLAg7Dl85coA";

    private static final String URL_PRE = "https://customizingbox-app.oss-us-west-1.aliyuncs.com/";


    public static String uploadFile(MultipartFile file, Integer code) {

        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
            String ossPath = OssFileEnum.getMsg(code);
            String originalFileName = file.getOriginalFilename();
            assert originalFileName != null;
            String suffix = originalFileName.substring(originalFileName.lastIndexOf("."));
            String fileName = System.currentTimeMillis() + new Random().nextInt(10000) + suffix;
            String contentType = getContentType(suffix);

            ossPath = ossPath + "/" + fileName;

            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(contentType);

            OSS ossClient = new OSSClientBuilder().build(ENDPOINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
            ossClient.putObject(BUCKET_NAME, ossPath, inputStream, metadata);
            ossClient.shutdown();

            String url = URL_PRE + ossPath;
            log.info("<--oss uploadFile url:{}-->", url);
            return url;
        } catch (IOException e) {
            log.error("error", e);
            throw new ApiException("文件上传失败");
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                log.error("error", e);
            }
        }
    }


    /**
     * Description: 判断OSS服务文件上传时文件的contentType
     *
     * @param fileSuffix 文件后缀
     * @return String
     */
    public static String getContentType(String fileSuffix) {
        if (fileSuffix.equalsIgnoreCase(".bmp")) {
            return "image/bmp";
        }
        if (fileSuffix.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (fileSuffix.equalsIgnoreCase(".jpg")
                || fileSuffix.equalsIgnoreCase(".png")) {
            return "image/jpg";
        }
        if (fileSuffix.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (fileSuffix.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (fileSuffix.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (fileSuffix.equalsIgnoreCase(".pptx")
                || fileSuffix.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (fileSuffix.equalsIgnoreCase(".docx")
                || fileSuffix.equalsIgnoreCase(".doc")) {
            return "application/msword";
        }
        if (fileSuffix.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        if (fileSuffix.equalsIgnoreCase(".pdf")) {
            return "application/pdf";
        }
        return "image/jpg";
    }


}
