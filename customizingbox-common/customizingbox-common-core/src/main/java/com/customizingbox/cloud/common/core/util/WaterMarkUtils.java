package com.customizingbox.cloud.common.core.util;

import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

@Slf4j
public class WaterMarkUtils {

	/**
	 * 图片添加水印
	 *
	 * @param imgFile          需要添加水印的图片
	 * @param markContentColor 水印文字的颜色
	 * @param waterMarkContent 水印的文字
	 * @return 水印图片
	 */
	public static File markStr(File imgFile, Color markContentColor, String waterMarkContent) {
		try {
			// 加水印
			BufferedImage bufImg = ImageIO.read(imgFile);
			//图片宽
			int width = bufImg.getWidth();
			//图片高
			int height = bufImg.getHeight();
			Graphics2D g = bufImg.createGraphics();
			g.drawImage(bufImg, 0, 0, width, height, null);
			Font font = new Font("微软雅黑", Font.ITALIC, 45);
			// 根据图片的背景设置水印颜色
			g.setColor(markContentColor);

			g.setFont(font);
			//这是一个计算水印位置的函数，可以根据需求添加
			int x = width - 2 * getWatermarkLength(waterMarkContent, g);
			int y = height - getWatermarkLength(waterMarkContent, g);
			g.drawString(waterMarkContent, x, y);
			g.dispose();

			ImageIO.write(bufImg, "png", imgFile);
			return imgFile;
		} catch (Exception e) {
			log.error("error", e);
		}
		return null;
	}

	/**
	 * 获取水印文字总长度
	 *
	 * @param waterMarkContent 水印的文字
	 * @param g
	 * @return 水印文字总长度
	 */
	public static int getWatermarkLength(String waterMarkContent, Graphics2D g) {
		return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());
	}
}
