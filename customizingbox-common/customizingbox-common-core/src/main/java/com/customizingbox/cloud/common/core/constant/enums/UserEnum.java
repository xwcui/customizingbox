package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

public class UserEnum {

    // 用户类型; 1: 客户经理, 2: 采购员, 3: (srm)工厂用户
    @Getter
    @AllArgsConstructor
    public enum TYPE {
        CONSUMER_MANAGER(1, "客户经理"),
        PURCHASE(2, "采购员"),
        SRM(3, "(srm)工厂用户"),
        ;
        private final Integer code;
        private final String msg;
    }
}
