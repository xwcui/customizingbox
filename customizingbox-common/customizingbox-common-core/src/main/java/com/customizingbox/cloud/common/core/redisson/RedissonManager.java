package com.customizingbox.cloud.common.core.redisson;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.config.Config;


/**
 * @Description: Redisson核心配置，用于提供初始化的redisson实例
 */
@Slf4j
public class RedissonManager {


    private Config config = new Config();

    private Redisson redisson = null;

    public RedissonManager() {
    }

    public RedissonManager(RedissonProperties redissonProperties) {
        try {
            //通过不同部署方式获得不同cofig实体
            config = RedissonConfigFactory.getInstance().createConfig(redissonProperties);
            redisson = (Redisson) Redisson.create(config);
        } catch (Exception e) {
            log.error("Redisson init error", e);
            throw new IllegalArgumentException("please input correct configurations," +
                    "connectionType must in standalone/sentinel/cluster/masterslave");
        }
    }

    public Redisson getRedisson() {
        return redisson;
    }

    /**
     * Redisson连接方式配置工厂
     * 双重检查锁
     */
    static class RedissonConfigFactory {

        private RedissonConfigFactory() {
        }

        private static volatile RedissonConfigFactory factory = null;

        public static RedissonConfigFactory getInstance() {
            if (factory == null) {
                synchronized (Object.class) {
                    if (factory == null) {
                        factory = new RedissonConfigFactory();
                    }
                }
            }
            return factory;
        }


        /**
         * 根据连接类型获取对应连接方式的配置,基于策略模式
         *
         * @param redissonProperties redis连接信息
         * @return Config
         */
        Config createConfig(RedissonProperties redissonProperties) {
            return this.createRedissonConfig(redissonProperties);
        }

        /**
         * Redisson配置构建接口
         */
        public Config createRedissonConfig(RedissonProperties redissonProperties) {
            Config config = new Config();
            try {
                String address = redissonProperties.getHost();
                String password = redissonProperties.getPassword();
                int database = redissonProperties.getDatabase();
                String port = redissonProperties.getPort();
                String redisAddr = "redis://" + address + ":" + port;
                config.useSingleServer().setAddress(redisAddr);
                config.useSingleServer().setDatabase(database);
                //密码可以为空
                if (StringUtils.isNotBlank(password)) {
                    config.useSingleServer().setPassword(password);
                }
                log.info("初始化[单机部署]方式Config,redisAddress:" + address);
            } catch (Exception e) {
                log.error("单机部署 Redisson init error", e);
            }
            return config;
        }

    }
}


