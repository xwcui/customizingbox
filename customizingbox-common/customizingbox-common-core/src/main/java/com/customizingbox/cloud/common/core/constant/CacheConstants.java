package com.customizingbox.cloud.common.core.constant;

/**
 * 缓存的key 常量
 */
public interface CacheConstants {

    /**
     * 用户信息缓存
     */
    String APP_USER_CACHE = "app_user_cache";

    String ADMIN_USER_CACHE = "admin_user_cache";

    /**
     * 默认验证码前缀
     */
    String VER_CODE_DEFAULT = "ver_code_default:";


    /**
     * 唯一码在redis的前缀
     */
    String ONLY_CODE_PREFIX = "only_code_";
}
