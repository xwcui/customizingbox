package com.customizingbox.cloud.common.core.constant.exception;


import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.Collection;

public class ApiException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误编码
     */
    private Integer errorCode = ApiResStatus.FAIL.getStatus();

    /**
     * 消息是否为属性文件中的Key
     */
    private boolean propertiesKey = true;

    /**
     * 构造一个基本异常.
     *
     * @param message 信息描述
     */
    public ApiException(String message) {
        super(message);
    }


    /**
     * 构造一个基本异常.
     *
     * @param status 信息描述
     */
    public ApiException(ApiResStatus status) {
        super(status.getMessage());
        this.setErrorCode(status.getStatus());
    }

    /**
     * 构造一个基本异常.
     *
     * @param errorCode 错误编码
     * @param message   信息描述
     */
    public ApiException(Integer errorCode, String message) {
        this(errorCode, message, true);
    }

    /**
     * 构造一个基本异常.
     *
     * @param errorCode 错误编码
     * @param message   信息描述
     */
    public ApiException(Integer errorCode, String message, Throwable cause) {
        this(errorCode, message, cause, true);
    }

    /**
     * 构造一个基本异常.
     *
     * @param errorCode     错误编码
     * @param message       信息描述
     * @param propertiesKey 消息是否为属性文件中的Key
     */
    public ApiException(Integer errorCode, String message, boolean propertiesKey) {
        super(message);
        this.setErrorCode(errorCode);
        this.setPropertiesKey(propertiesKey);
    }

    /**
     * 构造一个基本异常.
     *
     * @param errorCode 错误编码
     * @param message   信息描述
     */
    public ApiException(Integer errorCode, String message, Throwable cause, boolean propertiesKey) {
        super(message, cause);
        this.setErrorCode(errorCode);
        this.setPropertiesKey(propertiesKey);
    }

    /**
     * 自定义错误信息
     *
     */
    public static void throwException(String msg) {
        throw new ApiException(ApiResStatus.FAIL.getStatus(), msg);
    }

    /**
     * 下单时校验对象是否为空, 为空抛出异常
     * @param msg
     */
    public static void isNullOrderException(Object obj, String msg) {
        if (obj instanceof Collection) {
            if (CollectionUtils.isEmpty((Collection<?>) obj))
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), msg);
        } else if (obj instanceof String) {
            if (StringUtils.isEmpty((String) obj))
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), msg);
        } else {
            if (ObjectUtils.isEmpty(obj))
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), msg);
        }
    }

    /**
     * 构造一个基本异常.
     *
     * @param message 信息描述
     * @param cause   根异常类（可以存入任何异常）
     */
    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isPropertiesKey() {
        return propertiesKey;
    }

    public void setPropertiesKey(boolean propertiesKey) {
        this.propertiesKey = propertiesKey;
    }


}