package com.customizingbox.cloud.common.core.constant;

public interface TransactionConstants {

    /**
     * 用户交易, 分布式锁 的key 前缀
     */
    String TRANSACTION_PREFIX = "TRANSACTION";

    /**
     * 用户交易, 分布式锁 释放锁的时间, 单位秒
     */
    Integer TRANSACTION_REDIS_LOCK_LEASE_TIME = 10;

    /**
     * 用户交易, 分布式锁 等待锁的时间, 单位秒
     */
    Integer TRANSACTION_REDIS_LOCK_WAIT_TIME = 3;

}
