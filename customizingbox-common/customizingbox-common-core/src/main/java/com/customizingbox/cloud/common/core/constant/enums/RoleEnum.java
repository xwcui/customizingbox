package com.customizingbox.cloud.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleEnum {

    ADMINISTRATOR("1", "管理员"),
    ;
    private final String code;
    private final String msg;
}
