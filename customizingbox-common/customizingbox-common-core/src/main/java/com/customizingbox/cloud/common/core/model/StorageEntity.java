package com.customizingbox.cloud.common.core.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StorageEntity {

	/**
	 * 地域节点
	 */
	@ApiModelProperty(value = "地域节点")
	private String endpoint;

	/**
	 * accessKeyId
	 */
	@ApiModelProperty(value = "accessKeyId")
	private String accessKeyId;

	/**
	 * 密钥
	 */
	@ApiModelProperty(value = "密钥")
	private String accessKeySecret;

	/**
	 * 域名
	 */
	@ApiModelProperty(value = "域名")
	private String bucket;

	/**
	 * 存储类型1、阿里OSS；2、七牛云；3、本地
	 */
	@ApiModelProperty(value = "存储类型1、阿里OSS；2、七牛云；3、本地")
	private String storageType;
}
