package com.customizingbox.cloud.common.core.constant.exception;

import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.lang.reflect.UndeclaredThrowableException;

/**
 * 控制器全局异常处理
 */
@Slf4j
@RestControllerAdvice
public class ExceptionGlobalAdvice {

    /**
     * 全局异常捕捉处理
     */
    @ExceptionHandler(value = Exception.class)
    public ApiResponse<?> errorHandler(Exception e) {
        log.error("error", e);
        return new ApiResponse<>(ApiResStatus.FAIL.getStatus(), e.getLocalizedMessage());
    }

    /**
     * 自定义异常捕捉处理
     */
    @ExceptionHandler(value = ApiException.class)
    public ApiResponse<?> myErrorHandler(ApiException e) {
        log.error("error:{}, {}", e.getMessage(), e);
        return new ApiResponse<>(e.getErrorCode(), e.getMessage());
    }

    /**
     * sentinel
     */
    @ExceptionHandler(value = UndeclaredThrowableException.class)
    public ApiResponse<?> sentinelException(UndeclaredThrowableException e) {
        log.error("sentinel current limiting:{}", e.getLocalizedMessage());
        return new ApiResponse<>(ApiResStatus.FAIL.getStatus(), "System busy. Please try again later");
    }

    /**
     * 数据校验全局处理
     * BindException是@Valid使用校验失败时产生的异常
     */
    @ExceptionHandler(value = BindException.class)
    public ApiResponse<?> BindExceptionHandler(BindException e)
    {
        log.error("校验参数异常; error:{}", e.getBindingResult().getFieldError().getDefaultMessage());
        return ApiResponse.failed(e.getBindingResult().getFieldError().getDefaultMessage());
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public ApiResponse<?> BindExceptionHandler(MissingServletRequestParameterException e)
    {
        log.error("校验参数异常; error:{}", e.getMessage());
        return ApiResponse.failed("参数不全");
    }


}
