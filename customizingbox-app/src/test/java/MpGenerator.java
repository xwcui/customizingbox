import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.jupiter.api.Test;

public class MpGenerator {

    private final static String PRODUCT = "app.";

    private final static String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private final static String URL = "jdbc:mysql://39.104.178.160:3306/customizingbox_data?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "Customerzing";
    private final static String TABLE_PREFIX = "";
    private final static String PARENT = "com.customizingbox";
    private final static String CONTROLLER = PRODUCT + "controller";
    private final static String SERVICE = PRODUCT + "service";
    private final static String SERVICE_IMPL = PRODUCT + "service.impl";
    private final static String MAPPER = "dao.mapper";
    private final static String XML = "dao.mapper.xml";
    private final static String ENTITY = "dao.model.entity";

    //private final static String MODULE_NAME = "platform";
    private final static String SERVICE_VM = "templates/service.vm";
    private final static String ENTITY_VM = "templates/entity.vm";
    private final static String CONTROLLER_VM = "templates/controller.vm";

    @Test
    public void test() {

        // 需要生成的表
        String[] table = new String[]{"app_panda_order_to_sh_history"};
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir("D:\\桌面\\auto_code");
        gc.setFileOverride(true);
        gc.setActiveRecord(true);
        // XML 二级缓存
        gc.setEnableCache(false);
        // XML ResultMap
        gc.setBaseResultMap(true);
        // XML columList
        gc.setBaseColumnList(true);
        gc.setAuthor("Z");

        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setControllerName("%sController");
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(URL);
        dsc.setDbType(DbType.MYSQL);
        dsc.setDriverName(DRIVER_NAME);
        dsc.setUsername(USERNAME);
        dsc.setPassword(PASSWORD);
        mpg.setDataSource(dsc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();

        // 全局大写命名 ORACLE 注意
        // strategy.setCapitalMode(true);

        // 此处可以修改为您的表前缀
        strategy.setTablePrefix(TABLE_PREFIX);

        // 表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);

        // 修改实现类的继承
//        strategy.setSuperServiceImplClass("");

        // 设置逻辑删除
        strategy.setLogicDeleteFieldName("is_del");

        strategy.setEntityLombokModel(true);

        strategy.setRestControllerStyle(true);


        strategy.setInclude(table);

        // 排除生成的表
        // strategy.setExclude(new String[]{"test"});
        mpg.setStrategy(strategy);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(PARENT);
        pc.setController(CONTROLLER);
        pc.setService(SERVICE);
        pc.setServiceImpl(SERVICE_IMPL);
        pc.setMapper(MAPPER);
        pc.setXml(XML);
        pc.setEntity(ENTITY);
        mpg.setPackageInfo(pc);

        TemplateConfig tc = new TemplateConfig();
        tc.setEntity(ENTITY_VM);
        tc.setController(CONTROLLER_VM);
        tc.setService(SERVICE_VM);
        mpg.setTemplate(tc);

        // 执行生成
        mpg.execute();
    }


}
