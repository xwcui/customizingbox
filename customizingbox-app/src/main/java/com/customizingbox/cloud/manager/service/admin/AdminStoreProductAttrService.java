package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-31 09:34:36
 */
public interface AdminStoreProductAttrService extends IService<AdminStoreProductAttr> {

}
