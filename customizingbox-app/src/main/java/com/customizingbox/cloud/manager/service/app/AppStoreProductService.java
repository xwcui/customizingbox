package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductListVO;

import java.util.List;

/**
 *  产品数据业务处理
 *
 * @author Y
 * @date 2022-03-26 10:32:23
 */
public interface AppStoreProductService extends IService<AppStoreProduct> {

    /**
     * 商品列表
     * @return
     */
    IPage<List<AppStoreProductListVO>> listAll(Page page);
}
