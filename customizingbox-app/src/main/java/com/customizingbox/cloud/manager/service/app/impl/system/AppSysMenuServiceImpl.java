package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.manager.service.app.AppSysMenuService;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysMenuMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysRoleMenuMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysMenu;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRoleMenu;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppMenuVO;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AppSysMenuServiceImpl extends ServiceImpl<AppSysMenuMapper, AppSysMenu> implements AppSysMenuService {

    private final AppSysRoleMenuMapper appSysRoleMenuMapper;

    @Override
    public List<AppMenuVO> findMenuByRoleId(String roleId) {
        return baseMapper.listMenusByRoleId(roleId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse removeMenuById(String id) {
        // 查询父节点为当前节点的节点
        List<AppSysMenu> menuList = this.list(Wrappers.<AppSysMenu>query()
                .lambda().eq(AppSysMenu::getParentId, id));
        if (CollUtil.isNotEmpty(menuList)) {
            return ApiResponse.failed("菜单含有下级不能删除");
        }

        //删除角色菜单关联
        appSysRoleMenuMapper.delete(Wrappers.<AppSysRoleMenu>query().lambda().eq(AppSysRoleMenu::getMenuId, id));

        //删除当前菜单及其子菜单
        return ApiResponse.ok(this.removeById(id));
    }

    @Override
    public Boolean updateMenuById(AppSysMenu sysMenu) {
        return this.updateById(sysMenu);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMenu(AppSysMenu entity) {
        String roleId = entity.getRoleId();
        super.save(entity);
        if (StrUtil.isNotBlank(roleId)) {
            AppSysRoleMenu appSysRoleMenu = new AppSysRoleMenu();
            appSysRoleMenu.setRoleId(roleId);
            appSysRoleMenu.setMenuId(entity.getId());
            appSysRoleMenuMapper.insert(appSysRoleMenu);
        }
    }
}
