package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrgan;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrganRelation;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface AppSysOrganRelationService extends IService<AppSysOrganRelation> {

    /**
     * 新建机构关系
     *
     * @param appSysOrgan 机构
     */
    void insertOrganRelation(AppSysOrgan appSysOrgan);

    /**
     * 通过ID删除机构关系
     */
    void deleteAllOrganRelation(String id);

    /**
     * 更新机构关系
     */
    void updateOrganRelation(AppSysOrganRelation relation);
}
