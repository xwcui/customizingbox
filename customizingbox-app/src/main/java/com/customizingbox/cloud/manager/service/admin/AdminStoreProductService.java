package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-31 09:34:35
 */
public interface AdminStoreProductService extends IService<AdminStoreProduct> {

}
