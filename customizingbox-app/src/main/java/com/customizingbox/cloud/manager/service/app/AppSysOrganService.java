package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysOrganTree;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrgan;

import java.util.List;

/**
 * <p>
 * 机构管理 服务类
 * </p>
 */
public interface AppSysOrganService extends IService<AppSysOrgan> {

    /**
     * 查询机构树菜单
     *
     * @return 树
     */
    List<AppSysOrganTree> selectTree();

    /**
     * 添加信息机构
     *
     */
    Boolean saveOrgan(AppSysOrgan appSysOrgan);

    /**
     * 删除机构
     *
     * @param id 机构 ID
     * @return 成功、失败
     */
    Boolean removeOrganById(String id);

    /**
     * 更新机构
     *
     * @param appSysOrgan 机构信息
     * @return 成功、失败
     */
    Boolean updateOrganById(AppSysOrgan appSysOrgan);

}
