package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysTenantMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.*;
import com.customizingbox.cloud.manager.service.app.*;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 租户管理
 */
@Service
@AllArgsConstructor
public class AppSysTenantServiceImpl extends ServiceImpl<AppSysTenantMapper, AppSysTenant> implements AppSysTenantService {

    private final AppSysUserService appSysUserService;
    private final AppSysRoleService appSysRoleService;
    private final AppSysUserRoleService appSysUserRoleService;
    private final AppSysOrganRelationService appSysOrganRelationService;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    private final AppSysTenantMapper appSysTenantMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(AppSysTenant entity) {
        baseMapper.insert(entity);
        //新建机构关联
        AppSysOrgan appSysOrgan = new AppSysOrgan();
        BeanUtil.copyProperties(entity, appSysOrgan);
        appSysOrganRelationService.insertOrganRelation(appSysOrgan);
        //新建用户
        AppSysUser sysUser = new AppSysUser();
        sysUser.setOrganId(appSysOrgan.getId());
        sysUser.setUsername(entity.getUsername());
        sysUser.setPassword(ENCODER.encode(entity.getPassword()));
        appSysUserService.save(sysUser);
        //新建角色
        AppSysRole appSysRole = new AppSysRole();
        appSysRole.setRoleName("管理员");
        appSysRole.setRoleCode(CommonConstants.ROLE_CODE_ADMIN);
        appSysRole.setRoleDesc(entity.getName() + "管理员");
        appSysRole.setDsType(CommonConstants.DS_TYPE_0);
        appSysRoleService.save(appSysRole);
        //新建用户角色
        AppSysUserRole appSysUserRole = new AppSysUserRole();
        appSysUserRole.setRoleId(appSysRole.getId());
        appSysUserRole.setUserId(sysUser.getId());
        appSysUserRoleService.save(appSysUserRole);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        baseMapper.deleteSysTenantById(id);
        return Boolean.TRUE;
    }
}
