package com.customizingbox.cloud.manager.service.admin.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductAttrMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductAttr;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductAttrService;
import org.springframework.stereotype.Service;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-31 09:34:36
 */
@Service
public class AdminStoreProductAttrServiceImpl extends ServiceImpl<AdminStoreProductAttrMapper, AdminStoreProductAttr> implements AdminStoreProductAttrService {

}
