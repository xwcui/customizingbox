package com.customizingbox.cloud.manager.service.app;

public interface AppWebHookService {

    /**
     * 批量设置shopify WebHook
     */
    Boolean batchSettingShopify(String shop, String token);
}
