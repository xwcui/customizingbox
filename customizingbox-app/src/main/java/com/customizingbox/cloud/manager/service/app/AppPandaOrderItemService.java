package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.List;

/**
 * 原平台订单item表
 *
 * @author Y
 * @date 2022-04-12 16:41:47
 */
public interface AppPandaOrderItemService extends IService<AppPandaOrderItem> {

    /**
     * 保存商户订单item
     * @param appOrderItem
     * @param pandaOrderId
     */
    void saveOrderItemToPanda(AppOrderItem appOrderItem, Long pandaOrderId);

    /**
     * 根据订单id查询item订单列表
     * @param pandaOrderId
     * @return
     */
    List<AppPandaOrderItem> queryByOrderId(Long pandaOrderId);

    /**
     * 根据订单获取总产品体积和重量
     * @param pandaOrderId
     * @return
     */
    AdminTransportPriceDTO getOrderWeightAndVolume(Long pandaOrderId);

//    /**
//     * 修改订单中产品映射关系(未支付)
//     * @param storeId 商户店铺id
//     * @param sourceProductId 商户产品id
//     * @param sourceVariantId 商户产品变体id
//     * @param adminProductId admin 产品id
//     * @param adminVariantId admin 产品变体id
//     * @return
//     */
//    Boolean updateOrderItemProductRelevancy(Long storeId, Long sourceProductId, Long sourceVariantId, Long adminProductId, Long adminVariantId);


    /**
     * 重新识别订单商品
     * @param pandaOrderId 订单id
     * @return
     */
    Boolean reloadOrderItemProductRelevancy(Long pandaOrderId);



}


