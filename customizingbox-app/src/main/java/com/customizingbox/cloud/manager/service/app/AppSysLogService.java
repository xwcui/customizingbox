package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLog;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppSysPreLogVO;

import java.util.List;

/**
 * <p>
 * 日志表 服务类
 * </p>
 */
public interface AppSysLogService extends IService<AppSysLog> {


    /**
     * 批量插入前端错误日志
     *
     * @param appSysPreLogVOList 日志信息
     * @return true/false
     */
    Boolean saveBatchLogs(List<AppSysPreLogVO> appSysPreLogVOList);
}
