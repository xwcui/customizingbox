//package com.customizingbox.cloud.manager.controller.system;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.customizingbox.cloud.manager.service.app.AppSysGenTableService;
//import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
//import com.customizingbox.cloud.common.core.util.ApiResponse;
//import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysGenTable;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.*;
//
///**
// * 代码生成配置表
// */
//@Slf4j
//@RestController
//@AllArgsConstructor
//@RequestMapping("/gentable")
//@Api(value = "gentable", tags = "代码生成配置表管理")
//public class AppSysGenTableController {
//
//    private final AppSysGenTableService appSysGenTableService;
//
//
//    @ApiOperation(value = "代码生成配置表查询")
//    @GetMapping("/{tableName}")
//    public ApiResponse getById(@PathVariable("tableName") String tableName) {
//        return ApiResponse.ok(appSysGenTableService.getOne(Wrappers.<AppSysGenTable>query().lambda().eq(AppSysGenTable::getTableName, tableName)));
//    }
//
//
//    @ApiOperation(value = "代码生成配置表修改")
//    @SysLog("修改代码生成配置表")
//    @PutMapping
//    public ApiResponse updateById(@RequestBody AppSysGenTable appSysGenTable) {
//        return ApiResponse.ok(appSysGenTableService.saveOrUpdate(appSysGenTable));
//    }
//
//}
