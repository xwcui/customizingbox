package com.customizingbox.cloud.manager.service.app.impl.product;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductAttrMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductAttr;
import com.customizingbox.cloud.manager.service.app.AppStoreProductAttrService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-26 10:32:21
 */
@Service
public class AppStoreProductAttrServiceImpl extends ServiceImpl<AppStoreProductAttrMapper, AppStoreProductAttr> implements AppStoreProductAttrService {

    @Override
    public AppStoreProductAttr findByProductIdAndSourceId(Long productId, Long sourceAttrId, Integer platformType) {
        return this.getOne(Wrappers.<AppStoreProductAttr>lambdaQuery().eq(AppStoreProductAttr::getProductId, productId)
                .eq(AppStoreProductAttr::getSourceId, sourceAttrId)
                .eq(AppStoreProductAttr::getPlatformType, platformType));
    }

    @Override
    public List<AppStoreProductAttr> findByProductId(Long productId) {
        return this.list(Wrappers.<AppStoreProductAttr>lambdaQuery().select(AppStoreProductAttr::getId, AppStoreProductAttr::getSourceId, AppStoreProductAttr::getProductId)
                .eq(AppStoreProductAttr::getProductId, productId)
                .eq(AppStoreProductAttr::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
    }
}
