package com.customizingbox.cloud.manager.service.app.impl.transaction;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.transaction.AppPaypalPaymentMapper;
import com.customizingbox.cloud.common.datasource.model.app.paypal.AppPaypalPayment;
import com.customizingbox.cloud.common.datasource.util.IdGenerate;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.app.AppPaypalPaymentService;
import com.customizingbox.cloud.manager.service.app.AppSysUserTransactionRecordService;
import com.paypal.http.exceptions.SerializeException;
import com.paypal.http.serializer.Json;
import com.paypal.orders.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * payapl交易记录 服务实现类
 * </p>
 *
 * @author Z
 * @since 2022-04-22
 */
@Service
public class AppPaypalPaymentServiceImpl extends ServiceImpl<AppPaypalPaymentMapper, AppPaypalPayment> implements AppPaypalPaymentService {

    @Autowired
    private AppSysUserTransactionRecordService appSysUserTransactionRecordService;

    /**
     * 处理充值信息
     * @param order
     * @param user
     * @param orderId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String recharge(Order order, BaseUser user, String orderId) throws SerializeException {
        PurchaseUnit purchaseUnit = order.purchaseUnits().get(0);
        AmountWithBreakdown amountWithBreakdown = purchaseUnit.amountWithBreakdown();
        Capture capture = purchaseUnit.payments().captures().get(0);
        MerchantReceivableBreakdown merchantReceivableBreakdown = capture.sellerReceivableBreakdown();
        Payee payee = purchaseUnit.payee();
        Payer payer = order.payer();
        AppPaypalPayment appPaypalPayment = new AppPaypalPayment();
        appPaypalPayment.setId(IdGenerate.nextId());
        appPaypalPayment.setSaleId(orderId);
        appPaypalPayment.setPaymentId(capture.invoiceId());
        appPaypalPayment.setOrderType(0);
        appPaypalPayment.setPaymentMethod("paypal");
        appPaypalPayment.setPayerEmail(payer.email());
        appPaypalPayment.setPayerName(payer.name().givenName() + " " + payer.name().surname());
        appPaypalPayment.setPayerId(payer.payerId());
        appPaypalPayment.setAmount(String.valueOf(merchantReceivableBreakdown.netAmount().value()));
        appPaypalPayment.setCurrency(merchantReceivableBreakdown.netAmount().currencyCode());
        appPaypalPayment.setFixFee(String.valueOf(merchantReceivableBreakdown.paypalFee().value()));
        appPaypalPayment.setCreateTime(order.createTime());
        appPaypalPayment.setUpdateTime(order.updateTime());
        appPaypalPayment.setPayeeEmail(payee.email());
        appPaypalPayment.setMerchantId(payee.merchantId());
        appPaypalPayment.setSource(0);
        appPaypalPayment.setType(1);
        appPaypalPayment.setRemark(purchaseUnit.description());
        try {
            log.error(new Json().serialize(order));
            appPaypalPayment.setResultJson(new Json().serialize(order));
        } catch (SerializeException e) {
            e.printStackTrace();
        }
        appPaypalPayment.setUserId(Long.parseLong(user.getId()));
        appPaypalPayment.setState(capture.status());
        this.save(appPaypalPayment);
        if (StringUtils.equalsIgnoreCase("COMPLETED",capture.status())){
            appSysUserTransactionRecordService.payIn(new BigDecimal(appPaypalPayment.getAmount()),Long.parseLong(user.getId()),
                    appPaypalPayment.getPaymentId() ,appPaypalPayment.getRemark());
            return "grossAmount :" + merchantReceivableBreakdown.grossAmount().value() +
                    "netAmount :" + merchantReceivableBreakdown.netAmount().value() +
                    "paypalFee :" + merchantReceivableBreakdown.paypalFee().value();
        }else if(StringUtils.equalsIgnoreCase("COMPLETED",capture.status())){
            return "PayPal order status is PENDING";
        }else{
            return capture.status();
        }

    }
}
