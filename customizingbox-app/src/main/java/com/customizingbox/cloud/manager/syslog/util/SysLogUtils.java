package com.customizingbox.cloud.manager.syslog.util;

import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLog;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * 系统日志工具类
 *
 */
@UtilityClass
public class SysLogUtils {
	public AppSysLog getSysLog() {
		HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
		AppSysLog appSysLog = new AppSysLog();
		appSysLog.setCreateBy(Objects.requireNonNull(getUsername()));
		appSysLog.setCreateId(Objects.requireNonNull(getUserId()));
		appSysLog.setRemoteAddr(ServletUtil.getClientIP(request));
		appSysLog.setRequestUri(URLUtil.getPath(request.getRequestURI()));
		appSysLog.setMethod(request.getMethod());
		appSysLog.setUserAgent(request.getHeader("user-agent"));
		appSysLog.setParams(HttpUtil.toParams(request.getParameterMap()));
		appSysLog.setServiceId(getClientId());
		return appSysLog;
	}

	/**
	 * 获取客户端
	 *
	 * @return clientId
	 */
	public String getClientId() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof OAuth2Authentication) {
			OAuth2Authentication auth2Authentication = (OAuth2Authentication) authentication;
			return auth2Authentication.getOAuth2Request().getClientId();
		}
		return null;
	}

	/**
	 * 获取用户ID
	 *
	 * @return username
	 */
	public String getUserId() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return null;
		}
		return SecurityUtils.getUser(authentication).getId();
	}

	/**
	 * 获取用户名称
	 *
	 * @return username
	 */
	public String getUsername() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return null;
		}
		return authentication.getName();
	}

}
