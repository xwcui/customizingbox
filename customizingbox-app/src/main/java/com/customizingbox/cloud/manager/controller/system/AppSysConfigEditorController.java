//package com.customizingbox.cloud.manager.controller.system;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.customizingbox.cloud.manager.service.app.AppSysConfigEditorService;
//import com.customizingbox.cloud.common.core.util.ApiResponse;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * 编辑器配置
// */
//@Slf4j
//@RestController
//@AllArgsConstructor
//@RequestMapping("/configeditor")
//@Api(value = "configeditor", tags = "编辑器配置管理")
//public class AppSysConfigEditorController {
//
//    private final AppSysConfigEditorService appSysConfigEditorService;
//
//    @GetMapping()
//    @ApiOperation(value = "查询编辑器配置")
//    public ApiResponse get() {
//        return ApiResponse.ok(appSysConfigEditorService.getOne(Wrappers.emptyWrapper()));
//    }
//}
