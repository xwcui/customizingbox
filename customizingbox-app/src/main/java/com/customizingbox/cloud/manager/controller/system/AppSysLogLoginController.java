package com.customizingbox.cloud.manager.controller.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.manager.service.app.AppSysLogLoginService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLogLogin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 登录日志
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/loglogin")
@Api(value = "loglogin", tags = "登录日志管理")
public class AppSysLogLoginController {

    private final AppSysLogLoginService appSysLogLoginService;


    @ApiOperation(value = "分页列表")
    @GetMapping("/page")
    public ApiResponse getPage(Page page, AppSysLogLogin appSysLogLogin) {
        return ApiResponse.ok(appSysLogLoginService.page(page, Wrappers.query(appSysLogLogin)));
    }


    @ApiOperation(value = "登录日志查询")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:loglogin:get')")
    public ApiResponse getById(@PathVariable("id") String id) {
        return ApiResponse.ok(appSysLogLoginService.getById(id));
    }


    @ApiOperation(value = "登录日志修改")
    @SysLog("修改登录日志")
    @PutMapping
    @PreAuthorize("@ato.hasAuthority('sys:loglogin:edit')")
    public ApiResponse updateById(@RequestBody AppSysLogLogin appSysLogLogin) {
        return ApiResponse.ok(appSysLogLoginService.updateById(appSysLogLogin));
    }


    @ApiOperation(value = "登录日志删除")
    @SysLog("删除登录日志")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:loglogin:del')")
    public ApiResponse removeById(@PathVariable String id) {
        return ApiResponse.ok(appSysLogLoginService.removeById(id));
    }

}
