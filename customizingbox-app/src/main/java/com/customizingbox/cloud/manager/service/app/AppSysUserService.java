package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserInfo;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppSysUserVO;

import java.util.List;


public interface AppSysUserService extends IService<AppSysUser> {
    /**
     * 查询用户信息
     *
     * @param sysUser 用户
     * @return userInfo
     */
    AppSysUserInfo findUserInfo(AppSysUser sysUser);

    /**
     * 分页查询用户信息（含有角色信息）
     *
     * @param page    分页对象
     * @param appSysUserDTO 参数列表
     * @return
     */
    IPage getUsersWithRolePage(Page page, AppSysUserDTO appSysUserDTO);

    /**
     * 删除用户
     *
     * @param sysUser 用户
     * @return boolean
     */
    Boolean deleteUserById(AppSysUser sysUser);

    /**
     * 更新当前用户基本信息
     *
     * @param appSysUserDto 用户信息
     * @return Boolean
     */
    Boolean updateUserInfo(AppSysUserDTO appSysUserDto);

    /**
     * 更新指定用户信息
     *
     * @param appSysUserDto 用户信息
     * @return
     */
    Boolean updateUser(AppSysUserDTO appSysUserDto);

    /**
     * 通过ID查询用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     */
    AppSysUserVO selectUserVoById(String id);

    /**
     * 查询上级机构的用户信息
     *
     * @param username 用户名
     * @return R
     */
    List<AppSysUser> listAncestorUsers(String username);

    /**
     * 保存用户信息
     *
     * @param appSysUserDto DTO 对象
     * @return ok/fail
     */
    Boolean saveUser(AppSysUserDTO appSysUserDto);

    /**
     * 无租户查询
     *
     * @param sysUser
     * @return SysUser
     */
    AppSysUser getByNoTenant(AppSysUser sysUser);

}
