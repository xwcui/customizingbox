package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import com.customizingbox.cloud.common.datasource.model.app.store.vo.AppStoreDistinctVO;

import java.util.List;

/**
 * 商户表
 *
 * @author Y
 * @date 2022-03-23 09:51:00
 */
public interface AppStoreService extends IService<AppStore> {

    /**
     * 查询商户是否存在
     * @param shop 商户名
     * @param platformType 平台类型
     * @return
     */
    AppStore queryStoreIfExit(String shop, Integer platformType);

    /**
     * 根据id修改商户状态, 状态为禁用则取消同步所有webhook
     * @param id
     * @param status
     * @return
     */
    Boolean updateStatusById(Long id, Integer status);

    Boolean delByStoreId(Long id);

    AppStore queryById(Long id);

    /**
     * 查询去重的商户信息
     */
    List<AppStoreDistinctVO> queryDistinctStore();
}
