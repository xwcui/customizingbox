package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysConfigEditor;

/**
 * 编辑器配置
 */
public interface AppSysConfigEditorService extends IService<AppSysConfigEditor> {

}
