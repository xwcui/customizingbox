package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysConfigStorageMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysConfigStorage;
import com.customizingbox.cloud.manager.service.app.AppSysConfigStorageService;
import org.springframework.stereotype.Service;

/**
 * 存储配置
 */
@Service
public class AppSysConfigStorageServiceImpl extends ServiceImpl<AppSysConfigStorageMapper, AppSysConfigStorage> implements AppSysConfigStorageService {

}
