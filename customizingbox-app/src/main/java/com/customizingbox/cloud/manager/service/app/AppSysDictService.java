package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDict;

/**
 * <p>
 * 字典表 服务类
 * </p>
 */
public interface AppSysDictService extends IService<AppSysDict> {

    /**
     * 根据ID 删除字典
     */
    ApiResponse removeDict(String id);

    /**
     * 更新字典
     */
    ApiResponse updateDict(AppSysDict appSysDict);
}
