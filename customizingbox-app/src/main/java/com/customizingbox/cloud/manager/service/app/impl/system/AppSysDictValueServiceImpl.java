package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysDictValueMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDictValue;
import com.customizingbox.cloud.manager.service.app.AppSysDictValueService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

/**
 * 字典项
 */
@Service
@AllArgsConstructor
public class AppSysDictValueServiceImpl extends ServiceImpl<AppSysDictValueMapper, AppSysDictValue> implements AppSysDictValueService {

    /**
     * 删除字典项
     *
     * @param id 字典项ID
     * @return
     */
    @Override
    public ApiResponse removeDictItem(String id) {
        return ApiResponse.ok(this.removeById(id));
    }

    /**
     * 更新字典项
     *
     * @param item 字典项
     * @return
     */
    @Override
    public ApiResponse updateDictItem(AppSysDictValue item) {
        return ApiResponse.ok(this.updateById(item));
    }
}
