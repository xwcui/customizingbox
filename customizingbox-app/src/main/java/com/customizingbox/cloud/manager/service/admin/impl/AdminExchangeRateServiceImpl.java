package com.customizingbox.cloud.manager.service.admin.impl;

import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.setting.AdminExchangeRateMapper;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminExchangeRate;
import com.customizingbox.cloud.manager.service.admin.AdminExchangeRateService;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 汇率表
 *
 * @author Y
 * @date 2022-03-31 09:34:33
 */
@Service
@AllArgsConstructor
public class AdminExchangeRateServiceImpl extends ServiceImpl<AdminExchangeRateMapper, AdminExchangeRate> implements AdminExchangeRateService {
    private final RedisTemplate<String,String> redisTemplate;

    @Override
    public AdminExchangeRate getByCode(String code) {
        return this.getOne(Wrappers.<AdminExchangeRate>lambdaQuery().eq(AdminExchangeRate::getCurrencyCode, code));
    }

    @Override
    public BigDecimal getRateByCode(String code) {
        String rate = redisTemplate.opsForValue().get(code);
        if (rate == null){
            BigDecimal rateDecimal = getOne(Wrappers.<AdminExchangeRate>lambdaQuery().eq(AdminExchangeRate::getCurrencyCode, code)).getRate();
            redisTemplate.opsForValue().set(code,rateDecimal.toString());
            rate = rateDecimal.toString();
        }
        return NumberUtil.toBigDecimal(rate);
    }
}
