//package com.customizingbox.cloud.manager.controller.system;
//
//import cn.hutool.core.util.IdUtil;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.customizingbox.cloud.manager.service.app.AppSysTenantService;
//import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
//import com.customizingbox.cloud.common.core.constant.CommonConstants;
//import com.customizingbox.cloud.common.core.util.ApiResponse;
//import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysTenant;
//import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.AllArgsConstructor;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
///**
// * <p>
// * 租户管理
// * </p>
// */
//@RestController
//@AllArgsConstructor
//@RequestMapping("/tenant")
//@Api(value = "tenant", tags = "租户管理模块")
//public class AppSysTenantController {
//
//    private final AppSysTenantService appSysTenantService;
//
//
//    @ApiOperation(value = "分页查询")
//    @GetMapping("/page")
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:index')")
//    public ApiResponse getUserPage(Page page, AppSysTenant appSysTenant) {
//        appSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(appSysTenantService.page(page, Wrappers.query(appSysTenant)));
//    }
//
//
//    @ApiOperation(value = "list查询")
//    @GetMapping("/list")
//    public ApiResponse getList(AppSysTenant appSysTenant) {
//        appSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(appSysTenantService.list(Wrappers.query(appSysTenant)));
//    }
//
//
//    @ApiOperation(value = "通过ID查询")
//    @GetMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:get')")
//    public ApiResponse getById(@PathVariable String id) {
//        TenantContextHolder.setTenantId(id);
//        return ApiResponse.ok(appSysTenantService.getById(id));
//    }
//
//
//    @ApiOperation(value = "添加")
//    @SysLog("添加租户")
//    @PostMapping
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:add')")
//    public ApiResponse save(@Valid @RequestBody AppSysTenant appSysTenant) {
//        String id = String.valueOf(IdUtil.getSnowflake(1, 2).nextId());
//        TenantContextHolder.setTenantId(id);
//        appSysTenant.setId(id);
//        appSysTenant.setTenantId(id);
//        appSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(appSysTenantService.save(appSysTenant));
//    }
//
//
//    @ApiOperation(value = "删除")
//    @SysLog("删除租户")
//    @DeleteMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:del')")
//    public ApiResponse removeById(@PathVariable String id) {
//        TenantContextHolder.setTenantId(id);
//        return ApiResponse.ok(appSysTenantService.removeById(id));
//    }
//
//
//    @ApiOperation(value = "编辑")
//    @SysLog("编辑租户")
//    @PutMapping
//    @PreAuthorize("@ato.hasAuthority('sys:tenant:edit')")
//    public ApiResponse update(@Valid @RequestBody AppSysTenant appSysTenant) {
//        TenantContextHolder.setTenantId(appSysTenant.getId());
//        appSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(appSysTenantService.updateById(appSysTenant));
//    }
//
//
//    @ApiOperation(value = "list查询服务间调用")
//    @GetMapping("/inside/list")
//    public ApiResponse getListInside() {
//        AppSysTenant appSysTenant = new AppSysTenant();
//        appSysTenant.setParentId(CommonConstants.PARENT_ID);
//        return ApiResponse.ok(appSysTenantService.list(Wrappers.query(appSysTenant)));
//    }
//}
