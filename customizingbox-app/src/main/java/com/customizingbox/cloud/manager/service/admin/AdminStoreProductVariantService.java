package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-31 09:34:38
 */
public interface AdminStoreProductVariantService extends IService<AdminStoreProductVariant> {

}
