package com.customizingbox.cloud.manager.service.admin.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductVariantService;
import org.springframework.stereotype.Service;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-31 09:34:38
 */
@Service
public class AdminStoreProductVariantServiceImpl extends ServiceImpl<AdminStoreProductVariantMapper, AdminStoreProductVariant> implements AdminStoreProductVariantService {

}
