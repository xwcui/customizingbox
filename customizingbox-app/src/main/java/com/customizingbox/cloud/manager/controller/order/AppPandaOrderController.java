package com.customizingbox.cloud.manager.controller.order;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderParentPaidParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaProcessingOrderParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderPaySuccessVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaPaidOrderParentVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaProcessingOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaToOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderStatusParam;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * panda 订单表
 *
 * @author Y
 * @date 2022-04-13 17:03:42
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/panda/order")
@Api(value = "/panda/order", tags = "panda 订单表管理")
public class AppPandaOrderController {

    private final AppPandaOrderService appPandaOrderService;

    @ApiOperation(value = "1 - 创建app订单,  只创建已经关联产品的订单")
    @PostMapping("/toOrder")
    public ApiResponse<Boolean> toOrder(@RequestBody @ApiParam(value = "订单id列表") List<Long> orderIds) {
        appPandaOrderService.toPandaOrder(orderIds);
        return ApiResponse.ok();
    }

    @ApiOperation(value = "2 - 分页查询待付款订单")
    @PostMapping("/queryToOrderPage")
    public ApiResponse<IPage<AppPandaToOrderVO>> queryToOrderPage(Page page, @RequestBody AppOrderBaseParam baseParam) {
        IPage<AppPandaToOrderVO> result = appPandaOrderService.queryToOrderPage(page, baseParam);
        return ApiResponse.ok(result);
    }

    @ApiOperation(value = "3 - 预下单, 获取支付页面")
    @PostMapping("/queryCheckoutOrderPage")
    public ApiResponse<AppPandaPaidOrderParentVO> checkoutOrderPage(@ApiParam(value = "订单id列表") @RequestBody List<Long> orderIds) {
        if (CollectionUtils.isEmpty(orderIds)) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "请选择需要支付的订单");
        }
        return appPandaOrderService.checkoutOrderPage(orderIds);
    }

    @ApiOperation(value = "4 - 支付")
    @PostMapping("/paid")
    public ApiResponse<AppPandaOrderPaySuccessVO> paid(@RequestBody AppPandaOrderParentPaidParam appPandaOrderParentPaidParam) {
        return appPandaOrderService.paid(appPandaOrderParentPaidParam);
    }

    @ApiOperation(value = "5 - 支付后订单列表")
    @PostMapping("/paidOrderList")
    public ApiResponse<IPage<AppPandaProcessingOrderVO>> paidOrderList(Page page, @RequestBody AppPandaProcessingOrderParam param) {
        return ApiResponse.ok(appPandaOrderService.paidOrderList(page, param));
    }

    @ApiOperation(value = "6 - 分页查询ToOrder取消订单数据")
    @PostMapping("/cancelledOrderPage")
    public ApiResponse<IPage<AppPandaToOrderVO>> cancelledToOrderOrderPage(Page page, @RequestBody AppOrderBaseParam baseParam) {
        return ApiResponse.ok(appPandaOrderService.cancelledToOrderOrderPage(page, baseParam));
    }


    @ApiOperation(value = "7 - 修改ToOrder订单状态")
    @PostMapping("/updateOrderStatus")
    public ApiResponse<Boolean> updateToOrderOrderStatus(@RequestBody AppOrderStatusParam param) {
        return ApiResponse.ok(appPandaOrderService.updateToOrderOrderStatus(param));
    }

}
