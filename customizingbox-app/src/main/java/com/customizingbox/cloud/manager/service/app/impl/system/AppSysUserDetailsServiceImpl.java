package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.constant.SecurityConstants;
import com.customizingbox.cloud.common.core.constant.enums.RoleEnum;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserInfo;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysTenant;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.service.app.AppSysTenantService;
import com.customizingbox.cloud.manager.service.app.AppSysUserService;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户详细信息
 */
@Slf4j
@Service
@AllArgsConstructor
public class AppSysUserDetailsServiceImpl implements UserDetailsService {

    private final CacheManager cacheManager;
    private final AppSysUserService appSysUserService;
    private final AppSysTenantService appSysTenantService;

    /**
     * 用户密码登录
     */
    @Override
    @SneakyThrows
    public UserDetails loadUserByUsername(String username) {
        //查询缓存中是否有此用户信息，有则直接返回
        Cache cache = cacheManager.getCache(CacheConstants.APP_USER_CACHE);
        if (cache != null && cache.get(username) != null) {
            return (BaseUser) cache.get(username).get();
        }
        //缓存中无此用户信息
        AppSysUserInfo appSysUserInfo = this.getUserinfo(username);
        UserDetails userDetails = this.getUserDetails(appSysUserInfo);
        if (userDetails.isAccountNonLocked()) {
            //合法用户，放入缓存
            if (!ObjectUtils.isEmpty(cache)) {
                cache.put(username, userDetails);
            }
        }
        return userDetails;
    }


    private AppSysUserInfo getUserinfo(String username) {
        AppSysUser sysUser = new AppSysUser();
        sysUser.setUsername(username);
        sysUser = appSysUserService.getByNoTenant(sysUser);
        if (sysUser == null) {
            return null;
        }

        TenantContextHolder.setTenantId(sysUser.getTenantId());
        return appSysUserService.findUserInfo(sysUser);
    }

    /**
     * 构建 userDetails
     *
     * @param appSysUserInfo 用户信息
     */
    private UserDetails getUserDetails(AppSysUserInfo appSysUserInfo) {
        if (appSysUserInfo == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        Boolean isAdministrator = false;
        Set<String> dbAuthsSet = new HashSet<>();
        if (ArrayUtil.isNotEmpty(appSysUserInfo.getRoles())) {
            // 获取资源
            dbAuthsSet.addAll(Arrays.asList(appSysUserInfo.getPermissions()));
            // 获取角色
            for (String roleId : appSysUserInfo.getRoles()) {
                dbAuthsSet.add(SecurityConstants.ROLE + roleId);
                if (RoleEnum.ADMINISTRATOR.getCode().equals(roleId)) {
                    isAdministrator = true;
                }
            }

        }

        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(dbAuthsSet.toArray(new String[0]));
        AppSysUser user = appSysUserInfo.getSysUser();
        boolean enabled = StrUtil.equals(user.getLockFlag(), CommonConstants.STATUS_NORMAL);
        // 构造security用户
        return new BaseUser(user.getId(), user.getOrganId(), user.getTenantId(), user.getUsername(),
                SecurityConstants.BCRYPT + user.getPassword(), isAdministrator, enabled, true,
                true, CommonConstants.STATUS_NORMAL.equals(user.getLockFlag()), authorities);
    }
}
