package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUserRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 */
public interface AppSysUserRoleService extends IService<AppSysUserRole> {

    /**
     * 根据用户Id删除该用户的角色关系
     */
    Boolean deleteByUserId(String userId);
}
