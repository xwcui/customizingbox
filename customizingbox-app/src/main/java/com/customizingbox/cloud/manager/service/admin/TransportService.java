package com.customizingbox.cloud.manager.service.admin;

import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;

import java.util.List;

public interface TransportService {


    /**
     * 获取订单运费
     * @param adminTransportPriceDTO
     * @return
     */
    List<AdminTransportPriceResultDto> getTransportPrice(AdminTransportPriceDTO adminTransportPriceDTO);
}
