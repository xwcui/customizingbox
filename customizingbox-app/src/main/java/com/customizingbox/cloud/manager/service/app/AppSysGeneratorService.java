package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysGenTable;

import java.util.List;
import java.util.Map;


public interface AppSysGeneratorService {
	/**
	 * 生成代码预览
	 *
	 * @param appSysGenTable
	 */
	Map<String, String> generatorView(AppSysGenTable appSysGenTable);

	/**
	 * 生成代码
	 *
	 * @param appSysGenTable 生成表配置
	 */
	byte[] generatorCode(AppSysGenTable appSysGenTable);

	/**
	 * 分页查询表
	 *
	 * @param tableName       表名
	 * @param sysDatasourceId 数据源ID
	 */
	IPage<List<Map<String, Object>>> getPage(Page page, String tableName, String sysDatasourceId);
}
