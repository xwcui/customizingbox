package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderVO;

import java.util.List;

/**
 * panda 订单item表
 *
 * @author Y
 * @date 2022-03-30 13:48:16
 */
public interface AppOrderItemService<T> extends IService<AppOrderItem> {

    /**
     * 订单id查询子订单列表
     * @param orderId
     * @return
     */
    List<AppOrderItem> findByOrderId(Long orderId);

    /**
     * 根据订单id查询未下单的订单
     * @param orderId
     * @return
     */
    List<AppOrderItem> notPlaceOrder(Long orderId);

    /**
     * 修改订单中产品映射关系
     * @param sourceVariantId 商户产品变体id
     * @param adminProductId admin 产品id
     * @param adminVariantId admin 产品变体id
     * @return
     */
    Boolean updateOrderItemProductRelevancy(Long sourceVariantId, Long adminProductId, Long adminVariantId);


}
