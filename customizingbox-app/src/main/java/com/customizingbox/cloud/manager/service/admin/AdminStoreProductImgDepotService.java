package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-31 09:34:40
 */
public interface AdminStoreProductImgDepotService extends IService<AdminStoreProductImgDepot> {

}
