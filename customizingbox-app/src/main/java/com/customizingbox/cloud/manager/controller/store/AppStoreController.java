package com.customizingbox.cloud.manager.controller.store;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import com.customizingbox.cloud.common.datasource.model.app.store.vo.AppStoreDistinctVO;
import com.customizingbox.cloud.common.datasource.model.app.store.vo.AppStorePageVo;
import com.customizingbox.cloud.manager.service.app.AppStoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商户表
 *
 * @author Y
 * @date 2022-03-23 09:51:00
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/store")
@Api(value = "store", tags = "商户表管理")
public class AppStoreController {

    private final AppStoreService appStoreService;

    @ApiOperation(value = "分页查询商户信息")
    @GetMapping("/page")
//    @PreAuthorize("@ato.hasAuthority('app:store:index')")
    public ApiResponse<IPage<AppStorePageVo>> getRolePage(Page page) {
        Page resultPage = appStoreService.page(page);
        IPage convert = resultPage.convert(appStore -> BeanUtil.copyProperties(appStore, AppStorePageVo.class));
        return ApiResponse.ok(convert);
    }

    @ApiOperation(value = "修改商户状态")
    @GetMapping("/updateStatus")
//    @PreAuthorize("@ato.hasAuthority('app:store:updateStatus')")
    public ApiResponse<Boolean> updateStatus(@ApiParam(value = "商户id") @RequestParam Long id,
                                 @ApiParam(value = "商户状态. 1: 正常(默认), 2: 禁用") @RequestParam Integer status) {
        return ApiResponse.execute(appStoreService.updateStatusById(id, status));
    }

    @ApiOperation(value = "删除店铺测试")
    @GetMapping("/delById")
    public ApiResponse<Boolean> delById(@ApiParam(value = "店铺id") @RequestParam Long id) {
        Boolean result = appStoreService.delByStoreId(id);
        return ApiResponse.ok(result);
    }

    @ApiOperation(value = "queryById")
    @GetMapping("/queryById")
    public ApiResponse<AppStore> queryById(@ApiParam(value = "店铺id") @RequestParam Long id) {
        AppStore result = appStoreService.queryById(id);
        return ApiResponse.ok(result);
    }

    @ApiOperation(value = "查询店铺(去重)")
    @GetMapping("/queryDistinctStore")
    public ApiResponse<List<AppStoreDistinctVO>> queryDistinctStore() {
        List<AppStoreDistinctVO> result = appStoreService.queryDistinctStore();
        return ApiResponse.ok(result);
    }



}
