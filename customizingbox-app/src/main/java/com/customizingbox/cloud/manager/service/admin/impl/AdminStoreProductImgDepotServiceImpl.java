package com.customizingbox.cloud.manager.service.admin.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductImgDepotMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductImgDepot;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductImgDepotService;
import org.springframework.stereotype.Service;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-31 09:34:40
 */
@Service
public class AdminStoreProductImgDepotServiceImpl extends ServiceImpl<AdminStoreProductImgDepotMapper, AdminStoreProductImgDepot> implements AdminStoreProductImgDepotService {

}
