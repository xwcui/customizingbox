package com.customizingbox.cloud.manager.auth.exception;

import com.customizingbox.cloud.manager.auth.component.BaseAuth2ExceptionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.http.HttpStatus;

/**
 * 自定义 OAuth2Exception
 */
@JsonSerialize(using = BaseAuth2ExceptionSerializer.class)
public class UnauthorizedException extends BaseAuth2Exception {

    public UnauthorizedException(String msg, Throwable t) {
        super(msg);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "unauthorized";
    }

    @Override
    public int getHttpErrorCode() {
        return HttpStatus.UNAUTHORIZED.value();
    }

}
