package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysConfigEditorMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysConfigEditor;
import com.customizingbox.cloud.manager.service.app.AppSysConfigEditorService;
import org.springframework.stereotype.Service;

/**
 * 编辑器配置
 */
@Service
public class AppSysConfigEditorServiceImpl extends ServiceImpl<AppSysConfigEditorMapper, AppSysConfigEditor> implements AppSysConfigEditorService {

}
