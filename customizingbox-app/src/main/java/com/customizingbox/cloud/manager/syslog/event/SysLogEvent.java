package com.customizingbox.cloud.manager.syslog.event;

import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLog;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统日志事件
 */
@Getter
@AllArgsConstructor
public class SysLogEvent {

	private final AppSysLog appSysLog;
}
