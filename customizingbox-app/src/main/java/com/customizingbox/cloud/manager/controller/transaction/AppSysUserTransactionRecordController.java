package com.customizingbox.cloud.manager.controller.transaction;

import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.app.AppSysUserTransactionRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 交易管理
 *
 * @author Y
 * @date 2022-04-21 16:09:21
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/transaction")
@Api(value = "transaction", tags = "交易管理")
public class AppSysUserTransactionRecordController {

    private final AppSysUserTransactionRecordService appSysUserTransactionRecordService;

    @ApiOperation(value = "获取当前用户余额")
    @PostMapping("/user/balance")
    public ApiResponse<BigDecimal> queryUserBalance() {
        Long userId = SecurityUtils.getUserId();
        BigDecimal balance = appSysUserTransactionRecordService.queryUserBalance(userId);
        return ApiResponse.ok(balance);
    }
}
