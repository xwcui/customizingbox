package com.customizingbox.cloud.manager.service.admin.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductService;
import org.springframework.stereotype.Service;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-31 09:34:35
 */
@Service
public class AdminStoreProductServiceImpl extends ServiceImpl<AdminStoreProductMapper, AdminStoreProduct> implements AdminStoreProductService {

}
