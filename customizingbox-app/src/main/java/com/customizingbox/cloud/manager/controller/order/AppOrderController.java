package com.customizingbox.cloud.manager.controller.order;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaToOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderCancelParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderQuotationParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderStatusParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderVO;
import com.customizingbox.cloud.manager.service.app.AppOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/order")
@Api(value = "order", tags = "订单管理")
public class AppOrderController {

    private final AppOrderService appOrderService;

    @ApiOperation(value = "分页查询订单信息")
    @PostMapping("/page")
    public ApiResponse<IPage<AppQuotationOrderVO>> pageAll(Page page, @RequestBody AppOrderQuotationParam param) {
        IPage<AppQuotationOrderVO> result = appOrderService.pageAll(page, param);
        return ApiResponse.ok(result);
    }

    @ApiOperation(value = "分页查询Quotation取消订单数据")
    @PostMapping("/cancelledOrderPage")
    public ApiResponse<IPage<AppQuotationOrderVO>> cancelledOrderPage(Page page, @RequestBody AppOrderQuotationParam param) {
        return ApiResponse.ok(appOrderService.cancelledOrderPage(page, param));
    }

    @ApiOperation(value = "修改Quotation订单状态")
    @PostMapping("updateOrderStatus")
    public ApiResponse<Boolean> updateOrderStatus(@RequestBody AppOrderStatusParam param) {
        return ApiResponse.ok(appOrderService.updateOrderStatus(param));
    }


//
//    @ApiOperation(value = "根据订单id查询item数据")
//    @GetMapping("/queryById")
//    public ApiResponse<AppQuotationOrderVO> queryByOrderId(@ApiParam(value = "订单id") @RequestParam String orderId) {
//        AppQuotationOrderVO result = appOrderItemService.queryQuotation(orderId);
//        return ApiResponse.ok(result);
//    }

}
