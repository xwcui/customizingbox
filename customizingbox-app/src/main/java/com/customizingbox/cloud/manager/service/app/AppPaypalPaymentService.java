package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.paypal.AppPaypalPayment;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.paypal.http.exceptions.SerializeException;
import com.paypal.orders.Order;

/**
 * <p>
 * payapl交易记录 服务类
 * </p>
 *
 * @author Z
 * @since 2022-04-22
 */
public interface AppPaypalPaymentService extends IService<AppPaypalPayment> {


    /**
     * 处理充值信息
     * @param order
     * @param user
     * @param orderId
     */
    String recharge(Order order, BaseUser user, String orderId) throws SerializeException;
}
