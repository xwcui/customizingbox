package com.customizingbox.cloud.manager.controller.paypal;


import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.paypal.PayPalPayVo;
import com.customizingbox.cloud.common.datasource.model.app.paypal.PaypalAmount;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.config.PayPalCheckoutConstant;
import com.customizingbox.cloud.manager.config.PayPalConfig;
import com.customizingbox.cloud.manager.service.app.PayPalCheckoutService;
import com.paypal.http.HttpResponse;
import com.paypal.http.exceptions.SerializeException;
import com.paypal.http.serializer.Json;
import com.paypal.orders.LinkDescription;
import com.paypal.orders.Order;
import com.paypal.orders.OrdersCreateRequest;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @program: 
 * @description:
 * @author: 
 * @create: 
 **/

@Slf4j
@RestController
@RequestMapping("/paypal")
@Api(value = "paypal", tags = "paypal")
public class PaypalController {

    @Autowired
    private PayPalCheckoutService payPalCheckoutService;

    @Autowired
    private PayPalConfig payPalConfig;

    /**
     * 成功页面
     */
    public static final String PAYPAL_SUCCESS_URL = "/pay/success";

    /**
     * 取消页面
     */
    public static final String PAYPAL_CANCEL_URL = "/pay/cancel";




    @PostMapping("/pay")
    public ApiResponse payment(HttpServletRequest httpServletRequest, @RequestBody @Valid PayPalPayVo payPalPayVo) throws SerializeException {
        //获取取消页面
        String cancelUrl = /*httpServletRequest.getHeader("Origin") +*/ "https://www.bemooy.com" + PayPalCheckoutConstant.RETURN_URL;
        //获取成功页面
        String successUrl = /*httpServletRequest.getHeader("Origin") +*/ "https://www.bemooy.com" + PayPalCheckoutConstant.CANCEL_URL;

        if (payPalPayVo.getAmount().compareTo(BigDecimal.ZERO)==0){
            throw new ApiException("充值金额不能为0");
        }
        OrdersCreateRequest request = new OrdersCreateRequest();
        request.header("prefer", "return=representation");
        PaypalAmount paypalAmount = new PaypalAmount(payPalPayVo.getAmount());
        String origin = httpServletRequest.getHeader("Origin");
        request.requestBody(payPalCheckoutService.buildCreateRequestBody(origin,paypalAmount));
        HttpResponse<Order> response = null;
        try {
            response = payPalConfig.client().execute(request);
        } catch (IOException e1) {
            try {
                log.error("第1次调用paypal订单创建失败");
                response = payPalConfig.client().execute(request);
            } catch (Exception e) {
                try {
                    log.error("第2次调用paypal订单创建失败");
                    response = payPalConfig.client().execute(request);
                } catch (Exception e2) {
                    log.error("第3次调用paypal订单创建失败，失败原因：{}", e2.getMessage());
                }
            }
        }
        String approve = "";
        if (response.statusCode() == 201) {
            log.info("paypal创建订单返回值= {}",new Json().serialize(response.result()));
            log.info("Status Code = {}, Status = {}, OrderID = {}, Intent = {}", response.statusCode(), response.result().status(), response.result().id(), response.result().checkoutPaymentIntent());
            for (LinkDescription link : response.result().links()) {
                log.info("Links-{}: {}    \tCall Type: {}", link.rel(), link.href(), link.method());
                if (link.rel().equals("approve")) {
                    approve = link.href();
                    System.err.println(approve);
                    return ApiResponse.ok(approve);
                }
            }
        }
        return ApiResponse.failed("创建订单失败");
    }

    /**
     * 取消页面
     * @return
     */
    @GetMapping(value = PAYPAL_CANCEL_URL)
    public String cancelPay(){
        return "cancel";
    }

    /**
     * 执行支付
     * @param orderNo
     * @param payerId
     * @return
     */
    @GetMapping(value = PAYPAL_SUCCESS_URL)
    public ApiResponse successPay(@RequestParam("PayerID") String payerId, @RequestParam("token") String orderNo){
        try {
            BaseUser user = SecurityUtils.getUser();

            // 执行支付
            return ApiResponse.ok( payPalCheckoutService.captureOrder(orderNo,user));
        } catch (Exception e) {
            // 如果同步通知返回异常，可根据paymentId 来查询刷新订单状态
            // 同时IPN异步通知也可以更新订单状态
            log.error(e.getMessage());
            return ApiResponse.failed(e.getMessage());
        }
    }
}

