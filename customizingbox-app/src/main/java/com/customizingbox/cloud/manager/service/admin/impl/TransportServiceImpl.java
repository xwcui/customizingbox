package com.customizingbox.cloud.manager.service.admin.impl;

import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminLogisticsFreightMapper;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;
import com.customizingbox.cloud.manager.service.admin.TransportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 *  物流service
 */
@Service
public class TransportServiceImpl implements TransportService {

    @Resource
    private AdminLogisticsFreightMapper adminLogisticsFreightMapper;

    /**
     * 获取运费
     * @param adminTransportPriceDTO
     * @return
     */
    @Override
    public List<AdminTransportPriceResultDto> getTransportPrice(AdminTransportPriceDTO adminTransportPriceDTO) {
        adminTransportPriceDTO.setNowDate(LocalDateTime.now());
        return adminLogisticsFreightMapper.getTransportPrice(adminTransportPriceDTO);
    }
}
