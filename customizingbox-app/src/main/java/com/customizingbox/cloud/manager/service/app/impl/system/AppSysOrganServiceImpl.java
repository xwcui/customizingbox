package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.manager.service.app.AppSysOrganRelationService;
import com.customizingbox.cloud.manager.service.app.AppSysOrganService;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysOrganMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysOrganTree;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrgan;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrganRelation;
import com.customizingbox.cloud.common.datasource.util.AppSysTreeUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 机构管理
 */
@Service
@AllArgsConstructor
public class AppSysOrganServiceImpl extends ServiceImpl<AppSysOrganMapper, AppSysOrgan> implements AppSysOrganService {

    private final AppSysOrganRelationService appSysOrganRelationService;

    /**
     * 添加信息机构
     *
     * @param organ 机构
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveOrgan(AppSysOrgan organ) {
        AppSysOrgan appSysOrgan = new AppSysOrgan();
        BeanUtil.copyProperties(organ, appSysOrgan);
        this.save(appSysOrgan);
        appSysOrganRelationService.insertOrganRelation(appSysOrgan);
        return Boolean.TRUE;
    }


    /**
     * 删除机构
     *
     * @param id 机构 ID
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeOrganById(String id) {
        //级联删除机构
        List<String> idList = appSysOrganRelationService
                .list(Wrappers.<AppSysOrganRelation>query().lambda().eq(AppSysOrganRelation::getAncestor, id))
                .stream()
                .map(AppSysOrganRelation::getDescendant)
                .collect(Collectors.toList());

        if (CollUtil.isNotEmpty(idList)) {
            this.removeByIds(idList);
        }

        //删除机构级联关系
        appSysOrganRelationService.deleteAllOrganRelation(id);
        return Boolean.TRUE;
    }

    /**
     * 更新机构
     *
     * @param appSysOrgan 机构信息
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateOrganById(AppSysOrgan appSysOrgan) {
        //更新机构状态
        this.updateById(appSysOrgan);
        //更新机构关系
        AppSysOrganRelation relation = new AppSysOrganRelation();
        relation.setAncestor(appSysOrgan.getParentId());
        relation.setDescendant(appSysOrgan.getId());
        appSysOrganRelationService.updateOrganRelation(relation);
        return Boolean.TRUE;
    }

    /**
     * 查询全部机构树
     *
     * @return 树
     */
    @Override
    public List<AppSysOrganTree> selectTree() {
        return getTree(this.list(Wrappers.emptyWrapper()));
    }

    /**
     * 构建机构树
     */
    private List<AppSysOrganTree> getTree(List<AppSysOrgan> entitys) {
        List<AppSysOrganTree> treeList = entitys.stream()
                .filter(entity -> !entity.getId().equals(entity.getParentId()))
                .sorted(Comparator.comparingInt(AppSysOrgan::getSort))
                .map(entity -> {
                    AppSysOrganTree node = new AppSysOrganTree();
                    BeanUtil.copyProperties(entity, node);
                    return node;
                }).collect(Collectors.toList());
        return AppSysTreeUtil.build(treeList, CommonConstants.PARENT_ID);
    }
}
