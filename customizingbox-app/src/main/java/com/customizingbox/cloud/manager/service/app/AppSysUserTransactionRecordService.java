package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.transaction.entity.AppSysUserTransactionRecord;

import java.math.BigDecimal;

/**
 * 交易记录表
 *
 * @author Y
 * @date 2022-04-21 16:09:21
 */
public interface AppSysUserTransactionRecordService extends IService<AppSysUserTransactionRecord> {
    /**
     * 支付
     * @param money 交易金额
     * @param userId 用户id
     * @param orderId 订单id
     * @param paymentId 支付id(多个订单合并支付, 只有一个支付id)
     * @param mark 备注
     * @return 交易记录id
     */
    Long payOut(BigDecimal money, Long userId, Long orderId, String paymentId, String mark);

    /**
     * 充值
     * @param money 交易金额
     * @param userId 用户id
     * @param transaction 流水号(用于充值时第三方支付的交易单号)
     * @param mark 备注
     * @return 交易记录id
     */
    Long payIn(BigDecimal money, Long userId, String transaction, String mark);

    /**
     * 退款
     * @param money 交易金额
     * @param userId 用户id
     * @param orderId 订单id
     * @param refundOrderId 退款订单id
     * @param mark 备注
     * @return 交易记录id
     */
    Long refund(BigDecimal money, Long userId, Long orderId, Long refundOrderId, String mark);

//    /**
//     * 交易
//     * @param money 交易金额
//     * @param userId 用户id
//     * @param type 交易类型
//     * @param sourceType 来源类型, 1: 充值, 2: 订单支付, 3: 订单退款
//     * @param transaction 流水号(用于充值时第三方支付的交易单号)
//     * @param orderId 订单id
//     * @param refundOrderId 退款订单id
//     * @param paymentId 支付id(多个订单合并支付, 只有一个支付id)
//     * @param mark 备注
//     * @return 交易记录id
//     */
//    Long transaction(BigDecimal money, Long userId, Integer type, Integer sourceType, String transaction, Long orderId, Long refundOrderId, String paymentId, String mark);

    /**
     * 获取当前用户余额
     * @param userId
     * @return
     */
    BigDecimal queryUserBalance(Long userId);
}
