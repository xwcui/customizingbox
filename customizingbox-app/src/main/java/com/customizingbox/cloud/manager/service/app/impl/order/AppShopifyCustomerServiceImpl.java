package com.customizingbox.cloud.manager.service.app.impl.order;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppShopifyCustomerMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShopifyCustomer;
import com.customizingbox.cloud.manager.service.app.AppShopifyCustomerService;
import org.springframework.stereotype.Service;

/**
 * 商户客户表
 * @author Y
 * @date 2022-03-30 13:48:14
 */
@Service
public class AppShopifyCustomerServiceImpl extends ServiceImpl<AppShopifyCustomerMapper, AppShopifyCustomer> implements AppShopifyCustomerService {

}
