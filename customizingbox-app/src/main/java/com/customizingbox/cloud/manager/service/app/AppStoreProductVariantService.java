package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductVariantListVO;

import java.util.List;
import java.util.Map;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
public interface AppStoreProductVariantService extends IService<AppStoreProductVariant> {

    AppStoreProductVariant findByProductIdAndSourceId(Long productId, Long sourceId);

    /**
     * 根据产品id查询变体列表
     * @param productId
     * @return
     */
    List<AppStoreProductVariant> findByProductId(Long productId);

    /**
     * 根据原始订单中的产品变体id获取admin的产品变体
     * @param sourceVariantId 原变体id
     * @param latformType 平台类型
     * @return
     */
    AdminStoreProductVariant getAdminProductVariantBySourceId(Long sourceVariantId, Integer latformType);

    /**
     * 根据产品id查询变体列表(app 和admin数据合并)
     * @return
     */
    IPage<List<AppStoreProductVariantListVO>> appAdminVariantByPId(Page page, Long productId);

    /**
     * 根据原始订单中的产品变体id获取app的产品变体
     * @param sourceVariantId 原平台产品变体id
     * @param latformType 平台编码
     * @return
     */
    AppStoreProductVariant getBySourceId(Long sourceVariantId, Integer latformType);
}
