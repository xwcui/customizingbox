package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysUserRoleMapper;
import com.customizingbox.cloud.manager.service.app.AppSysUserRoleService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUserRole;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 */
@Service
public class AppSysUserRoleServiceImpl extends ServiceImpl<AppSysUserRoleMapper, AppSysUserRole> implements AppSysUserRoleService {

    /**
     * 根据用户Id删除该用户的角色关系
     */
    @Override
    public Boolean deleteByUserId(String userId) {
        return baseMapper.deleteByUserId(userId);
    }
}
