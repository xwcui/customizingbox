package com.customizingbox.cloud.manager.auth.exception;

import com.customizingbox.cloud.common.core.constant.exception.ExceptionGlobalAdvice;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 控制器全局异常处理
 */
@Slf4j
@RestControllerAdvice
public class AuthExceptionGlobalAdvice extends ExceptionGlobalAdvice {

    /**
     * 权限异常捕获
     */
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ApiResponse<?> oauthErrorHandler(AccessDeniedException e) {
        String message = SpringSecurityMessageSource.getAccessor()
                .getMessage("AbstractAccessDecisionManager.accessDenied", e.getMessage());
        log.error("拒绝授权异常信息 message:{}", message);
        e.printStackTrace();
        return ApiResponse.failed(message);
    }
}
