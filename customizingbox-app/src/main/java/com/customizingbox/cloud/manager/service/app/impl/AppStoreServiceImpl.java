package com.customizingbox.cloud.manager.service.app.impl;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.store.AppStoreMapper;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import com.customizingbox.cloud.common.datasource.model.app.store.vo.AppStoreDistinctVO;
import com.customizingbox.cloud.manager.service.app.AppStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 商户表
 * @author Y
 * @date 2022-03-23 09:51:00
 */
@Service
public class AppStoreServiceImpl extends ServiceImpl<AppStoreMapper, AppStore> implements AppStoreService {

    @Autowired
    private AppStoreMapper appStoreMapper;

    @Override
    public AppStore queryStoreIfExit(String domain, Integer platformType) {
        return appStoreMapper.selectOne(Wrappers.<AppStore>lambdaQuery().eq(AppStore::getDomain, domain)
                .eq(AppStore::getPlatformType, platformType));
    }

    @Override
    public Boolean updateStatusById(Long id, Integer status) {
        this.update(Wrappers.<AppStore>lambdaUpdate().eq(AppStore::getId, id).set(AppStore::getStoreState, status));
        if (StoreEnum.State.DISABLED.getCode().equals(status)) {
            // TODO 这里取消所有webhook
        }
        return true;
    }

    @Override
    public Boolean delByStoreId(Long id) {
        return this.removeById(id);
    }

    @Override
    public AppStore queryById(Long id) {
        appStoreMapper.queryById(id);
        return null;
    }

    @Override
    public List<AppStoreDistinctVO> queryDistinctStore() {
        List<AppStore> appStores = this.list(Wrappers.<AppStore>query().select("DISTINCT id, name"));
        List<AppStoreDistinctVO> resultAppStore = null;
        if (!CollectionUtils.isEmpty(appStores)) {
            resultAppStore = appStores.stream().map(appStore -> {
                AppStoreDistinctVO appStoreDistinctVO = new AppStoreDistinctVO();
                appStoreDistinctVO.setId(appStore.getId());
                appStoreDistinctVO.setName(appStore.getName());
                return appStoreDistinctVO;
            }).collect(Collectors.toList());
        }
        return resultAppStore;
    }
}
