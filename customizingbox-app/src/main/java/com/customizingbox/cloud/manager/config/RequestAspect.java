package com.customizingbox.cloud.manager.config;

import com.customizingbox.cloud.common.core.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Slf4j
@Aspect
@Component
public class RequestAspect {

    /**
     * 环绕通知
     */
    @Around("execution(* com.customizingbox.cloud.manager.controller..*Controller.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            return point.proceed();
        }
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(requestAttributes)).getRequest();
        int port = request.getLocalPort();
        String ip = WebUtils.getIp(request);
        String url = request.getRequestURI();
        String token = request.getHeader("token");
        String version = request.getHeader("miniVersion");
        log.info("<---ip:{}; port:{}; version:{}; url:{}; token:{}-->", ip, port, version, url, token);
        long start = System.currentTimeMillis();
        //执行方法
        Object result = point.proceed();
        long end = System.currentTimeMillis();
        log.info("<---ip:{}; url:{} 执行时间:{} 毫秒-->", ip, request.getRequestURI(), end - start);
        return result;
    }


}
