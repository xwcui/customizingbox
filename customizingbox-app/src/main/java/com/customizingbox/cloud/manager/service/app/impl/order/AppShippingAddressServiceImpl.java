package com.customizingbox.cloud.manager.service.app.impl.order;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppShippingAddressMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShippingAddress;
import com.customizingbox.cloud.manager.service.app.AppShippingAddressService;
import org.springframework.stereotype.Service;

/**
 * 用户地址表
 *
 * @author Y
 * @date 2022-03-30 13:48:15
 */
@Service
public class AppShippingAddressServiceImpl extends ServiceImpl<AppShippingAddressMapper, AppShippingAddress> implements AppShippingAddressService {

}
