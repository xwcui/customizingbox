package com.customizingbox.cloud.manager.service.app.impl.product;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductListVO;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.app.AppStoreProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class AppStoreProductServiceImpl extends ServiceImpl<AppStoreProductMapper, AppStoreProduct> implements AppStoreProductService {

    private AppStoreProductMapper appStoreProductMapper;

    @Override
    public IPage<List<AppStoreProductListVO>> listAll(Page page) {
        String tenantId = SecurityUtils.getUser().getTenantId();
        IPage<List<AppStoreProductListVO>> productList = appStoreProductMapper.listAll(page, tenantId);
        return productList;
    }
}
