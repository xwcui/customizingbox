package com.customizingbox.cloud.manager.service.app.impl.order;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderItemMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppOrderItemMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProduct;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductService;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductVariantService;
import com.customizingbox.cloud.manager.service.app.AppOrderItemService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderItemService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderService;
import com.customizingbox.cloud.manager.service.app.AppStoreProductVariantService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * panda订单item表
 *
 * @author Y
 * @date 2022-04-13 17:03:41
 */
@Service
@AllArgsConstructor
public class AppPandaOrderItemServiceImpl extends ServiceImpl<AppPandaOrderItemMapper, AppPandaOrderItem> implements AppPandaOrderItemService {

    private final AdminStoreProductVariantService adminStoreProductVariantService;
    private final AdminStoreProductService adminStoreProductService;
    private final AppStoreProductVariantService appStoreProductVariantService;
    private final AppPandaOrderService appPandaOrderService;

    @Override
    public void saveOrderItemToPanda(AppOrderItem appOrderItem, Long pandaOrderId) {
        // 获取产品的sku
        AdminStoreProductVariant adminStoreProductVariant = adminStoreProductVariantService.getById(appOrderItem.getAdminVariantId());
        AdminStoreProduct adminStoreProduct = adminStoreProductService.getById(appOrderItem.getAdminProductId());

        AppPandaOrderItem appPandaOrderItem = new AppPandaOrderItem();
        appPandaOrderItem.setSourceId(appOrderItem.getSourceId());
        appPandaOrderItem.setOrderId(pandaOrderId);
        appPandaOrderItem.setSourceOrderId(appOrderItem.getSourceOrderId());
        appPandaOrderItem.setPlatformType(appOrderItem.getPlatformType());
        appPandaOrderItem.setProductId(appOrderItem.getProductId());
        appPandaOrderItem.setVariantId(appOrderItem.getVariantId());
        appPandaOrderItem.setSourceProductId(appOrderItem.getSourceProductId());
        appPandaOrderItem.setSourceVariantId(appOrderItem.getSourceVariantId());
        appPandaOrderItem.setAdminProductId(appOrderItem.getAdminProductId());
        appPandaOrderItem.setAdminVariantId(appOrderItem.getAdminVariantId());
        appPandaOrderItem.setQuantity(appOrderItem.getFulfillableQuantity());
        appPandaOrderItem.setProperties(appOrderItem.getProperties());
        appPandaOrderItem.setFulfillableQuantity(appOrderItem.getFulfillableQuantity());
        appPandaOrderItem.setRefundQuantity(0);
        appPandaOrderItem.setSku(adminStoreProductVariant.getSku());
        appPandaOrderItem.setSupplierId(adminStoreProduct.getSupplierId());
        appPandaOrderItem.setRefundStatus(OrderEnum.REFUND_STATUS.NOT.getCode());
        appPandaOrderItem.setPurchaseStatus(OrderEnum.PURCHASE_ORDER_STATUS.WAIT_ORDER.getCode());
        appPandaOrderItem.setUploadShStatus(OrderEnum.SH_UPLOAD_STATUS.NOT.getCode());
        this.save(appPandaOrderItem);
    }

    @Override
    public List<AppPandaOrderItem> queryByOrderId(Long pandaOrderId) {
        return this.list(Wrappers.<AppPandaOrderItem>lambdaQuery().eq(AppPandaOrderItem::getOrderId, pandaOrderId));
    }

    @Override
    public AdminTransportPriceDTO getOrderWeightAndVolume(Long pandaOrderId) {
        return baseMapper.getOrderWeightAndVolume(pandaOrderId);
    }

//    @Override
//    public Boolean updateOrderItemProductRelevancy(Long storeId, Long sourceProductId, Long sourceVariantId, Long adminProductId, Long adminVariantId) {
//        baseMapper.updateOrderItemProductRelevancy(storeId, sourceProductId, sourceVariantId, adminProductId, adminVariantId);
//        return true;
//    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean reloadOrderItemProductRelevancy(Long pandaOrderId) {
        AppPandaOrder appPandaOrder = appPandaOrderService.getById(pandaOrderId);
        if(ObjectUtils.isEmpty(appPandaOrder) || !OrderEnum.PAY_STATUS.NOT_PAY.getCode().equals(appPandaOrder.getPayStatus())) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "未支付订单才能重新识别订单商品");
        }
        List<AppPandaOrderItem> appPandaOrderItems = this.queryByOrderId(pandaOrderId);
        for (AppPandaOrderItem appPandaOrderItem : appPandaOrderItems) {
            Long variantId = appPandaOrderItem.getVariantId();

            AppStoreProductVariant variant = appStoreProductVariantService.getById(variantId);
            if (!variant.getAdminVariantId().equals(appPandaOrderItem.getAdminVariantId())) {
                // 关联admin发生了变化
                appPandaOrderItem.setAdminVariantId(variant.getAdminVariantId());
                appPandaOrderItem.setAdminProductId(variant.getAdminProductId());
                this.updateById(appPandaOrderItem);
            }

        }
        return true;
    }


}
