package com.customizingbox.cloud.manager.auth.handler;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.constant.SecurityConstants;
import com.customizingbox.cloud.common.core.util.WebUtils;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLogLogin;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.app.AppSysLogLoginService;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.CompletableFuture;

/**
 * 认证成功事件处理器
 */
@Slf4j
@Component
@AllArgsConstructor
public class AuthenticationSuccessEventHandler implements ApplicationListener<AuthenticationSuccessEvent> {

    private final AppSysLogLoginService sysLogLoginService;

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        Authentication authentication = (Authentication) event.getSource();
        if (CollUtil.isNotEmpty(authentication.getAuthorities())) {
            this.handle(authentication);
        }
    }

    /**
     * 处理登录成功方法
     * <p>
     * 获取到登录的authentication 对象
     *
     * @param authentication 登录对象
     */
    public void handle(Authentication authentication) {
        BaseUser baseUser = SecurityUtils.getUser(authentication);
        TenantContextHolder.setTenantId(baseUser.getTenantId());
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (URLUtil.getPath(request.getRequestURI()).contains(SecurityConstants.OAUTH_TOKEN_URL)) {
            //异步处理登录日志
            CompletableFuture.runAsync(() -> {
                AppSysLogLogin sysLogLogin = new AppSysLogLogin();
                sysLogLogin.setCreateId(baseUser.getId());
                sysLogLogin.setCreateBy(baseUser.getUsername());
                sysLogLogin.setType(CommonConstants.LOG_TYPE_0);
                sysLogLogin.setRemoteAddr(ServletUtil.getClientIP(request));
                sysLogLogin.setRequestUri(URLUtil.getPath(request.getRequestURI()));
                sysLogLogin.setUserAgent(request.getHeader("user-agent"));
                sysLogLogin.setParams(HttpUtil.toParams(request.getParameterMap()));
                sysLogLogin.setAddress(WebUtils.getAddresses(sysLogLogin.getRemoteAddr()));
                sysLogLoginService.save(sysLogLogin);
            });
        }
    }
}
