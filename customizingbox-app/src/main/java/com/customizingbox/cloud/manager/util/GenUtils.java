package com.customizingbox.cloud.manager.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysColumnEntity;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysGenTable;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysTableEntity;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成器   工具类
 *
 * @date 2018-07-30
 */
@Slf4j
@UtilityClass
public class GenUtils {

    private final String ENTITY_JAVA_VM = "Entity.java.vm";
    private final String MAPPER_JAVA_VM = "Mapper.java.vm";
    private final String SERVICE_JAVA_VM = "Service.java.vm";
    private final String SERVICE_IMPL_JAVA_VM = "ServiceImpl.java.vm";
    private final String CONTROLLER_JAVA_VM = "Controller.java.vm";
    private final String MAPPER_XML_VM = "Mapper.xml.vm";
    //	private final String MENU_SQL_VM = "menu.sql.vm";
    private final String INDEX_VUE_VM = "index.vue.vm";
    private final String API_JS_VM = "api.js.vm";
    private final String CRUD_JS_VM = "crud.js.vm";

    private List<String> getTemplates() {
        List<String> templates = new ArrayList<>();
        templates.add("template/Entity.java.vm");
        templates.add("template/Mapper.java.vm");
        templates.add("template/Mapper.xml.vm");
        templates.add("template/Service.java.vm");
        templates.add("template/ServiceImpl.java.vm");
        templates.add("template/Controller.java.vm");
//		templates.add("template/menu.sql.vm");

        templates.add("template/index.vue.vm");
        templates.add("template/api.js.vm");
        templates.add("template/crud.js.vm");
        return templates;
    }

    /**
     * 生成代码
     */
    public Map<String, String> generatorCode(AppSysGenTable appSysGenTable, Map<String, String> table,
                                             List<Map<String, Object>> columns, ZipOutputStream zip) {
        //配置信息
        Configuration config = getConfig();
        boolean hasBigDecimal = false;
        //表信息
        AppSysTableEntity appSysTableEntity = new AppSysTableEntity();
        appSysTableEntity.setTableName(table.get("tableName"));
        appSysTableEntity.setGenKey(appSysGenTable.getGenKey());
        if (StrUtil.isNotBlank(appSysGenTable.getTableComment())) {
            appSysTableEntity.setComments(appSysGenTable.getTableComment());
        } else {
            appSysTableEntity.setComments(table.get("tableComment"));
        }

        String tablePrefix;
        if (StrUtil.isNotBlank(appSysGenTable.getTablePrefix())) {
            tablePrefix = appSysGenTable.getTablePrefix();
        } else {
            tablePrefix = config.getString("tablePrefix");
        }

        //表名转换成Java类名
        String className = tableToJava(appSysTableEntity.getTableName(), tablePrefix);
        appSysTableEntity.setCaseClassName(className);
        appSysTableEntity.setLowerClassName(StringUtils.uncapitalize(className));

        //列信息
        List<AppSysColumnEntity> columnList = new ArrayList<>();
        for (Map<String, Object> column : columns) {
            AppSysColumnEntity appSysColumnEntity = new AppSysColumnEntity();
            appSysColumnEntity.setColumnName(String.valueOf(column.get("columnName")));
            appSysColumnEntity.setDataType(String.valueOf(column.get("dataType")));
            appSysColumnEntity.setComments(String.valueOf(column.get("columnComment")));
            appSysColumnEntity.setExtra(String.valueOf(column.get("extra")));
            appSysColumnEntity.setIsNullable(String.valueOf(column.get("IS_NULLABLE")));
            appSysColumnEntity.setCharacterMaximumLength((Long) column.get("CHARACTER_MAXIMUM_LENGTH"));

            //列名转换成Java属性名
            String attrName = columnToJava(appSysColumnEntity.getColumnName());
            appSysColumnEntity.setCaseAttrName(attrName);
            appSysColumnEntity.setLowerAttrName(StringUtils.uncapitalize(attrName));

            //列的数据类型，转换成Java类型
            String attrType = config.getString(appSysColumnEntity.getDataType(), "unknowType");
            appSysColumnEntity.setAttrType(attrType);
            if (!hasBigDecimal && "BigDecimal".equals(attrType)) {
                hasBigDecimal = true;
            }
            //是否主键
            if ("PRI".equalsIgnoreCase(String.valueOf(column.get("columnKey"))) && appSysTableEntity.getPk() == null) {
                appSysTableEntity.setPk(appSysColumnEntity);
            }

            columnList.add(appSysColumnEntity);
        }
        appSysTableEntity.setColumns(columnList);

        //没主键，则第一个字段为主键
        if (appSysTableEntity.getPk() == null) {
            appSysTableEntity.setPk(appSysTableEntity.getColumns().get(0));
        }

        //设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);
        //封装模板数据
        Map<String, Object> map = new HashMap<>(16);
        map.put("tableName", appSysTableEntity.getTableName());
        map.put("pk", appSysTableEntity.getPk());
        map.put("className", appSysTableEntity.getCaseClassName());
        map.put("classname", appSysTableEntity.getLowerClassName());
        map.put("genKey", appSysTableEntity.getGenKey());
        map.put("pathName", appSysTableEntity.getLowerClassName().toLowerCase());
        map.put("columns", appSysTableEntity.getColumns());
        map.put("hasBigDecimal", hasBigDecimal);
        map.put("datetime", DateUtil.now());

        if (StrUtil.isNotBlank(appSysGenTable.getTableComment())) {
            map.put("comments", appSysGenTable.getTableComment());
        } else {
            map.put("comments", appSysTableEntity.getComments());
        }

        if (StrUtil.isNotBlank(appSysGenTable.getAuthor())) {
            map.put("author", appSysGenTable.getAuthor());
        } else {
            map.put("author", config.getString("author"));
        }

        if (StrUtil.isNotBlank(appSysGenTable.getModuleName())) {
            map.put("moduleName", appSysGenTable.getModuleName());
        } else {
            map.put("moduleName", config.getString("moduleName"));
        }

        if (StrUtil.isNotBlank(appSysGenTable.getPackageName())) {
            map.put("package", appSysGenTable.getPackageName());
            map.put("mainPath", appSysGenTable.getPackageName());
        } else {
            map.put("package", config.getString("package"));
            map.put("mainPath", config.getString("mainPath"));
        }
        VelocityContext context = new VelocityContext(map);

        Map<String, String> dataMap = new LinkedHashMap<>();
        //获取模板列表
        List<String> templates = getTemplates();
        for (String template : templates) {
            //渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, CharsetUtil.UTF_8);
            tpl.merge(context, sw);
            dataMap.put(template, sw.toString());

            if (zip != null) {
                try {
                    //添加到zip
                    zip.putNextEntry(new ZipEntry(Objects.requireNonNull(getFileName(template, appSysTableEntity.getCaseClassName(),
                            map.get("package").toString(), map.get("moduleName").toString(),
                            map.get("genKey").toString()))));
                    IoUtil.write(zip, StandardCharsets.UTF_8, false, sw.toString());
                    IoUtil.close(sw);
                    zip.closeEntry();
                } catch (IOException e) {
                    throw new ApiException("渲染模板失败，表名：" + appSysTableEntity.getTableName(), e);
                }
            }
        }
        return dataMap;
    }

    /**
     * 列名转换成Java属性名
     */
    private String columnToJava(String columnName) {
        return WordUtils.capitalizeFully(columnName, new char[]{'_'}).replace("_", "");
    }

    /**
     * 表名转换成Java类名
     */
    private String tableToJava(String tableName, String tablePrefix) {
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.replaceFirst(tablePrefix, "");
        }
        return columnToJava(tableName);
    }

    /**
     * 获取配置信息
     */
    private Configuration getConfig() {
        try {
            return new PropertiesConfiguration("generator.properties");
        } catch (ConfigurationException e) {
            throw new ApiException("获取配置文件失败，", e);
        }
    }

    /**
     * 获取文件名
     */
    private String getFileName(String template, String className, String packageName, String moduleName, String genKey) {
        String packagePath = CommonConstants.BACK_END_PROJECT + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator;
        if (StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator + moduleName + File.separator;
        }

        if (template.contains(ENTITY_JAVA_VM)) {
            return packagePath + "entity" + File.separator + className + ".java";
        }

        if (template.contains(MAPPER_JAVA_VM)) {
            return packagePath + "mapper" + File.separator + className + "Mapper.java";
        }

        if (template.contains(SERVICE_JAVA_VM)) {
            return packagePath + "service" + File.separator + className + "Service.java";
        }

        if (template.contains(SERVICE_IMPL_JAVA_VM)) {
            return packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
        }

        if (template.contains(CONTROLLER_JAVA_VM)) {
            return packagePath + "controller" + File.separator + className + "Controller.java";
        }

        if (template.contains(MAPPER_XML_VM)) {
            return CommonConstants.BACK_END_PROJECT + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "mapper" + File.separator + className + "Mapper.xml";
        }

//		if (template.contains(MENU_SQL_VM)) {
//			return className.toLowerCase() + "_menu.sql";
//		}

        if (template.contains(INDEX_VUE_VM)) {
            return CommonConstants.FRONT_END_PROJECT + File.separator + "src" + File.separator + "views" +
                    File.separator + genKey + File.separator + className.toLowerCase() + File.separator + "index.vue";
        }

        if (template.contains(API_JS_VM)) {
            return CommonConstants.FRONT_END_PROJECT + File.separator + "src" + File.separator + "api" +
                    File.separator + genKey + File.separator + className.toLowerCase() + ".js";
        }

        if (template.contains(CRUD_JS_VM)) {
            return CommonConstants.FRONT_END_PROJECT + File.separator + "src" + File.separator + "const" + File.separator + "crud" +
                    File.separator + genKey + File.separator + className.toLowerCase() + ".js";
        }

        return null;
    }
}
