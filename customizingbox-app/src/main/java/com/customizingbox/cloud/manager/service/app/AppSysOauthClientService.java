package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOauthClient;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface AppSysOauthClientService extends IService<AppSysOauthClient> {

}
