package com.customizingbox.cloud.manager.service.app.impl.order;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppOrderItemMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderItemVO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrderItem;
import com.customizingbox.cloud.manager.service.app.AppOrderItemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * panda 订单item表
 *
 * @author Y
 * @date 2022-03-30 13:48:16
 */
@Service
@Slf4j
@AllArgsConstructor
public class AppOrderItemServiceImpl extends ServiceImpl<AppOrderItemMapper, AppOrderItem> implements AppOrderItemService<AppShopifyOrderItem> {
    private final AppStoreProductVariantMapper appStoreProductVariantMapper;
    private final AppOrderItemMapper appOrderItemMapper;



    @Override
    public List<AppOrderItem> findByOrderId(Long orderId) {
        return this.list(Wrappers.<AppOrderItem>lambdaQuery().eq(AppOrderItem::getOrderId, orderId));
    }

    @Override
    public List<AppOrderItem> notPlaceOrder(Long orderId) {
        return this.list(Wrappers.<AppOrderItem>lambdaQuery().eq(AppOrderItem::getOrderId, orderId)
                .eq(AppOrderItem::getPlaceStatus, OrderEnum.PLACE_STATUS.NOT_PLACE.getCode()));
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Boolean updateOrderItemProductRelevancy(Long variantId, Long adminProductId, Long adminVariantId) {
        return this.update(Wrappers.<AppOrderItem>lambdaUpdate().set(AppOrderItem::getAdminProductId, adminProductId)
                .set(AppOrderItem::getAdminVariantId, adminVariantId)
                .eq(AppOrderItem::getVariantId, variantId)
        );
    }

//    @Override
//    public AppQuotationOrderVO queryQuotation(String orderId) {
//        // TODO 这里查出美元汇率
//        BigDecimal usRate = BigDecimal.valueOf(6.3631);
//        List<AppQuotationOrderVO> orderPageVOS = appOrderItemMapper.queryByOrderId(orderId, usRate);
//        AppQuotationOrderVO resultOrderPageVo = null;
//        if (!CollectionUtils.isEmpty(orderPageVOS)) {
//            resultOrderPageVo = new AppQuotationOrderVO();
//            for (int i = 0; i < orderPageVOS.size(); i++) {
//                if (i == 0) {
//                    BeanUtils.copyProperties(orderPageVOS.get(i), resultOrderPageVo);
//                }
//                resultOrderPageVo.getOrderItems().add(BeanUtil.copyProperties(orderPageVOS.get(i), AppQuotationOrderItemVO.class));
//            }
//        }
//
//        return resultOrderPageVo;
//    }
}