package com.customizingbox.cloud.manager.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

@Component
public class TestJobHandler {

    @XxlJob("testJob")
    public void test() {
        System.out.println(">>>>test xxl-job execute ....");
    }
}
