package com.customizingbox.cloud.manager.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminExchangeRate;

import java.math.BigDecimal;

/**
 * 汇率表
 *
 * @author Y
 * @date 2022-03-31 09:34:33
 */
public interface AdminExchangeRateService extends IService<AdminExchangeRate> {

    /**
     * 根据code查询汇率
     */
    AdminExchangeRate getByCode(String code);

    /**
     * 根据code查询汇率值
     * @param code 货币代码
     * @return
     */
    BigDecimal getRateByCode(String code);
}
