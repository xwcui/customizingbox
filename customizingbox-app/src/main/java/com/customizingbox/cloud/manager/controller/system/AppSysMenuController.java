package com.customizingbox.cloud.manager.controller.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.customizingbox.cloud.manager.service.app.AppSysMenuService;
import com.customizingbox.cloud.manager.service.app.AppSysRoleService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysMenuTree;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysMenu;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRole;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppMenuVO;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import com.customizingbox.cloud.common.datasource.util.AppSysTreeUtil;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@AllArgsConstructor
@RequestMapping("/menu")
@Api(value = "menu", tags = "菜单管理模块")
public class AppSysMenuController {

    private final AppSysMenuService appSysMenuService;
    private final AppSysRoleService appSysRoleService;


    @ApiOperation(value = "返回树形菜单集合")
    @GetMapping(value = "/all/tree")
    public ApiResponse getAllTree() {
        List<AppSysMenuTree> appSysMenuTreeList = AppSysTreeUtil.buildTree(appSysMenuService.list(Wrappers.<AppSysMenu>lambdaQuery()
                .orderByAsc(AppSysMenu::getSort)).stream().collect(Collectors.toList()), CommonConstants.PARENT_ID);
        return ApiResponse.ok(appSysMenuTreeList);
    }


    @ApiOperation(value = "返回当前用户的树形菜单集合")
    @GetMapping
    public ApiResponse getUserMenu() {
        // 获取符合条件的菜单
        Set<AppMenuVO> all = new HashSet<>();
        SecurityUtils.getRoles().forEach(roleId -> all.addAll(appSysMenuService.findMenuByRoleId(roleId)));
        List<AppSysMenuTree> appSysMenuTreeList = all.stream()
                .filter(menuVo -> CommonConstants.MENU.equals(menuVo.getType()))
                .map(AppSysMenuTree::new)
                .sorted(Comparator.comparingInt(AppSysMenuTree::getSort))
                .collect(Collectors.toList());
        return ApiResponse.ok(AppSysTreeUtil.build(appSysMenuTreeList, CommonConstants.PARENT_ID));
    }


    @ApiOperation(value = "返回树形菜单集合")
    @GetMapping(value = "/tree")
    public ApiResponse getTree() {
        Set<AppMenuVO> all = new HashSet<>();
        SecurityUtils.getRoles().forEach(roleId -> all.addAll(appSysMenuService.findMenuByRoleId(roleId)));
        List<AppSysMenuTree> appSysMenuTreeList = all.stream().map(AppSysMenuTree::new).collect(Collectors.toList());
        return ApiResponse.ok(AppSysTreeUtil.build(appSysMenuTreeList, CommonConstants.PARENT_ID));
    }


    @ApiOperation(value = "返回角色的菜单集合")
    @GetMapping("/tree/{roleId}")
    public ApiResponse getRoleTree(@PathVariable String roleId) {
        return ApiResponse.ok(appSysMenuService.findMenuByRoleId(roleId)
                .stream()
                .map(AppMenuVO::getId)
                .collect(Collectors.toList()));
    }


    @ApiOperation(value = "通过ID查询菜单的详细信息")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:menu:get')")
    public ApiResponse getById(@PathVariable String id) {
        return ApiResponse.ok(appSysMenuService.getById(id));
    }


    @ApiOperation(value = "新增菜单")
    @SysLog("新增菜单")
    @PostMapping
    @PreAuthorize("@ato.hasAuthority('sys:menu:add')")
    public ApiResponse save(@Valid @RequestBody AppSysMenu sysMenu) {
        appSysMenuService.saveMenu(sysMenu);
        return ApiResponse.ok();
    }


    @ApiOperation(value = "删除菜单")
    @SysLog("删除菜单")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:menu:del')")
    public ApiResponse removeById(@PathVariable String id) {
        return appSysMenuService.removeMenuById(id);
    }


    @ApiOperation(value = "更新菜单")
    @SysLog("更新菜单")
    @PutMapping
    @PreAuthorize("@ato.hasAuthority('sys:menu:edit')")
    public ApiResponse update(@Valid @RequestBody AppSysMenu sysMenu) {
        sysMenu.setUpdateTime(LocalDateTime.now());
        return ApiResponse.ok(appSysMenuService.updateMenuById(sysMenu));
    }


    @ApiOperation(value = "返回租户管理员角色的菜单集合")
    @GetMapping("/tree/tenant/{tenantId}")
    @PreAuthorize("@ato.hasAuthority('sys:tenant:edit')")
    public ApiResponse getRoleTreeTenant(@PathVariable String tenantId) {
        TenantContextHolder.setTenantId(tenantId);
        //找出指定租户的管理员角色
        AppSysRole appSysRole = appSysRoleService.getOne(Wrappers.<AppSysRole>lambdaQuery().eq(AppSysRole::getRoleCode, CommonConstants.ROLE_CODE_ADMIN));
        List<String> listMenuVO = appSysMenuService.findMenuByRoleId(appSysRole.getId()).stream().map(AppMenuVO::getId).collect(Collectors.toList());
        Map<String, Object> map = new HashMap<>();
        map.put("sysRole", appSysRole);
        map.put("listMenuVO", listMenuVO);
        //菜单集合
        return ApiResponse.ok(map);
    }
}
