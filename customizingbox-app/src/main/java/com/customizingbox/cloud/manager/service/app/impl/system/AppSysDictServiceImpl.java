package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.manager.service.app.AppSysDictService;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysDictMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysDictValueMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDict;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDictValue;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AppSysDictServiceImpl extends ServiceImpl<AppSysDictMapper, AppSysDict> implements AppSysDictService {

    private final AppSysDictValueMapper appSysDictValueMapper;

    /**
     * 根据ID 删除字典
     *
     * @param id 字典ID
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse removeDict(String id) {
        baseMapper.deleteById(id);
        appSysDictValueMapper.delete(Wrappers.<AppSysDictValue>lambdaQuery()
                .eq(AppSysDictValue::getDictId, id));
        return ApiResponse.ok();
    }

    /**
     * 更新字典
     *
     * @param dict 字典
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse updateDict(AppSysDict dict) {
        this.updateById(dict);
        AppSysDictValue appSysDictValue = new AppSysDictValue();
        appSysDictValue.setType(dict.getType());
        appSysDictValueMapper.update(appSysDictValue, Wrappers.<AppSysDictValue>lambdaQuery().eq(AppSysDictValue::getDictId, dict.getId()));
        return ApiResponse.ok();
    }
}
