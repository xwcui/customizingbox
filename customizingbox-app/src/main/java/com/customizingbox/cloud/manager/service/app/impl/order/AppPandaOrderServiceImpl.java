package com.customizingbox.cloud.manager.service.app.impl.order;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.core.constant.enums.TransactionEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.mapper.admin.transport.AdminCountryInfoMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.order.panda.AppPandaOrderMapper;
import com.customizingbox.cloud.common.datasource.model.admin.setting.entity.AdminExchangeRate;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.dto.AppToPandaOrderDTO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderPaidParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderParentPaidParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaProcessingOrderParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.*;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderStatusParam;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.common.datasource.util.IdGenerate;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.admin.AdminExchangeRateService;
import com.customizingbox.cloud.manager.service.admin.TransportService;
import com.customizingbox.cloud.manager.service.app.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * panda 订单表
 *
 * @author Y
 * @date 2022-04-13 17:03:42
 */
@Service
@Slf4j
public class AppPandaOrderServiceImpl extends ServiceImpl<AppPandaOrderMapper, AppPandaOrder> implements AppPandaOrderService {

    @Autowired
    private AppOrderService appOrderService;
    @Autowired
    private AppOrderItemService appOrderItemService;
    @Autowired
    private AppStoreProductVariantService appStoreProductVariantService;
    @Autowired
    private AppPandaOrderItemService appPandaOrderItemService;
    @Autowired
    private AdminExchangeRateService adminExchangeRateService;
    @Autowired
    private AppSysUserService appSysUserService;
    @Autowired
    private TransportService transportService;
    @Autowired
    private AdminCountryInfoMapper adminCountryInfoMapper;
    @Autowired
    private AppSysUserTransactionRecordService appSysUserTransactionRecordService;

    @Value("${user.vat}")
    private String vat;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean toPandaOrder(List<Long> orderIds) {
        Boolean result = true;
        List<AppToPandaOrderDTO> orderItemDB = new LinkedList<>();
        for (Long orderId : orderIds) {
            AppOrder appOrder = appOrderService.getById(orderId);
            if (ObjectUtils.isEmpty(appOrder)) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "网络异常请重新操作");
            }
            // 查询订单关联的变体
            List<AppOrderItem> orderItems = appOrderItemService.notPlaceOrder(orderId);
            if (CollectionUtils.isEmpty(orderItems)) {
                log.info("订单: {}, 已全部下单, 不可重复下单");
                continue;
            }
            // 有一个变体的admin产品id没对上, 就让重新下单, 但是本次请求会改变admin产品id
            result = verifyOrderProduct(orderItems, appOrder) ? result : false;
            // 为了节省请求数据库次数, 这里同一个订单的item放到一个List里, 全部校验通过了就保存
            AppToPandaOrderDTO appToPandaOrderDTO = new AppToPandaOrderDTO();
            appToPandaOrderDTO.setAppOrder(appOrder);
            appToPandaOrderDTO.setAppOrderItems(orderItems);
            orderItemDB.add(appToPandaOrderDTO);
        }
        if (!result) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "订单产品发生变动, 请重新下单");
        }
        orderItemDB.forEach(appToPandaOrderDTO -> {
            List<AppOrderItem> appOrderItems = appToPandaOrderDTO.getAppOrderItems();
            // 筛选出已经关联产品的item订单, 开始下单
            List<AppOrderItem> collect = appOrderItems.stream().filter(appOrderItem -> appOrderItem.getAdminVariantId() != null).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(collect)) {
                Long pandaOrderId = saveOrderToPanda(appToPandaOrderDTO.getAppOrder());
                for (AppOrderItem appOrderItem : collect) {
                    appPandaOrderItemService.saveOrderItemToPanda(appOrderItem, pandaOrderId);
                    // 修改接口为已下单
                    appOrderItem.setPlaceStatus(OrderEnum.PLACE_STATUS.SUCCESS_PLACE.getCode());
                    appOrderItemService.updateById(appOrderItem);
                }
            }
        });

        return true;
    }

    @Override
    public IPage<AppPandaToOrderVO> queryToOrderPage(Page page, AppOrderBaseParam baseParam) {
        AdminExchangeRate adminExchangeRate = adminExchangeRateService.getByCode(TransactionEnum.CURRENCY.USD.getCode());
        IPage<AppPandaToOrderVO> pageData = baseMapper.queryToOrderPage(page, adminExchangeRate.getRate()
                , OrderEnum.STATUS.ACTIVITY.getCode(), baseParam);

        List<AppPandaToOrderVO> resultOrderList = new ArrayList<>();
        List<AppPandaToOrderVO> records = pageData.getRecords();
        if (!CollectionUtils.isEmpty(records)) {
            pageData.setRecords(toOrderConvert(records));
        }
        return pageData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse<AppPandaPaidOrderParentVO> checkoutOrderPage(List<Long> orderIds) {
        if (!verifyPandaOrderProductOrderId(orderIds)) {
            return ApiResponse.orderFailed("产品发生变动, 请刷新页面后下单");
        }
        AppPandaPaidOrderParentVO appPandaPaidOrderParentVO = new AppPandaPaidOrderParentVO();
        AdminExchangeRate adminExchangeRate = adminExchangeRateService.getByCode(TransactionEnum.CURRENCY.USD.getCode());
        for (int i = 0; i < orderIds.size(); i++) {
            Long pandaOrderId = orderIds.get(i);
            List<AppPandaPaidOrderVO> appPandaPaidOrders = baseMapper.queryPaidOrder(pandaOrderId, adminExchangeRate.getRate());
            ApiException.isNullOrderException(appPandaPaidOrders, "订单查询异常, 请重新下单");
            // 计算订单变体信息
            AppPandaPaidOrderVO appPaidOrder = paidOrderConvert(appPandaPaidOrders, adminExchangeRate.getRate());
            // 计算运费
            List<AdminTransportPriceResultDto> adminTransportPriceResult = getFreight(pandaOrderId, null, appPaidOrder.getAddressId());
            appPaidOrder.setTransportPrice(adminTransportPriceResult);
            appPandaPaidOrderParentVO.getPaidOrders().add(appPaidOrder);
            // 加产品总价
            appPandaPaidOrderParentVO.setProductTotalAmount(
                    appPandaPaidOrderParentVO.getProductTotalAmount().add(appPaidOrder.getProductAmount()));
            // 加VAT总价
            appPandaPaidOrderParentVO.setVatTotalAmount(
                    appPandaPaidOrderParentVO.getVatTotalAmount().add(appPaidOrder.getVatAmount()));
            // 支付总额
        }
        appPandaPaidOrderParentVO.setTotalPaidAmount(appPandaPaidOrderParentVO.getProductTotalAmount().add(appPandaPaidOrderParentVO.getVatTotalAmount())
                .add(appPandaPaidOrderParentVO.getFreightTotalAmount()));
        return ApiResponse.ok(appPandaPaidOrderParentVO);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ApiResponse<AppPandaOrderPaySuccessVO> paid(AppPandaOrderParentPaidParam appPandaOrderParentPaidParam) {
        // TODO 判断余额是否足够支付, 不够的话直接返回
        if (!verifyPandaOrderProduct(appPandaOrderParentPaidParam)) {
            return ApiResponse.orderFailed("产品发生变动, 请刷新页面后下单");
        }
        List<AppPandaOrder> orders = new ArrayList<>();
        AdminExchangeRate adminExchangeRate = adminExchangeRateService.getByCode(TransactionEnum.CURRENCY.USD.getCode());
        BigDecimal productTotalAmount = BigDecimal.ZERO;
        BigDecimal freightTotalAmount = BigDecimal.ZERO;
        BigDecimal vatTotalAmount = BigDecimal.ZERO;
        BigDecimal paidTotalAmount = BigDecimal.ZERO;
        for (AppPandaOrderPaidParam orderPaidParam : appPandaOrderParentPaidParam.getOrderPaidParams()) {
            Long pandaOrderId = orderPaidParam.getOrderId();
            AppPandaOrder appPandaOrder = this.getById(pandaOrderId);

            // 校验产品价格
            List<AppPandaPaidOrderVO> appPandaPaidOrders = baseMapper.queryPaidOrder(pandaOrderId, adminExchangeRate.getRate());
            ApiException.isNullOrderException(appPandaPaidOrders, "订单查询异常, 请重新下单");
            // 计算订单变体信息
            AppPandaPaidOrderVO appPaidOrder = paidOrderConvert(appPandaPaidOrders, adminExchangeRate.getRate());
            BigDecimal productAmount = appPaidOrder.getProductAmount();
            if (productAmount.compareTo(orderPaidParam.getProductAmount()) != 0) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "产品金额异常");
            }
            productTotalAmount = productTotalAmount.add(productAmount);

            // 校验vat
            vatTotalAmount = vatTotalAmount.add(appPaidOrder.getVatAmount());
            if (orderPaidParam.getVatAmount().compareTo(appPaidOrder.getVatAmount()) != 0) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "支付金额异常");
            }
            // 计算运费0
            List<AdminTransportPriceResultDto> adminTransportPriceResult = getFreight(pandaOrderId, orderPaidParam.getTransportId(), appPaidOrder.getAddressId());
            ApiException.isNullOrderException(adminTransportPriceResult, "运费查询异常");
            AdminTransportPriceResultDto adminTransportPriceResultDto = adminTransportPriceResult.get(0);
            if (orderPaidParam.getFreightAmount().compareTo(adminTransportPriceResultDto.getTransportPrice()) != 0) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "运费异常");
            }
            freightTotalAmount = freightTotalAmount.add(adminTransportPriceResultDto.getTransportPrice());
            appPandaOrder.setFreightAmount(adminTransportPriceResultDto.getTransportPrice());
            // 校验总支付价格
            BigDecimal dbPaidAmount = productAmount.add(appPaidOrder.getVatAmount()).add(adminTransportPriceResultDto.getTransportPrice());
            if (dbPaidAmount.compareTo(orderPaidParam.getPaidAmount()) != 0) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "金额异常");
            }
            appPandaOrder.setTransportId(orderPaidParam.getTransportId());
            appPandaOrder.setProductAmount(productAmount);
            appPandaOrder.setVatAmount(appPaidOrder.getVatAmount());
            appPandaOrder.setPayAmount(appPaidOrder.getPaidAmount());
            orders.add(appPandaOrder);

        }
        // 校验总金额
        paidTotalAmount = productTotalAmount.add(freightTotalAmount).add(vatTotalAmount);
        if (paidTotalAmount.compareTo(appPandaOrderParentPaidParam.getPaidTotalAmount()) != 0) {
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "总金额异常");
        }

        String paymentId = IdGenerate.nextId().toString();
        log.info("订单校验完成, 开始支付订单");
        for (AppPandaOrder order : orders) {
            paidPandaOrder(order, paymentId);
        }
        log.info("订单完成");
        AppPandaOrderPaySuccessVO appPandaOrderPaySuccessVO = new AppPandaOrderPaySuccessVO();
        appPandaOrderPaySuccessVO.setPaidTotalAmount(paidTotalAmount);
        appPandaOrderPaySuccessVO.setTransactionId(paymentId);
        return ApiResponse.ok(appPandaOrderPaySuccessVO);
    }

    @Override
    public IPage<AppPandaProcessingOrderVO> paidOrderList(Page page, AppPandaProcessingOrderParam param) {
        IPage<AppPandaProcessingOrderVO> pageResult = baseMapper.paidOrderList(page, param);
        List<AppPandaProcessingOrderVO> records = pageResult.getRecords();
        if (!CollectionUtils.isEmpty(records)) {
            pageResult.setRecords(toProcessingOrderConvert(records));
        }
        return pageResult;
    }

    @Override
    public Boolean updateToOrderOrderStatus(AppOrderStatusParam param) {
        return this.update(Wrappers.<AppPandaOrder>lambdaUpdate().set(AppPandaOrder::getStatus, param.getStatus())
                .eq(AppPandaOrder::getId, param.getId())
                .eq(AppPandaOrder::getPayStatus, OrderEnum.PAY_STATUS.NOT_PAY.getCode()));
    }

    @Override
    public IPage<AppPandaToOrderVO> cancelledToOrderOrderPage(Page page, AppOrderBaseParam baseParam) {
        AdminExchangeRate adminExchangeRate = adminExchangeRateService.getByCode(TransactionEnum.CURRENCY.USD.getCode());
        IPage<AppPandaToOrderVO> pageData = baseMapper.queryToOrderPage(page, adminExchangeRate.getRate()
                , OrderEnum.STATUS.CANCELLED.getCode(), baseParam);

        List<AppPandaToOrderVO> records = pageData.getRecords();
        if (!CollectionUtils.isEmpty(records)) {
            pageData.setRecords(toOrderConvert(records));
        }
        return pageData;
    }

    private void paidPandaOrder(AppPandaOrder order, String paymentId) {
        BigDecimal payAmount = order.getPayAmount();
        Long userId = SecurityUtils.getUserId();
        log.info("订单: {} 开始支付, 支付金额为: {}", order.getId(), payAmount);
        // 订单支付
        Long transactionId = appSysUserTransactionRecordService.payOut(payAmount, userId
                , order.getId(), paymentId, "订单支付, paymentId: " + paymentId);
        log.info("订单: {} 开始结束, 交易id: {}, 开始修改订单状态", order.getId(), transactionId);
        if (!ObjectUtils.isEmpty(transactionId)) {
            // 修改订单状态
            order.setPayStatus(OrderEnum.PAY_STATUS.SUCCESS_PAY.getCode());
            order.setPaymentId(paymentId);
            order.setTransactionRecordId(transactionId);
            order.setPayType(OrderEnum.PAY_TYPE.BALANCE.getCode());
            order.setPayTime(LocalDateTime.now());
            this.updateById(order);
            log.info("订单: {} 修改支付状态完成, 交易id: {}", order.getId(), transactionId);
        } else {
            // 支付失败
            throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "订单支付失败");
        }
    }


    /**
     * 保存panda订单
     */
    private Long saveOrderToPanda(AppOrder appOrder) {
        Long userId = SecurityUtils.getUserId();
        AppSysUser appSysUser = appSysUserService.getById(userId);

        AppPandaOrder appPandaOrder = new AppPandaOrder();
        appPandaOrder.setSourceId(appOrder.getSourceId());
        appPandaOrder.setOrderNo(UUID.randomUUID().toString().replace("-", ""));
        appPandaOrder.setStoreId(appOrder.getStoreId());
        appPandaOrder.setOrderName(appOrder.getOrderName());
        appPandaOrder.setStoreName(appOrder.getStoreName());
        appPandaOrder.setPlatformType(appOrder.getPlatformType());
        appPandaOrder.setUserId(appOrder.getUserId());
        appPandaOrder.setPayStatus(OrderEnum.PAY_STATUS.NOT_PAY.getCode());
        appPandaOrder.setFreightStatus(OrderEnum.FREIGHT_STATUS.WAIT_OUT.getCode());
        appPandaOrder.setRefundStatus(OrderEnum.REFUND_STATUS.NOT.getCode());
        appPandaOrder.setSourcePaidStatus(appOrder.getFulfillmentStatus());
        appPandaOrder.setUploadShStatus(OrderEnum.SH_UPLOAD_STATUS.NOT.getCode());
        appPandaOrder.setSourceMark(appOrder.getSourceMark());
        appPandaOrder.setAddressId(appOrder.getAddressId());
        if (!ObjectUtils.isEmpty(appSysUser)) {
            appPandaOrder.setAdminUserId(appSysUser.getAdminUserId());
            appPandaOrder.setAdminPlaceOrderUserId(appSysUser.getAdminUserId());
        }
        this.save(appPandaOrder);
        // 更新appOrder下单状态
        appOrder.setPlaceStatus(OrderEnum.PLACE_STATUS.PORTION_PLACE.getCode());
        appOrderService.updateById(appOrder);
        return appPandaOrder.getId();
    }

    /**
     * 校验订单中产品映射关系是否改变, 如果改变则修改数据库
     *
     * @param orderItems
     * @return true: 校验通过, 没有改变, false : 校验失败, 有变动
     */
    private boolean verifyOrderProduct(List<AppOrderItem> orderItems, AppOrder appOrder) {
        Boolean result = true;
        // 这个状态是用来修改app order表的关联关系的
        Boolean relevancy = true;
        for (AppOrderItem orderItem : orderItems) {
            Long sourceVariantId = orderItem.getSourceVariantId();
            // 查询产品变体表里最新的关联关系
            AppStoreProductVariant variant = appStoreProductVariantService.getBySourceId(sourceVariantId, StoreEnum.Type.SHOPIFY.getCode());

            if (!ObjectUtils.isEmpty(variant) && (variant.getAdminVariantId() != null && !variant.getAdminVariantId().equals(orderItem.getAdminVariantId()))
                    || (orderItem.getAdminVariantId() != null && !orderItem.getAdminVariantId().equals(variant.getAdminVariantId()))) {
                // 关联admin发生了变化, 这里让重新下单, 并修改映射关系
                orderItem.setAdminVariantId(variant.getAdminVariantId());
                orderItem.setAdminProductId(variant.getProductId());
                appOrderItemService.updateById(orderItem);
                if (ObjectUtils.isEmpty(variant.getAdminVariantId())) {
                    // 有一个没关联上, 状态就是存在未关联
                    relevancy = false;
                }
                result = false;
            }
        }
        if (!relevancy) {
            // order 表产品和admin产品关联关系出现变动, 这里修改
            appOrder.setRelevancyStatus(OrderEnum.RELEVANCY_STATUS.ALL_NOT_RELEVANCY.getCode());
            appOrderService.updateById(appOrder);
        }
        return result;
    }


    /**
     * 校验Panda订单中产品映射关系是否改变, 如果改变则修改数据库
     *
     * @return true: 校验通过, 没有改变, false : 校验失败, 有变动
     */
    private Boolean verifyPandaOrderProductOrderId(List<Long> orderIds) {
        Boolean result = true;
        for (Long orderId : orderIds) {
            AppPandaOrder appPandaOrder = this.getById(orderId);
            ApiException.isNullOrderException(appPandaOrder, "订单数据异常");

            // 查看订单是否是未支付状态
            if (!OrderEnum.PAY_STATUS.NOT_PAY.getCode().equals(appPandaOrder.getPayStatus())) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "订单状态异常");
            }
            List<AppPandaOrderItem> pandaOrderItems = appPandaOrderItemService.queryByOrderId(orderId);
            result = verifyPandaOrderProduct(pandaOrderItems) ? result : false;
        }
        return result;
    }

    /**
     * 校验Panda订单中产品映射关系是否改变, 如果改变则修改数据库
     *
     * @return true: 校验通过, 没有改变, false : 校验失败, 有变动
     */
    private Boolean verifyPandaOrderProduct(AppPandaOrderParentPaidParam appPandaOrderParentPaidParam) {
        Boolean result = true;
        for (AppPandaOrderPaidParam appPandaOrderPaidParam : appPandaOrderParentPaidParam.getOrderPaidParams()) {
            Long orderId = appPandaOrderPaidParam.getOrderId();
            AppPandaOrder appPandaOrder = this.getById(orderId);
            ApiException.isNullOrderException(appPandaOrder, "订单数据异常");
            // 查看订单是否是未支付状态
            if (!OrderEnum.PAY_STATUS.NOT_PAY.getCode().equals(appPandaOrder.getPayStatus())) {
                throw new ApiException(ApiResStatus.TO_ORDER_ERROR.getStatus(), "订单状态异常");
            }

            List<AppPandaOrderItem> pandaOrderItems = appPandaOrderItemService.queryByOrderId(orderId);
            result = verifyPandaOrderProduct(pandaOrderItems) ? result : false;
        }
        return result;
    }

    /**
     * 校验Panda订单中产品映射关系是否改变, 如果改变则修改数据库
     *
     * @param pandaOrderItems
     * @return true: 校验通过, 没有改变, false : 校验失败, 有变动
     */
    private boolean verifyPandaOrderProduct(List<AppPandaOrderItem> pandaOrderItems) {
        Boolean result = true;
        for (AppPandaOrderItem pandaOrderItem : pandaOrderItems) {
            Long variantId = pandaOrderItem.getVariantId();
            // 查询产品变体表里最新的关联关系
            AppStoreProductVariant variant = appStoreProductVariantService.getById(variantId);
            if ((variant.getAdminVariantId() != null && !variant.getAdminVariantId().equals(pandaOrderItem.getAdminVariantId()))
                    || (pandaOrderItem.getAdminVariantId() != null && !pandaOrderItem.getAdminVariantId().equals(variant.getAdminVariantId()))) {
                // 关联admin发生了变化, 这里让重新下单, 并修改映射关系
                pandaOrderItem.setAdminVariantId(variant.getAdminVariantId());
                pandaOrderItem.setAdminProductId(variant.getAdminProductId());
                appOrderItemService.updateById(pandaOrderItem);
                result = false;
            }
        }
        return result;
    }


    /**
     * 将多条item订单合成一个主订单(里面包含item订单)
     *
     * @param records
     * @return
     */
    private List<AppPandaToOrderVO> toOrderConvert(List<AppPandaToOrderVO> records) {
        List<AppPandaToOrderVO> resultOrderList = new ArrayList<>();
        Long lastOrderId = null;
        AppPandaToOrderVO orderPageVO = null;
        for (int i = 0; i < records.size(); i++) {
            Long id = records.get(i).getId();
            if (!id.equals(lastOrderId)) {
                lastOrderId = id;
                orderPageVO = new AppPandaToOrderVO();
                resultOrderList.add(orderPageVO);
                BeanUtils.copyProperties(records.get(i), orderPageVO);
            }
            orderPageVO.getOrderItems().add(BeanUtil.copyProperties(records.get(i), AppPandaToOrderItemVO.class));
        }
        return resultOrderList;
    }

    /**
     * 将多条item订单合成一个主订单(里面包含item订单)
     *
     * @param records
     * @param usRate 美元倍率
     * @return
     */
    private AppPandaPaidOrderVO paidOrderConvert(List<AppPandaPaidOrderVO> records, BigDecimal usRate) {
        AppPandaPaidOrderVO pandaPaidOrderVO = BeanUtil.copyProperties(records.get(0), AppPandaPaidOrderVO.class);
        BigDecimal productAmount = BigDecimal.ZERO;
        for (int i = 0; i < records.size(); i++) {
            AppPandaPaidOrderItemVO appPandaPaidOrderItemVO = BeanUtil.copyProperties(records.get(i), AppPandaPaidOrderItemVO.class);
            // 计算商品总金额
            BigDecimal itemPayAmount = appPandaPaidOrderItemVO.getQuotePrice().multiply(BigDecimal.valueOf(appPandaPaidOrderItemVO.getFulfillableQuantity()));
            productAmount = productAmount.add(itemPayAmount);
            pandaPaidOrderVO.getOrderItems().add(appPandaPaidOrderItemVO);

            // 修改item订单中的 产品价格
            Long pandaOrderItemId = appPandaPaidOrderItemVO.getPandaOrderItemId();
            appPandaOrderItemService.update(Wrappers.<AppPandaOrderItem>lambdaUpdate().set(AppPandaOrderItem::getUsExchangeRate, usRate)
                    .set(AppPandaOrderItem::getQuoteRate, appPandaPaidOrderItemVO.getSupplyRate())
                    .set(AppPandaOrderItem::getCostPrice, appPandaPaidOrderItemVO.getCostPrice())
                    .set(AppPandaOrderItem::getQuotePrice, appPandaPaidOrderItemVO.getQuotePrice())
                    .set(AppPandaOrderItem::getSupplyPrice, appPandaPaidOrderItemVO.getSupplyPrice())
                    .set(AppPandaOrderItem::getPayAmount, itemPayAmount)
                    .eq(AppPandaOrderItem::getId, pandaOrderItemId));
        }

        // 查询用户是否需要vat
        AppSysUser appSysUser = appSysUserService.getById(SecurityUtils.getUserId());
        ApiException.isNullOrderException(appSysUser, "用户异常, 请刷新重新登录");
        if (StringUtils.isEmpty(appSysUser.getIoss())) {
            // 需要vat
            pandaPaidOrderVO.setVatAmount(productAmount.multiply(new BigDecimal(vat)).setScale(2, BigDecimal.ROUND_HALF_UP));
        } else {
            pandaPaidOrderVO.setVatAmount(BigDecimal.ZERO);
        }
        pandaPaidOrderVO.setProductAmount(productAmount);
        // 计算当前订单支付总金额 (TODO 这里还有VAT和用费)
        BigDecimal paidAmount = pandaPaidOrderVO.getPaidAmount().add(productAmount).add(pandaPaidOrderVO.getVatAmount());
        pandaPaidOrderVO.setPaidAmount(paidAmount);
        return pandaPaidOrderVO;
    }

    /**
     * 根据订单获取可用物流列表,
     *
     * @param pandaOrderId
     * @return
     */
    private List<AdminTransportPriceResultDto> getFreight(Long pandaOrderId, Long transportId, Long addressId) {
        AdminTransportPriceDTO transportPrices = appPandaOrderItemService.getOrderWeightAndVolume(pandaOrderId);
        AdminTransportPriceDTO adminTransportPriceDTO = new AdminTransportPriceDTO();
        adminTransportPriceDTO.setVolume(transportPrices.getVolume());
        adminTransportPriceDTO.setWeight(transportPrices.getWeight());
        String[] split = transportPrices.getLogisticsIds().split(",");
        List<Long> logisticsIds = Arrays.stream(split).map(val -> Long.valueOf(val)).collect(Collectors.toList());
        adminTransportPriceDTO.setLogisticsId(logisticsIds);
        adminTransportPriceDTO.setTransportId(transportId);
        // 目的地
        Long endCountryId = adminCountryInfoMapper.queryEndCountryByAddrId(addressId);
        adminTransportPriceDTO.setEndCountryId(endCountryId);
        // TODO 这里从全局配置中取
        adminTransportPriceDTO.setBeginCountryId(1514547870002905090L);
        // 查询运输方式
        List<AdminTransportPriceResultDto> adminTransportPriceResultDtos = transportService.getTransportPrice(adminTransportPriceDTO);
        return adminTransportPriceResultDtos;
    }

    /**
     * 将多条item订单合成一个主订单(里面包含item订单)
     */
    private List<AppPandaProcessingOrderVO> toProcessingOrderConvert(List<AppPandaProcessingOrderVO> records) {
        List<AppPandaProcessingOrderVO> resultOrderList = new ArrayList<>();
        Long lastOrderId = null;
        AppPandaProcessingOrderVO processingOrderVO = null;
        for (int i = 0; i < records.size(); i++) {
            Long id = records.get(i).getId();
            if (!id.equals(lastOrderId)) {
                lastOrderId = id;
                processingOrderVO = new AppPandaProcessingOrderVO();
                resultOrderList.add(processingOrderVO);
                BeanUtils.copyProperties(records.get(i), processingOrderVO);
            }
            processingOrderVO.getOrderItems().add(BeanUtil.copyProperties(records.get(i), AppPandaProcessingOrderItemVO.class));
        }
        return resultOrderList;
    }
}
