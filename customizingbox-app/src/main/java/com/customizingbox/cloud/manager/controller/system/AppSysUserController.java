package com.customizingbox.cloud.manager.controller.system;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.manager.service.app.AppSysRoleService;
import com.customizingbox.cloud.manager.service.app.AppSysTenantService;
import com.customizingbox.cloud.manager.service.app.AppSysUserRoleService;
import com.customizingbox.cloud.manager.service.app.AppSysUserService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserInfo;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRole;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysTenant;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUserRole;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/user")
@Api(value = "user", tags = "用户管理模块")
public class AppSysUserController {

    private final AppSysUserService appSysUserService;
    private final AppSysRoleService appSysRoleService;
    private final AppSysTenantService appSysTenantService;
    private final AppSysUserRoleService appSysUserRoleService;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();


    @ApiOperation(value = "获取当前用户全部信息")
    @GetMapping(value = {"/info"})
    public ApiResponse info() {
        String username = SecurityUtils.getUser().getUsername();
        AppSysUser user = appSysUserService.getOne(Wrappers.<AppSysUser>query()
                .lambda().eq(AppSysUser::getUsername, username));
        if (user == null) {
            return ApiResponse.failed("获取当前用户信息失败");
        }
        return ApiResponse.ok(appSysUserService.findUserInfo(user));
    }


    @ApiOperation(value = "获取指定用户全部信息")
    @GetMapping("/info/{username}")
    public ApiResponse info(@PathVariable String username) {
        AppSysUser sysUser = new AppSysUser();
        sysUser.setUsername(username);
        sysUser = appSysUserService.getByNoTenant(sysUser);
        if (sysUser == null) {
            return ApiResponse.failed(String.format("用户信息为空 %s", username));
        }

        TenantContextHolder.setTenantId(sysUser.getTenantId());
        if (CommonConstants.STATUS_NORMAL.equals(sysUser.getLockFlag())) {
            //查询所属租户状态是否正常，否则禁止登录
            AppSysTenant appSysTenant = appSysTenantService.getById(sysUser.getTenantId());
            if (CommonConstants.STATUS_LOCK.equals(appSysTenant.getStatus())) {
                sysUser.setLockFlag(CommonConstants.STATUS_LOCK);
            }
        }
        AppSysUserInfo appSysUserInfo = appSysUserService.findUserInfo(sysUser);
        return ApiResponse.ok(appSysUserInfo);
    }


    @ApiOperation(value = "通过ID查询用户信息")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:user:get')")
    public ApiResponse user(@PathVariable String id) {
        return ApiResponse.ok(appSysUserService.selectUserVoById(id));
    }


    @ApiOperation(value = "根据用户名查询用户信息")
    @GetMapping("/detail/{username}")
    public ApiResponse userByUsername(@PathVariable String username) {
        AppSysUser sysUser = new AppSysUser();
        sysUser.setUsername(username);
        return ApiResponse.ok(appSysUserService.getByNoTenant(sysUser));
    }


    @SysLog("删除用户信息")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:user:del')")
    @ApiOperation(value = "删除用户", notes = "根据ID删除用户")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "int", paramType = "path")
    public ApiResponse userDel(@PathVariable String id) {
        //拥有管理员角色的用户不让删除
        AppSysRole appSysRole = appSysRoleService.getOne(Wrappers.<AppSysRole>update().lambda()
                .eq(AppSysRole::getRoleCode, CommonConstants.ROLE_CODE_ADMIN));
        List<AppSysUserRole> listAppSysUserRole = appSysUserRoleService.list(Wrappers.<AppSysUserRole>update().lambda()
                .eq(AppSysUserRole::getUserId, id).eq(AppSysUserRole::getRoleId, appSysRole.getId()));
        if (listAppSysUserRole.size() > 0) {
            return ApiResponse.failed("无法删除拥有管理员角色的用户");
        }
        AppSysUser sysUser = appSysUserService.getById(id);
        return ApiResponse.ok(appSysUserService.deleteUserById(sysUser));
    }


    @ApiOperation(value = "添加用户")
    @SysLog("添加用户")
    @PostMapping
    @PreAuthorize("@ato.hasAuthority('sys:user:add')")
    public ApiResponse user(@RequestBody AppSysUserDTO appSysUserDto) {
        try {
            return ApiResponse.ok(appSysUserService.saveUser(appSysUserDto));
        } catch (DuplicateKeyException e) {
            if (e.getMessage().contains("uk_username")) {
                return ApiResponse.failed("用户名已被占用");
            } else if (e.getMessage().contains("uk_email")) {
                return ApiResponse.failed("邮箱已被占用");
            } else {
                return ApiResponse.failed(e.getMessage());
            }
        }
    }


    @ApiOperation(value = "更新用户信息")
    @SysLog("更新用户信息")
    @PutMapping
    @PreAuthorize("@ato.hasAuthority('sys:user:edit')")
    public ApiResponse updateUser(@Valid @RequestBody AppSysUserDTO appSysUserDto) {
        try {
            //查询出管理员角色，判断管理员角色是否至少有1个用户
            AppSysRole appSysRole = appSysRoleService.getOne(Wrappers.<AppSysRole>update().lambda()
                    .eq(AppSysRole::getRoleCode, CommonConstants.ROLE_CODE_ADMIN));
            if (!CollUtil.contains(appSysUserDto.getRoleIds(), appSysRole.getId())) {
                List<AppSysUserRole> listAppSysUserRole = appSysUserRoleService.list(Wrappers.<AppSysUserRole>update().lambda()
                        .eq(AppSysUserRole::getRoleId, appSysRole.getId()));
                if (listAppSysUserRole.size() <= 1) {
                    //只有一条记录，判断是否当前用户拥有
                    listAppSysUserRole = appSysUserRoleService.list(Wrappers.<AppSysUserRole>update().lambda()
                            .eq(AppSysUserRole::getRoleId, appSysRole.getId()).eq(AppSysUserRole::getUserId, appSysUserDto.getId()));
                    if (listAppSysUserRole.size() > 0) {
                        return ApiResponse.failed("至少需要一个用户拥有管理员角色");
                    }
                }
            }
            return ApiResponse.ok(appSysUserService.updateUser(appSysUserDto));
        } catch (DuplicateKeyException e) {
            if (e.getMessage().contains("uk_username")) {
                return ApiResponse.failed("用户名已被占用");
            } else if (e.getMessage().contains("uk_email")) {
                return ApiResponse.failed("邮箱已被占用");
            } else {
                return ApiResponse.failed(e.getMessage());
            }
        }
    }


    @ApiOperation(value = "修改用户密码")
    @SysLog("修改用户密码")
    @PutMapping("/password")
    @PreAuthorize("@ato.hasAuthority('sys:user:password')")
    public ApiResponse editPassword(@Valid @RequestBody AppSysUserDTO appSysUserDto) {
        AppSysUser sysUser = new AppSysUser();
        sysUser.setId(appSysUserDto.getId());
        sysUser.setPassword(ENCODER.encode(appSysUserDto.getPassword()));
        appSysUserService.updateById(sysUser);
        return ApiResponse.ok();
    }


    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @PreAuthorize("@ato.hasAuthority('sys:user:index')")
    public ApiResponse getUserPage(Page page, AppSysUserDTO appSysUserDTO) {
        return ApiResponse.ok(appSysUserService.getUsersWithRolePage(page, appSysUserDTO));
    }


    @ApiOperation(value = "数量查询")
    @GetMapping("/count")
    @PreAuthorize("@ato.hasAuthority('sys:user:index')")
    public ApiResponse getCount(AppSysUser sysUser) {
        return ApiResponse.ok(appSysUserService.count(Wrappers.query(sysUser)));
    }


    @ApiOperation(value = "修改个人信息")
    @SysLog("修改个人信息")
    @PutMapping("/edit")
    public ApiResponse updateUserInfo(@Valid @RequestBody AppSysUserDTO appSysUserDto) {
        return ApiResponse.ok(appSysUserService.updateUserInfo(appSysUserDto));
    }


    @ApiOperation(value = "查询")
    @GetMapping("/ancestor/{username}")
    public ApiResponse listAncestorUsers(@PathVariable String username) {
        return ApiResponse.ok(appSysUserService.listAncestorUsers(username));
    }

}
