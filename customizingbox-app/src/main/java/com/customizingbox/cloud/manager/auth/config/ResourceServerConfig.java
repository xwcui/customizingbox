package com.customizingbox.cloud.manager.auth.config;

import com.customizingbox.cloud.manager.auth.component.BaseResourceAuthExceptionEntryPoint;
import com.customizingbox.cloud.manager.auth.component.BaseUserAuthenticationConverter;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;

/**
 * 资源服务配置
 */
@Configuration
@AllArgsConstructor
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private final BaseResourceAuthExceptionEntryPoint baseResourceAuthExceptionEntryPoint;

    @Override
    @SneakyThrows
    public void configure(HttpSecurity security) {
        security
                .headers().frameOptions().disable()
                .and()
                .authorizeRequests()
                //接口白名单
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/**").permitAll()
                .antMatchers("/token/**", "/actuator/**", "/druid/**").permitAll()
                .antMatchers("/shopify/**").permitAll()
                .antMatchers("/paypal/**").permitAll()
                //其他请求都需要经过验证
                .anyRequest().authenticated()
                .and().csrf().disable();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        UserAuthenticationConverter userTokenConverter = new BaseUserAuthenticationConverter();
        accessTokenConverter.setUserTokenConverter(userTokenConverter);

        resources.authenticationEntryPoint(baseResourceAuthExceptionEntryPoint);
    }
    
}
