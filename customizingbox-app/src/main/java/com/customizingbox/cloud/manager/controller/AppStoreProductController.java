package com.customizingbox.cloud.manager.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductListVO;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductVariantListVO;
import com.customizingbox.cloud.manager.service.app.AppStoreProductService;
import com.customizingbox.cloud.manager.service.app.AppStoreProductVariantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/product")
@Api(value = "product", tags = "商户产品管理")
public class AppStoreProductController {

    private final AppStoreProductService storeProductService;
    private final AppStoreProductVariantService storeProductVariantService;

    @PostMapping("/page")
    @ApiOperation("分页查询商品列表")
    public ApiResponse<IPage<List<AppStoreProductListVO>>> pageAll(Page page) {
        IPage<List<AppStoreProductListVO>>  productList = storeProductService.listAll(page);
        return ApiResponse.ok(productList);
    }

    @PostMapping("/variantByPId")
    @ApiOperation("查询商品变体列表")
    public ApiResponse<IPage<List<AppStoreProductVariantListVO>>> variantByPId(Page page,
                                                                         @ApiParam(value = "产品id") @RequestParam Long productId) {
        IPage<List<AppStoreProductVariantListVO>>  productList = storeProductVariantService.appAdminVariantByPId(page, productId);
        return ApiResponse.ok(productList);
    }
}
