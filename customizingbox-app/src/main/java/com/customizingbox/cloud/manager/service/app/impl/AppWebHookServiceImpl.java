package com.customizingbox.cloud.manager.service.app.impl;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.manager.service.app.AppWebHookService;
import com.customizingbox.cloud.manager.util.ShopifyUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class AppWebHookServiceImpl implements AppWebHookService {

    private static String shopifyWebHook = "https://%s/admin/api/2022-01/webhooks.json";
    private static String shopifyWebHookCallback = "https://www.bemooy.com/consumer/shopify/webhook/callback";

    @Override
    @Async
    public Boolean batchSettingShopify(String shop, String token) {
        for (StoreEnum.Topic topic : StoreEnum.Topic.values()) {
            String type = topic.getType();
            String url = String.format(shopifyWebHook, shop);
            JSONObject webhook = new JSONObject();
            webhook.put("topic", type);
            webhook.put("address", shopifyWebHookCallback);
            webhook.put("format", "json");
            JSONObject param = new JSONObject();
            param.put("webhook", webhook);
            String body = HttpRequest.post(url).header(ShopifyUtils.createShopifyHeader(token)).body(param.toString()).execute().body();
            log.info("商户：{} 设置shopify -{} ,返回值: {}", shop, topic, body);
            // TODO 保存数据库

        }
        return false;
    }

//    public static void main(String[] args) {
//        AppWebHookServiceImpl appWebHookService = new AppWebHookServiceImpl();
//        appWebHookService.batchSettingShopify("customizingboxtest.myshopify.com", "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");
//    }
}
