//package com.customizingbox.cloud.manager.controller.system;
//
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.customizingbox.cloud.manager.service.app.AppSysDictService;
//import com.customizingbox.cloud.manager.service.app.AppSysDictValueService;
//import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
//import com.customizingbox.cloud.common.core.constant.CacheConstants;
//import com.customizingbox.cloud.common.core.util.ApiResponse;
//import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDict;
//import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysDictValue;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.AllArgsConstructor;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
///**
// * <p>
// * 字典表 前端控制器
// * </p>
// */
//@RestController
//@AllArgsConstructor
//@RequestMapping("/dict")
//@Api(value = "dict", tags = "字典管理模块")
//public class AppSysDictController {
//
//    private final AppSysDictService appSysDictService;
//    private final AppSysDictValueService appSysDictValueService;
//
//
//    @ApiOperation(value = "通过ID查询字典信息")
//    @GetMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:dict:get')")
//    public ApiResponse getById(@PathVariable String id) {
//        return ApiResponse.ok(appSysDictService.getById(id));
//    }
//
//
//    @ApiOperation(value = "分页列表")
//    @GetMapping("/page")
//    @PreAuthorize("@ato.hasAuthority('sys:dict:index')")
//    public ApiResponse<IPage> getDictPage(Page page, AppSysDict appSysDict) {
//        return ApiResponse.ok(appSysDictService.page(page, Wrappers.query(appSysDict)));
//    }
//
//
//    @ApiOperation(value = "通过字典类型查找")
//    @GetMapping("/type/{type}")
//    public ApiResponse getDictByType(@PathVariable String type) {
//        return ApiResponse.ok(appSysDictValueService.list(Wrappers.<AppSysDictValue>query().lambda().eq(AppSysDictValue::getType, type)));
//    }
//
//
//    @ApiOperation(value = "添加字典")
//    @SysLog("添加字典")
//    @PostMapping
//    @PreAuthorize("@ato.hasAuthority('sys:dict:add')")
//    public ApiResponse save(@Valid @RequestBody AppSysDict appSysDict) {
//        return ApiResponse.ok(appSysDictService.save(appSysDict));
//    }
//
//
//    @ApiOperation(value = "删除字典，并且清除字典缓存")
//    @SysLog("删除字典")
//    @DeleteMapping("/{id}")
//    @PreAuthorize("@ato.hasAuthority('sys:dict:del')")
//    public ApiResponse removeById(@PathVariable String id) {
//        return appSysDictService.removeDict(id);
//    }
//
//
//    @ApiOperation(value = "修改字典")
//    @PutMapping
//    @SysLog("修改字典")
//    @PreAuthorize("@ato.hasAuthority('sys:dict:edit')")
//    public ApiResponse updateById(@Valid @RequestBody AppSysDict appSysDict) {
//        return appSysDictService.updateDict(appSysDict);
//    }
//
//
//    @ApiOperation(value = "分页查询")
//    @GetMapping("/item/page")
//    public ApiResponse getSysDictItemPage(Page page, AppSysDictValue appSysDictValue) {
//        return ApiResponse.ok(appSysDictValueService.page(page, Wrappers.query(appSysDictValue)));
//    }
//
//
//    @ApiOperation(value = "通过id查询字典项")
//    @GetMapping("/item/{id}")
//    public ApiResponse getDictItemById(@PathVariable("id") String id) {
//        return ApiResponse.ok(appSysDictValueService.getById(id));
//    }
//
//
//    @ApiOperation(value = "新增字典项")
//    @SysLog("新增字典项")
//    @PostMapping("/item")
//    public ApiResponse save(@RequestBody AppSysDictValue appSysDictValue) {
//        return ApiResponse.ok(appSysDictValueService.save(appSysDictValue));
//    }
//
//
//    @ApiOperation(value = "修改字典项")
//    @SysLog("修改字典项")
//    @PutMapping("/item")
//    public ApiResponse updateById(@RequestBody AppSysDictValue appSysDictValue) {
//        return appSysDictValueService.updateDictItem(appSysDictValue);
//    }
//
//
//    @ApiOperation(value = "通过id删除字典项")
//    @SysLog("删除字典项")
//    @DeleteMapping("/item/{id}")
//    public ApiResponse removeDictItemById(@PathVariable String id) {
//        return appSysDictValueService.removeDictItem(id);
//    }
//}
