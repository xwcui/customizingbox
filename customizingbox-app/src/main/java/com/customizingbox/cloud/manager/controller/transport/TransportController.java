package com.customizingbox.cloud.manager.controller.transport;


import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceDTO;
import com.customizingbox.cloud.common.datasource.model.admin.transport.dto.AdminTransportPriceResultDto;
import com.customizingbox.cloud.manager.service.admin.TransportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/transport")
@Api(value = "transport", tags = "app 物流")
public class TransportController {

    @Autowired
    private TransportService transportService;


    @ApiOperation(value = "获取订单对应物流方式")
    @PostMapping("/getTransportPrice")
    public ApiResponse<List<AdminTransportPriceResultDto>> getTransportPrice(@RequestBody AdminTransportPriceDTO adminTransportPriceDTO) {
        return ApiResponse.ok(transportService.getTransportPrice(adminTransportPriceDTO));
    }

}
