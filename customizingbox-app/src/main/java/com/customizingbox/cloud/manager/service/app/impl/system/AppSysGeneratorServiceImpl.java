package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.manager.util.GenUtils;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysGeneratorMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysGenTable;
import com.customizingbox.cloud.manager.service.app.AppSysGeneratorService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成器
 */
@Service
@AllArgsConstructor
public class AppSysGeneratorServiceImpl implements AppSysGeneratorService {

    private final AppSysGeneratorMapper appSysGeneratorMapper;

    /**
     * 分页查询表
     */
    @Override
    public IPage<List<Map<String, Object>>> getPage(Page page, String tableName, String sysDatasourceId) {
        return (IPage<List<Map<String, Object>>>) appSysGeneratorMapper.queryList(page, tableName);
    }

    @Override
    public Map<String, String> generatorView(AppSysGenTable appSysGenTable) {
        //查询表信息
        Map<String, String> table = queryTable(appSysGenTable.getTableName());
        //查询列信息
        List<Map<String, Object>> columns = queryColumns(appSysGenTable.getTableName());
        return GenUtils.generatorCode(appSysGenTable, table, columns, null);
    }

    /**
     * 生成代码
     *
     * @param appSysGenTable 生成表配置
     */
    @Override
    public byte[] generatorCode(AppSysGenTable appSysGenTable) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);

        //查询表信息
        Map<String, String> table = this.queryTable(appSysGenTable.getTableName());
        //查询列信息
        List<Map<String, Object>> columns = this.queryColumns(appSysGenTable.getTableName());
        //生成代码
        GenUtils.generatorCode(appSysGenTable, table, columns, zip);
        IoUtil.close(zip);
        return outputStream.toByteArray();
    }

    private Map<String, String> queryTable(String tableName) {
        return appSysGeneratorMapper.queryTable(tableName);
    }

    private List<Map<String, Object>> queryColumns(String tableName) {
        return appSysGeneratorMapper.queryColumns(tableName);
    }
}
