package com.customizingbox.cloud.manager.service.app.impl.paypal;

import com.customizingbox.cloud.common.core.constant.enums.TransactionEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.paypal.PaypalAmount;
import com.customizingbox.cloud.common.datasource.util.IdGenerate;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.config.PayPalCheckoutConstant;
import com.customizingbox.cloud.manager.config.PayPalConfig;
import com.customizingbox.cloud.manager.service.app.AppPaypalPaymentService;
import com.customizingbox.cloud.manager.service.app.PayPalCheckoutService;
import com.paypal.http.HttpResponse;
import com.paypal.http.exceptions.SerializeException;
import com.paypal.http.serializer.Json;
import com.paypal.orders.LinkDescription;
import com.paypal.orders.*;
import com.paypal.payments.Authorization;
import com.paypal.payments.Refund;
import com.paypal.payments.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PayPalCheckoutServiceImpl implements PayPalCheckoutService {

    @Autowired
    PayPalConfig config;

    @Autowired
    private AppPaypalPaymentService appPaypalPaymentService;


    /**
     * 创建退款请求体
     */
    public RefundRequest buildRefundRequestBody() {
        RefundRequest refundRequest = new RefundRequest();
        com.paypal.payments.Money money = new com.paypal.payments.Money();

        money.currencyCode("USD");
        money.value("40.00");
        refundRequest.amount(money);
        refundRequest.invoiceId("T202005230002");
        refundRequest.noteToPayer("7天无理由退款");
        return refundRequest;
    }

    /**
     * 申请退款
     */
    public HttpResponse<Refund> refundOrder(String orderNo) throws IOException {

        OrdersGetRequest ordersGetRequest = new OrdersGetRequest(orderNo);
        HttpResponse<Order> ordersGetResponse = null;
        try {
            ordersGetResponse = config.client().execute(ordersGetRequest);
        } catch (Exception e) {
            try {
                log.error("第1次调用paypal订单查询失败");
                ordersGetResponse = config.client().execute(ordersGetRequest);
            } catch (Exception e2) {
                try {
                    log.error("第2次调用paypal订单查询失败");
                    ordersGetResponse = config.client().execute(ordersGetRequest);
                } catch (Exception e3) {
                    log.error("第3次调用paypal订单查询失败，失败原因：{}", e3.getMessage());
                }
            }
        }
        String captureId = ordersGetResponse.result().purchaseUnits().get(0).payments().captures().get(0).id();
        CapturesRefundRequest request = new CapturesRefundRequest(captureId);
        request.prefer("return=representation");
        request.requestBody(buildRefundRequestBody());
        HttpResponse<Refund> response = null;
        try {
            response = config.client().execute(request);
        } catch (IOException e) {
            try {
                log.error("第1次调用paypal退款申请失败");
                response = config.client().execute(request);
            } catch (Exception e1) {
                try {
                    log.error("第2次调用paypal退款申请失败");
                    response = config.client().execute(request);
                } catch (Exception e2) {
                    log.error("第3次调用paypal退款申请失败，失败原因 {}", e2.getMessage());
                }
            }
        }
        log.info("Status Code = {}, Status = {}, RefundID = {}", response.statusCode(), response.result().status(), response.result().id());
        if("COMPLETED".equals(response.result().status())) {
            //进行数据库操作，修改状态为已退款（配合回调和退款查询确定退款成功）
            log.info("退款成功");
        }
        for (com.paypal.payments.LinkDescription link : response.result().links()) {
            log.info("Links-{}: {}    \tCall Type: {}", link.rel(), link.href(), link.method());
        }
        log.info("refundOrder response body: {}", new Json().serialize(response.result()));
        return response;
    }

    /**
     * 创建订单
     *
     * @param origin 客户的请求路径 测试使用 config.getDomain()， 线上使用 origin
     * @param paypalAmount 支付的金额
     * @return
     */
    @Override
    public OrderRequest buildCreateRequestBody(String origin, PaypalAmount paypalAmount) throws SerializeException {
        String local_order_no = IdGenerate.nextId().toString();
        String description = "Pay SourcinBox  recharge"  + " order amount: " + paypalAmount.getAmount() + ", Transaction ID: " + local_order_no;
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.checkoutPaymentIntent(PayPalCheckoutConstant.CAPTURE);
        ApplicationContext applicationContext = new ApplicationContext()
                .brandName(PayPalCheckoutConstant.BRANDNAME)
                .landingPage(PayPalCheckoutConstant.LANDINGPAGE)
                .cancelUrl(config.getDomain() /*origin*/+ PayPalCheckoutConstant.CANCEL_URL + "?local_order_no=" + local_order_no)
                .returnUrl(config.getDomain() /*origin*/ + PayPalCheckoutConstant.RETURN_URL);
        orderRequest.applicationContext(applicationContext);
        List<PurchaseUnitRequest> purchaseUnitRequests = new ArrayList<PurchaseUnitRequest>();
        PurchaseUnitRequest purchaseUnitRequest = new PurchaseUnitRequest()
                .description(description)
                .customId(local_order_no)
                .invoiceId(local_order_no)
                .amountWithBreakdown(new AmountWithBreakdown()
                        .currencyCode(TransactionEnum.CURRENCY.USD.getCode())
                        // value = itemTotal + shipping + handling + taxTotal + shippingDiscount;
                        .value(String.valueOf(paypalAmount.getAmount()))
/*                       .amountBreakdown(new AmountBreakdown()
                               // 所有项目的小计 itemTotal = Item[Supernote A6](value × quantity) + Item[帆布封套](value × quantity)
                                        .itemTotal(new com.paypal.orders.Money().currencyCode("USD").value(String.valueOf(paypalAmount.getAmount())))
                               //  给定范围内所有物品的手续费
                                        .handling(new com.paypal.orders.Money().currencyCode("USD").value(String.valueOf(paypalAmount.getPaypalFee())))
                        )*/
                )
                ;
        purchaseUnitRequests.add(purchaseUnitRequest);
        orderRequest.purchaseUnits(purchaseUnitRequests);
        log.info("创建paypal支付 {}",new Json().serialize(orderRequest));
        return orderRequest;
    }

    /**
     * service：执行扣款
     *      orderId：第三方订单号
     *      authroizeId：第三方交易流水号
     */
    @Override
    public ApiResponse captureOrder(String orderId, BaseUser user) throws IOException {
        OrdersCaptureRequest request = new OrdersCaptureRequest(orderId);
        request.header("prefer", "return=representation");
        request.requestBody(new OrderRequest());
        HttpResponse<Order> response = null;
        try {
            response = config.client().execute(request);
        } catch (IOException e1) {
            try {
                log.error("第1次调用paypal扣款失败");
                response = config.client().execute(request);
            } catch (Exception e) {
                try {
                    log.error("第2次调用paypal扣款失败");
                    response = config.client().execute(request);
                } catch (Exception e2) {
                    log.error("第3次调用paypal扣款失败，失败原因 {}", e2.getMessage() );
                }
            }
        }
        log.info("Status Code = {}, Status = {}, OrderID = {}", response.statusCode(), response.result().status(), response.result().id());
        for (LinkDescription link : response.result().links()) {
            log.info("Links-{}: {}    \tCall Type: {}", link.rel(), link.href(), link.method());
        }
        Order order = response.result();
        if ("COMPLETED".equals(response.result().status())){
            List<PurchaseUnit> purchaseUnits = order.purchaseUnits();
            if (!CollectionUtils.isEmpty(purchaseUnits)){
              return   ApiResponse.ok(appPaypalPaymentService.recharge(order,user,orderId));
            }
        }
        log.info(new Json().serialize(order));
        return ApiResponse.failed(response.result().status());
    }

    /**
     * service：执行扣款
     *      orderId：第三方订单号
     *      authroizeId：第三方交易流水号
     */
    public HttpResponse<Order> authorizeOrder(String orderId, BaseUser user) throws IOException {
        OrdersAuthorizeRequest request = new OrdersAuthorizeRequest(orderId);
        request.header("prefer", "return=representation");
        request.requestBody(new OrderRequest());
        HttpResponse<Order> response = null;
        try {
            response = config.client().execute(request);
        } catch (IOException e1) {
            try {
                log.error("第1次调用paypal扣款失败");
                response = config.client().execute(request);
            } catch (Exception e) {
                try {
                    log.error("第2次调用paypal扣款失败");
                    response = config.client().execute(request);
                } catch (Exception e2) {
                    log.error("第3次调用paypal扣款失败，失败原因 {}", e2.getMessage() );
                }
            }
        }
        log.info("Status Code = {}, Status = {}, OrderID = {}", response.statusCode(), response.result().status(), response.result().id());
        for (LinkDescription link : response.result().links()) {
            log.info("Links-{}: {}    \tCall Type: {}", link.rel(), link.href(), link.method());
        }
        Order order = response.result();
        if ("COMPLETED".equals(response.result().status())){

        }
        log.info(new Json().serialize(order));
        return response;
    }


    /**
     * service：获取授权订单信息（获取已付款交易详情）
     */
    public String getAuthorizeInfo (String transactionNo) throws IOException {

        AuthorizationsGetRequest request = new AuthorizationsGetRequest(transactionNo);
        HttpResponse<Authorization> response = config.client().execute(request);

        for (com.paypal.payments.LinkDescription link : response.result().links()) {
            System.out.println("\t" + link.rel() + ": " + link.href() + "\tCall Type: " + link.method());
        }

        return response.result().status();
    }

    /**
     * service：查询支付单详情
     */
    public HttpResponse<Order> getOrdersInfo(String orderNo)  {
        OrdersGetRequest request = new OrdersGetRequest(orderNo);
        HttpResponse<Order> response = null;
        try {
            response = config.client().execute(request);
        } catch (Exception e) {
            try {
                System.out.println("调用paypal订单查询失败，链接异常1");
                response = config.client().execute(request);
            } catch (Exception e2) {
                try {
                    System.out.println("调用paypal订单查询失败，链接异常2");
                    response = config.client().execute(request);
                } catch (Exception e3) {
                    System.out.println("调用paypal订单查询失败，链接异常3");
                    System.out.println(e3.getMessage());
                }
            }
        }
        return response;
    }


}