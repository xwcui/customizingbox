package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaOrderParentPaidParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.param.AppPandaProcessingOrderParam;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaOrderPaySuccessVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaPaidOrderParentVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaProcessingOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.vo.AppPandaToOrderVO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderStatusParam;

import java.util.List;

/**
 * panda订单
 *
 * @author Y
 * @date 2022-04-12 16:41:50
 */
public interface AppPandaOrderService extends IService<AppPandaOrder> {

    /**
     * 多订单预下单(只下单已经关联产品的订单)  -- 每一次都会做校验订单, 校验失败打回重新下单
     */
    Boolean toPandaOrder(List<Long> orderIds);

    /**
     * 分页查询待支付订单数据
     * @param page
     * @return
     */
    IPage<AppPandaToOrderVO> queryToOrderPage(Page page, AppOrderBaseParam baseParam);

    /**
     * 获取订单支付需要的值
     * @param orderIds 订单列表
     * @return
     */
    ApiResponse<AppPandaPaidOrderParentVO> checkoutOrderPage(List<Long> orderIds);

    /**
     * 支付订单
     * @param appPandaOrderParentPaidParam
     * @return
     */
    ApiResponse<AppPandaOrderPaySuccessVO> paid(AppPandaOrderParentPaidParam appPandaOrderParentPaidParam);

    /**
     *
     * @param page
     * @param param
     * @return
     */
    IPage<AppPandaProcessingOrderVO> paidOrderList(Page page, AppPandaProcessingOrderParam param);

    /**
     * 根据id修改ToOrder订单状态
     * @param param
     * @return
     */
    Boolean updateToOrderOrderStatus(AppOrderStatusParam param);

    /**
     * 查询取消ToOrder的订单列表
     * @param page
     * @param baseParam
     * @return
     */
    IPage<AppPandaToOrderVO> cancelledToOrderOrderPage(Page page, AppOrderBaseParam baseParam);
}
