package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysOauthClientMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOauthClient;
import com.customizingbox.cloud.manager.service.app.AppSysOauthClientService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class AppSysOauthClientServiceImpl extends ServiceImpl<AppSysOauthClientMapper, AppSysOauthClient> implements AppSysOauthClientService {

}
