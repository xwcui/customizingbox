package com.customizingbox.cloud.manager.syslog.event;


import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLog;
import com.customizingbox.cloud.manager.service.app.AppSysLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * 异步监听日志事件
 */
@Slf4j
@AllArgsConstructor
public class SysLogListener {

    private final AppSysLogService appSysLogService;

    @Async
    @Order
    @EventListener(SysLogEvent.class)
    public void saveSysLog(SysLogEvent event) {
        AppSysLog appSysLog = event.getAppSysLog();
        appSysLogService.save(appSysLog);
    }
}
