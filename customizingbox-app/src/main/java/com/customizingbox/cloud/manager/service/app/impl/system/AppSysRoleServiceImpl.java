package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.manager.service.app.AppSysRoleService;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysRoleMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysRoleMenuMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRole;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRoleMenu;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AppSysRoleServiceImpl extends ServiceImpl<AppSysRoleMapper, AppSysRole> implements AppSysRoleService {

    private final AppSysRoleMenuMapper appSysRoleMenuMapper;

    /**
     * 通过用户ID，查询角色信息
     */
    @Override
    public List<String> findRoleIdsByUserId(String userId) {
        return baseMapper.listRoleIdsByUserId(userId);
    }

    /**
     * 通过角色ID，删除角色,并清空角色菜单缓存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeRoleById(String id) {
        appSysRoleMenuMapper.delete(Wrappers.<AppSysRoleMenu>update().lambda().eq(AppSysRoleMenu::getRoleId, id));
        return this.removeById(id);
    }
}
