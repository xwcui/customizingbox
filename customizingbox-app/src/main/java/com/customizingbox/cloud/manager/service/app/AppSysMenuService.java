package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysMenu;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppMenuVO;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 */
public interface AppSysMenuService extends IService<AppSysMenu> {

	/**
	 * 通过角色编号查询URL 权限
	 *
	 * @param roleId 角色ID
	 * @return 菜单列表
	 */
	List<AppMenuVO> findMenuByRoleId(String roleId);

	/**
	 * 级联删除菜单
	 *
	 * @param id 菜单ID
	 * @return 成功、失败
	 */
	ApiResponse removeMenuById(String id);

	/**
	 * 更新菜单信息
	 *
	 * @param sysMenu 菜单信息
	 * @return 成功、失败
	 */
	Boolean updateMenuById(AppSysMenu sysMenu);

	/**
	 * 新增菜单
	 *
	 * @param sysMenu
	 */
	void saveMenu(AppSysMenu sysMenu);
}
