package com.customizingbox.cloud.manager.service.app.impl.product;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductImgDepotMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.dto.AppStoreProductImgDepotMappingDTO;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductImgDepot;
import com.customizingbox.cloud.manager.service.app.AppStoreProductImgDepotService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-26 10:32:19
 */
@Service
public class AppStoreProductImgDepotServiceImpl extends ServiceImpl<AppStoreProductImgDepotMapper, AppStoreProductImgDepot> implements AppStoreProductImgDepotService {

    @Override
    public Boolean delByProductId(Long productId) {
        return this.remove(Wrappers.<AppStoreProductImgDepot>lambdaQuery().eq(AppStoreProductImgDepot::getProductId, productId)
                .eq(AppStoreProductImgDepot::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
    }

    @Override
    public AppStoreProductImgDepot findByProductIdAndSourceId(Long productId, Long sourceId) {
        return this.getOne(Wrappers.<AppStoreProductImgDepot>lambdaQuery().eq(AppStoreProductImgDepot::getProductId, productId)
                .eq(AppStoreProductImgDepot::getSourceId, sourceId)
                .eq(AppStoreProductImgDepot::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
    }

    @Override
    public List<AppStoreProductImgDepot> findByProductId(Long productId) {
        return this.list(Wrappers.<AppStoreProductImgDepot>lambdaQuery()
                .select(AppStoreProductImgDepot::getId, AppStoreProductImgDepot::getSourceId, AppStoreProductImgDepot::getProductId)
                .eq(AppStoreProductImgDepot::getProductId, productId));
    }
}
