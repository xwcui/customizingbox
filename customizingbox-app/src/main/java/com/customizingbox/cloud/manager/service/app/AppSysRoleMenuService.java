package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRoleMenu;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 */
public interface AppSysRoleMenuService extends IService<AppSysRoleMenu> {

    /**
     * 更新角色菜单
     *
     * @param role
     * @param roleId  角色
     * @param menuIds 菜单ID拼成的字符串，每个id之间根据逗号分隔
     */
    Boolean saveRoleMenus(String role, String roleId, String menuIds);
}
