package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRole;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface AppSysRoleService extends IService<AppSysRole> {

    /**
     * 通过用户ID，查询角色信息
     */
    List<String> findRoleIdsByUserId(String userId);

    /**
     * 通过角色ID，删除角色
     */
    Boolean removeRoleById(String id);
}
