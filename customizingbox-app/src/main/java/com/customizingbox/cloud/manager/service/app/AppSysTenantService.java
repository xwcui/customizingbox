package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysTenant;


/**
 * <p>
 * 租户管理 服务类
 * </p>
 */
public interface AppSysTenantService extends IService<AppSysTenant> {

}
