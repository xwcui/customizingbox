package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysRoleMenuMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRoleMenu;
import com.customizingbox.cloud.manager.service.app.AppSysRoleMenuService;
import lombok.AllArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AppSysRoleMenuServiceImpl extends ServiceImpl<AppSysRoleMenuMapper, AppSysRoleMenu> implements AppSysRoleMenuService {

    private final CacheManager cacheManager;

    /**
     * @param roleId  角色
     * @param menuIds 菜单ID拼成的字符串，每个id之间根据逗号分隔
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveRoleMenus(String role, String roleId, String menuIds) {
        this.remove(Wrappers.<AppSysRoleMenu>query().lambda().eq(AppSysRoleMenu::getRoleId, roleId));

        if (StrUtil.isBlank(menuIds)) {
            return Boolean.TRUE;
        }
        List<AppSysRoleMenu> roleMenuList = Arrays.stream(menuIds.split(",")).map(menuId -> {
            AppSysRoleMenu roleMenu = new AppSysRoleMenu();
            roleMenu.setRoleId(roleId);
            roleMenu.setMenuId(menuId);
            return roleMenu;
        }).collect(Collectors.toList());

        //清空userinfo
        if (!ObjectUtils.isEmpty(cacheManager.getCache(CacheConstants.APP_USER_CACHE))) {
            cacheManager.getCache(CacheConstants.APP_USER_CACHE).clear();
        }
        return this.saveBatch(roleMenuList);
    }
}
