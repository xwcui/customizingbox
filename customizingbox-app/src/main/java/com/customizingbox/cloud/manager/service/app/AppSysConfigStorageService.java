package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysConfigStorage;

/**
 * 存储配置
 */
public interface AppSysConfigStorageService extends IService<AppSysConfigStorage> {

}
