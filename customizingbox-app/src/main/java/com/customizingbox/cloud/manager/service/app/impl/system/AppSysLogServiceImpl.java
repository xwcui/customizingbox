package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.manager.service.app.AppSysLogService;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysLogMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppSysPreLogVO;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLog;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 日志表 服务实现类
 * </p>
 */
@Service
public class AppSysLogServiceImpl extends ServiceImpl<AppSysLogMapper, AppSysLog> implements AppSysLogService {

    /**
     * 批量插入前端错误日志
     *
     * @param appSysPreLogVOList 日志信息
     * @return true/false
     */
    @Override
    public Boolean saveBatchLogs(List<AppSysPreLogVO> appSysPreLogVOList) {
        List<AppSysLog> appSysLogs = appSysPreLogVOList.stream().map(pre -> {
            AppSysLog log = new AppSysLog();
            log.setType(CommonConstants.LOG_TYPE_9);
            log.setTitle(pre.getInfo());
            log.setException(pre.getStack());
            log.setParams(pre.getMessage());
            log.setCreateTime(LocalDateTime.now());
            log.setRequestUri(pre.getUrl());
            log.setCreateBy(pre.getUser());
            return log;
        }).collect(Collectors.toList());
        return this.saveBatch(appSysLogs);
    }
}
