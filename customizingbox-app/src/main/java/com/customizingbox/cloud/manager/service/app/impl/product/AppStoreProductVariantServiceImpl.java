package com.customizingbox.cloud.manager.service.app.impl.product;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.admin.product.AdminStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductVariantListVO;
import com.customizingbox.cloud.manager.service.admin.AdminStoreProductVariantService;
import com.customizingbox.cloud.manager.service.app.AppStoreProductVariantService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
@Service
@Slf4j
@AllArgsConstructor
public class AppStoreProductVariantServiceImpl extends ServiceImpl<AppStoreProductVariantMapper, AppStoreProductVariant> implements AppStoreProductVariantService {

    private final AppStoreProductVariantMapper appStoreProductVariantMapper;
    private final AdminStoreProductVariantMapper adminStoreProductVariantMapper;


    @Override
    public AppStoreProductVariant findByProductIdAndSourceId(Long productId, Long sourceId) {
        return this.getOne(Wrappers.<AppStoreProductVariant>lambdaQuery().eq(AppStoreProductVariant::getProductId, productId)
                .eq(AppStoreProductVariant::getSourceId, sourceId));
    }

    @Override
    public List<AppStoreProductVariant> findByProductId(Long productId) {
        return this.list(Wrappers.<AppStoreProductVariant>lambdaQuery()
                .select(AppStoreProductVariant::getId, AppStoreProductVariant::getSourceId, AppStoreProductVariant::getProductId)
                .eq(AppStoreProductVariant::getProductId, productId));
    }

    @Override
    public AdminStoreProductVariant getAdminProductVariantBySourceId(Long sourceVariantId, Integer latformType) {
        AppStoreProductVariant appVariant = this.getOne(Wrappers.<AppStoreProductVariant>lambdaQuery()
                .eq(AppStoreProductVariant::getSourceId, sourceVariantId)
                .eq(AppStoreProductVariant::getPlatformType, latformType));
        if (ObjectUtils.isEmpty(appVariant)) {
            log.error("平台: {}, sourceVariantId: {} 原产品变体id 没有在数据库中找到", latformType, sourceVariantId);
            return null;
        }
        Long variantId = appVariant.getId();
        // 关联admin 产品
        AdminStoreProductVariant adminStoreProductVariant = adminStoreProductVariantMapper.selectById(variantId);
        return adminStoreProductVariant;
    }

    @Override
    public IPage<List<AppStoreProductVariantListVO>> appAdminVariantByPId(Page page, Long productId) {
        return appStoreProductVariantMapper.appAdminVariantByPId(page, productId);
    }

    @Override
    public AppStoreProductVariant getBySourceId(Long sourceVariantId, Integer latformType) {
        AppStoreProductVariant appVariant = this.getOne(Wrappers.<AppStoreProductVariant>lambdaQuery()
                .eq(AppStoreProductVariant::getSourceId, sourceVariantId)
                .eq(AppStoreProductVariant::getPlatformType, latformType));
        return appVariant;
    }
}
