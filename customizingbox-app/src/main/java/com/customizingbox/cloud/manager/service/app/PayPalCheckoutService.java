package com.customizingbox.cloud.manager.service.app;

import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.paypal.PaypalAmount;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.paypal.http.exceptions.SerializeException;
import com.paypal.orders.OrderRequest;

import java.io.IOException;

public interface PayPalCheckoutService {

    /**
     * 创建paypal订单返回支付地址
     *
     * @param origin
     * @param paypalAmount
     * @return
     */
    OrderRequest buildCreateRequestBody(String origin, PaypalAmount paypalAmount) throws SerializeException;

    /**
     * 执行支付
     * @param orderNo
     * @param user
     * @return
     */
    ApiResponse captureOrder(String orderNo, BaseUser user) throws IOException;
}
