package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderCancelParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderQuotationParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderStatusParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderVO;

/**
 * 原平台订单表(用于给客户展示, 客户可下单至panda订单表)
 *
 * @author Y
 * @date 2022-03-30 13:48:18
 */
public interface AppOrderService extends IService<AppOrder> {

    /**
     * 查询订单列表(含原产品变体信息)
     * @return
     */
    IPage<AppQuotationOrderVO> pageAll(Page page, AppOrderQuotationParam param);
    /**
     *  查询取消订单接口
     * @param page
     * @param param
     * @return
     */
    IPage<AppQuotationOrderVO> cancelledOrderPage(Page page, AppOrderQuotationParam param);

    /**
     * 根据id修改Quotation订单状态
     * @param param
     * @return
     */
    Boolean updateOrderStatus(AppOrderStatusParam param);
}
