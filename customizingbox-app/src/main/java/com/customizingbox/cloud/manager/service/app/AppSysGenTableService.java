package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysGenTable;

/**
 * 代码生成配置表
 */
public interface AppSysGenTableService extends IService<AppSysGenTable> {

}
