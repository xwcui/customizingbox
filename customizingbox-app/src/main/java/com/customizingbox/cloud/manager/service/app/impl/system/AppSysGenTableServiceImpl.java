package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.manager.service.app.AppSysGenTableService;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysGenTableMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysGenTable;
import org.springframework.stereotype.Service;

/**
 * 代码生成配置表
 */
@Service
public class AppSysGenTableServiceImpl extends ServiceImpl<AppSysGenTableMapper, AppSysGenTable> implements AppSysGenTableService {


}
