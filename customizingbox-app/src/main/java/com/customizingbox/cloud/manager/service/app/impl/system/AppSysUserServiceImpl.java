package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysUserMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserDTO;
import com.customizingbox.cloud.common.datasource.model.app.system.dto.AppSysUserInfo;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrgan;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUser;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysUserRole;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppMenuVO;
import com.customizingbox.cloud.common.datasource.model.app.system.vo.AppSysUserVO;
import com.customizingbox.cloud.manager.service.app.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
@Service
@AllArgsConstructor
public class AppSysUserServiceImpl extends ServiceImpl<AppSysUserMapper, AppSysUser> implements AppSysUserService {

    private final AppSysMenuService appSysMenuService;
    private final AppSysRoleService appSysRoleService;
    private final AppSysOrganService appSysOrganService;
    private final AppSysUserRoleService appSysUserRoleService;
    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 保存用户信息
     *
     * @param appSysUserDto DTO 对象
     * @return ok/fail
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveUser(AppSysUserDTO appSysUserDto) {
        AppSysUser sysUser = new AppSysUser();
        BeanUtils.copyProperties(appSysUserDto, sysUser);
        sysUser.setDelFlag(CommonConstants.STATUS_NORMAL);
        sysUser.setPassword(ENCODER.encode(appSysUserDto.getPassword()));
        baseMapper.insert(sysUser);
        List<AppSysUserRole> userRoleList = appSysUserDto.getRoleIds().stream().map(roleId -> {
            AppSysUserRole userRole = new AppSysUserRole();
            userRole.setUserId(sysUser.getId());
            userRole.setRoleId(roleId);
            return userRole;
        }).collect(Collectors.toList());
        return appSysUserRoleService.saveBatch(userRoleList);
    }

    /**
     * 通过查用户的全部信息
     *
     * @param sysUser 用户
     */
    @Override
    public AppSysUserInfo findUserInfo(AppSysUser sysUser) {
        AppSysUserInfo appSysUserInfo = new AppSysUserInfo();
        appSysUserInfo.setSysUser(sysUser);
        //设置角色列表  （ID）
        List<String> roleIds = appSysRoleService.findRoleIdsByUserId(sysUser.getId());
        appSysUserInfo.setRoles(ArrayUtil.toArray(roleIds, String.class));

        //设置权限列表（menu.permission）
        Set<String> permissions = new HashSet<>();
        roleIds.forEach(roleId -> {
            List<String> permissionList = appSysMenuService.findMenuByRoleId(roleId).stream()
                    .filter(menuVo -> StringUtils.isNotEmpty(menuVo.getPermission()))
                    .map(AppMenuVO::getPermission)
                    .collect(Collectors.toList());
            permissions.addAll(permissionList);
        });
        appSysUserInfo.setPermissions(ArrayUtil.toArray(permissions, String.class));
        return appSysUserInfo;
    }

    /**
     * 分页查询用户信息（含有角色信息）
     *
     * @param page    分页对象
     * @param appSysUserDTO 参数列表
     * @return
     */
    @Override
    public IPage getUsersWithRolePage(Page page, AppSysUserDTO appSysUserDTO) {
        return baseMapper.getUserVosPage(page, appSysUserDTO);
    }

    /**
     * 通过ID查询用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     */
    @Override
    public AppSysUserVO selectUserVoById(String id) {
        return baseMapper.getUserVoById(id);
    }

    /**
     * 删除用户
     *
     * @param sysUser 用户
     * @return Boolean
     */
    @Override
    public Boolean deleteUserById(AppSysUser sysUser) {
        appSysUserRoleService.deleteByUserId(sysUser.getId());
        this.removeById(sysUser.getId());
        return Boolean.TRUE;
    }

    @Override
    public Boolean updateUserInfo(AppSysUserDTO appSysUserDto) {
        AppSysUserVO appSysUserVO = baseMapper.getUserVoById(appSysUserDto.getId());
        AppSysUser sysUser = new AppSysUser();
        if (StrUtil.isNotBlank(appSysUserDto.getPassword()) && StrUtil.isNotBlank(appSysUserDto.getNewPassword())) {
            if (ENCODER.matches(appSysUserDto.getPassword(), appSysUserVO.getPassword())) {
                sysUser.setPassword(ENCODER.encode(appSysUserDto.getNewPassword()));
            } else {
                log.warn("原密码错误，修改密码失败:{}", appSysUserDto.getUsername());
                throw new RuntimeException("原密码错误，修改失败");
            }
        }
        sysUser.setId(appSysUserVO.getId());
        sysUser.setAvatar(appSysUserDto.getAvatar());
        sysUser.setEmail(appSysUserDto.getEmail());
        baseMapper.updateById(sysUser);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUser(AppSysUserDTO appSysUserDto) {
        AppSysUser sysUser = new AppSysUser();
        BeanUtils.copyProperties(appSysUserDto, sysUser);
        sysUser.setUpdateTime(LocalDateTime.now());
        sysUser.setPassword(null);
        this.updateById(sysUser);

        appSysUserRoleService.remove(Wrappers.<AppSysUserRole>update().lambda().eq(AppSysUserRole::getUserId, appSysUserDto.getId()));
        appSysUserDto.getRoleIds().forEach(roleId -> {
            AppSysUserRole userRole = new AppSysUserRole();
            userRole.setUserId(sysUser.getId());
            userRole.setRoleId(roleId);
            userRole.insert();
        });
        return Boolean.TRUE;
    }

    /**
     * 查询上级机构的用户信息
     *
     * @param username 用户名
     * @return R
     */
    @Override
    public List<AppSysUser> listAncestorUsers(String username) {
        AppSysUser sysUser = this.getOne(Wrappers.<AppSysUser>query().lambda()
                .eq(AppSysUser::getUsername, username));

        AppSysOrgan appSysOrgan = appSysOrganService.getById(sysUser.getOrganId());
        if (appSysOrgan == null) {
            return null;
        }

        String parentId = appSysOrgan.getParentId();
        return this.list(Wrappers.<AppSysUser>query().lambda()
                .eq(AppSysUser::getOrganId, parentId));
    }

    @Override
    public AppSysUser getByNoTenant(AppSysUser sysUser) {
        return baseMapper.getByNoTenant(sysUser);
    }

}
