package com.customizingbox.cloud.manager.service.app.impl.system;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysOrganRelationMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrgan;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysOrganRelation;
import com.customizingbox.cloud.manager.service.app.AppSysOrganRelationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
@AllArgsConstructor
public class AppSysOrganRelationServiceImpl extends ServiceImpl<AppSysOrganRelationMapper, AppSysOrganRelation> implements AppSysOrganRelationService {

    private final AppSysOrganRelationMapper appSysOrganRelationMapper;

    /**
     * 维护机构关系
     *
     * @param appSysOrgan 机构
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertOrganRelation(AppSysOrgan appSysOrgan) {
        //增加机构关系表
        AppSysOrganRelation condition = new AppSysOrganRelation();
        condition.setDescendant(appSysOrgan.getParentId());
        List<AppSysOrganRelation> relationList = appSysOrganRelationMapper
                .selectList(Wrappers.<AppSysOrganRelation>query().lambda().eq(AppSysOrganRelation::getDescendant, appSysOrgan.getParentId()))
                .stream().peek(relation -> {
                    relation.setTenantId(null);
                    relation.setDescendant(appSysOrgan.getId());
                }).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(relationList)) {
            this.saveBatch(relationList);
        }

        //自己也要维护到关系表中
        AppSysOrganRelation own = new AppSysOrganRelation();
        own.setDescendant(appSysOrgan.getId());
        own.setAncestor(appSysOrgan.getId());
        appSysOrganRelationMapper.insert(own);
    }

    /**
     * 通过ID删除机构关系
     */
    @Override
    public void deleteAllOrganRelation(String id) {
        baseMapper.deleteOrganRelationsById(id);
    }

    /**
     * 更新机构关系
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateOrganRelation(AppSysOrganRelation relation) {
        baseMapper.deleteOrganRelations(relation);
        List<AppSysOrganRelation> relationList = baseMapper.listOrganRelations(relation).stream().map(relation2 -> {
            relation2.setTenantId(null);
            return relation2;
        }).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(relationList)) {
            this.saveBatch(relationList);
        }
    }

}
