package com.customizingbox.cloud.manager.service.app.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.system.AppSysLogLoginMapper;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLogLogin;
import com.customizingbox.cloud.manager.service.app.AppSysLogLoginService;
import org.springframework.stereotype.Service;

/**
 * 登录日志表
 */
@Service
public class AppSysLogLoginServiceImpl extends ServiceImpl<AppSysLogLoginMapper, AppSysLogLogin> implements AppSysLogLoginService {

}
