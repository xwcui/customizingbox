package com.customizingbox.cloud.manager.controller;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.utils.StringUtils;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.core.util.LocalDateTimeUtils;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import com.customizingbox.cloud.manager.auth.entity.BaseUser;
import com.customizingbox.cloud.manager.auth.util.SecurityUtils;
import com.customizingbox.cloud.manager.service.app.AppStoreService;
import com.customizingbox.cloud.manager.service.app.AppWebHookService;
import com.customizingbox.cloud.manager.util.ShopifyUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;

/**
 * <p>
 * shopify 授权
 * </p>
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shopify/auth")
@Api(value = "shopifyAuth", tags = "shopify 授权")
@Slf4j
public class AppShopifyAuthController {

    private final AppStoreService appStoreService;
    private final AppWebHookService appWebHookService;

    private static String URL = "https://%s/admin/oauth/authorize?client_id=%s&grant_options[]=%s&redirect_uri=%s&scope=%s&state=%s";
    private static String CLIENT_KEY = "270c63967402a1b59a932f24d53a3d54";
    private static String CLIENT_SECRET = "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";
    private static String SCOPE = "write_product_listings,read_orders,read_products,read_locations";
    private static String GRANT_OPTIONS = "per-user";
    private static String REDIRECT_URI = "https://www.bemooy.com/app/shopify/auth/token";
    private static String REGULAR = "(https|http)\\:\\/\\/[a-zA-Z0-9][a-zA-Z0-9\\-]*\\.myshopify\\.com[\\/]?/";
    private static String ACCESS_TOKEN_URL = "https://%s/admin/oauth/access_token";
    private static String STORE_INFO = "https://%s/admin/api/2021-07/shop.json";

    @GetMapping("/index")
    @ApiOperation("添加商户")
    public ApiResponse index(@ApiParam(name = "商户名称") @RequestParam String shop) throws Exception {
        boolean isv = true;
        if (isv) {
            String domain = shop + ".myshopify.com";
            byte[] textByte = domain.getBytes("UTF-8");
            String params = Base64.encodeBase64String(textByte);
            System.out.println("index state:" + params);
            String path = String.format(URL, domain, CLIENT_KEY, GRANT_OPTIONS, REDIRECT_URI, SCOPE, params);
            AppStore store = appStoreService.queryStoreIfExit(domain, StoreEnum.Type.SHOPIFY.getCode());
            if (ObjectUtils.isEmpty(store)) {
                // 这里先保存商户店铺
                AppStore appStore = new AppStore();
                appStore.setName(shop);
                Long userId = SecurityUtils.getUserId();
                appStore.setUserId(1L);
                appStore.setDomain(domain);
                appStore.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
                appStoreService.save(appStore);
                log.info("商户：{}, 保存商户", shop);
            } else {
                log.info("商户：{},已经存在。不需要保存", shop);
            }
            return ApiResponse.ok(path);
        } else {
            return ApiResponse.failed("关联失败");
        }
    }

    @ResponseBody
    @GetMapping("/token")
    public ModelAndView getBrand(@RequestParam("code") String code, @RequestParam("shop") String shop, @RequestParam("state") String state,
                                 HttpServletRequest request) throws Exception {
        Enumeration<String> parameterNames = request.getParameterNames();
        System.out.println("收到shopify回调");
        String params = new String(Base64.decodeBase64(state), "UTF-8");
        if (!params.equals(shop)) {
            log.info("回调接口商户为空");
//            return;
        }
        boolean hostname = Pattern.matches(REGULAR, "https://" + shop + "/");
        if (!hostname) {
//            return;
        }
        String url = String.format(ACCESS_TOKEN_URL, shop);
        Map paramMap = new HashMap();
        paramMap.put("client_id", CLIENT_KEY);
        paramMap.put("client_secret", CLIENT_SECRET);
        paramMap.put("code", code);
        String tokenResult = HttpUtil.post(url, paramMap);
        log.info("商户：{}, 获取token数据： {}", shop, tokenResult);
        JSONObject jsonObject = JSONObject.parseObject(tokenResult);
        if (ObjectUtils.isEmpty(jsonObject) || ObjectUtils.isEmpty(jsonObject.getString("access_token"))) {
            log.info("商户{}，回调信息失败， 返回信息{}", shop, tokenResult);
            return new ModelAndView("redirect:https://customizingbox.myshopify.com/admin");
        }
        // 保存店铺
        saveStore(shop, CLIENT_KEY, jsonObject.getString("access_token"));
        // 同步历史商品
        // 设置webhook
        createWebHook(shop, jsonObject.getString("access_token"));
        return new ModelAndView("redirect:https://customizingbox.myshopify.com/admin");
    }

//    public static void main(String[] args) {
//
//        String url = String.format(STORE_INFO, "customizingboxtest.myshopify.com");
//        String shopResult = HttpRequest.get(url).header(ShopifyUtils.createShopifyHeader("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")).execute().body();
//        System.out.println(shopResult);
//    }

    /**
     * 获取保存店铺信息
     */
    private void saveStore(String shop, String apiKey, String token) {
        AppStore store = appStoreService.queryStoreIfExit(shop, StoreEnum.Type.SHOPIFY.getCode());
        if (ObjectUtils.isEmpty(store)) {
            log.info("商户：{}, 没有找到相关商户, 不予保存", shop);
            return;
        }
        Map paramMap = new HashMap();
        paramMap.put("api_key", apiKey);
        paramMap.put("token", token);
        String url = String.format(STORE_INFO, shop);
        String shopResult = HttpRequest.get(url).header(ShopifyUtils.createShopifyHeader(token)).execute().body();
        log.info("商户：{}, 查询shopify商户详细数据：{}", shop, shopResult);
        if (StringUtils.isEmpty(shopResult)) {
            return;
        }
        JSONObject storeJSON = JSONObject.parseObject(shopResult);
        JSONObject shopJSON = storeJSON.getJSONObject("shop");
        if (ObjectUtils.isEmpty(shopJSON)) {
            log.info("商户：{}, 查询shopify商户数据异常", shop, shopResult);
            return;
        }
        store = setStoreProperty(store, token, shopJSON);
        store.setDomain(shop);
        appStoreService.updateById(store);
    }

    /**
     * 创建webhook
     */
    private void createWebHook(String shop, String token) {
        appWebHookService.batchSettingShopify(shop, token);
    }


    private AppStore setStoreProperty(AppStore store, String token, JSONObject shopJSON) {
        store.setSourceId(shopJSON.getLong("id"));
        store.setAccessToken(token);
        store.setStoreState(StoreEnum.State.NORMAL.getCode());
        store.setCountryCode(shopJSON.getString("country_code"));
        store.setLocationId(shopJSON.getLong("primary_location_id"));
        store.setCountryCode(shopJSON.getString("country_code"));
        store.setRelateType(0);
        store.setName(shopJSON.getString("name"));
        store.setTimezone(shopJSON.getString("timezone"));
        store.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        store.setCurrency(shopJSON.getString("currency"));

        // 商户系统店铺创建时间
        String platCreateTime = shopJSON.getString("created_at");
        store.setCreated_at(LocalDateTimeUtil.parse(platCreateTime.substring(0,platCreateTime.indexOf("+"))));
        LocalDateTime shangHaiTime = LocalDateTimeUtils.getBJTime(platCreateTime);
        store.setBj_created_at(shangHaiTime);

        // 商户系统店铺更新时间
        String platUpdateTime = shopJSON.getString("updated_at");
        store.setUpdated_at(LocalDateTimeUtil.parse(platCreateTime.substring(0,platUpdateTime.indexOf("+"))));
        LocalDateTime bjTime = LocalDateTimeUtils.getBJTime(platUpdateTime);
        store.setBj_updated_at(bjTime);

        return store;
    }



}
