package com.customizingbox.cloud.manager.service.app;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysLogLogin;

/**
 * 登录日志表
 */
public interface AppSysLogLoginService extends IService<AppSysLogLogin> {

}
