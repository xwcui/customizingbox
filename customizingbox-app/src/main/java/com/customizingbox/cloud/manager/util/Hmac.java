package com.customizingbox.cloud.manager.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Hmac {

    public static final String HMAC_ALGORITHM = "HmacSHA256";

    public static String calculateHmac(String message, String secret) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac hmac = Mac.getInstance(HMAC_ALGORITHM);
        SecretKeySpec key = new SecretKeySpec(secret.getBytes(), HMAC_ALGORITHM);
        hmac.init(key);
        return Base64.encodeBase64String(hmac.doFinal(message.getBytes()));
    }


    public static boolean checkHmac(String message, String hmac, String secret) throws InvalidKeyException, NoSuchAlgorithmException {
        return hmac.equals(calculateHmac(message, secret));
    }

    public static String sha256HMAC(String key, String data) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException{
        Mac hmac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        hmac.init(secret_key);
        return Hex.encodeHexString(hmac.doFinal(data.getBytes("UTF-8")));
    }

    public static boolean validateShopifyAskForPermission(String key, String hmac, String shop, String timestamp) throws Exception {
        return (sha256HMAC(key, "shop="+shop+"&timestamp="+timestamp).compareTo(hmac) == 0);
    }
    public static boolean validateShopifyAskForPermission(String key, String hmac,String code, String shop,String state ,String timestamp) throws Exception {
        return (sha256HMAC(key, "code="+code+"&shop="+shop+"&state="+state+"&timestamp="+timestamp).compareTo(hmac) == 0);
    }
}
