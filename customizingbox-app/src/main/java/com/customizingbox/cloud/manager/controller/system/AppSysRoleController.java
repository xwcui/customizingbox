package com.customizingbox.cloud.manager.controller.system;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.customizingbox.cloud.manager.service.app.AppSysRoleMenuService;
import com.customizingbox.cloud.manager.service.app.AppSysRoleService;
import com.customizingbox.cloud.manager.syslog.annotation.SysLog;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRole;
import com.customizingbox.cloud.common.datasource.model.app.system.entity.AppSysRoleMenu;
import com.customizingbox.cloud.manager.tenant.TenantContextHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/role")
@Api(value = "role", tags = "角色管理模块")
public class AppSysRoleController {

    private final AppSysRoleService appSysRoleService;
    private final AppSysRoleMenuService appSysRoleMenuService;


    @ApiOperation(value = "通过ID查询角色信息")
    @GetMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:role:get')")
    public ApiResponse getById(@PathVariable String id) {
        return ApiResponse.ok(appSysRoleService.getById(id));
    }


    @ApiOperation(value = "添加角色")
    @SysLog("添加角色")
    @PostMapping
    @PreAuthorize("@ato.hasAuthority('sys:role:add')")
    public ApiResponse save(@Valid @RequestBody AppSysRole appSysRole) {
        return ApiResponse.ok(appSysRoleService.save(appSysRole));
    }


    @ApiOperation(value = "修改角色")
    @SysLog("修改角色")
    @PutMapping
    @PreAuthorize("@ato.hasAuthority('sys:role:edit')")
    public ApiResponse update(@Valid @RequestBody AppSysRole appSysRole) {
        if (!this.judeAdmin(appSysRole.getId())) {
            return ApiResponse.failed("管理员角色不能操作");
        }
        return ApiResponse.ok(appSysRoleService.updateById(appSysRole));
    }


    @ApiOperation(value = "删除角色")
    @SysLog("删除角色")
    @DeleteMapping("/{id}")
    @PreAuthorize("@ato.hasAuthority('sys:role:del')")
    public ApiResponse removeById(@PathVariable String id) {
        if (!this.judeAdmin(id)) {
            return ApiResponse.failed("管理员角色不能操作");
        }
        return ApiResponse.ok(appSysRoleService.removeRoleById(id));
    }


    @ApiOperation(value = "获取角色列表")
    @GetMapping("/list")
    public List<AppSysRole> getList(AppSysRole appSysRole) {
        return appSysRoleService.list(Wrappers.query(appSysRole));
    }


    @ApiOperation(value = "分页查询角色信息")
    @GetMapping("/page")
    @PreAuthorize("@ato.hasAuthority('sys:role:index')")
    public ApiResponse getRolePage(Page page) {
        return ApiResponse.ok(appSysRoleService.page(page, Wrappers.emptyWrapper()));
    }


    @ApiOperation(value = "更新角色菜单")
    @SysLog("更新角色菜单")
    @PutMapping("/menu")
    @PreAuthorize("@ato.hasAuthority('sys:role:perm','sys:tenant:edit')")
    public ApiResponse saveRoleMenus(@RequestBody AppSysRoleMenu appSysRoleMenu) {
        String roleId = appSysRoleMenu.getRoleId();
        String menuIds = appSysRoleMenu.getMenuId();
        if (StrUtil.isBlank(roleId) || StrUtil.isBlank(menuIds)) {
            return ApiResponse.ok();
        }
        if (!this.judeAdmin(roleId)) {
            return ApiResponse.failed("管理员角色不能操作");
        }
        AppSysRole appSysRole = appSysRoleService.getById(roleId);
        return ApiResponse.ok(appSysRoleMenuService.saveRoleMenus(appSysRole.getRoleCode(), roleId, menuIds));
    }


    boolean judeAdmin(String roleId) {
        AppSysRole appSysRole = appSysRoleService.getById(roleId);
        if (CommonConstants.ROLE_CODE_ADMIN.equals(appSysRole.getRoleCode())) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }


    @ApiOperation(value = "更新租户管理员角色菜单")
    @SysLog("更新租户管理员角色菜单")
    @PutMapping("/menu/tenant")
    @PreAuthorize("@ato.hasAuthority('sys:tenant:edit')")
    public ApiResponse saveRoleMenusTenant(@RequestBody AppSysRoleMenu appSysRoleMenu) {
        String tenantId = appSysRoleMenu.getTenantId();
        String roleId = appSysRoleMenu.getRoleId();
        String menuIds = appSysRoleMenu.getMenuId();
        if (StrUtil.isBlank(tenantId) || StrUtil.isBlank(menuIds) || StrUtil.isBlank(menuIds)) {
            return ApiResponse.ok();
        }
        TenantContextHolder.setTenantId(tenantId);
        AppSysRole appSysRole = appSysRoleService.getById(roleId);
        return ApiResponse.ok(appSysRoleMenuService.saveRoleMenus(appSysRole.getRoleCode(), roleId, menuIds));
    }
}
