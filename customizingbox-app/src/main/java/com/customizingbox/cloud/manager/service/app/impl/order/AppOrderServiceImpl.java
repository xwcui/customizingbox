package com.customizingbox.cloud.manager.service.app.impl.order;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppOrderMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.panda.entity.AppPandaOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderBaseParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderCancelParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderQuotationParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.param.AppOrderStatusParam;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderItemVO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.vo.AppQuotationOrderVO;
import com.customizingbox.cloud.manager.service.app.AppOrderItemService;
import com.customizingbox.cloud.manager.service.app.AppOrderService;
import com.customizingbox.cloud.manager.service.app.AppPandaOrderItemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class AppOrderServiceImpl extends ServiceImpl<AppOrderMapper, AppOrder> implements AppOrderService {

    private final AppOrderMapper appOrderMapper;

    @Override
    public IPage<AppQuotationOrderVO> pageAll(Page page, AppOrderQuotationParam param) {
        if (ObjectUtils.isEmpty(param)) {
            param = new AppOrderQuotationParam();
        }
        param.setPlaceStatus(OrderEnum.PLACE_STATUS.NOT_PLACE.getCode());
        param.setOrderStatus(OrderEnum.STATUS.ACTIVITY.getCode());
        IPage<AppQuotationOrderVO> appOrderPageVOIPage = appOrderMapper.pageNotPlaceOrderAll(page, param);

        List<AppQuotationOrderVO>  resultOrderList = new ArrayList<>();
        List<AppQuotationOrderVO> records = appOrderPageVOIPage.getRecords();
        Integer orderProductRelevancy = OrderEnum.RELEVANCY_STATUS.ALL_NOT_RELEVANCY.getCode();
        if (!CollectionUtils.isEmpty(records)) {
            Long lastOrderId = null;
            AppQuotationOrderVO orderPageVO = null;
            for (int i = 0; i < records.size(); i++) {
                Long id = records.get(i).getId();
                if (!id.equals(lastOrderId)) {
                    lastOrderId = id;
                    orderPageVO = new AppQuotationOrderVO();
                    resultOrderList.add(orderPageVO);
                    BeanUtils.copyProperties(records.get(i), orderPageVO);
                }
                if (!ObjectUtils.isEmpty(records.get(i).getAdminVariantId())) {
                    // 只要订单中有一个item 有admin产品映射关系, 则就不是全部未关联
                    orderPageVO.setRelevanceStatus(OrderEnum.RELEVANCY_STATUS.PORTION_RELEVANCY.getCode());
                }
                orderPageVO.getOrderItems().add(BeanUtil.copyProperties(records.get(i), AppQuotationOrderItemVO.class));
            }
            appOrderPageVOIPage.setRecords(resultOrderList);
        }
        return appOrderPageVOIPage;
    }

    @Override
    public IPage<AppQuotationOrderVO> cancelledOrderPage(Page page, AppOrderQuotationParam param) {
        if (ObjectUtils.isEmpty(param)) {
            param = new AppOrderQuotationParam();
        }
        param.setPlaceStatus(OrderEnum.PLACE_STATUS.NOT_PLACE.getCode());
        param.setOrderStatus(OrderEnum.STATUS.CANCELLED.getCode());
        IPage<AppQuotationOrderVO> appOrderPageVOIPage = appOrderMapper.pageNotPlaceOrderAll(page, param);

        List<AppQuotationOrderVO>  resultOrderList = new ArrayList<>();
        List<AppQuotationOrderVO> records = appOrderPageVOIPage.getRecords();
        if (!CollectionUtils.isEmpty(records)) {
            Long lastOrderId = null;
            AppQuotationOrderVO orderPageVO = null;
            for (int i = 0; i < records.size(); i++) {
                Long id = records.get(i).getId();
                if (!id.equals(lastOrderId)) {
                    lastOrderId = id;
                    orderPageVO = new AppQuotationOrderVO();
                    resultOrderList.add(orderPageVO);
                    BeanUtils.copyProperties(records.get(i), orderPageVO);
                }
                if (!ObjectUtils.isEmpty(records.get(i).getAdminVariantId())) {
                    // 只要订单中有一个item 有admin产品映射关系, 则就不是全部未关联
                    orderPageVO.setRelevanceStatus(OrderEnum.RELEVANCY_STATUS.PORTION_RELEVANCY.getCode());
                }
                orderPageVO.getOrderItems().add(BeanUtil.copyProperties(records.get(i), AppQuotationOrderItemVO.class));
            }
            appOrderPageVOIPage.setRecords(resultOrderList);
        }
        return appOrderPageVOIPage;
    }

    @Override
    public Boolean updateOrderStatus(AppOrderStatusParam param) {
        return this.update(Wrappers.<AppOrder>lambdaUpdate().set(AppOrder::getStatus, param.getStatus())
                .eq(AppOrder::getId, param.getId()));
    }
}
