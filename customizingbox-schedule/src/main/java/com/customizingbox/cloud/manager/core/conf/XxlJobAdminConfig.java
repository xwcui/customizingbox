package com.customizingbox.cloud.manager.core.conf;


import com.customizingbox.cloud.common.datasource.mapper.xxljob.*;
import com.customizingbox.cloud.manager.core.alarm.JobAlarmer;
import com.customizingbox.cloud.manager.core.scheduler.XxlJobScheduler;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Arrays;


@Component
public class XxlJobAdminConfig implements InitializingBean, DisposableBean {

    private static XxlJobAdminConfig adminConfig = null;
    public static XxlJobAdminConfig getAdminConfig() {
        return adminConfig;
    }


    private XxlJobScheduler xxlJobScheduler;

    @Override
    public void afterPropertiesSet() throws Exception {
        adminConfig = this;

        xxlJobScheduler = new XxlJobScheduler();
        xxlJobScheduler.init();
    }

    @Override
    public void destroy() throws Exception {
        xxlJobScheduler.destroy();
    }

    @Value("${xxl.job.i18n}")
    private String i18n;

    @Value("${xxl.job.accessToken}")
    private String accessToken;

    @Value("${spring.mail.from}")
    private String emailFrom;

    @Value("${xxl.job.triggerpool.fast.max}")
    private int triggerPoolFastMax;

    @Value("${xxl.job.triggerpool.slow.max}")
    private int triggerPoolSlowMax;

    @Value("${xxl.job.logretentiondays}")
    private int logretentiondays;

    // dao, service

    @Resource
    private JobAlarmer jobAlarmer;
    @Resource
    private DataSource dataSource;
    @Resource
    private JavaMailSender mailSender;
    @Resource
    private XxlJobLogMapper xxlJobLogDao;
    @Resource
    private XxlJobInfoMapper xxlJobInfoDao;
    @Resource
    private XxlJobRegistryMapper xxlJobRegistryDao;
    @Resource
    private XxlJobGroupMapper xxlJobGroupDao;
    @Resource
    private XxlJobLogReportMapper xxlJobLogReportDao;


    public String getI18n() {
        if (!Arrays.asList("zh_CN", "zh_TC", "en").contains(i18n)) {
            return "zh_CN";
        }
        return i18n;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public int getTriggerPoolFastMax() {
        if (triggerPoolFastMax < 200) {
            return 200;
        }
        return triggerPoolFastMax;
    }

    public int getTriggerPoolSlowMax() {
        if (triggerPoolSlowMax < 100) {
            return 100;
        }
        return triggerPoolSlowMax;
    }

    public int getLogretentiondays() {
        if (logretentiondays < 7) {
            return -1;
        }
        return logretentiondays;
    }

    public XxlJobLogMapper getXxlJobLogDao() {
        return xxlJobLogDao;
    }

    public XxlJobInfoMapper getXxlJobInfoDao() {
        return xxlJobInfoDao;
    }

    public XxlJobRegistryMapper getXxlJobRegistryDao() {
        return xxlJobRegistryDao;
    }

    public XxlJobGroupMapper getXxlJobGroupDao() {
        return xxlJobGroupDao;
    }

    public XxlJobLogReportMapper getXxlJobLogReportDao() {
        return xxlJobLogReportDao;
    }

    public JavaMailSender getMailSender() {
        return mailSender;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public JobAlarmer getJobAlarmer() {
        return jobAlarmer;
    }

}
