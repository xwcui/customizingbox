package com.customizingbox.cloud.manager.core.alarm;


import com.customizingbox.cloud.common.datasource.model.xxljob.XxlJobInfo;
import com.customizingbox.cloud.common.datasource.model.xxljob.XxlJobLog;

public interface JobAlarm {

    /**
     * job alarm
     *
     */
    boolean doAlarm(XxlJobInfo info, XxlJobLog jobLog);

}
