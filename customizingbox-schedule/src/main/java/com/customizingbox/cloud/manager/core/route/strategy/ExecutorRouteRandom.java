package com.customizingbox.cloud.manager.core.route.strategy;


import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.biz.model.TriggerParam;
import com.customizingbox.cloud.manager.core.route.ExecutorRouter;

import java.util.List;
import java.util.Random;


public class ExecutorRouteRandom extends ExecutorRouter {

    private static final Random LOCAL_RANDOM = new Random();

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList) {
        String address = addressList.get(LOCAL_RANDOM.nextInt(addressList.size()));
        return new ReturnT<>(address);
    }

}
