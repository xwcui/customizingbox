package com.customizingbox.cloud.manager.job;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class DemoJob {

    @XxlJob("demoJobHandler")
    public ReturnT<String> testJobJob(String param) {
        log.info("<--测试-->");

        return ReturnT.SUCCESS;
    }
}
