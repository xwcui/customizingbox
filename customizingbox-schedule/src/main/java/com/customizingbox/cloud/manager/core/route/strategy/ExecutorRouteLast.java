package com.customizingbox.cloud.manager.core.route.strategy;


import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.biz.model.TriggerParam;
import com.customizingbox.cloud.manager.core.route.ExecutorRouter;

import java.util.List;


public class ExecutorRouteLast extends ExecutorRouter {

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList) {
        return new ReturnT<>(addressList.get(addressList.size()-1));
    }

}
