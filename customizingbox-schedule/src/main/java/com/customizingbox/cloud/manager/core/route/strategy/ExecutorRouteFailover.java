package com.customizingbox.cloud.manager.core.route.strategy;


import com.xxl.job.core.biz.ExecutorBiz;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.biz.model.TriggerParam;
import com.customizingbox.cloud.manager.core.route.ExecutorRouter;
import com.customizingbox.cloud.manager.core.scheduler.XxlJobScheduler;
import com.customizingbox.cloud.manager.core.util.I18nUtil;

import java.util.List;


public class ExecutorRouteFailover extends ExecutorRouter {

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList) {

        StringBuilder stringBuffer = new StringBuilder();
        for (String address : addressList) {
            // beat
            ReturnT<String> beatResult;
            try {
                ExecutorBiz executorBiz = XxlJobScheduler.getExecutorBiz(address);
                beatResult = executorBiz.beat();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                beatResult = new ReturnT<String>(ReturnT.FAIL_CODE, ""+e );
            }
            stringBuffer.append((stringBuffer.length() > 0) ? "<br><br>" : "")
                    .append(I18nUtil.getString("jobconf_beat")).append("：")
                    .append("<br>address：").append(address)
                    .append("<br>code：").append(beatResult.getCode())
                    .append("<br>msg：").append(beatResult.getMsg());

            // beat success
            if (beatResult.getCode() == ReturnT.SUCCESS_CODE) {

                beatResult.setMsg(stringBuffer.toString());
                beatResult.setContent(address);
                return beatResult;
            }
        }
        return new ReturnT<>(ReturnT.FAIL_CODE, stringBuffer.toString());

    }
}
