package com.customizingbox.cloud.gateway.filter;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.customizingbox.cloud.common.core.constant.CacheConstants;
import com.customizingbox.cloud.common.core.constant.SecurityConstants;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * 验证码处理
 */
@Slf4j
@Component
@AllArgsConstructor
public class ValidateCodeGatewayFilter extends AbstractGatewayFilterFactory {

    private final ObjectMapper objectMapper;
    private final RedisTemplate redisTemplate;

    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();

            // 不是登录请求，直接向下执行
            if (!StrUtil.containsAnyIgnoreCase(request.getURI().getPath(), SecurityConstants.OAUTH_TOKEN_URL)) {
                return chain.filter(exchange);
            }

            // 刷新token，直接向下执行
            String grantType = request.getQueryParams().getFirst("grant_type");
            if (StrUtil.equals(SecurityConstants.REFRESH_TOKEN, grantType)) {
                return chain.filter(exchange);
            }

//            try {
//                //校验验证码
//                this.checkCode(request);
//            } catch (Exception e) {
//                ServerHttpResponse response = exchange.getResponse();
//                response.setStatusCode(HttpStatus.PRECONDITION_REQUIRED);
//                try {
//                    return response.writeWith(Mono.just(response.bufferFactory()
//                            .wrap(objectMapper.writeValueAsBytes(ApiResponse.failed(e.getMessage())))));
//                } catch (JsonProcessingException ex) {
//                    log.error("error", ex);
//                }
//            }

            return chain.filter(exchange);
        };
    }

    /**
     * 检查code
     */
    @SneakyThrows
    private void checkCode(ServerHttpRequest request) {
        String code = request.getQueryParams().getFirst("code");
        if (StrUtil.isBlank(code)) {
            throw new ApiException("验证码不能为空");
        }

        String randomStr = request.getQueryParams().getFirst("randomStr");
        String grantType = request.getQueryParams().getFirst("grant_type");
        if (StrUtil.equals(SecurityConstants.SMS_LOGIN, grantType)) {
            randomStr = SecurityConstants.SMS_LOGIN + ":" + request.getQueryParams().getFirst("phone");
        }

        String key = CacheConstants.VER_CODE_DEFAULT + randomStr;
        redisTemplate.setKeySerializer(new StringRedisSerializer());

        if (!redisTemplate.hasKey(key)) {
            throw new ApiException("验证码不合法");
        }

        Object codeObj = redisTemplate.opsForValue().get(key);
        if (codeObj == null) {
            throw new ApiException("验证码不合法");
        }

        String saveCode = codeObj.toString();
        if (StrUtil.isBlank(saveCode)) {
            redisTemplate.delete(key);
            throw new ApiException("验证码不合法");
        }

        if (!StrUtil.equals(saveCode, code)) {
            throw new ApiException("验证码不合法");
        }

        redisTemplate.delete(key);
    }
}
