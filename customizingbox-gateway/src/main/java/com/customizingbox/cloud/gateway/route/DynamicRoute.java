package com.customizingbox.cloud.gateway.route;

import com.alibaba.cloud.nacos.NacosConfigProperties;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.customizingbox.cloud.common.core.constant.CommonConstants;
import com.customizingbox.cloud.gateway.model.GatewayRouteList;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.route.RouteDefinitionWriter;
import org.springframework.context.annotation.Configuration;
import org.yaml.snakeyaml.Yaml;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.Properties;
import java.util.concurrent.Executor;


@Slf4j
@Configuration
@AllArgsConstructor
public class DynamicRoute {

    private NacosConfigProperties nacosProperties;
    private RouteDefinitionWriter routeDefinitionWriter;

    /**
     * 初始化路由网关
     */
    @PostConstruct
    public void initRoute() {
        try {
            Properties properties = new Properties();
            properties.put(PropertyKeyConst.SERVER_ADDR, nacosProperties.getServerAddr());
            properties.put(PropertyKeyConst.USERNAME, nacosProperties.getUsername());
            properties.put(PropertyKeyConst.PASSWORD, nacosProperties.getPassword());
            ConfigService configService = NacosFactory.createConfigService(properties);
            String content = configService.getConfig(CommonConstants.CONFIG_DATA_ID, CommonConstants.CONFIG_GROUP, CommonConstants.CONFIG_TIMEOUT_MS);
            updateRoute(content);
            //开户监听，实现动态
            configService.addListener(CommonConstants.CONFIG_DATA_ID, CommonConstants.CONFIG_GROUP, new Listener() {
                @Override
                public void receiveConfigInfo(String configInfo) {
                    updateRoute(configInfo);
                }

                @Override
                public Executor getExecutor() {
                    return null;
                }
            });
        } catch (NacosException e) {
            log.error("error", e);
        }
    }

    public void updateRoute(String content) {
        Yaml yaml = new Yaml();
        GatewayRouteList gatewayRouteList = yaml.loadAs(content, GatewayRouteList.class);
        gatewayRouteList.getRoutes().forEach(route -> {
            log.info("<--加载路由：{},{}-->", route.getId(), route);
            routeDefinitionWriter.save(Mono.just(route)).subscribe();
        });
    }
}
