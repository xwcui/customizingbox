package com.customizingbox.cloud.gateway.filter;

import cn.hutool.core.util.StrUtil;
import com.customizingbox.cloud.common.core.constant.SecurityConstants;
import com.customizingbox.cloud.common.core.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;


@Slf4j
@Component
public class BaseGlobalFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        log.info("<---filter ip:{}, url:{}-->", WebUtils.getIp(request), request.getPath());
        return chain.filter(exchange);
//        //登录直接放行
//        if (StrUtil.containsAnyIgnoreCase(request.getURI().getPath(), SecurityConstants.OAUTH_TOKEN_URL)) {
//            return chain.filter(exchange);
//        }
//        String requestMethod = request.getMethodValue();
//        if (HttpMethod.POST.toString().equals(requestMethod) || HttpMethod.PUT.toString().equals(requestMethod)) {
//            return DataBufferUtils.join(exchange.getRequest().getBody()).flatMap(dataBuffer -> {
//                byte[] bytes = new byte[dataBuffer.readableByteCount()];
//                dataBuffer.read(bytes);
//                String postRequestBodyStr = new String(bytes, StandardCharsets.UTF_8);
//                exchange.getAttributes().put("POST_BODY", postRequestBodyStr);
//                DataBufferUtils.release(dataBuffer);
//                Flux<DataBuffer> cachedFlux = Flux.defer(() -> {
//                    DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
//                    return Mono.just(buffer);
//                });
//                // 下面的将请求体再次封装写回到request里，传到下一级，否则，由于请求体已被消费，后续的服务将取不到值
//                ServerHttpRequest mutatedRequest = new ServerHttpRequestDecorator(exchange.getRequest()) {
//                    @Override
//                    public Flux<DataBuffer> getBody() {
//                        return cachedFlux;
//                    }
//                };
//                // 封装request，传给下一级
//                return chain.filter(exchange.mutate().request(mutatedRequest).build());
//            });
//        }
//
//        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

}
