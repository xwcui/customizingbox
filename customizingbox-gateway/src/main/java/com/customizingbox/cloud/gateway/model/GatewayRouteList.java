package com.customizingbox.cloud.gateway.model;

import lombok.Data;
import org.springframework.cloud.gateway.route.RouteDefinition;

import java.io.Serializable;
import java.util.List;

/**
 * 路由定义模型
 */
@Data
public class GatewayRouteList implements Serializable {

    private static final long serialVersionUID = 1L;

    List<RouteDefinition> routes;

}
