package com.customizingbox.cloud.manager.service.impl.shopify;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductImgDepotMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.dto.AppStoreProductImgDepotMappingDTO;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductImgDepot;
import com.customizingbox.cloud.manager.service.AppStoreProductImgDepotService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-26 10:32:19
 */
@Service
@AllArgsConstructor
public class AppStoreShopifyProductImgDepotServiceImpl extends ServiceImpl<AppStoreProductImgDepotMapper, AppStoreProductImgDepot> implements AppStoreProductImgDepotService{

    /**
     * @return key 商户图片id， value： 本地图片id
     */
    @Override
    public Map<Long, Long> saveBatch(List<AppStoreProductImgDepot> images, Long productId, Long sourceProductId, String tenantId) {
        if (CollectionUtils.isEmpty(images)) {
            return null;
        }
        Map<Long, Long> resultMap = new HashMap<>();
        for(AppStoreProductImgDepot imgDepot : images) {
            imgDepot.setTenantId(tenantId);
            AppStoreProductImgDepotMappingDTO imgDepotMappingDTO = localSaveOrUpdate(imgDepot, null, productId, sourceProductId);
            resultMap.put(imgDepotMappingDTO.getSourceId(), imgDepotMappingDTO.getLocalId());
            if (imgDepot.getPosition() == 1) {
                // 商品主图
                resultMap.put(-1L, imgDepotMappingDTO.getLocalId());
            }
        }
        return resultMap;
    }

    /**
     * @return key 商户图片id， value： 本地图片id
     */
    @Override
    public Map<Long, Long> updateBatch(List<AppStoreProductImgDepot> images, Long productId, Long sourceProductId, String tenantId) {
        if (CollectionUtils.isEmpty(images)) {
            // TODO 这里要删除之前的
            return null;
        }
        Map<Long, Long> resultMap = new HashMap<>();
        AppStoreProductImgDepotMappingDTO imgDepotMappingDTO = null;
        List<AppStoreProductImgDepot> dbImgDepots  = this.findByProductId(productId);

        for(AppStoreProductImgDepot imgDepot : images) {
            imgDepot.setTenantId(tenantId);
            // 因为已经查出全部了，这里从集合里取
            AppStoreProductImgDepot oldImgDepot = filterProdcutAttr(dbImgDepots, imgDepot.getId(), productId);
            if (ObjectUtils.isEmpty(oldImgDepot)) {
                // 没有查到, 则保存
                imgDepotMappingDTO = localSaveOrUpdate(imgDepot, null, productId, sourceProductId);
            } else {
                imgDepotMappingDTO = localSaveOrUpdate(imgDepot, oldImgDepot.getId(), productId, sourceProductId);
            }
            if (imgDepot.getPosition() == 1) {
                // 商品主图
                resultMap.put(-1L, imgDepotMappingDTO.getLocalId());
            }
            resultMap.put(imgDepotMappingDTO.getSourceId(), imgDepotMappingDTO.getLocalId());
        }

        // 数据库和透传过来参数对比, 删除已经在shopify中已删除的数据
        Set<Long> sourceIds = images.stream().map(AppStoreProductImgDepot::getId).collect(Collectors.toSet());
        List<AppStoreProductImgDepot> imgDepots = dbImgDepots.stream().filter(dbImgDepot -> !sourceIds.contains(dbImgDepot.getSourceId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(imgDepots)) {
            imgDepots.forEach(delvariant -> {
                this.removeById(delvariant);
            });
        }
        return resultMap;
    }

    /**
     * 保存或修改, id有就是修改, id没有就是保存
     * @return
     */
    private AppStoreProductImgDepotMappingDTO localSaveOrUpdate(AppStoreProductImgDepot imgDepot, Long id, Long productId, Long sourceProductId) {
        AppStoreProductImgDepot newImgDepot = new AppStoreProductImgDepot();
        BeanUtils.copyProperties(imgDepot, newImgDepot);
        Long sourceId = newImgDepot.getId();
        newImgDepot.setSourceId(sourceId);
        newImgDepot.setId(id);
        newImgDepot.setProductId(productId);
        newImgDepot.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        newImgDepot.setSourceProductId(sourceProductId);
        newImgDepot.setTenantId(imgDepot.getTenantId());
        this.saveOrUpdate(newImgDepot);
        AppStoreProductImgDepotMappingDTO imgDepotMappingDTO = new AppStoreProductImgDepotMappingDTO();
        imgDepotMappingDTO.setSourceId(imgDepot.getId());
        imgDepotMappingDTO.setLocalId(newImgDepot.getId());
        return imgDepotMappingDTO;
    }

    /**
     * 过滤需要修改的产品属性
     */
    private AppStoreProductImgDepot filterProdcutAttr(List<AppStoreProductImgDepot> imgDepots, Long sourceId, Long productId) {
        List<AppStoreProductImgDepot> collect = imgDepots.stream().filter(imgDepot
                -> {
            return imgDepot.getSourceId().equals(sourceId) &&
                    imgDepot.getProductId().equals(productId);
        }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(collect)) {
            return null;
        } else {
            return collect.get(0);
        }
    }

    @Override
    public Boolean delByProductId(Long productId) {
        return this.remove(Wrappers.<AppStoreProductImgDepot>lambdaQuery().eq(AppStoreProductImgDepot::getProductId, productId)
                .eq(AppStoreProductImgDepot::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
    }

    @Override
    public AppStoreProductImgDepot findByProductIdAndSourceId(Long productId, Long sourceId) {
        return this.getOne(Wrappers.<AppStoreProductImgDepot>lambdaQuery().eq(AppStoreProductImgDepot::getProductId, productId)
                .eq(AppStoreProductImgDepot::getSourceId, sourceId)
                .eq(AppStoreProductImgDepot::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
    }

    @Override
    public List<AppStoreProductImgDepot> findByProductId(Long productId) {
        return this.list(Wrappers.<AppStoreProductImgDepot>lambdaQuery()
                .select(AppStoreProductImgDepot::getId, AppStoreProductImgDepot::getSourceId, AppStoreProductImgDepot::getProductId)
                .eq(AppStoreProductImgDepot::getProductId, productId));
    }
}
