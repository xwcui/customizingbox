package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.source.dto.OrderShowStatusDTO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrderItem;

import java.util.List;

/**
 * panda 订单item表
 *
 * @author Y
 * @date 2022-03-30 13:48:16
 */
public interface AppOrderItemService<T> extends IService<AppOrderItem> {

    /**
     * 订单id查询子订单列表
     * @param orderId
     * @return
     */
    List<AppOrderItem> findByOrderId(Long orderId);

    /**
     * 保存订单, 并返回订单是否可以显示给商户支付状态信息
     * @param lineItems
     * @param orderId
     * @param sourceOrderId
     * @return
     */
    public OrderShowStatusDTO saveOrderItem(List<AppShopifyOrderItem> lineItems, Long orderId, Long sourceOrderId, String tenantId);

    /**
     * 同步已存在订单item
     * @param shopifyOrderItems
     * @param orderId  panda 订单id
     */
    public OrderShowStatusDTO syncExistOrderItemData(List<AppShopifyOrderItem> shopifyOrderItems, Long orderId, Long sourceOrderId, String tenantId);
}
