package com.customizingbox.cloud.manager.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.utils.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ApiResStatus;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.core.constant.exception.ApiException;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppOrderMapper;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductMapper;
import com.customizingbox.cloud.common.datasource.model.app.order.source.dto.OrderShowStatusDTO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrder;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShippingAddress;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShopifyCustomer;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrder;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import com.customizingbox.cloud.manager.service.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Slf4j
@AllArgsConstructor
public class AppShopifyOrderDataSyncServeiceImpl extends ServiceImpl<AppOrderMapper, AppOrder> implements AppShopifyDataSyncServeice {


    private final AppShippingAddressService shippingAddressService;
    private final AppShopifyCustomerService customerService;
    private final AppOrderItemService appOrderItemService;
    private final AppStoreService appStoreService;

    @Override
    public Boolean create(String json) {
        AppShopifyOrder appShopifyOrder = analysisOrderData(json);
        if (ObjectUtils.isEmpty(appShopifyOrder)) {
            log.error("shopify order create 订单数据解析失败, json: {}", json);
            return null;
        }
        if (!validateCreateOrder(appShopifyOrder)) {
            return true;
        }
        String shopifyShopName = appShopifyOrder.getShopifyShopName();
        // 根据店铺查询租户
        AppStore appStore = appStoreService.queryStoreIfExit(shopifyShopName, StoreEnum.Type.SHOPIFY.getCode());
        if (ObjectUtils.isEmpty(appStore)) {
            log.error("订单里的商户, 不在系统数据库中");
            return false;
        }
        String tenantId = appStore.getTenantId();

        // 订单是否存在
        Boolean existdOrder = isExistdBySourceIdAndPlatformType(appShopifyOrder.getId(), StoreEnum.Type.SHOPIFY.getCode());
        if (existdOrder) {
            log.info("shopify order create, 订单已经存在, 不重复保存");
            return false;
        }

        // 保存收货地址
        AppShippingAddress appShippingAddress = appShopifyOrder.getShippingAddress();
        appShippingAddress.setTenantId(tenantId);
        shippingAddressService.save(appShippingAddress);
        // 保存客户
        AppShopifyCustomer customer = appShopifyOrder.getCustomer();
        customer.setSourceId(customer.getId());
        customer.setTenantId(tenantId);
        customer.setId(null);
        customerService.save(customer);
        AppOrder appOrder = initPandaOrder(appStore.getId(), appShippingAddress.getId(), appStore);
        shopifyToPandaOrder(appShopifyOrder, appOrder);
        appOrder.setStoreName(shopifyShopName);
        this.save(appOrder);
        OrderShowStatusDTO orderShowStatusDTO = appOrderItemService.saveOrderItem(appShopifyOrder.getLineItems()
                , appOrder.getId(), appShopifyOrder.getId(), tenantId);
        appOrder.setRelevancyStatus(orderShowStatusDTO.getRelevancyStatus());
        this.updateById(appOrder);
        return null;
    }

    @Override
    public Boolean delete(String json) {
        return null;
    }

    @Override
    public Boolean update(String json) {
        // TODO shopify 修改订单逻辑. 如果订单存在, 且未支付, 则可以修改数量.  如果不存在, 则保存订单.
        AppShopifyOrder appShopifyOrder = analysisOrderData(json);
        if (ObjectUtils.isEmpty(appShopifyOrder)) {
            log.error("shopify order create 订单数据解析失败, json: {}", json);
            return true;
        }
        if (!validateUpdateOrder(appShopifyOrder)) {
            return true;
        }
        // 判断订单是否已经被支付, 如果支付则不同步任何数据. TODO 这里加锁
        Long id = appShopifyOrder.getId();
        AppOrder appOrder = this.getOne(Wrappers.<AppOrder>lambdaQuery().eq(AppOrder::getSourceId, id)
                .eq(AppOrder::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
        if (ObjectUtils.isEmpty(appOrder)) {
            log.info("数据库没有找到该订单, 不做处理");
            return true;
        }
        // 这里判断是否已经被下单, 只要有一个下单, 就不让同步数据了
        if (!OrderEnum.PLACE_STATUS.NOT_PLACE.getCode().equals(appOrder.getPlaceStatus())) {
            log.info("订单状态不是未下单, 不能修改");
        }

        // 处理item订单
        OrderShowStatusDTO orderShowStatusDTO = appOrderItemService.syncExistOrderItemData(appShopifyOrder.getLineItems()
                , appOrder.getId(), appOrder.getSourceId(), appOrder.getTenantId());
        appOrder.setRelevancyStatus(orderShowStatusDTO.getRelevancyStatus());

        shopifyToPandaOrder(appShopifyOrder, appOrder);
        this.updateById(appOrder);

        // 修改订单物流
        AppShippingAddress dbShippingAddress = shippingAddressService.getById(appOrder.getAddressId());
        AppShippingAddress shopifyShippingAddress = appShopifyOrder.getShippingAddress();
        shopifyShippingAddress.setId(dbShippingAddress.getId());
        BeanUtils.copyProperties(shopifyShippingAddress, dbShippingAddress);
        shippingAddressService.updateById(shopifyShippingAddress);
        return true;
    }

    /**
     * 解析订单数据
     * @param json webhook回调数据
     */
    private AppShopifyOrder analysisOrderData(String json) {
        AppShopifyOrder appShopifyOrder = null;
        try {
            appShopifyOrder = JSONObject.parseObject(json, AppShopifyOrder.class);
        } catch (Exception e) {
            throw new ApiException(ApiResStatus.ORDER_DATA_ANALYSIS_ERROR);
        }
        return appShopifyOrder;
    }


    private Boolean validateCreateOrder(AppShopifyOrder appShopifyOrder) {
        // 支付状态
        String financialStatus = appShopifyOrder.getFinancialStatus();
        if (!"paid".equals(financialStatus)) {
            log.info("shopify order, 订单未支付 不保存");
            return false;
        }
        // 发货状态
        String fulfillmentStatus = appShopifyOrder.getFulfillmentStatus();
        if (!StringUtils.isEmpty(fulfillmentStatus)) {
            log.info("shopify order create, 订单不是都未发货状态, 不保存");
            return false;
        }
        AppShippingAddress shippingAddress = appShopifyOrder.getShippingAddress();
        if(ObjectUtils.isEmpty(shippingAddress)) {
            log.info("shopify order create, 订单收货地址为空, 不保存");
            return false;
        }
        return true;
    }
    /**
     * 校验订单是否需要创建
     * @param appShopifyOrder
     * @return true 通过校验, false:;没哟通过校验
     */
    private Boolean validateUpdateOrder(AppShopifyOrder appShopifyOrder) {
        // 支付状态
        String financialStatus = appShopifyOrder.getFinancialStatus();
        if ("partially_refunded".equals(financialStatus) && "partially_paid".equals(financialStatus)) {
            log.info("shopify order, 订单未支付或者订单不是退款订单或者不是部分已支付 不保存");
            return false;
        }
//        // 发货状态
//        String fulfillmentStatus = appShopifyOrder.getFulfillmentStatus();
//        if (!StringUtils.isEmpty(fulfillmentStatus)) {
//            log.info("shopify order create, 订单不是都未发货状态, 不保存");
//            return false;
//        }
        AppShippingAddress shippingAddress = appShopifyOrder.getShippingAddress();
        if(ObjectUtils.isEmpty(shippingAddress)) {
            log.info("shopify order create, 订单收货地址为空, 不保存");
            return false;
        }
        return true;
    }

    private AppOrder initPandaOrder(Long storeId, Long addressId, AppStore appStore) {
        AppOrder appOrder = new AppOrder();
        appOrder.setTenantId(appStore.getTenantId());
        // TODO 生产订单号
        appOrder.setOrderNo(UUID.randomUUID().toString().replace("-", ""));
        appOrder.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        appOrder.setAddressId(addressId);
        // 这里关联用户
        appOrder.setUserId(appStore.getUserId());
        appOrder.setPlaceStatus(OrderEnum.PLACE_STATUS.NOT_PLACE.getCode());
        // 初始化
        appOrder.setCreateTime(LocalDateTime.now());
        appOrder.setStoreId(storeId);
        appOrder.setPlaceStatus(OrderEnum.PLACE_STATUS.NOT_PLACE.getCode());
        appOrder.setStatus(OrderEnum.STATUS.ACTIVITY.getCode());
        return appOrder;
    }

    // 将shopify订单转换成panda订单
    private AppOrder shopifyToPandaOrder(AppShopifyOrder appShopifyOrder, AppOrder appOrder) {
        appOrder.setSourceId(appShopifyOrder.getId());
        appOrder.setOrderName(appShopifyOrder.getName());
        appOrder.setSourceMark(appShopifyOrder.getNote());
        appOrder.setFulfillmentStatus(appShopifyOrder.getFulfillmentStatus());
        appOrder.setFinancialStatus(appShopifyOrder.getFinancialStatus());
        return appOrder;
    }

    /**
     * 根据 商户订单id, 和商户类型 查询是否已存在订单
     * return true 已经存在, false 不存在
     */
    private Boolean isExistdBySourceIdAndPlatformType(Long sourceId, Integer platformType) {
        AppOrder appOrder = this.getOne(Wrappers.<AppOrder>lambdaQuery().eq(AppOrder::getSourceId, sourceId)
                .eq(AppOrder::getPlatformType, platformType).select(AppOrder::getId));
        return !ObjectUtils.isEmpty(appOrder);
    }
}
