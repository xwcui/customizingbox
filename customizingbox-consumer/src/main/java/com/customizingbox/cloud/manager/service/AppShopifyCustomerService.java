package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShopifyCustomer;

/**
 * 商户客户表
 *
 * @author Y
 * @date 2022-03-30 13:48:14
 */
public interface AppShopifyCustomerService extends IService<AppShopifyCustomer> {

}
