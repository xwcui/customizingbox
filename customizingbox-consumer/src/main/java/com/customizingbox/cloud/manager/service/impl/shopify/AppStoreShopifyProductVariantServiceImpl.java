package com.customizingbox.cloud.manager.service.impl.shopify;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductVariantMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.manager.service.AppStoreProductVariantService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
@Service
@AllArgsConstructor
@Slf4j
public class AppStoreShopifyProductVariantServiceImpl extends ServiceImpl<AppStoreProductVariantMapper, AppStoreProductVariant> implements AppStoreProductVariantService {

    private final AppStoreProductVariantMapper appStoreProductVariantMapper;

    /**
     * 批量保存shopify变体数据
     * @param variants 原变体数据
     * @param productId 产品id
     * @param sourceProductId 原产品id
     * @param imgMapping 图片id和原图片id映射关系
     */
    @Override
    public void saveBatch(List<AppStoreProductVariant> variants, Long productId, Long sourceProductId, Map<Long, Long> imgMapping, String tenantId) {
        if (CollectionUtils.isEmpty(variants)) {
            return;
        }
        for(AppStoreProductVariant variant : variants) {
            Long imageId = variant.getImageId();
            if (!ObjectUtils.isEmpty(imgMapping)) {
                variant.setImageId(imgMapping.getOrDefault(imageId, imgMapping.get("-1")));
            }
            variant.setTenantId(tenantId);
            localSaveOrUpdate(variant, null, productId, sourceProductId);
        }
    }

    /**
     * 批量修改shopify变体数据
     * @param variants 原变体数据
     * @param productId 产品id
     * @param sourceProductId 原产品id
     * @param imgMapping 图片id和原图片id映射关系
     */
    @Override
    public void updateBatch(List<AppStoreProductVariant> variants, Long productId, Long sourceProductId, Map<Long, Long> imgMapping, String tenantId) {
        if (CollectionUtils.isEmpty(variants)) {
            return;
        }
        List<AppStoreProductVariant> dbVariants  = this.findByProductId(productId);

        for(AppStoreProductVariant variant : variants) {
            Long imageId = variant.getImageId();
            variant.setTenantId(tenantId);
            if (!CollectionUtils.isEmpty(imgMapping)) {
                variant.setImageId(imgMapping.getOrDefault(imageId, imgMapping.get("-1")));
            }
            // 因为已经查出全部了，这里从集合里取
            AppStoreProductVariant dbVariant = filterProdcutAttr(dbVariants, variant.getId(), productId);
            variant.setAdminVariantId(dbVariant.getAdminVariantId());
            variant.setAdminProductId(dbVariant.getAdminProductId());
            variant.setSupplyRate(dbVariant.getSupplyRate());
            if (ObjectUtils.isEmpty(dbVariant)) {
                // 没有查到, 则保存
                localSaveOrUpdate(variant, null, productId, sourceProductId);
            } else {
                localSaveOrUpdate(variant, dbVariant.getId(), productId, sourceProductId);
            }
        }
        // 数据库和透传过来参数对比, 删除已经在shopify中已删除的数据
        Set<Long> sourceIds = variants.stream().map(AppStoreProductVariant::getId).collect(Collectors.toSet());
        List<AppStoreProductVariant> dbDelVariants= dbVariants.stream().filter(dbDelVariant -> !sourceIds.contains(dbDelVariant.getSourceId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(dbDelVariants)) {
            dbDelVariants.forEach(variant -> {
                this.removeById(variant);
            });
        }
    }

    @Override
    public AppStoreProductVariant findVariantBySourceId(Long sourceVariantId, Integer platformType) {
        return this.getOne(Wrappers.<AppStoreProductVariant>lambdaQuery().eq(AppStoreProductVariant::getSourceId, sourceVariantId)
                .eq(AppStoreProductVariant::getPlatformType, platformType));
    }

    /**
     * 保存或修改, id有就是修改, id没有就是保存
     * @return
     */
    private Boolean localSaveOrUpdate(AppStoreProductVariant variant, Long id, Long productId, Long sourceProductId) {
        AppStoreProductVariant newvariant = new AppStoreProductVariant();
        BeanUtils.copyProperties(variant, newvariant);
        Long sourceId = newvariant.getId();
        newvariant.setSourceId(sourceId);
        newvariant.setId(id);
        newvariant.setProductId(productId);
        newvariant.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        newvariant.setSourceProductId(sourceProductId);
        newvariant.setTenantId(variant.getTenantId());
        newvariant.setAdminProductId(variant.getAdminProductId());
        newvariant.setAdminVariantId(variant.getAdminVariantId());
        if (ObjectUtils.isEmpty(variant.getSupplyRate())) {
            variant.setSupplyRate(BigDecimal.valueOf(1.2));
        }
        newvariant.setSupplyRate(variant.getSupplyRate());
        return this.saveOrUpdate(newvariant);
    }

    /**
     * 过滤需要修改的产品属性
     */
    private AppStoreProductVariant filterProdcutAttr(List<AppStoreProductVariant> variants, Long sourceId, Long productId) {
        List<AppStoreProductVariant> collect = variants.stream().filter(variant
                -> {
            return variant.getSourceId().equals(sourceId) &&
                    variant.getProductId().equals(productId);
        }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(collect)) {
            return null;
        } else {
            return collect.get(0);
        }
    }

    @Override
    public AppStoreProductVariant findByProductIdAndSourceId(Long productId, Long sourceId) {
        return this.getOne(Wrappers.<AppStoreProductVariant>lambdaQuery().eq(AppStoreProductVariant::getProductId, productId)
                .eq(AppStoreProductVariant::getSourceId, sourceId));
    }

    @Override
    public List<AppStoreProductVariant> findByProductId(Long productId) {
        return this.list(Wrappers.<AppStoreProductVariant>lambdaQuery()
                .select(AppStoreProductVariant::getId, AppStoreProductVariant::getSourceId, AppStoreProductVariant::getProductId
                , AppStoreProductVariant::getAdminProductId, AppStoreProductVariant::getAdminVariantId)
                .eq(AppStoreProductVariant::getProductId, productId));
    }

    @Override
    public AdminStoreProductVariant getProductVariant(Long sourceVariantId, Integer platformType) {
        AppStoreProductVariant appVariant = this.getOne(Wrappers.<AppStoreProductVariant>lambdaQuery()
                .eq(AppStoreProductVariant::getSourceId, sourceVariantId)
                .eq(AppStoreProductVariant::getPlatformType, platformType));
        if (ObjectUtils.isEmpty(appVariant)) {
            log.error("平台: {}, sourceVariantId: {} 原产品变体id 没有在数据库中找到", platformType, sourceVariantId);
            return null;
        }
        Long variantId = appVariant.getId();
        // 关联admin 产品
        AdminStoreProductVariant adminStoreProductVariant = appStoreProductVariantMapper.getAdminProductByAppVariantId(variantId);
        return adminStoreProductVariant;
    }
}
