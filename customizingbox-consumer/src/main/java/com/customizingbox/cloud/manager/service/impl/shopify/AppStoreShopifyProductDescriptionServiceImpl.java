package com.customizingbox.cloud.manager.service.impl.shopify;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductDescriptionMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductDescription;
import com.customizingbox.cloud.manager.service.AppStoreProductDescriptionService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

/**
 * shopify 产品详情表
 *
 * @author Y
 * @date 2022-03-26 10:32:17
 */
@Service
public class AppStoreShopifyProductDescriptionServiceImpl extends ServiceImpl<AppStoreProductDescriptionMapper, AppStoreProductDescription> implements AppStoreProductDescriptionService {
    @Override
    public void updateById(String bodyHtml, Long id, String tenantId) {
        AppStoreProductDescription description = this.getById(id);
        if (!ObjectUtils.isEmpty(description)) {
            description.setBodyHtml(bodyHtml);
            description.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
            this.updateById(description);
        }
    }
}
