package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductImgDepot;

import java.util.List;
import java.util.Map;

/**
 * 产品图库
 *
 * @author Y
 * @date 2022-03-26 10:32:19
 */
public interface AppStoreProductImgDepotService extends IService<AppStoreProductImgDepot> {

    Boolean delByProductId(Long productId);

    AppStoreProductImgDepot findByProductIdAndSourceId(Long productId, Long sourceId);

    List<AppStoreProductImgDepot> findByProductId(Long productId);

    /**
     * @return key 商户图片id， value： 本地图片id
     */
    public Map<Long, Long> saveBatch(List<AppStoreProductImgDepot> images, Long productId, Long sourceProductId, String tenantId);

    /**
     * @return key 商户图片id， value： 本地图片id
     */
    public Map<Long, Long> updateBatch(List<AppStoreProductImgDepot> images, Long productId, Long sourceProductId, String tenantId);

}
