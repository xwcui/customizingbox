package com.customizingbox.cloud.manager.service.impl.shopify;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductAttrMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductAttr;
import com.customizingbox.cloud.manager.service.AppStoreProductAttrService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-26 10:32:21
 */
@Service
@AllArgsConstructor
public class AppStoreShopifyProductAttrServiceImpl extends ServiceImpl<AppStoreProductAttrMapper, AppStoreProductAttr> implements AppStoreProductAttrService {

    /**
     * 批量保存产品属性
     */
    public void saveBatch(List<AppStoreProductAttr> options, Long productId, Long sourceProductId, String tenantId) {
        if (CollectionUtils.isEmpty(options)) {
            return;
        }
        for(AppStoreProductAttr productAttr : options) {
            productAttr.setTenantId(tenantId);
            localSaveOrUpdate(productAttr, null, productId, sourceProductId);
        }
    }

    /**
     * 批量修改产品属性
     */
    public void updateBatch(List<AppStoreProductAttr> options, Long productId, Long sourceProductId, String tenantId) {
        if (CollectionUtils.isEmpty(options)) {
            return;
        }
        List<AppStoreProductAttr> dbProdcutAttrs  = this.findByProductId(productId);

        for(AppStoreProductAttr productAttr : options) {
            productAttr.setTenantId(tenantId);
            AppStoreProductAttr oldProdcutAttr = filterProdcutAttr(dbProdcutAttrs, productAttr.getId(), productId);
            if (ObjectUtils.isEmpty(oldProdcutAttr)) {
                // 没有查到, 则保存
                localSaveOrUpdate(productAttr, null, productId, sourceProductId);
            } else {
                localSaveOrUpdate(productAttr, oldProdcutAttr.getId(), productId, sourceProductId);
            }
        }
        // 数据库和透传过来参数对比, 删除已经在shopify中已删除的数据
        Set<Long> sourceIds = options.stream().map(AppStoreProductAttr::getId).collect(Collectors.toSet());
        List<AppStoreProductAttr> dbAppStoreProductAttrs = dbProdcutAttrs.stream().filter(dbProdcutAttr -> !sourceIds.contains(dbProdcutAttr.getSourceId())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(dbAppStoreProductAttrs)) {
            dbAppStoreProductAttrs.forEach(dbProdcutAttr -> {
                this.removeById(dbProdcutAttr);
            });
        }

    }


    /**
     * 保存或修改, id有就是修改, id没有就是保存
     * @return
     */
    private Boolean localSaveOrUpdate(AppStoreProductAttr options, Long id, Long productId, Long sourceProductId) {
        AppStoreProductAttr productAttr = new AppStoreProductAttr();
        BeanUtils.copyProperties(options, productAttr);
        Long sourceId = productAttr.getId();
        productAttr.setSourceId(sourceId);
        productAttr.setId(id);
        productAttr.setProductId(productId);
        productAttr.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        productAttr.setSourceProductId(sourceProductId);
        productAttr.setTenantId(options.getTenantId());
        return this.saveOrUpdate(productAttr);
    }

    /**
     * 过滤需要修改的产品属性
     */
    private AppStoreProductAttr filterProdcutAttr(List<AppStoreProductAttr> options, Long sourceId, Long productId) {
        List<AppStoreProductAttr> collect = options.stream().filter(storeProductAttr
                -> {
            return storeProductAttr.getSourceId().equals(sourceId) &&
                    storeProductAttr.getProductId().equals(productId);
        }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(collect)) {
            return null;
        } else {
            return collect.get(0);
        }
    }

    @Override
    public AppStoreProductAttr findByProductIdAndSourceId(Long productId, Long sourceAttrId, Integer platformType) {
        return this.getOne(Wrappers.<AppStoreProductAttr>lambdaQuery().eq(AppStoreProductAttr::getProductId, productId)
                .eq(AppStoreProductAttr::getSourceId, sourceAttrId)
                .eq(AppStoreProductAttr::getPlatformType, platformType));
    }

    @Override
    public List<AppStoreProductAttr> findByProductId(Long productId) {
        return this.list(Wrappers.<AppStoreProductAttr>lambdaQuery().select(AppStoreProductAttr::getId, AppStoreProductAttr::getSourceId, AppStoreProductAttr::getProductId)
                .eq(AppStoreProductAttr::getProductId, productId)
                .eq(AppStoreProductAttr::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
    }
}
