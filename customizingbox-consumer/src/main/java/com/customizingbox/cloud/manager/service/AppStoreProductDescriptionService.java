package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductDescription;

/**
 * shopify 产品详情表
 *
 * @author Y
 * @date 2022-03-26 10:32:17
 */
public interface AppStoreProductDescriptionService extends IService<AppStoreProductDescription> {

    /**
     * 修改商品描述
     * @param bodyHtml 描述内容
     * @param id  描述id
     */
    void updateById(String bodyHtml, Long id, String tenantId);
}
