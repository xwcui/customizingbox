package com.customizingbox.cloud.manager.service.impl.shopify;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.manager.service.AppStoreProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 产品表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
@Service
@AllArgsConstructor
@Slf4j
public class AppStoreShopifyProductServiceImpl extends ServiceImpl<AppStoreProductMapper, AppStoreProduct> implements AppStoreProductService {

    @Override
    public AppStoreProduct findProductBySourceId(Long sourceProductId, Integer platformType) {
        return this.getOne(Wrappers.<AppStoreProduct>lambdaQuery().eq(AppStoreProduct::getSourceProductId, sourceProductId)
                .eq(AppStoreProduct::getPlatformType, platformType));
    }
}
