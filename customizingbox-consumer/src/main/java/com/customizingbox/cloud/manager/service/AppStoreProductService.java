package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.product.vo.AppStoreProductListVO;

import java.util.List;

/**
 *  产品数据业务处理
 *
 * @author Y
 * @date 2022-03-26 10:32:23
 */
public interface AppStoreProductService extends IService<AppStoreProduct> {

    /**
     * 根据原商户产品id, 查询app产品id
     * @param sourceProductId
     * @param platformType
     * @return
     */
    AppStoreProduct findProductBySourceId(Long sourceProductId, Integer platformType);
}
