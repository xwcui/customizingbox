package com.customizingbox.cloud.manager.resolver;

import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.manager.service.AppShopifyDataSyncServeice;
import com.customizingbox.cloud.manager.service.impl.AppShopifyOrderDataSyncServeiceImpl;
import com.customizingbox.cloud.manager.service.impl.AppShopifyProductDataSyncServeiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
@Slf4j
public class ShopifyWebHookDispatcher {

    private Map<String, Function<String, Boolean>> dispatcher = new HashMap<>();

    @Resource(type = AppShopifyOrderDataSyncServeiceImpl.class)
    private AppShopifyDataSyncServeice orderDataSyncService;

    @Resource(type = AppShopifyProductDataSyncServeiceImpl.class)
    private AppShopifyDataSyncServeice productDataSyncService;

    @PostConstruct
    public void init() {
        dispatcher.put(StoreEnum.Topic.PRODUCTS_CREATE.getType(),  json -> productDataSyncService.create(json));
        dispatcher.put(StoreEnum.Topic.PRODUCTS_UPDATE.getType(),  json -> productDataSyncService.update(json));
        dispatcher.put(StoreEnum.Topic.PRODUCTS_DELETE.getType(),  json -> productDataSyncService.delete(json));

        // 只有订单支付时, 才创建订单
        dispatcher.put(StoreEnum.Topic.ORDERS_PAID.getType(),  json -> orderDataSyncService.create(json));
        dispatcher.put(StoreEnum.Topic.ORDERS_UPDATED.getType(),  json -> orderDataSyncService.update(json));
        dispatcher.put(StoreEnum.Topic.ORDERS_DELETE.getType(),  json -> orderDataSyncService.delete(json));
    }

    public Function<String, Boolean> getHandler(String type) {
        return  dispatcher.get(type);
    }
}
