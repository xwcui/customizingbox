package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductAttr;

import java.util.List;

/**
 * 商品属性表
 *
 * @author Y
 * @date 2022-03-26 10:32:21
 */
public interface AppStoreProductAttrService extends IService<AppStoreProductAttr> {

    AppStoreProductAttr findByProductIdAndSourceId(Long productId, Long sourceId, Integer platformType);

    List<AppStoreProductAttr> findByProductId(Long productId);

    /**
     * 批量保存产品属性
     */
    public void saveBatch(List<AppStoreProductAttr> options, Long productId, Long sourceProductId, String tenantId);

    /**
     * 批量修改产品属性
     */
    public void updateBatch(List<AppStoreProductAttr> options, Long productId, Long sourceProductId, String tenantId);
}
