package com.customizingbox.cloud.manager.controller;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.customizingbox.cloud.common.core.util.ApiResponse;
import com.customizingbox.cloud.manager.resolver.ShopifyWebHookDispatcher;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.util.function.Function;

/**
 * <p>
 * shopify webhook 回调
 * </p>
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shopify/webhook")
@Api(value = "shopifyWebhook", tags = "shopify webhook")
@Slf4j
public class AppShopifyWebhookController {

    private final ShopifyWebHookDispatcher shopifyWebHookDispatcher;

    private static String CLIENT_SECRET = "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";

    @RequestMapping("/callback")
    public ApiResponse createProduct(@RequestBody String json, HttpServletRequest request) {
        String topic = request.getHeader("X-Shopify-Topic");
        String hmac = request.getHeader("X-Shopify-Hmac-Sha256");
        String shopName = request.getHeader("X-Shopify-Shop-Domain");
        log.info("<======类型: {}, 店铺: {} webhook 数据：{} ======>", topic, shopName, json);
//        Enumeration<String> headerNames = request.getHeaderNames();
//        while (headerNames.hasMoreElements()) {
//            String s = headerNames.nextElement();
//            System.out.println("name: " + s + ", value: " + request.getHeader(s));
//        }

//        if (!verify(hmac, shopName, json)) {
//            log.error("校验webhook数据失败");
//            return  ApiResponse.failed("异常数据");
//        }
        Function<String, Boolean> function = shopifyWebHookDispatcher.getHandler(topic);
        if (function == null) {
            log.info("没有相关订阅, 类型: {}", topic);
        } else {
            JSONObject jsonObject = JSONObject.parseObject(json);
            jsonObject.put("shopify_shop_name", shopName);
            function.apply(jsonObject.toString());
        }

        return ApiResponse.ok();
    }

    private Boolean verify(String hmac, String shopName, String json) {
        if (StringUtils.isEmpty(hmac)) {
            log.error("校验hmac数据为空");
            return false;
        }
        if (StringUtils.isEmpty(shopName)) {
            log.error("数据店铺名为空");
            return false;
        }
        String verifyResult = verifyWebhook(json, CLIENT_SECRET);
        if (hmac.equals(verifyResult)) {
            return true;
        }
        return false;
    }

    public static String verifyWebhook(String json,String appSecret){
        String result = null;
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(appSecret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            result= Base64.encodeBase64String(sha256_HMAC.doFinal(json.getBytes()));
        }catch (Exception e){
            log.error("校验shopify webhook数据失败", e);
        }
        return result;
    }

}
