package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppShippingAddress;

/**
 * 用户收货地址表
 *
 * @author Y
 * @date 2022-03-30 13:48:15
 */
public interface AppShippingAddressService extends IService<AppShippingAddress> {

}
