package com.customizingbox.cloud.manager.service.impl.shopify;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.OrderEnum;
import com.customizingbox.cloud.common.core.constant.enums.ProductEnum;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.order.source.AppOrderItemMapper;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.order.source.dto.OrderShowStatusDTO;
import com.customizingbox.cloud.common.datasource.model.app.order.source.entity.AppOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.order.shopify.entity.AppShopifyOrderItem;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;
import com.customizingbox.cloud.manager.service.AppOrderItemService;
import com.customizingbox.cloud.manager.service.AppStoreProductService;
import com.customizingbox.cloud.manager.service.AppStoreProductVariantService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * panda 订单item表
 *
 * @author Y
 * @date 2022-03-30 13:48:16
 */
@Service
@Slf4j
@AllArgsConstructor
public class AppShopifyOrderItemServiceImpl extends ServiceImpl<AppOrderItemMapper, AppOrderItem> implements AppOrderItemService<AppShopifyOrderItem> {

    private final AppStoreProductVariantService appStoreProductVariantService;
    private final AppStoreProductService appStoreProductService;
    /**
     * 同步已存在订单item
     * @param shopifyOrderItems
     * @param orderId  panda 订单id
     */
    @Override
    public OrderShowStatusDTO syncExistOrderItemData(List<AppShopifyOrderItem> shopifyOrderItems, Long orderId, Long sourceOrderId, String tenantId) {
        List<AppOrderItem> dbOrderItems = this.findByOrderId(orderId);
        if (CollectionUtils.isEmpty(dbOrderItems)) {
            log.error("订单: {}, 没哟找到子订单数据", orderId);
            // TODO 保存全部新来的订单
            return null;
        }
        if (CollectionUtils.isEmpty(shopifyOrderItems)) {
            // TODO 删除数据库全部订单
            return null;
        }
        OrderShowStatusDTO orderShowStatusDTO = new OrderShowStatusDTO();
        Boolean allRelevancy = true;
        for (AppShopifyOrderItem shopifyOrderItem : shopifyOrderItems) {
            AppOrderItem dbOrderItem = filterOrderItem(dbOrderItems, shopifyOrderItem.getId(), orderId);
            Long orderItemId = null;
            if (!ObjectUtils.isEmpty(dbOrderItem)) {
                // 数据库中存在数据, 修改
                orderItemId = dbOrderItem.getId();
            }

            OrderShowStatusDTO orderStatus = localSaveOrUpdate(shopifyOrderItem, orderItemId, orderId, sourceOrderId, tenantId);
            if (!ObjectUtils.isEmpty(orderStatus.getRelevancyStatus())) {
                // 只要有一条数据没有映射上, 就是部分关联
                orderShowStatusDTO.setRelevancyStatus(orderStatus.getRelevancyStatus());
            } else {
                // 这里存在一次没有关联, 那总订单就不是全部关联了
                allRelevancy = false;
            }
            if (!ObjectUtils.isEmpty(orderStatus.getTransportStatus())) {
                orderShowStatusDTO.setTransportStatus(orderStatus.getTransportStatus());
            }
        }
        if (allRelevancy) {
            // 这里全部关联了
            orderShowStatusDTO.setRelevancyStatus(OrderEnum.RELEVANCY_STATUS.ALL_RELEVANCY.getCode());
        }
        return orderShowStatusDTO;
    }

    /**
     * 查找传过来的订单item数据是否在数据库中已经存在
     * @param dbOrderItems 数据库item列表
     * @param sourceOrderItemId 原订单itemid
     * @param orderId 订单id
     */
    private AppOrderItem filterOrderItem(List<AppOrderItem> dbOrderItems, Long sourceOrderItemId, Long orderId) {
        List<AppOrderItem> collect = dbOrderItems.stream().filter(orderItem
                -> {
            return orderItem.getSourceId().equals(sourceOrderItemId) &&
                    orderItem.getOrderId().equals(orderId);
        }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(collect)) {
            return null;
        } else {
            return collect.get(0);
        }
    }

    /**
     * 保存订单, 并返回订单是否可以显示给商户支付状态信息
     * @param lineItems
     * @param orderId
     * @param sourceOrderId
     * @return
     */
    @Override
    public OrderShowStatusDTO saveOrderItem(List<AppShopifyOrderItem> lineItems, Long orderId, Long sourceOrderId, String tenantId) {
        OrderShowStatusDTO orderShowStatusDTO = new OrderShowStatusDTO();
        orderShowStatusDTO.setRelevancyStatus(OrderEnum.RELEVANCY_STATUS.ALL_RELEVANCY.getCode());
        lineItems.forEach(appShopifyOrderItem -> {
            OrderShowStatusDTO orderStatus = localSaveOrUpdate(appShopifyOrderItem, null, orderId, sourceOrderId, tenantId);
            if (!ObjectUtils.isEmpty(orderStatus.getRelevancyStatus())) {
                orderShowStatusDTO.setRelevancyStatus(orderStatus.getRelevancyStatus());
            }
            if (!ObjectUtils.isEmpty(orderStatus.getTransportStatus())) {
                orderShowStatusDTO.setTransportStatus(orderStatus.getTransportStatus());
            }
        });
        return orderShowStatusDTO;
    }

    /**
     * 保存或修改, id有就是修改, id没有就是保存
     * @return
     */
    private OrderShowStatusDTO localSaveOrUpdate(AppShopifyOrderItem appShopifyOrderItem, Long id, Long orderId, Long sourceOrderId, String tenantId) {
        OrderShowStatusDTO orderShowStatusDTO = new OrderShowStatusDTO();
        AppOrderItem appOrderItem = new AppOrderItem();
        appOrderItem.setId(id);
        appOrderItem.setSourceId(appShopifyOrderItem.getId());
        appOrderItem.setOrderId(orderId);
        appOrderItem.setSourceOrderId(sourceOrderId);
        appOrderItem.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        appOrderItem.setSourceProductId(appShopifyOrderItem.getProductId());
        appOrderItem.setSourceVariantId(appShopifyOrderItem.getVariantId());
        appOrderItem.setQuantity(appShopifyOrderItem.getFulfillableQuantity());
        appOrderItem.setFulfillableQuantity(appShopifyOrderItem.getFulfillableQuantity());
        appOrderItem.setTenantId(tenantId);
        // 查询本地库产品
        AppStoreProduct appStoreProduct = appStoreProductService.findProductBySourceId(appShopifyOrderItem.getProductId(), StoreEnum.Type.SHOPIFY.getCode());
        if (!ObjectUtils.isEmpty(appStoreProduct)) {
            appOrderItem.setProductId(appStoreProduct.getId());
        }
        AppStoreProductVariant variant = appStoreProductVariantService.findVariantBySourceId(appShopifyOrderItem.getVariantId(), StoreEnum.Type.SHOPIFY.getCode());
        if (!ObjectUtils.isEmpty(variant)) {
            appOrderItem.setVariantId(variant.getId());
            // 获取订单中产品id对应admin产品的id  (产品必须是已经映射成功的)
            Long adminVariantId = variant.getAdminVariantId();
            Long adminProductId = variant.getAdminProductId();
            if (!ObjectUtils.isEmpty(adminVariantId)) {
                // 只要有一条映射上, 就是部分映射  // TODO 这里有物流信息匹配
                appOrderItem.setRelevancyStatus(OrderEnum.RELEVANCY_STATUS.ALL_RELEVANCY.getCode());
                orderShowStatusDTO.setRelevancyStatus(OrderEnum.RELEVANCY_STATUS.PORTION_RELEVANCY.getCode());

                appOrderItem.setAdminProductId(adminProductId);
                appOrderItem.setAdminVariantId(adminVariantId);
            } else {
                appOrderItem.setRelevancyStatus(OrderEnum.RELEVANCY_STATUS.ALL_NOT_RELEVANCY.getCode());
            }
        }

        this.saveOrUpdate(appOrderItem);
        return orderShowStatusDTO;
    }

    @Override
    public List<AppOrderItem> findByOrderId(Long orderId) {
        return this.list(Wrappers.<AppOrderItem>lambdaQuery().eq(AppOrderItem::getOrderId, orderId));
    }

}