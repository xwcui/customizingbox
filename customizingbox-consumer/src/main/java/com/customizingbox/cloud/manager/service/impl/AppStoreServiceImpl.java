package com.customizingbox.cloud.manager.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.store.AppStoreMapper;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import com.customizingbox.cloud.manager.service.AppStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商户表
 * @author Y
 * @date 2022-03-23 09:51:00
 */
@Service
public class AppStoreServiceImpl extends ServiceImpl<AppStoreMapper, AppStore> implements AppStoreService {

    @Autowired
    private AppStoreMapper appStoreMapper;

    @Override
    public AppStore queryStoreIfExit(String shop, Integer platformType) {
        AppStore appStore = appStoreMapper.queryStoreIfExit(shop, platformType);
        return appStore;
    }

    @Override
    public Boolean updateStatusById(String id, Integer status) {
        this.update(Wrappers.<AppStore>lambdaUpdate().eq(AppStore::getId, id).set(AppStore::getStoreState, status));
        if (StoreEnum.State.DISABLED.getCode().equals(status)) {
            // TODO 这里取消所有webhook
        }
        return true;
    }
}
