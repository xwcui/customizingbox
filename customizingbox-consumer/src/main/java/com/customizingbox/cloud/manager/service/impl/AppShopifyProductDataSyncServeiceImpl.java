package com.customizingbox.cloud.manager.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.customizingbox.cloud.common.core.constant.enums.ProductEnum;
import com.customizingbox.cloud.common.core.constant.enums.StoreEnum;
import com.customizingbox.cloud.common.datasource.mapper.app.product.AppStoreProductMapper;
import com.customizingbox.cloud.common.datasource.model.app.product.dto.AppStoreProductAsyncDTO;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProduct;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductDescription;
import com.customizingbox.cloud.common.datasource.model.app.store.entity.AppStore;
import com.customizingbox.cloud.manager.service.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Map;

@Service
@Slf4j
@AllArgsConstructor
public class AppShopifyProductDataSyncServeiceImpl extends ServiceImpl<AppStoreProductMapper, AppStoreProduct> implements AppShopifyDataSyncServeice {


    private AppStoreProductAttrService productAttrService;
    private AppStoreProductImgDepotService appStoreProductImgDepotService;
    private AppStoreProductVariantService appStoreProductVariantService;
    private final AppStoreService storeShopifyService;
    private final AppStoreProductDescriptionService appStoreProductDescriptionService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean create(String json) {
        AppStoreProductAsyncDTO storeShopifyProductDTO = jsonToProduct(json);
        if (ObjectUtils.isEmpty(storeShopifyProductDTO)) {
            log.error("[shoipfy wenhook product update] 回调数据解析为空， data: {}", json);
            return null;
        }
        AppStoreProduct oldStoreShopifyProduct = findBySourceProductId(storeShopifyProductDTO.getId(), StoreEnum.Type.SHOPIFY.getCode());
        if (!ObjectUtils.isEmpty(oldStoreShopifyProduct)) {
            log.error("[shoipfy wenhook product create] 回调数据产品已经存在不重复保存， data: {}", json);
            // TODO 这里没查到要不要重新保存
            return false;
        }

        String shopifyShopName = storeShopifyProductDTO.getShopifyShopName();
        // 查询商户, 只在创建的时候关联商户
        AppStore store = storeShopifyService.getOne(Wrappers.<AppStore>lambdaQuery().eq(AppStore::getDomain, shopifyShopName)
                .eq(AppStore::getPlatformType, StoreEnum.Type.SHOPIFY.getCode()));
        if (ObjectUtils.isEmpty(store)) {
            log.error("商户: {}, 没有找到关联商户.", shopifyShopName);
            return false;
        }

        AppStoreProductDescription description = new AppStoreProductDescription();
        description.setBodyHtml(storeShopifyProductDTO.getBodyHtml());
        description.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        description.setTenantId(store.getTenantId());
        appStoreProductDescriptionService.save(description);

        AppStoreProduct appStoreProduct = copyToStoreProduct(storeShopifyProductDTO, null, description.getId());
        appStoreProduct.setStoreId(store.getId());
        appStoreProduct.setTenantId(store.getTenantId());

        this.save(appStoreProduct);
        Map<Long, Long> imgMapping = appStoreProductImgDepotService.saveBatch(storeShopifyProductDTO.getImages()
                , appStoreProduct.getId(), appStoreProduct.getSourceProductId(), store.getTenantId());
        productAttrService.saveBatch(storeShopifyProductDTO.getOptions(), appStoreProduct.getId()
                , appStoreProduct.getSourceProductId(), store.getTenantId());
        appStoreProductVariantService.saveBatch(storeShopifyProductDTO.getVariants(), appStoreProduct.getId()
                , appStoreProduct.getSourceProductId(), imgMapping, store.getTenantId());

        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delete(String json) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean update(String json) {
        AppStoreProductAsyncDTO storeShopifyProductDTO = jsonToProduct(json);
        if (ObjectUtils.isEmpty(storeShopifyProductDTO
        )) {
            log.error("[shoipfy wenhook product update] 回调数据解析为空， data: {}", json);
            return false;
        }
        // 这里id是shopify产品id
        Long sourceProductId = storeShopifyProductDTO.getId();
        AppStoreProduct dbStoreShopifyProduct = this.findBySourceProductId(sourceProductId, StoreEnum.Type.SHOPIFY.getCode());
        if (ObjectUtils.isEmpty(dbStoreShopifyProduct)) {
            log.error("[shoipfy wenhook product update] 回调数据没有本地库没有查到产品数据, 重新保存， data: {}", json);
            // 产品数据没找到, 这里直接保存
            create(json);
            return true;
        }
        String tenantId = dbStoreShopifyProduct.getTenantId();
        AppStoreProduct appStoreProduct = copyToStoreProduct(storeShopifyProductDTO, dbStoreShopifyProduct.getId(), dbStoreShopifyProduct.getDescriptionId());
        appStoreProduct.setStoreId(dbStoreShopifyProduct.getStoreId());
        appStoreProduct.setTenantId(tenantId);
        this.updateById(appStoreProduct);

        appStoreProductDescriptionService.updateById(storeShopifyProductDTO.getBodyHtml(), dbStoreShopifyProduct.getDescriptionId(), tenantId);
        Map<Long, Long> imgMapping = appStoreProductImgDepotService.updateBatch(storeShopifyProductDTO.getImages(), appStoreProduct.getId(), appStoreProduct.getSourceProductId(), tenantId);
        productAttrService.updateBatch(storeShopifyProductDTO.getOptions(), appStoreProduct.getId(), appStoreProduct.getSourceProductId(), tenantId);
        appStoreProductVariantService.updateBatch(storeShopifyProductDTO.getVariants(), appStoreProduct.getId(), appStoreProduct.getSourceProductId(), imgMapping, tenantId);

        return true;
    }

    /**
     * json 数据转product
     * @param json
     * @return
     */
    private AppStoreProductAsyncDTO jsonToProduct(String json) {
        AppStoreProductAsyncDTO storeShopifyProduct;
        try {
            storeShopifyProduct = JSONObject.parseObject(json, AppStoreProductAsyncDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return storeShopifyProduct;
    }

    private AppStoreProduct copyToStoreProduct(AppStoreProductAsyncDTO storeShopifyProductDTO, Long productId, Long descriptionId) {
        AppStoreProduct appStoreProduct = new AppStoreProduct();
        BeanUtils.copyProperties(storeShopifyProductDTO, appStoreProduct);
        appStoreProduct.setSourceProductId(storeShopifyProductDTO.getId());
        appStoreProduct.setId(productId);
        appStoreProduct.setDescriptionId(descriptionId);
        appStoreProduct.setPlatformType(StoreEnum.Type.SHOPIFY.getCode());
        appStoreProduct.setStatus(ProductEnum.Status.getStatus(storeShopifyProductDTO.getStatus()).getCode());
        return appStoreProduct;
    }
    // 根据原产品id, 查询产品是否存在
    public AppStoreProduct findBySourceProductId(Long sourceProductId, Integer platformType) {
        return this.getOne(Wrappers.<AppStoreProduct>lambdaQuery().eq(AppStoreProduct::getSourceProductId, sourceProductId)
                .eq(AppStoreProduct::getPlatformType, platformType));
    }
}
