package com.customizingbox.cloud.manager.service;

/**
 * webhook 同步过来产品数据业务处理
 *
 * @author Y
 * @date 2022-03-26 10:32:23
 */
public interface AppShopifyDataSyncServeice {
    /**
     * 同步创建数据
     */
    Boolean create(String json);
    /**
     * 同步删除数据
     */
    Boolean delete(String json);
    /**
     * 同步修改数据
     */
    Boolean update(String json);
}
