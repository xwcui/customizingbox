package com.customizingbox.cloud.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customizingbox.cloud.common.datasource.model.admin.product.entity.AdminStoreProductVariant;
import com.customizingbox.cloud.common.datasource.model.app.product.entity.AppStoreProductVariant;

import java.util.List;
import java.util.Map;

/**
 * 产品属性值表
 *
 * @author Y
 * @date 2022-03-26 10:32:20
 */
public interface AppStoreProductVariantService extends IService<AppStoreProductVariant> {

    AppStoreProductVariant findByProductIdAndSourceId(Long productId, Long sourceId);

    /**
     * 根据产品id查询变体列表
     * @param productId
     * @return
     */
    List<AppStoreProductVariant> findByProductId(Long productId);

    /**
     * 根据原始订单中的产品变体id获取admin的产品变体
     * @param sourceVariantId 原变体id
     * @param platformType 平台类型
     * @return
     */
    AdminStoreProductVariant getProductVariant(Long sourceVariantId, Integer platformType);

    /**
     * 批量保存shopify变体数据
     * @param variants 原变体数据
     * @param productId 产品id
     * @param sourceProductId 原产品id
     * @param imgMapping 图片id和原图片id映射关系
     */
    void saveBatch(List<AppStoreProductVariant> variants, Long productId, Long sourceProductId, Map<Long, Long> imgMapping, String tenantId);

    /**
     * 批量修改shopify变体数据
     * @param variants 原变体数据
     * @param productId 产品id
     * @param sourceProductId 原产品id
     * @param imgMapping 图片id和原图片id映射关系
     */
    void updateBatch(List<AppStoreProductVariant> variants, Long productId, Long sourceProductId, Map<Long, Long> imgMapping, String tenantId);

    /**
     * 根据原商户产品变体id, 查询我们系统中变体id
     * @param sourceVariantId
     * @return
     */
    AppStoreProductVariant findVariantBySourceId(Long sourceVariantId, Integer platformType);
}
